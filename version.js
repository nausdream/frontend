/**
 * Created by lucas on 07/07/2017.
 */
const elixir = require('laravel-elixir');

elixir(function (mix) {
    mix
        .version([
            './public/administration/assets/css/style.css',
            './public/administration/assets/js/app.js',
            './public/administration/assets/js/components.js',
            './public/administration/assets/js/shared.js',
            './public/administration/assets/js/services.js',
            './public/administration/assets/js/components-templates.js',

            './public/translation/assets/css/style.css',
            './public/translation/assets/js/app.js',
            './public/translation/assets/js/components.js',
            './public/translation/assets/js/shared.js',
            './public/translation/assets/js/services.js',
            './public/translation/assets/js/components-templates.js',

            './public/users/assets/css/style.css',
            './public/users/assets/js/libraries.js',
            './public/users/assets/js/app.js',

            './public/shared/assets/css/style.css',
            './public/shared/assets/css/iconfont.css',
            './public/shared/assets/css/login.css',
            './public/shared/assets/js/components.js',
            './public/shared/assets/js/services.js',
            './public/shared/assets/js/app.js',
        ]);
});