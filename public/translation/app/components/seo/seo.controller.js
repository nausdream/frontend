angular
    .module('translation')
    .controller('seoController', [
        '$uibModal', 'validationModalService', 'errorModalService', 'successModalService', 'languageService', '__env',
        '$routeParams', 'requestService', '$cookies', '$q', '$route', 'translatorService',
        function ($uibModal, validationModalService, errorModalService, successModalService, languageService, __env,
                  $routeParams, requestService, $cookies, $q, $route, translatorService) {
            // Make context accessible inside other scopes
            var ctrl = this;

            // Get ID
            ctrl.id = $routeParams.id;
            ctrl.translatorLanguage = $cookies.get('language');
            ctrl.fallbackLanguage = __env.fallbackLanguage;

            // Set initial view
            ctrl.loading = true;

            ctrl.postSeo = function (sentence) {
                if (!ctrl.isFormValid(ctrl.form)) {
                    return;
                }

                ctrl.loading = true;

                let params = {
                    language: $cookies.get('language'),
                    description: ctrl.seoDestinationLanguage.description,
                    title: ctrl.seoDestinationLanguage.title
                };

                let relationships = {
                    experience : {
                        data: { type: "experiences", id: ctrl.experienceId }
                    }
                };

                return requestService.postSingle('seos', params, relationships).then(
                    function (response) {
                        ctrl.seoDestinationLanguage.slug_url = response.data.data.attributes.slug_url;
                        ctrl.success = true;
                        ctrl.loading = false;
                    },
                    function (response) {
                        errorModalService.open(response);
                        ctrl.loading = false;
                    }
                )
            };

            // Check if form can be submitted
            ctrl.isFormValid = function (form) {
                return form.$valid;
            };

            // Get SEO from id
            requestService.getSingleAsync('seos', ctrl.id).then(
                function (response) {
                    ctrl.seoOriginLanguage = response.data.data.attributes;
                    ctrl.experienceId = response.data.data.relationships.experience.data.id;

                    // Get SEO in translator language
                    requestService.getRelatedSingleAsync('experiences', ctrl.experienceId, 'seo', {language: ctrl.translatorLanguage}).then(
                        function (response) {
                            if (response.data.data) {
                                ctrl.seoDestinationLanguage = response.data.data.attributes;
                            } else {
                                ctrl.seoDestinationLanguage = {};
                            }
                            ctrl.loading = false;
                        },
                        function () {
                            ctrl.seoDestinationLanguage = {};
                            ctrl.loading = false;
                        }
                    )
                },
                function () {
                    ctrl.experienceId = null;
                    ctrl.seoOriginLanguage = {};
                    ctrl.seoDestinationLanguage = {};

                    ctrl.loading = false;
                }
            );
        }]);