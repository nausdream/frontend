angular.module('translation').component('pathIcon', {
    templateUrl: '/translation/app/components/sidebar/components/path-icon/path-icon.view.html',
    controller: 'pathIconController',
    bindings: {
        datatype: '@'
    }
});