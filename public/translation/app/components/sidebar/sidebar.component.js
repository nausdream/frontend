angular.module('translation').component('sidebar', {
    templateUrl: '/translation/app/components/sidebar/sidebar.view.html',
    controller: 'sidebarController'
});