angular.module('translation').component('header', {
    templateUrl: '/translation/app/components/header/header.view.html',
    controller: 'headerController'
});