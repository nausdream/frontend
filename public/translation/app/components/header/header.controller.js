angular
    .module('translation')
    .controller('headerController', ['languageService', 'iconService', 'requestService', '$cookies', function (languageService, iconService, requestService, $cookies) {
        this.profilePic = '/shared/assets/img/ragazza.jpg';
        this.language = $cookies.get('language');

        this.logout = function () {
            requestService.logout('/login');
        };
    }]);