angular
    .module('translation')
    .controller('notificationIconController',[
        'iconService',
        function (iconService) {
        this.$onInit = function () {
            if (!this.iconName) this.iconName = this.datatype;

            // Fetch icons data from iconService
            var data = iconService.getIconsData(this.datatype);

            this.count = data.count;
            this.name = data.name;
            this.text = data.text;

            if (this.popover) {
                // Set popover template
                this.popoverTemplate = '/translation/app/components/notification-icon/views/popover-list.view.html';
                // Create popover list
                this.popoverData = {
                    page: iconService.getIconsData('page'),
                    experience: iconService.getIconsData('experience'),
                    seo: iconService.getIconsData('seo')
                };
            }
        };
    }]);