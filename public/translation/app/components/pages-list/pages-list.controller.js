angular
    .module('translation')
    .controller('pagesListController',[
        'statusService', '$scope', 'requestService', '$cookies', 'translatorService',
        function (statusService, $scope, requestService, $cookies, translatorService) {
        // Make context accessible inside other scopes
        var ctrl = this;
        ctrl.loading = true;

        // Fetch data from service
        this.getList = function () {
            requestService.getList(function (data, totalItems) {
                ctrl.loading = false;
                ctrl.totalItems = totalItems;
                ctrl.data = data;
            }, 'pages', ctrl.currentPage);
        };
        ctrl.getList();

        // Set table properties
        this.columns = [
            {name: 'name', heading: 'Page name'},
            {name: 'date', heading: 'Insert date'},
            {name: 'action', heading: 'Actions'},
        ];

        // Set top icons
        this.icons = [
            {type: 'search', template: 'search-page'}
        ];

        this.controlIconsColor = 'navy-blue';

        this.getStatusName = function (status) {
            return statusService.getBoatStatusName(status);
        };

        // Search Page
        $scope.searchString = '';
        $scope.searchPage = function (text) {
            console.log('Text: ' + text)
        };

        // Pagination
        this.currentPage = 1;
        this.perPage = 6;
        this.pagesToDisplay = 5;
        this.boundaryLinks = true;
        this.directionLinks = false;

        // Request new data
        this.pageChanged = function () {
            // Start loading new page
            ctrl.loading = true;

            ctrl.getList();
        };

        translatorService.getIsAdminCookie().then(function (is_admin) {
            ctrl.is_admin = is_admin;
        });
    }]);