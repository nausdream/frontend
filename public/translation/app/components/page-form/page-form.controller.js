angular
    .module('translation')
    .controller('pageFormController',[
        '$uibModal', 'validationModalService', 'errorModalService', 'successModalService', 'languageService', '__env', '$routeParams', 'requestService', '$cookies', '$q', '$route', 'translatorService',
        function ($uibModal, validationModalService, errorModalService, successModalService, languageService, __env, $routeParams, requestService, $cookies, $q, $route, translatorService) {
        // Make context accessible inside other scopes
        var ctrl = this;

        // Get ID
        ctrl.id = $routeParams.id;
        ctrl.fallbackLanguage = __env.fallbackLanguage;

        // Set initial view
        ctrl.loading = true;

        ctrl.close = function (id) {
            ctrl.openPopover[id] = false;
        };

        ctrl.languageOriginName = languageService.getLanguageName(__env.fallbackLanguage);
        languageService.getLanguageCookie().then(function (data) {
            ctrl.languageDestinationName = languageService.getLanguageName($cookies.get('language'));
            ctrl.language = $cookies.get('language');

            ctrl.loadDataSentencesOriginLanguage = function () {
                var deferred = $q.defer();
                requestService.getRelatedList(function (data) {
                    ctrl.sentences = data.data;
                    deferred.resolve(ctrl.sentences);
                }, 'pages', ctrl.id, 'sentences', '?language=' + __env.fallbackLanguage);
                return deferred.promise;
            };

            ctrl.loadDataSentencesOriginLanguage().then(function (data) {
                requestService.getRelatedList(function (data) {
                    ctrl.sentencesTranslation = data.data;

                    ctrl.sentences.forEach(function(currentValue, index) {
                        var i;
                        for (i = 0; i < ctrl.sentencesTranslation.length; i++)
                        {
                            if (currentValue.id == ctrl.sentencesTranslation[i].id) {
                                currentValue.attributes.translation = ctrl.sentencesTranslation[i].attributes.text;
                            }
                        }
                        if (angular.isUndefined(currentValue.attributes.translation) || currentValue.attributes.translation == '') {
                            currentValue.attributes.translation = '';
                        }
                    });
                    ctrl.loading = false;
                }, 'pages', ctrl.id, 'sentences', '?language=' + $cookies.get('language'));
            });
        });

        translatorService.getIsAdminCookie().then(function (is_admin) {
            ctrl.is_admin = is_admin;
        });

        ctrl.postTranslation = function (sentence) {
            if (!ctrl.isFormValid(eval("ctrl.form_" + sentence.id))) {
                validationModalService.open();
                return;
            }
            // Close popup
            ctrl.close(sentence.id);

            // If request already started, don't do it again
            if (ctrl.requestStarted) return;

            // Start request
            ctrl.requestStarted = true;

            return requestService.postTranslation(sentence.id, sentence.attributes.translation, $cookies.get('language'))
                .then(
                    function () {
                        // Request finished
                        ctrl.requestStarted = false;
                    },
                    function (response) {
                        errorModalService.open(response);
                        // Request finished
                        ctrl.requestStarted = false;
                    }
                );
        };

        ctrl.openModal = function () {
            ctrl.modalInstance = $uibModal.open({
                animation: true,
                templateUrl: '/translation/app/components/modal/insert-element/insert-element.view.html',
                controller: 'insertElementController',
                controllerAs: 'ctrl',
            });

            ctrl.modalInstance.result.then(function (data) {
                if (data.actionDone) {
                    requestService.postSentence(ctrl.id, data.name, data.text)
                        .then(
                            function () {
                               $route.reload();
                            },
                            function (response) {
                                ctrl.errorCallback(response);
                            }
                        );
                }
            });
        };

        ctrl.errorCallback = function (response) {
            errorModalService.open(response);
        };

        // Check if form can be submitted
        ctrl.isFormValid = function (form) {
            return form.$valid;
        }
    }]);