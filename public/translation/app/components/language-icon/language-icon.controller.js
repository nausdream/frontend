angular
    .module('translation')
    .controller('languageIconController',[
        'languageService', '$cookies', '$route',
        function (languageService, $cookies, $route) {
        this.$onInit = function () {
            // Fetch icons data from iconService
            var ctrl = this;
            languageService.getLanguagesData().then(function(data) {
                if (ctrl.popover) {
                    // Set popover template
                    ctrl.popoverTemplate = '/translation/app/components/language-icon/views/language-list.view.html';
                    // Create popover list
                    ctrl.languageData = data;
                }
            });
            languageService.getLanguageCookie().then(function (data) {
                ctrl.language = $cookies.get('language');
            });

            this.changeLanguage = function (language) {
                $cookies.put('language', language);
                ctrl.language = language;
                $route.reload();
            };
            this.getLanguageName = function(language) {
                return languageService.getLanguageName(language);
            }
        };
    }]);