angular
    .module('translation')
    .controller('seosController', [
        'statusService', '$scope', 'requestService', '$q', '$location', '$cookies',
        function (statusService, $scope, requestService, $q, $location, $cookies) {
            // Make context accessible inside other scopes
            var ctrl = this;
            ctrl.loading = true;

            var deferred = $q.defer();

            // Filters
            ctrl.not_translated = $location.search().not_translated || null;

            // Set table properties
            this.columns = [
                {name: 'title', heading: 'Meta title'},
                {name: 'date', heading: 'Created on'},
                {name: 'language', heading: 'Language'},
                {name: 'translated', heading: 'Translated'},
                {name: '', heading: ''}
            ];

            // When user changes filter
            ctrl.changeFilter = function () {
                $location.search('not_translated', ctrl.not_translated);
            };

            // Pagination
            this.currentPage = parseInt($location.search().page) || 1;
            this.perPage = 12;
            this.pagesToDisplay = 5;
            this.boundaryLinks = true;
            this.directionLinks = false;

            // Reload with new page
            this.pageChanged = function () {
                $location.search('page', ctrl.currentPage);
            };

            // Load seos with current params
            ctrl.load = function () {
                ctrl.loading = true;

                deferred.resolve();
                deferred = $q.defer();

                let params = {
                    language: $cookies.get('language'),
                    page: ctrl.currentPage,
                    per_page: ctrl.perPage
                };

                if (ctrl.not_translated) params.translated = 0;

                let seosPromise = requestService.getListAsync('seos', params, deferred);
                seosPromise.then(
                    function (response) {
                        ctrl.data = response.data.data;
                        ctrl.loading = false;
                        ctrl.totalItems = response.data.meta.total_elements;
                    },
                    function (response) {
                        ctrl.data = response.data.data;
                        ctrl.loading = false;
                        ctrl.totalItems = response.data.meta.total_elements;
                    }
                );
            };

            // Initial load
            ctrl.load();
        }]);