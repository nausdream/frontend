angular.module('translation').component('formInsertPage', {
    templateUrl: '/translation/app/components/form-insert-page/form-insert-page.view.html',
    controller: 'formInsertPageController'
});