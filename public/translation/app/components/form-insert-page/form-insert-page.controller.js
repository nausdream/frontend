angular
    .module('translation')
    .controller('formInsertPageController', [
        '$uibModal', 'validationModalService', 'errorModalService', 'successModalService', 'requestService', '$route', '$q',
        function ($uibModal, validationModalService, errorModalService, successModalService, requestService, $route, $q) {
        // Insert page and return promise
        var ctrl = this;

        ctrl.insertPage = function () {
            ctrl.loading = true;
            if (!ctrl.isFormValid(ctrl.form)) {
                validationModalService.open();
                return;
            }
            return requestService.postPage(ctrl.page.name).then(
                function () {
                    $route.reload();
                },
                function (response) {
                    errorModalService.open(response);
                }
            );
        };

        // Check if form can be submitted
        ctrl.isFormValid = function (form) {
            return form.$valid;
        }
    }]);