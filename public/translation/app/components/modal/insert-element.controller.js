/**
 * Created by Giuseppe on 08/03/2017.
 */
angular
    .module('translation')
    .controller('insertElementController',[
        '$uibModalInstance',
        function ($uibModalInstance) {
        this.$onInit = function () {
            var ctrl = this;

            ctrl.save = function () {
                if (ctrl.isFormValid(ctrl.form)) {
                    $uibModalInstance.close({actionDone: true, name: ctrl.name, text: ctrl.text});
                }
            };

            ctrl.close = function () {
                $uibModalInstance.close(false);
            };

            // Check if form can be submitted
            ctrl.isFormValid = function (form) {
                return form.$valid;
            }
        };
    }]);
