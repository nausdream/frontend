angular
    .module('translation')
    .config([
        '$routeProvider',
        function($routeProvider) {
        $routeProvider
            /*.when("/", {
                title: "Dashboard",
                templateUrl: "/translation/app/components/pages-list/pages-list.view.html",
                controller: "pagesListController",
                controllerAs: "ctrl"
            })*/
            .when("/pages", {
                title: "Pages",
                templateUrl: "/translation/app/components/pages-list/pages-list.view.html",
                controller: "pagesListController",
                controllerAs: "ctrl"
            })
            .when("/pages/:id", {
                currentMenu: "page",
                title: "Page",
                templateUrl: "/translation/app/components/page-form/page-form.view.html",
                controller: "pageFormController",
                controllerAs: "ctrl"
            })
            .when("/seos", {
                title: "SEOs",
                templateUrl: "/translation/app/components/seos/seos.view.html",
                controller: "seosController",
                controllerAs: "ctrl"
            })
            .when("/seos/:id", {
                currentMenu: "seo",
                title: "SEO",
                templateUrl: "/translation/app/components/seo/seo.view.html",
                controller: "seoController",
                controllerAs: "ctrl"
            })
            .otherwise({
                redirectTo: "/pages"
            });
}]);

// Auto update title and currentMenu element on route change
angular
    .module('translation')
    .run(['$rootScope', '$location', function($rootScope, $location) {
        $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
            $rootScope.title = current.$$route.title;
            $rootScope.currentMenu = current.$$route.currentMenu;
            $rootScope.currentUrl = $location.path();
    });
}]);