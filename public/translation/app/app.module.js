'use strict';

// Define app constants
var env = {};
// Import variables if present (from env.js)
if (window) {
    Object.assign(env, window.__env);
}

angular.module('translation', ['shared']);

// Set __env global constant
angular.module('translation').constant('__env', env);

angular.module('translation').config(['$cookiesProvider', function($cookiesProvider) {
    $cookiesProvider.defaults.path = '/translator';
}]);

angular.module('translation', ['shared']).run(['$rootScope', '$cookies', function($rootScope, $cookies) {

    // Extract translator information from access token

    // TODO: CHECK USER TYPE
    var accesstoken = $cookies.get('access_token');
    var tmp = accesstoken.substr(accesstoken.indexOf(".") + 1);
    var userDataEncoded = tmp.substr(0, tmp.indexOf("."));
    var userData = JSON.parse(atob(userDataEncoded));

    // Set cookie for translator id
    $cookies.put('id_translator', userData.sub);
}]);

angular.module('translation').config(['$locationProvider', function($locationProvider) {
    $locationProvider.hashPrefix('');
    // use the HTML5 History API
    $locationProvider.html5Mode(true);
}]);