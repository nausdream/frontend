angular.module('translation').service('languageService',[
    'requestService',  '$q', '$cookies', '__env',
    function(requestService,  $q, $cookies, __env) {

    var ctrl = this;

    // Fetch data from service
    this.getLanguagesData = function () {
        var deferred = $q.defer();
        //insert logic and async calls here
        //after everything is done, do this
        requestService.getSingle(function (data) {
            if (data.data.attributes.is_admin) {
                requestService.getList(function (data) {
                    ctrl.data = data;
                    deferred.resolve(ctrl.data);
                }, 'languages');
            } else {
                ctrl.data = data.included;
                deferred.resolve(ctrl.data);
            }
        }, 'translators', $cookies.get('id_translator'));
        return deferred.promise;
    };

    this.getLanguageCookie = function () {
        var deferred = $q.defer();
        language = $cookies.get('language');
        if (angular.isUndefined(language) || language == '') {
            requestService.getSingle(function (data) {
                $cookies.put('is_admin', data.data.attributes.is_admin);
                if (data.data.attributes.is_admin) {
                    $cookies.put('language', __env.fallbackLanguage);
                } else {
                    language = data.included[0].attributes.language;
                    if (angular.isUndefined(language) || language == '') {
                        $cookies.put('language', __env.fallbackLanguage);
                    } else {
                        $cookies.put('language', language);
                    }
                }
                deferred.resolve(data);
            }, 'translators', $cookies.get('id_translator'));
        } else {
            deferred.resolve(language);
        }
        return deferred.promise;
    }

    this.getLanguageName = function (language) {
        switch (language) {
            case 'it':
                return 'Italian';
                break;
            case 'en':
                return 'English';
                break;
            default:
                return 'English';
        }
    };

}]);