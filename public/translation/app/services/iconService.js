angular.module('translation').service('iconService', function() {

    // Inizialize notifications numbers array
    // TODO: Fetch data from API
    var data = {

        page: {
            name: 'Pages', count: 0,
            dropdownData: [
                {name: 'Pages', url: 'pages'},
                // {name: 'Eventi e Feste', url: 'events'},
                // {name: 'Inserisci nuovo annuncio', url: 'new-booking'}
            ]
        },

        experience: {
            name: 'Experiences', count: 0,
            dropdownData: [
                {name: 'Experiences', url: 'experiences'},
                // {name: 'Eventi e Feste', url: 'events'},
                // {name: 'Inserisci nuovo annuncio', url: 'new-booking'}
            ]
        },

        seo: {
            name: 'SEO', count: 0, text: 'SEO',
            dropdownData: [
                {name: 'SEO', url: 'seos'},
                // {name: 'Eventi e Feste', url: 'events'},
                // {name: 'Inserisci nuovo annuncio', url: 'new-booking'}
            ]
        },

        notifications: {
            name: null, count: 0,
            dropdownData: null
        }
    };
    data.notifications.count =
        data.page.count +
        data.experience.count;

    this.getIconsData = function (iconName) {
        return data[iconName];
    };
});