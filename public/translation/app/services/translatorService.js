/**
 * Created by Giuseppe on 03/03/2017.
 */
angular.module('translation').service('translatorService', [
    'requestService', '$q', '$cookies',
    function (requestService, $q, $cookies) {

        var ctrl = this;

        this.getIsAdminCookie = function () {
            var deferred = $q.defer();
            is_admin = $cookies.get('is_admin') === 'true';
            if (angular.isUndefined(is_admin) || is_admin == '') {
                requestService.getSingle(function (data) {
                    $cookies.put('is_admin', data.data.attributes.is_admin);
                    is_admin = $cookies.get('is_admin') === 'true';
                    deferred.resolve(is_admin);
                }, 'translators', $cookies.get('id_translator'));
            } else {
                deferred.resolve(is_admin);
            }
            return deferred.promise;
        }
    }]);