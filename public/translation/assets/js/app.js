/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmory imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmory exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		Object.defineProperty(exports, name, {
/******/ 			configurable: false,
/******/ 			enumerable: true,
/******/ 			get: getter
/******/ 		});
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 2);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

"use strict";
'use strict';

// Define app constants
var env = {};
// Import variables if present (from env.js)
if (window) {
    Object.assign(env, window.__env);
}

angular.module('translation', ['shared']);

// Set __env global constant
angular.module('translation').constant('__env', env);

angular.module('translation').config(['$cookiesProvider', function($cookiesProvider) {
    $cookiesProvider.defaults.path = '/translator';
}]);

angular.module('translation', ['shared']).run(['$rootScope', '$cookies', function($rootScope, $cookies) {

    // Extract translator information from access token

    // TODO: CHECK USER TYPE
    var accesstoken = $cookies.get('access_token');
    var tmp = accesstoken.substr(accesstoken.indexOf(".") + 1);
    var userDataEncoded = tmp.substr(0, tmp.indexOf("."));
    var userData = JSON.parse(atob(userDataEncoded));

    // Set cookie for translator id
    $cookies.put('id_translator', userData.sub);
}]);

angular.module('translation').config(['$locationProvider', function($locationProvider) {
    $locationProvider.hashPrefix('');
    // use the HTML5 History API
    $locationProvider.html5Mode(true);
}]);

/***/ },
/* 1 */
/***/ function(module, exports) {

angular
    .module('translation')
    .config([
        '$routeProvider',
        function($routeProvider) {
        $routeProvider
            /*.when("/", {
                title: "Dashboard",
                templateUrl: "/translation/app/components/pages-list/pages-list.view.html",
                controller: "pagesListController",
                controllerAs: "ctrl"
            })*/
            .when("/pages", {
                title: "Pages",
                templateUrl: "/translation/app/components/pages-list/pages-list.view.html",
                controller: "pagesListController",
                controllerAs: "ctrl"
            })
            .when("/pages/:id", {
                currentMenu: "page",
                title: "Page",
                templateUrl: "/translation/app/components/page-form/page-form.view.html",
                controller: "pageFormController",
                controllerAs: "ctrl"
            })
            .when("/seos", {
                title: "SEOs",
                templateUrl: "/translation/app/components/seos/seos.view.html",
                controller: "seosController",
                controllerAs: "ctrl"
            })
            .when("/seos/:id", {
                currentMenu: "seo",
                title: "SEO",
                templateUrl: "/translation/app/components/seo/seo.view.html",
                controller: "seoController",
                controllerAs: "ctrl"
            })
            .otherwise({
                redirectTo: "/pages"
            });
}]);

// Auto update title and currentMenu element on route change
angular
    .module('translation')
    .run(['$rootScope', '$location', function($rootScope, $location) {
        $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
            $rootScope.title = current.$$route.title;
            $rootScope.currentMenu = current.$$route.currentMenu;
            $rootScope.currentUrl = $location.path();
    });
}]);

/***/ },
/* 2 */
/***/ function(module, exports, __webpack_require__) {

__webpack_require__(0);
module.exports = __webpack_require__(1);


/***/ }
/******/ ]);
//# sourceMappingURL=app.js.map