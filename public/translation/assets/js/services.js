/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmory imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmory exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		Object.defineProperty(exports, name, {
/******/ 			configurable: false,
/******/ 			enumerable: true,
/******/ 			get: getter
/******/ 		});
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

angular.module('translation').service('iconService', function() {

    // Inizialize notifications numbers array
    // TODO: Fetch data from API
    var data = {

        page: {
            name: 'Pages', count: 0,
            dropdownData: [
                {name: 'Pages', url: 'pages'},
                // {name: 'Eventi e Feste', url: 'events'},
                // {name: 'Inserisci nuovo annuncio', url: 'new-booking'}
            ]
        },

        experience: {
            name: 'Experiences', count: 0,
            dropdownData: [
                {name: 'Experiences', url: 'experiences'},
                // {name: 'Eventi e Feste', url: 'events'},
                // {name: 'Inserisci nuovo annuncio', url: 'new-booking'}
            ]
        },

        seo: {
            name: 'SEO', count: 0, text: 'SEO',
            dropdownData: [
                {name: 'SEO', url: 'seos'},
                // {name: 'Eventi e Feste', url: 'events'},
                // {name: 'Inserisci nuovo annuncio', url: 'new-booking'}
            ]
        },

        notifications: {
            name: null, count: 0,
            dropdownData: null
        }
    };
    data.notifications.count =
        data.page.count +
        data.experience.count;

    this.getIconsData = function (iconName) {
        return data[iconName];
    };
});

/***/ },
/* 1 */
/***/ function(module, exports) {

angular.module('translation').service('languageService',[
    'requestService',  '$q', '$cookies', '__env',
    function(requestService,  $q, $cookies, __env) {

    var ctrl = this;

    // Fetch data from service
    this.getLanguagesData = function () {
        var deferred = $q.defer();
        //insert logic and async calls here
        //after everything is done, do this
        requestService.getSingle(function (data) {
            if (data.data.attributes.is_admin) {
                requestService.getList(function (data) {
                    ctrl.data = data;
                    deferred.resolve(ctrl.data);
                }, 'languages');
            } else {
                ctrl.data = data.included;
                deferred.resolve(ctrl.data);
            }
        }, 'translators', $cookies.get('id_translator'));
        return deferred.promise;
    };

    this.getLanguageCookie = function () {
        var deferred = $q.defer();
        language = $cookies.get('language');
        if (angular.isUndefined(language) || language == '') {
            requestService.getSingle(function (data) {
                $cookies.put('is_admin', data.data.attributes.is_admin);
                if (data.data.attributes.is_admin) {
                    $cookies.put('language', __env.fallbackLanguage);
                } else {
                    language = data.included[0].attributes.language;
                    if (angular.isUndefined(language) || language == '') {
                        $cookies.put('language', __env.fallbackLanguage);
                    } else {
                        $cookies.put('language', language);
                    }
                }
                deferred.resolve(data);
            }, 'translators', $cookies.get('id_translator'));
        } else {
            deferred.resolve(language);
        }
        return deferred.promise;
    }

    this.getLanguageName = function (language) {
        switch (language) {
            case 'it':
                return 'Italian';
                break;
            case 'en':
                return 'English';
                break;
            default:
                return 'English';
        }
    };

}]);

/***/ },
/* 2 */
/***/ function(module, exports) {

/**
 * Created by Giuseppe on 03/03/2017.
 */
angular.module('translation').service('translatorService', [
    'requestService', '$q', '$cookies',
    function (requestService, $q, $cookies) {

        var ctrl = this;

        this.getIsAdminCookie = function () {
            var deferred = $q.defer();
            is_admin = $cookies.get('is_admin') === 'true';
            if (angular.isUndefined(is_admin) || is_admin == '') {
                requestService.getSingle(function (data) {
                    $cookies.put('is_admin', data.data.attributes.is_admin);
                    is_admin = $cookies.get('is_admin') === 'true';
                    deferred.resolve(is_admin);
                }, 'translators', $cookies.get('id_translator'));
            } else {
                deferred.resolve(is_admin);
            }
            return deferred.promise;
        }
    }]);

/***/ },
/* 3 */
/***/ function(module, exports, __webpack_require__) {

__webpack_require__(0);
__webpack_require__(1);
module.exports = __webpack_require__(2);


/***/ }
/******/ ]);
//# sourceMappingURL=services.js.map