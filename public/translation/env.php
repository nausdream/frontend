<?php

?>

<script>(function (window) {
        window.__env = window.__env || {};

        window.__env.apiUrl = <?php echo "\"" . env('BACKEND_URL') . env('API_VERSION') . "\""?>;
        window.__env.backendUrl = <?php echo "\"" . env('BACKEND_URL') . "\""?>;
        window.__env.baseUrl = <?php echo "\"" . env('APP_URL') . env('TRANSLATOR_ROOT') . "\"" ?>;
        window.__env.mapsApiKey = <?php echo "\"" . env('MAPS_API_KEY') . "\"" ?>;
        window.__env.fbAppId = <?php echo "\"" . env('FB_APP_ID') . "\"" ?>;
        window.__env.fallbackCurrency = <?php echo "\"" . env('FALLBACK_CURRENCY') . "\"" ?>;
        window.__env.fallbackLanguage = <?php echo "\"" . env('FALLBACK_LANGUAGE') . "\"" ?>;

    }(this));</script>