<!-- Favicon -->
<link rel="apple-touch-icon" sizes="57x57" href="/shared/assets/img/ico/apple-icon1-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="/shared/assets/img/ico/apple-icon1-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="/shared/assets/img/ico/apple-icon1-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="/shared/assets/img/ico/apple-icon1-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="/shared/assets/img/ico/apple-icon1-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="/shared/assets/img/ico/apple-icon1-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="/shared/assets/img/ico/apple-icon1-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="/shared/assets/img/ico/apple-icon1-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="/shared/assets/img/ico/apple-icon1-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/shared/assets/img/ico/android-icon1-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="/shared/assets/img/ico/favicon1-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="/shared/assets/img/ico/favicon1-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="/shared/assets/img/ico/favicon1-16x16.png">
<link rel="manifest" href="/shared/assets/img/ico/manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="/shared/assets/img/ico/ms-icon-144x144.png">