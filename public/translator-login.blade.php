<!DOCTYPE html>
<html>
<head>
    <title>Login</title>
    @include('favicon')
    <!-- INCLUDE FRONTEND ENV FILE -->
    <?php
        include env('TRANSLATOR_APP_FOLDER') . "/env.php";
    ?>
    <!-- HTTPS required. HTTP will give a 403 forbidden response -->
    <script src="https://sdk.accountkit.com/en_EN/sdk.js"></script>
    <script>
        // initialize Account Kit with CSRF protection
        AccountKit_OnInteractive = function () {
            AccountKit.init(
                {
                    appId: window.__env.fbAppId,
                    state: "12345",
                    version: "v1.1"
                }
            );
        };
    </script>
    <!-- JQUERY -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600&amp;subset=latin-ext" rel="stylesheet" type="text/css">
    <!-- CSS -->
    <link href="{{ elixir('shared/assets/css/login.css') }}" rel="stylesheet" type="text/css">
    <!-- TO AVOID ANGULAR # ON ADDRESS -->
    <base href="/translator/">
</head>
<body>
<div id="box">
    <img id="logo" src="/shared/assets/img/login_logo.svg"/>
    <div id="welcome">
        <span><strong>Hello!</strong></span><br>
        <span>Welcome to<br>
            Nausdream Translator</span>
    </div>
    <div id="login" onclick="phone_btn_onclick()">
        LOGIN
    </div>
</div>
<script>

    // login callback
    function loginCallback(response) {
        console.log('\nResponse received from Fb.');
        if (response.status === "PARTIALLY_AUTHENTICATED") {
            sendCodeAndCrsf(response);
        }
        else if (response.status === "NOT_AUTHENTICATED") {
            // handle authentication failure
        }
        else if (response.status === "BAD_PARAMS") {
            // handle bad parameters
        }
    }

    // phone form submission handler
    function phone_btn_onclick() {
        var country_code_default = '+39';
        AccountKit.login('PHONE',
                {countryCode: country_code_default}, // will use default values if this is not specified
                loginCallback);
    }

    //send code and csrf_nonce to API server
    function sendCodeAndCrsf(request) {
        $.ajax({
            url: window.__env.apiUrl + "auth/translator",
            method: "POST",
            data: {meta: {code: request.code}},
            xhrFields: {
                withCredentials: true
            },
            crossDomain: true
        }).done(function (response) {
            if (response.data.type == "translators") {
                setCookie("csrf_token", response.meta.csrf_token, 'translator');
                setCookie("access_token", response.meta.access_token, 'translator');

                window.location = window.__env.baseUrl;
            }
        });

    }

    function setCookie(cname, cvalue, cpath) {
        document.cookie = cname + "=" + cvalue + ";path=/" + cpath;
    }

</script>
</body>
</html>