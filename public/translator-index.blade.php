<!DOCTYPE html>
<html lang="en" data-ng-app="translation">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @include('favicon')
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title data-ng-bind="title"></title>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Fonts -->
    <link href="{{ elixir('shared/assets/css/iconfont.css') }}" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600&amp;subset=latin-ext" rel="stylesheet" type="text/css">
    <!-- CSS -->
    <link type="text/css" rel="stylesheet" href="{{ elixir('translation/assets/css/style.css') }}">
    <link type="text/css" rel="stylesheet" href="{{ elixir('shared/assets/css/style.css') }}">
    <!-- TO AVOID ANGULAR # ON ADDRESS -->
    <base href="/translator/">
</head>
<body>
<!-- INCLUDE FRONTEND ENV FILE -->
<?php
include env('TRANSLATOR_APP_FOLDER') . "/env.php";
?>

<script>
    // Check if translator has access_token cookie
    var value = "; " + document.cookie;
    var parts = value.split("; access_token=");
    console.log(document.cookie);
    if (parts.length == 1) {
        window.location = window.__env.baseUrl + '/login';
    }
</script>
<!-- Dashboard frame start-->
<!-- Header -->
<header></header>
<!-- Left Sidebar -->
<sidebar></sidebar>
<!-- Dashboard frame end-->

<!-- Content -->
<div id="content" data-ng-view></div>

<!-- Angular -->
<script src="/shared/assets/libraries/angular.min.js"></script>
<script src="/shared/assets/libraries/angular-route.min.js"></script>
<script src="/shared/assets/libraries/angular-animate.min.js"></script>
<script src="/shared/assets/libraries/angular-sanitize.min.js"></script>
<script src="/shared/assets/libraries/angular-cookies.min.js"></script>
<script src="/shared/assets/libraries/ng-file-upload-shim.min.js"></script> <!-- for no html5 browsers support -->
<script src="/shared/assets/libraries/ng-file-upload.min.js"></script>
<script src="/shared/assets/libraries/ng-img-crop.js"></script>
<link rel="stylesheet" type="text/css" href="/shared/assets/css/ng-img-crop.css">
<script src="/shared/assets/libraries/angular-css.js"></script>
<script src="/shared/assets/libraries/ng-touch/angular-touch.js"></script>
<script src="/shared/assets/libraries/angular-translate/dist/angular-translate.min.js"></script>
<script src="/shared/assets/libraries/angular-translate-loader-partial/angular-translate-loader-partial.min.js"></script>
<!-- UI Bootstrap -->
<script type="text/javascript" src="/shared/assets/libraries/angular-ui-bootstrap/dist/ui-bootstrap-tpls.js"></script>
<!-- Input mask -->
<script type="text/javascript" src="/shared/assets/libraries/mask.min.js"></script>
<!-- App -->
<script src="{{ elixir('shared/assets/js/app.js') }}"></script>
<script src="{{ elixir('shared/assets/js/components.js') }}"></script>
<script src="{{ elixir('shared/assets/js/services.js') }}"></script>
<script src="{{ elixir('translation/assets/js/app.js') }}"></script>
<script src="{{ elixir('translation/assets/js/components.js') }}"></script>
<script src="{{ elixir('translation/assets/js/services.js') }}"></script>
<script src="{{ elixir('translation/assets/js/components-templates.js') }}"></script>
</body>
</html>
