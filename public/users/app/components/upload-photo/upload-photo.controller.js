angular
    .module('users')
    .controller('uploadPhotoController', ['__env', function (__env) {
        this.$onInit = function () {
            var ctrl = this;
            // Upload photo config
            ctrl.ngFileUpload = {
                dragOver: {accept: 'dragover', reject: 'dragover-err', pattern: 'image/*'},
                validate: {size: {max: '6291456B', min: '1KB'}, duration: {max: '5m'}},
                ignoreInvalid: false
            };
            // Placeholder url
            ctrl.placeholder = __env.cloudinaryUrl + '/image/upload/production/assets/img/photo-placeholder.png';
        };
    }]);