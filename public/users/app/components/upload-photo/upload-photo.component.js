angular.module('users').component('uploadPhoto', {
    templateUrl: '/users/app/components/upload-photo/upload-photo.view.html',
    controller: 'uploadPhotoController',
    controllerAs: 'ctrl',
    bindings: {
        upPhoto: '=',
        upAction: '&'
    }
});