angular.module('users').controller('bookingEditPriceController', ['requestService', 'validationService',
    function (requestService, validationService) {
        this.$onInit = function () {
            var ctrl = this;
            ctrl.errors = {};

            ctrl.price = angular.copy(ctrl.price);

            // Clear field errors from ctrl.errors
            ctrl.resetField = function () {
                validationService.resetField('price', ctrl.errors, ctrl.form);
            };
            // Validate field
            ctrl.validateField = function () {
                validationService.validateField(ctrl.form.price, ctrl.errors);
            };

            // Accept booking
            ctrl.accept = function () {
                if (!ctrl.form.$valid) return;

                ctrl.loading = true;

                var attributes = {
                    price: ctrl.price,
                    status: 'accepted'
                };

                requestService.patchBooking(ctrl.bookingId, attributes).then(
                    function () {
                        ctrl.loading = false;
                        ctrl.onClose({update: true});
                    },
                    function (response) {
                        validationService.setBackendErrors(ctrl.form, ctrl.errors, response.data.errors);

                        ctrl.loading = false;
                    }
                )
            };
        };
    }]
);