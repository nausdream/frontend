angular.module('users').component('bookingEditPrice', {
    templateUrl: '/users/app/components/bookings/booking-edit-price/booking-edit-price.view.html',
    controller: 'bookingEditPriceController',
    controllerAs: 'ctrl',
    bindings: {
        bookingId: '@',
        onClose: '&',
        price: '<',
        currency: '@'
    }
});