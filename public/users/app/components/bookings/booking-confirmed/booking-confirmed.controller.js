angular.module('users').controller('bookingConfirmedController',
    function () {
        this.$onInit = function () {
            var ctrl = this;

            ctrl.user = angular.copy(ctrl.user);
            ctrl.email = ctrl.user.attributes.email;
        };
    }
);