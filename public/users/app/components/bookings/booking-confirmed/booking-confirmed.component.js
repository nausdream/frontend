angular.module('users').component('bookingConfirmed', {
    templateUrl: '/users/app/components/bookings/booking-confirmed/booking-confirmed.view.html',
    controller: 'bookingConfirmedController',
    controllerAs: 'ctrl',
    bindings: {
        user: '<',
        onClose: '&'
    }
});