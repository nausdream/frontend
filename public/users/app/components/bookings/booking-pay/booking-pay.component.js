angular.module('users').component('bookingPay', {
    templateUrl: '/users/app/components/bookings/booking-pay/booking-pay.view.html',
    controller: 'bookingPayController',
    controllerAs: 'ctrl',
    bindings: {
        bookingPromise: '<',
        payNow: '='
    }
});