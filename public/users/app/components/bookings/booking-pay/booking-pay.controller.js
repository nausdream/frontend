angular.module('users').controller('bookingPayController', [
    '__env', 'userService', '$routeParams', '$location', '$translate', 'translationService', '$q', '$timeout',
    '$rootScope', '$filter', 'requestService', '$sce',
    function (__env, userService, $routeParams, $location, $translate, translationService, $q, $timeout,
              $rootScope, $filter, requestService, $sce) {
        this.$onInit = function () {
            var ctrl = this;

            // Init translations object
            ctrl.translations = {};
            ctrl.translations.loading = true;
            ctrl.viewName = 'bookingPay';

            // Load translations
            var pages = [
                'booking'
            ];
            translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

            // Get booking id from routeParams
            ctrl.user = userService.get().attributes;

            ctrl.language = $routeParams.lang;

            // Submit form
            ctrl.submit = function () {
                // Get booking
                ctrl.bookingPromise.then(
                    function (response) {
                        var experienceId = response.data.data.relationships.experience.data.id;
                        ctrl.bookingId = response.data.data.id;
                        var booking = response.data.data.attributes;

                        if (booking.status != 'accepted') {
                            // Redirect to 403
                            $location.path('/' + ctrl.language + '/not-authorized');
                        }

                        /**
                         * Set all form fields
                         */
                        ctrl.action = $sce.trustAsResourceUrl(__env.paypalAddress);
                        ctrl.seller = __env.paypalSeller;

                        ctrl.successPage = __env.baseUrl + ctrl.language + '/bookings/' + ctrl.bookingId + '/success';
                        ctrl.failurePage = __env.baseUrl + ctrl.language + '/bookings/' + ctrl.bookingId + '/failure';
                        ctrl.bookingCurrency = booking.currency;
                        ctrl.notifyUrl = __env.backendUrl + 'v1/ipnlistener/';
                        // Language to locale
                        var localeMappings = {
                            it: 'it_IT',
                            en: 'en_US'
                        };
                        ctrl.locale = localeMappings[$translate.use()];

                        ctrl.price = booking.price_to_pay + '.00';

                        // Get experience
                        requestService.getSingleAsync('experiences', experienceId).then(
                            function (response) {
                                ctrl.experienceTitle = response.data.data.attributes.title;

                                $translate('booking.return_to_merchant').then(function (translation) {
                                    ctrl.returnToMerchant = translation;

                                    $timeout(function () {
                                        // Submit form
                                        if (ctrl.paypal_form.$valid) {
                                            // Submit clicking button
                                            $timeout(function () {
                                                angular.element(document.querySelector('#submit').click());
                                            }, 0);
                                        } else {
                                            // Redirect to error 500 page
                                            $location.path('/' + ctrl.language + '/error');
                                        }
                                    }, 0);
                                });
                            },
                            null
                        );
                    },
                    null
                )
            };

            if (ctrl.payNow) ctrl.submit();
        };
    }]
);