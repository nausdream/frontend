angular.module('users').controller('bookingPopupController', [
    'translationService', '__env', '$location', 'requestService', '$cookies', 'currencyService',
    'userService', '$q', 'mathService', 'fixedListService', 'dateService', 'validationService',
    '$scope', '$routeParams', 'adwordsService',
    function (translationService, __env, $location, requestService, $cookies, currencyService,
              userService, $q, mathService, fixedListService, dateService, validationService,
              $scope, $routeParams, adwordsService) {
        this.$onInit = function () {
            var ctrl = this;

            ctrl.language = $routeParams.lang;
            ctrl.currency = $cookies.get('currency');

            ctrl.user = userService.get();

            ctrl.modals = {};
            ctrl.dropdowns = {};

            // Validation bootstrap
            ctrl.errors = {};
            ctrl.formName = 'form';

            ctrl.fixedAdditionalServices = [];
            ctrl.additionalServices = [];

            // Init translations object
            ctrl.translations = {};
            ctrl.translations.loading = true;
            ctrl.viewName = 'bookingPopup';
            // Load translations
            var pages = [
                'booking', 'additional_services', 'inputs', 'errors'
            ];
            translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

            // Fetch currencies
            currencyService.getCurrencies().then(
                function (response) {
                    ctrl.currencies = response;
                },
                null
            );

            // Create promises to be resolved at specific loading steps
            ctrl.additionalServicesStepPromise = $q.defer();
            ctrl.everythingHasLoaded = $q.all([ctrl.additionalServicesStepPromise.promise]);

            // Fetch fixed additional services
            ctrl.fixedAdditionalServicesPromise = fixedListService.getFixedList('fixed-additional-services');

            // Set min number of hosts from correct period
            ctrl.setMinPersonFromPeriods = function (experienceResponse) {
                // Set min person from correct period
                var correctPeriod;
                var defaultPeriod;
                var nullDatePeriod;
                var periods = experienceResponse.included.filter(function (included) {
                    return included.type == 'periods';
                });
                periods.forEach(function (period) {
                    if (period.attributes.date_start == null) nullDatePeriod = period;
                    if (period.attributes.is_default) defaultPeriod = period;

                    var start = dateService.getDateFromString(period.attributes.date_start);
                    var end = dateService.getDateFromString(period.attributes.date_end);
                    if (ctrl.experienceDate >= start && ctrl.experienceDate <= end) {
                        correctPeriod = period;
                    }
                });
                if (correctPeriod) {
                    ctrl.minHosts = correctPeriod.attributes.min_person;
                    return;
                }
                if (defaultPeriod) {
                    ctrl.minHosts = defaultPeriod.attributes.min_person;
                    return;
                }
                if (nullDatePeriod) {
                    ctrl.minHosts = nullDatePeriod.attributes.min_person;
                    return;
                }
            };

            // Update hosts limits for inputs
            ctrl.updateHosts = function () {
                ctrl.minAdults = Math.max(0, ctrl.minHosts - ctrl.kids - ctrl.babies);
                ctrl.minKids = Math.max(0, ctrl.minHosts - ctrl.adults - ctrl.babies);
                ctrl.minBabies = Math.max(0, ctrl.minHosts - ctrl.adults - ctrl.kids);

                ctrl.maxAdults = ctrl.experienceSeats - ctrl.kids - ctrl.babies;
                ctrl.maxKids = Math.min(ctrl.experienceKids, ctrl.experienceSeats - ctrl.adults - ctrl.babies);
                ctrl.maxBabies = Math.min(ctrl.experienceBabies, ctrl.experienceSeats - ctrl.adults - ctrl.kids);
                ctrl.adultsRange = mathService.range(ctrl.minAdults, ctrl.maxAdults);
            };

            // Reload view
            ctrl.loadEstimation = function (experienceResponse, estimationResponse, isEstimation = false) {
                ctrl.loading = true;

                var estimation = estimationResponse.meta;

                // Set controller attributes from estimation
                ctrl.setAttributes(estimation);

                // Set additional services
                ctrl.setAdditionalServicesFromExperienceResponse(experienceResponse, estimationResponse, isEstimation);

                // Set min person from correct period
                // TODO

                // Set availabilities and days of weeks from experience
                ctrl.setAvailabilities(experienceResponse);
                ctrl.setAvailableDaysOfWeek(experienceResponse);

                // Hide loader when everything (sync or async has been loaded)
                ctrl.everythingHasLoaded.then(
                    function () {
                        ctrl.loading = false;
                    },
                    null
                )
            };

            // Set controller attributes from estimation or booking
            ctrl.setAttributes = function (object) {

                ctrl.priceFrom = object.price_per_person;
                ctrl.pricePerPersonWithoutServices = object.price_per_person_without_services;
                ctrl.servicesPrice = object.services_price;
                ctrl.entireBoat = object.entire_boat;
                ctrl.total = object.price_minus_discount;
                ctrl.totalGuestCurrency = object.price_minus_discount_guest;
                ctrl.priceOnBoard = object.price_on_board;
                ctrl.priceOnBoardGuestCurrency = object.price_on_board_guest;
                ctrl.priceToPay = object.price_to_pay;
                ctrl.priceToPayGuestCurrency = object.price_to_pay_guest;
                ctrl.discount = object.discount;
                ctrl.captainCurrency = object.captain_currency;

                // If it's from a booking, get different data
                if (object.experience_date) {
                    ctrl.adults = object.adults;
                    ctrl.kids = object.kids;
                    ctrl.babies = object.babies;
                    ctrl.experienceDate = object.experience_date;
                    ctrl.captainCurrency = object.currency;
                    ctrl.coupon = object.coupon;
                }

                currencyService.getCurrencySymbol(ctrl.captainCurrency).then(
                    function (response) {
                        ctrl.captainCurrencySymbol = response;
                    },
                    null
                );
                currencyService.getCurrencySymbol(ctrl.currency).then(
                    function (response) {
                        ctrl.userCurrencySymbol = response;
                    },
                    null
                );
                ctrl.isSameCurrency = ctrl.captainCurrency == ctrl.currency;

                if (typeof ctrl.experienceDate === 'string') ctrl.experienceDate = dateService.getDateFromString(ctrl.experienceDate);
                ctrl.hosts = ctrl.adults + ctrl.kids + ctrl.babies;
                ctrl.totalPriceWithoutServices = ctrl.pricePerPersonWithoutServices * ctrl.hosts;
            };

            // Take experience, loop through included and set list in controller
            ctrl.setAdditionalServicesFromExperienceResponse = function (experienceResponse, otherResponse, isEstimation = false) {
                // Load additional and fixed additional services
                if (experienceResponse.data.relationships.additional_services) {
                    ctrl.additionalServices = experienceResponse.included.filter(function (included) {
                        return included.type == 'additional-services';
                    });
                }
                if (experienceResponse.data.relationships.fixed_additional_service_prices) {
                    ctrl.fixedAdditionalServices = angular.copy(experienceResponse.included.filter(function (included) {
                        return included.type == 'fixed-additional-service-prices';
                    }));
                }

                // When fixed additional services are loaded, do a bunch of things:
                // - pre-select services that are in the booking or estimation
                // - add names to experience fixedAdditionalServices
                // - transform per_person prices to total prices (price * hosts)
                ctrl.fixedAdditionalServicesPromise.then(
                    function (response) {
                        ctrl.fixedAdditionalServicesList = response.data.data;

                        // Load additional services from booking/estimation
                        var otherObjectAdditionalServices = [];
                        var otherObjectFixedAdditionalServices = [];
                        if (!isEstimation) {
                            if (otherResponse.data.relationships.additional_services) {
                                otherObjectAdditionalServices = otherResponse.included.filter(function (included) {
                                    return included.type == 'additional-services';
                                })
                            }
                            if (otherResponse.data.relationships.fixed_additional_service_prices) {
                                otherObjectFixedAdditionalServices = otherResponse.included.filter(function (included) {
                                    return included.type == 'fixed-additional-service-prices';
                                })
                            }
                        }
                        if (isEstimation) {
                            if (otherResponse.included) {
                                otherObjectAdditionalServices = otherResponse.included.filter(function (included) {
                                    return included.type == 'additional-services';
                                });
                                otherObjectFixedAdditionalServices = otherResponse.included.filter(function (included) {
                                    return included.type == 'fixed-additional-service-prices';
                                });
                            }
                            ;
                        }
                        // Pre-select (checkbox) those services that are already present in the booking/estimation
                        otherObjectAdditionalServices.forEach(function (otherAdditionalService) {
                            ctrl.additionalServices.filter(function (additionalService) {
                                return additionalService.id == otherAdditionalService.id
                            })[0].checked = true;
                        });
                        otherObjectFixedAdditionalServices.forEach(function (otherAdditionalService) {
                            ctrl.fixedAdditionalServices.filter(function (additionalService) {
                                return additionalService.id == otherAdditionalService.id
                            })[0].checked = true;
                        });

                        // Add names into fixed additional services to be translated in the view
                        ctrl.fixedAdditionalServicesList.forEach(function (additionalService) {
                            var fixedAdditionalService = ctrl.fixedAdditionalServices.filter(function (experienceService) {
                                return additionalService.id == experienceService.relationships.fixed_additional_service.id;
                            })[0];

                            if (fixedAdditionalService) angular.merge(fixedAdditionalService.attributes, additionalService.attributes);
                        });

                        // Set correct prices on services
                        ctrl.fixedAdditionalServices.forEach(function (service) {
                            if (service.attributes.per_person) {
                                service.attributes.price *= ctrl.hosts;
                            }
                        });

                        // Tell the controller that this step is done
                        ctrl.additionalServicesStepPromise.resolve();
                    },
                    null
                );
            };

            // Set calendar availabilites from experience response
            ctrl.setAvailabilities = function (experienceResponse) {
                if (experienceResponse.data.relationships.experience_availabilities) {
                    ctrl.availabilities = angular.copy(experienceResponse.included.filter(function (included) {
                        return included.type == 'experience-availabilities';
                    }));
                }
            };

            // Set calendar available week days from experience response
            ctrl.setAvailableDaysOfWeek = function (experienceResponse) {
                ctrl.availableDaysOfWeek = [];

                var attributes = experienceResponse.data.attributes;

                ctrl.availableDaysOfWeek.push(
                    attributes.sunday,
                    attributes.monday,
                    attributes.tuesday,
                    attributes.wednesday,
                    attributes.thursday,
                    attributes.friday,
                    attributes.saturday
                );
            };

            // Get new prices
            ctrl.requestNewEstimation = function () {
                ctrl.loading = true;

                // Convert date to string
                var requestDate = dateService.getDateString(ctrl.experienceDate);

                var parameters = {
                    experience_id: ctrl.experienceId,
                    hosts: ctrl.adults + ctrl.kids + ctrl.babies,
                    request_date: requestDate,
                    currency: ctrl.currency
                };

                if (ctrl.coupon) parameters.coupon = ctrl.coupon;

                // Send only checked additional services
                var additionalServices = [];
                var fixedAdditionalServices = [];
                ctrl.additionalServices.forEach(function (additionalService) {
                    if (additionalService.checked) additionalServices.push(additionalService.id);
                });
                ctrl.fixedAdditionalServices.forEach(function (additionalService) {
                    if (additionalService.checked) fixedAdditionalServices.push(additionalService.id);
                });
                if (additionalServices.length > 0) {
                    parameters.additional_services = additionalServices.join(',');
                }
                if (fixedAdditionalServices.length > 0) {
                    parameters.fixed_additional_service_prices = fixedAdditionalServices.join(',');
                }

                ctrl.estimationPromise = requestService.getListAsync('estimations', parameters);
                ctrl.estimationPromise.then(
                    function (response) {
                        ctrl.loadEstimation(ctrl.experienceResponse, response.data, true);
                    },
                    function (response) {
                        validationService.setBackendErrors(ctrl[ctrl.formName], ctrl.errors, response.data.errors);

                        ctrl.loading = false;
                    }
                );
            };

            // Toggle warning based on message content
            ctrl.messageValidation = function () {
                var emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
                var urlRegex = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/g;
                var phoneRegex = /[0-9]{6,256}/g;

                // Runs all the validations
                ctrl.warning =
                    emailRegex.test(ctrl.message) ||
                    urlRegex.test(ctrl.message) ||
                    phoneRegex.test(ctrl.message);
            };

            /**
             * Send booking with current controller state
             *
             * Request chain:
             * 1. Patch user (if setting name or email)
             * 2. Post conversation (if it doesn't already exist)
             * 3. Post booking binding it to conversation
             * 4. Post message binding it to conversation (if not in the conversation already)
             */
            ctrl.sendRequest = function () {
                // Check if everything's valid
                if (!ctrl.areEstimationFieldsValid()) return;

                // Check form
                if (!ctrl[ctrl.formName].$valid) {
                    validationService.setFrontendErrors(ctrl[ctrl.formName], ctrl.errors);
                    return;
                }

                ctrl.loading = true;

                // Post single message
                var postMessage = function (response) {
                    if (ctrl.message) {
                        // Pass new booking id to parent
                        ctrl.newBookingId = response.data.data.id;

                        var conversationId = response.data.data.relationships.conversation.data.id;

                        var attributes = {message: ctrl.message};
                        var relationships = {
                            conversation: {
                                data: {
                                    type: 'conversations',
                                    id: conversationId
                                }
                            }
                        };
                        requestService.postSingle('messages', attributes, relationships).then(
                            function (response) {
                                // Everything's done! Open success popup
                                ctrl.toggleModals(true, true);
                                ctrl.loading = false;
                            },
                            function (response) {
                                validationService.setBackendErrors(ctrl[ctrl.formName], ctrl.errors, response.data.errors);
                                ctrl.loading = false;
                            }
                        );
                    }
                    else {
                        // No message, so nothing else to do
                        ctrl.toggleModals(true, true);
                        ctrl.loading = false;
                    }
                };

                // Post conversation if it doesn't exist yet, then post booking.
                // Post message only if not in the conversation
                var executeConversationBookingAndMessageSteps = function () {
                    var promise = $q.defer();
                    if (ctrl.conversationId) {
                        // Create booking
                        ctrl.postBooking(ctrl.conversationId).then(
                            function (response) {
                                // Pass new booking id to parent
                                ctrl.newBookingId = response.data.data.id;
                                // Success! Inform user with a popup
                                ctrl.toggleModals(true, true);
                                ctrl.loading = false;
                            },
                            function (response) {
                                validationService.setBackendErrors(ctrl[ctrl.formName], ctrl.errors, response.data.errors);
                                ctrl.loading = false;
                            }
                        )
                    }
                    else {
                        // Create new conversation
                        var attributes = {
                            request_date: dateService.getDateString(ctrl.experienceDate),
                            adults: ctrl.adults,
                            kids: ctrl.kids,
                            babies: ctrl.babies
                        };
                        var relationships = {
                            experience: {
                                data: {
                                    type: 'experiences',
                                    id: ctrl.experienceId
                                }
                            }
                        };
                        requestService.postSingle('conversations', attributes, relationships).then(
                            function (response) {
                                // Conversation created: attach new booking to this conversation
                                return ctrl.postBooking(response.data.data.id).then(
                                    // Response to Booking POST. If successful, post message
                                    function (response) {
                                        postMessage(response);
                                    },
                                    function (response) {
                                        // Display validation errors in form
                                        validationService.setBackendErrors(ctrl[ctrl.formName], ctrl.errors, response.data.errors);
                                        ctrl.loading = false;
                                    }
                                );
                            },
                            function (response) {
                                // Conversation not created: if a previous already exists,
                                // attach new booking to this conversation
                                if (response.data.errors[0].code == 'conversation_already_exists') {
                                    return ctrl.postBooking(response.data.meta.conversation_id).then(
                                        // Response to Booking POST. If successful, post message
                                        function (response) {
                                            postMessage();
                                        },
                                        function (response) {
                                            // Display validation errors in form
                                            validationService.setBackendErrors(ctrl[ctrl.formName], ctrl.errors, response.data.errors);
                                            ctrl.loading = false;
                                        }
                                    );
                                }
                            }
                        )
                    }
                };

                // Patch user name and email only at least one of them is not set
                var userHasToBePatched = !ctrl.user.attributes.first_name || !ctrl.user.attributes.email;
                if (userHasToBePatched) {
                    var user = angular.copy(ctrl.user);
                    user.attributes.first_name = ctrl.userName;
                    user.attributes.email = ctrl.userEmail;

                    requestService.patchUser(user).then(
                        function (response) {
                            // Replace user in userService
                            userService.save(response.data.data);

                            // Update controller user
                            ctrl.user = userService.get();

                            // Do other steps
                            executeConversationBookingAndMessageSteps();
                        },
                        function (response) {
                            // Display validation errors in form
                            validationService.setBackendErrors(ctrl[ctrl.formName], ctrl.errors, response.data.errors);
                            ctrl.loading = false;
                        }
                    );
                }
                else {
                    // Don't patch user
                    executeConversationBookingAndMessageSteps();
                }
            };

            // Close success/errors modals
            ctrl.toggleModals = function (open = true, success = false) {
                if (success) {
                    ctrl.modals.success = !ctrl.modals.success;

                    // When closing success modal, close booking popup too
                    if (!open && ctrl.onClose) {
                        ctrl.onClose({update: true, newBookingId: ctrl.newBookingId});
                    }

                    return;
                }
                ctrl.modals.error = !ctrl.modals.error;
            };

            // Update dates when language is changed
            $scope.$on('languageChanged', function (event, language) {
                ctrl.language = language;
            });

            // Update prices when currency is changed if is not the captain
            $scope.$on('currencyChanged', function (event, currency) {
                ctrl.currency = currency;
            });

            // Validation
            // Clear field errors from ctrl.errors
            ctrl.resetField = function (field) {
                validationService.resetField(field, ctrl.errors, ctrl[ctrl.formName]);
            };

            // Set new adult: close dropdown, clear errors on field, set ctrl.adults, update new maximums
            // Request new estimation
            ctrl.setAdult = function (newAdults) {
                ctrl.adults = newAdults;
                ctrl.dropdowns.adults = false;
                ctrl.errors.hosts = null;
                ctrl.updateHosts();

                if (!ctrl.areEstimationFieldsValid()) return;

                ctrl.requestNewEstimation();
            };
            // Set new kid or baby and request new estimation
            ctrl.setOtherHost = function () {
                ctrl.errors.hosts = null;
                ctrl.updateHosts();

                if (!ctrl.areEstimationFieldsValid()) return;

                ctrl.requestNewEstimation();
            };

            // Validate field
            ctrl.validateField = function (field) {
                validationService.validateField(ctrl[ctrl.formName][field], ctrl.errors);
            };

            // On blur, validate and then request new estimation
            ctrl.onBlur = function (field) {

                if (field) {
                    ctrl.validateField(field);
                }

                if (!ctrl.areEstimationFieldsValid()) return;

                ctrl.requestNewEstimation();
            };

            // Check if estimation request can be sent
            ctrl.areEstimationFieldsValid = function () {
                // HOSTS
                var hosts = ctrl.adults + ctrl.kids + ctrl.babies;
                if (hosts > ctrl.experienceSeats) return false;
                if (ctrl.kids > ctrl.experienceKids) return false;
                if (ctrl.babies > ctrl.experienceBabies) return false;

                // DATE
                if (!ctrl.experienceDate) return false;
                if (ctrl.errors.experience_date) return false;

                return true;
            };

            // Post booking binding it to a conversation
            ctrl.postBooking = function (conversationId) {
                var object = {
                    experience_date: dateService.getDateString(ctrl.experienceDate),
                    adults: ctrl.adults,
                    kids: ctrl.kids,
                    babies: ctrl.babies
                };
                if (ctrl.coupon) object.coupon = ctrl.coupon;

                var relationships = {conversation: {data: {type: 'conversations', id: conversationId}}};

                // Attach checked additional service to relationships
                var additionalServices = {data: []};
                ctrl.additionalServices.forEach(function (service) {
                    if (service.checked) {
                        additionalServices.data.push({id: service.id, type: 'additional-services'});
                    }
                });
                var fixedAdditionalServices = {data: []};
                ctrl.fixedAdditionalServices.forEach(function (service) {
                    if (service.checked) {
                        fixedAdditionalServices.data.push({id: service.id, type: 'fixed-additional-service-prices'});
                    }
                });

                // If there are checked services, add them as relationships
                if (additionalServices.data.length > 0) relationships.additional_services = additionalServices;
                if (fixedAdditionalServices.data.length > 0) relationships.fixed_additional_service_prices = fixedAdditionalServices;

                let bookingPromise = requestService.postSingle('bookings', object, relationships);
                // Track conversion
                bookingPromise.then(
                    function () {
                        adwordsService.sendConversion('booking');
                    }, null);

                // Save new booking id to be passed to the parent
                return bookingPromise;
            };

            // If date changes, request new estimation
            $scope.$watch(
                function () {
                    return ctrl.experienceDate;
                },
                function (newValue, oldValue) {
                    if (!oldValue ||
                        typeof oldValue.getTime != 'function' ||
                        typeof newValue.getTime != 'function') return;

                    if (newValue.getTime() != oldValue.getTime()) {
                        delete ctrl.errors.experience_date;
                        delete ctrl.errors.error;
                        ctrl.onBlur(null);
                    }
                });

            // LOAD DATA FOR THE FIRST TIME
            // Either an estimation or a booking promise can be passed to this component
            if (ctrl.estimationPromise) {
                var promises = $q.all([ctrl.estimationPromise, ctrl.experiencePromise]);
                promises.then(
                    function (responses) {
                        var estimationResponse = responses[0].data;
                        ctrl.experienceResponse = responses[1].data;
                        var experience = ctrl.experienceResponse.data.attributes;
                        ctrl.experienceId = ctrl.experienceResponse.data.id;
                        ctrl.experienceSeats = experience.seats;
                        ctrl.experienceBabies = experience.babies;
                        ctrl.experienceKids = experience.kids;

                        // Sets min and max adults kids and babies
                        ctrl.setMinPersonFromPeriods(ctrl.experienceResponse);
                        ctrl.updateHosts();

                        ctrl.loadEstimation(ctrl.experienceResponse, estimationResponse, true);
                    },
                    // There is no estimation (because it was wrong in the estimation box)
                    function (responses) {
                        ctrl.experiencePromise.then(
                            function (response) {
                                // TODO
                            },
                            null
                        );
                    }
                );
            }
            if (ctrl.bookingPromise) {
                var promises = $q.all([ctrl.bookingPromise, ctrl.experiencePromise]);
                promises.then(
                    function (responses) {
                        var bookingResponse = responses[0].data;
                        var booking = responses[0].data.data.attributes;
                        ctrl.experienceResponse = responses[1].data;
                        var experience = ctrl.experienceResponse.data.attributes;
                        ctrl.experienceId = ctrl.experienceResponse.data.id;
                        ctrl.experienceSeats = experience.seats;
                        ctrl.experienceBabies = experience.babies;
                        ctrl.experienceKids = experience.kids;

                        if (responses[0].data.data.relationships.conversation) {
                            ctrl.conversationId = responses[0].data.data.relationships.conversation.data.id;
                        }

                        // Set controller attributes from booking
                        ctrl.setAttributes(booking);

                        // Sets min/max adults kids and babies
                        ctrl.setMinPersonFromPeriods(ctrl.experienceResponse);
                        ctrl.updateHosts();

                        // Set additional services
                        ctrl.setAdditionalServicesFromExperienceResponse(ctrl.experienceResponse, bookingResponse);

                        // Set min person from correct period
                        // TODO

                        // Set availabilities and days of weeks from experience
                        ctrl.setAvailabilities(ctrl.experienceResponse);
                        ctrl.setAvailableDaysOfWeek(ctrl.experienceResponse);

                        // Hide loader when everything (sync or async has been loaded)
                        ctrl.everythingHasLoaded.then(
                            function () {
                                ctrl.loading = false;
                            },
                            null
                        )
                    },
                    null
                );
            }
        };
    }
]);