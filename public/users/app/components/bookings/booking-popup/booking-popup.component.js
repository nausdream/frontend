angular.module('users').component('bookingPopup', {
    templateUrl: '/users/app/components/bookings/booking-popup/booking-popup.view.html',
    controller: 'bookingPopupController',
    controllerAs: 'ctrl',
    bindings: {
        adults: '=?',
        kids: '=?',
        babies: '=?',
        experienceDate: '=?',
        estimationPromise: '<',
        experiencePromise: '<',
        conversationId: '@',
        bookingPromise: '<',
        onClose: '&'
    }
});