angular.module('users').controller('bookingsController', [
        'translationService', 'requestService', '$location', 'dateService', 'userService', '$rootScope', '$cookies', '$routeParams',
        function (translationService, requestService, $location, dateService, userService, $rootScope, $cookies, $routeParams) {
            this.$onInit = function () {
                var ctrl = this;
                ctrl.loading = true;

                // Set language for dates formats
                ctrl.language = $routeParams.lang;

                // Init translations object
                ctrl.translations = {};
                ctrl.translations.loading = true;
                ctrl.viewName = 'booking';

                // Get user from service
                ctrl.user = userService.get().attributes;
                ctrl.isUserCaptain = ctrl.user.is_captain;

                // Define default behaviour for back button url
                ctrl.backButtonUrl = '/' + ctrl.language + '/dashboard';

                // Open accordion by default
                ctrl.open = true;

                // Load translations
                var pages = [
                    'booking'
                ];
                translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

                // Get correct preview data from booking object
                // E.g., captain should see user message, and booking is read
                // if the user making the request has read it
                ctrl.getPreviewData = function (booking) {
                    if (!booking) return;

                    var bookingAttributes = booking.attributes;

                    var experienceTitle = bookingAttributes.experience_title;
                    var date = bookingAttributes.experience_date;
                    var guest = bookingAttributes.first_name_guest;
                    var status = bookingAttributes.status;


                    return {title: experienceTitle, date: date, guest: guest, status: status};
                };

                // Goes to the previous page
                ctrl.back = function (event, backButtonUrl) {
                    // Prevent parent dom from catching the event
                    event.stopPropagation();

                    // Assign default path to back button
                    if (!backButtonUrl) {
                        backButtonUrl = ctrl.backButtonUrl;
                    }

                    // Change route
                    $location.path(backButtonUrl);
                };

                // Update dates when language is changed
                $rootScope.$on('languageChanged', function (event, language) {
                    ctrl.language = language;
                })

                // Load bookings differently for users and captains
                var bookingsPromise;
                if (ctrl.captainBookings) {
                    bookingsPromise = requestService.getRelatedSingleAsync('captains', ctrl.userId, 'bookings', {per_page: 100});
                } else {
                    bookingsPromise = requestService.getRelatedSingleAsync('users', ctrl.userId, 'bookings', {per_page: 100});
                }
                bookingsPromise.then(
                    function (response) {
                        ctrl.bookings = response.data.data;

                        // Set correct preview data
                        ctrl.bookings.forEach(function (booking) {
                            booking.previewData = ctrl.getPreviewData(booking);
                        });

                        ctrl.loading = false;
                    },
                    function (response) {
                        ctrl.bookings = [];

                        ctrl.loading = false;
                        // TODO MANAGE DIFFERENT RESPONSES
                    }
                );
            };
        }
    ]
);