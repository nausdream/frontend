angular.module('users').component('bookings', {
    templateUrl: '/users/app/components/bookings/bookings/bookings.view.html',
    controller: 'bookingsController',
    controllerAs: 'ctrl',
    bindings: {
        userId: '<',
        onBookingClick: '&',
        showAccordion: '<',
        showBackButton: '<',
        captainBookings: '<'
    }
});