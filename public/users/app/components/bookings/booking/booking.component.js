angular.module('users').component('booking', {
    templateUrl: '/users/app/components/bookings/booking/booking.view.html',
    controller: 'bookingController',
    controllerAs: 'ctrl',
    bindings: {
        bookingPromise: '<',
        onBackClick: '&',
        onEdit: '&',
        action: '<'
    }
});