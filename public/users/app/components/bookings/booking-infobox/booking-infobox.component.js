angular.module('users').component('bookingInfobox', {
    templateUrl: '/users/app/components/bookings/booking-infobox/booking-infobox.view.html',
    controller: 'bookingInfoboxController',
    controllerAs: 'ctrl',
    bindings: {
        conversationPromise: '<'
    }
});