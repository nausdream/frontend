angular.module('users').controller('bookingInfoboxController', [
    'translationService', '__env', '$location', 'requestService', '$cookies', 'currencyService', '$scope',
    'userService', '$timeout', '$routeParams',
    function (translationService, __env, $location, requestService, $cookies, currencyService, $scope,
              userService, $timeout, $routeParams) {
        this.$onInit = function () {
            var ctrl = this;
            ctrl.loading = true;

            ctrl.language = $routeParams.lang;

            // Placeholder
            ctrl.photoPlaceholder = __env.cloudinaryUrl + '/image/upload/production/assets/img/photo-placeholder.png';

            // Fetch currencies
            currencyService.getCurrencies().then(
                function (response) {
                    ctrl.currencies = response;
                },
                null
            );

            // Init translations object
            ctrl.translations = {};
            ctrl.translations.loading = true;
            ctrl.viewName = 'bookingInfobox';
            // Load translations
            var pages = [
                'booking', 'status', 'experience', 'experiences', 'experience_types', 'additional_services'
            ];
            translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

            // Open/close edit modal
            ctrl.toggleEditModal = function (open = true, update = false) {
                ctrl.editModal = !ctrl.editModal;

                if (update) {
                    ctrl.conversationPromise = requestService.getSingleAsync('conversations', ctrl.conversationId);
                    ctrl.loadData();
                }
            };

            // Open/close edit modal
            ctrl.toggleRejectModal = function (open = true) {
                ctrl.rejectModal = !ctrl.rejectModal;
            };

            // Open/close new request modal
            ctrl.toggleNewRequestModal = function (open = true, update = false) {
                ctrl.newRequestModal = !ctrl.newRequestModal;

                // Reload data when closing new request popup
                if (!open && update) {
                    ctrl.conversationPromise = requestService.getSingleAsync('conversations', ctrl.conversationId);
                    ctrl.loadData();
                }
            };

            // Accept booking
            ctrl.accept = function () {
                var attributes = {
                    price: ctrl.total,
                    status: 'accepted'
                };

                ctrl.loading = true;

                requestService.patchBooking(ctrl.bookingId, attributes).then(
                    function (response) {
                        ctrl.setAttributes(response.data);
                        ctrl.loading = false;
                    },
                    null
                )
            };
            // Reject booking
            ctrl.reject = function () {
                var attributes = {
                    status: 'rejected'
                };

                ctrl.loading = true;

                requestService.patchBooking(ctrl.bookingId, attributes).then(
                    function (response) {
                        ctrl.setAttributes(response.data);
                        ctrl.loading = false;

                        ctrl.toggleRejectModal(false);
                    },
                    null
                )
            };

            // Set controller attributes from booking or estimation
            ctrl.setAttributes = function (responseData, isBooking = true) {
                var included = responseData.included || [];

                if (isBooking) {
                    var object = responseData.data.attributes;

                    ctrl.priceFrom = object.price_per_person;
                    ctrl.adults = object.adults;
                    ctrl.kids = object.kids;
                    ctrl.babies = object.babies;
                    ctrl.hosts = ctrl.adults + ctrl.kids + ctrl.babies;
                    ctrl.requestDate = object.experience_date;

                    ctrl.pricePerPersonWithoutServices = object.price_per_person_without_services;
                    ctrl.totalPriceWithoutServices = ctrl.pricePerPersonWithoutServices * ctrl.hosts;
                    ctrl.servicesPrice = object.services_price;
                    ctrl.discount = object.discount;

                    ctrl.status = object.status;

                    if (ctrl.isUserCaptain) {
                        ctrl.total = object.price;

                        if (object.status == 'accepted') {
                            ctrl.status = 'accepted_captain';
                        }
                    } else {
                        ctrl.total = object.price_minus_discount;
                    }

                    ctrl.totalGuestCurrency = object.price_minus_discount_guest;
                    ctrl.priceOnBoard = object.price_on_board;
                    ctrl.priceOnBoardGuestCurrency = object.price_on_board_guest;
                    ctrl.priceToPay = object.price_to_pay;
                    ctrl.priceToPayGuestCurrency = object.price_to_pay_guest;
                    ctrl.nausdreamFee = ctrl.priceToPay + ctrl.discount;
                    ctrl.coupon = object.coupon;

                    // Check if captain currency and guest currency are the same
                    // (if so, don't show prices in guest currency too)
                    ctrl.captainCurrency = object.currency;
                    currencyService.getCurrencySymbol(ctrl.captainCurrency).then(
                        function (response) {
                            ctrl.captainCurrencySymbol = response;
                        },
                        null
                    );
                    ctrl.isSameCurrency = ctrl.captainCurrency == ctrl.userCurrency;

                    // Fetch additional services
                    ctrl.fixedAdditionalServices = included.filter(function (included) {
                        return included.type == 'fixed-additional-service-prices';
                    });
                    ctrl.additionalServices = included.filter(function (included) {
                        return included.type == 'additional-services';
                    });
                }
                else {
                    var object = responseData.meta;

                    ctrl.priceFrom = object.price_per_person;
                    ctrl.pricePerPersonWithoutServices = object.price_per_person_without_services;
                    ctrl.totalPriceWithoutServices = ctrl.pricePerPersonWithoutServices * ctrl.hosts;
                    ctrl.servicesPrice = object.services_price;
                    ctrl.discount = object.discount;

                    if (ctrl.isUserCaptain) {
                        ctrl.total = ctrl.totalPriceWithoutServices + ctrl.servicesPrice;
                    } else {
                        ctrl.total = object.price_minus_discount;
                    }

                    ctrl.totalGuestCurrency = object.price_minus_discount_guest;
                    ctrl.priceOnBoard = object.price_on_board;
                    ctrl.priceOnBoardGuestCurrency = object.price_on_board_guest;
                    ctrl.priceToPay = object.price_to_pay;
                    ctrl.priceToPayGuestCurrency = object.price_to_pay_guest;
                    ctrl.nausdreamFee = ctrl.priceToPay + ctrl.discount;

                    // Check if captain currency and guest currency are the same
                    // (if so, don't show prices in guest currency too)
                    ctrl.captainCurrency = object.captain_currency;
                    currencyService.getCurrencySymbol(ctrl.captainCurrency).then(
                        function (response) {
                            ctrl.captainCurrencySymbol = response;
                        },
                        null
                    );
                    ctrl.isSameCurrency = ctrl.captainCurrency == ctrl.userCurrency;

                    // Fetch additional services
                    ctrl.fixedAdditionalServices = included.filter(function (included) {
                        return included.type == 'fixed-additional-service-prices';
                    });
                    ctrl.additionalServices = included.filter(function (included) {
                        return included.type == 'additional-services';
                    });
                }
            };

            // Pay
            ctrl.book = function () {
                $location.path('/' + ctrl.language + '/bookings/' + ctrl.bookingId + '/pay');
            };

            // Fetch all relevant data
            ctrl.loadData = function () {
                ctrl.conversationPromise.then(
                    function (response) {
                        ctrl.conversationId = response.data.data.id;
                        var conversation = response.data.data.attributes;
                        var included = response.data.included;

                        ctrl.fixedAdditionalServices = [];
                        ctrl.additionalServices = [];

                        // There has to be an experience
                        if (response.data.data.relationships.experience) {
                            var experienceId = response.data.data.relationships.experience.data[0].id;

                            ctrl.experiencePromise = requestService.getSingleAsync('experiences', experienceId);
                            ctrl.experiencePromise.then(
                                function (response) {
                                    var experience = response.data.data.attributes;

                                    ctrl.photo = experience.cover_photo || ctrl.photoPlaceholder;
                                    ctrl.name = experience.title;
                                    ctrl.experienceType = experience.type;
                                    ctrl.location = experience.departure_port;
                                    ctrl.departureTime = experience.departure_time;
                                    ctrl.departurePort = experience.departure_port;
                                    ctrl.arrivalTime = experience.arrival_time;
                                    ctrl.arrivalPort = experience.arrival_port;
                                },
                                null
                            )
                        } else {
                            $location.path('/' + ctrl.language + '/error');
                        }

                        // Get user
                        ctrl.user = userService.get();
                        // Get user currency
                        ctrl.userCurrency = $cookies.get('currency');
                        currencyService.getCurrencySymbol(ctrl.userCurrency).then(
                            function (response) {
                                ctrl.userCurrencySymbol = response;
                            },
                            null
                        );
                        // Get user language to pass to dateFilters
                        ctrl.userLanguage = $routeParams.lang;

                        // Determine whether the user is the guest or the captain
                        ctrl.isUserCaptain = conversation.role == 'captain';

                        // If the conversation has an associated booking, request it to the backend
                        // If there's no booking yet, then we need to display estimated prices based on conversation data
                        if (response.data.data.relationships.booking) {
                            ctrl.hasBooking = true;

                            ctrl.loadBooking = function () {
                                ctrl.loading = true;

                                ctrl.bookingPromise = requestService.getSingleAsync('bookings', response.data.data.relationships.booking.data[0].id);
                                ctrl.bookingPromise.then(
                                    function (response) {
                                        var booking = response.data.data.attributes;
                                        ctrl.bookingId = response.data.data.id;

                                        // Set controller attributes from booking
                                        ctrl.setAttributes(response.data);

                                        $timeout(function () {
                                            ctrl.loading = false;
                                        }, 0);
                                    },
                                    null
                                );
                            };
                            ctrl.loadBooking();

                        } else {
                            ctrl.hasBooking = false;
                            ctrl.status = null;

                            ctrl.adults = conversation.adults;
                            ctrl.kids = conversation.kids;
                            ctrl.babies = conversation.babies;
                            ctrl.hosts = ctrl.adults + ctrl.kids + ctrl.babies;
                            ctrl.requestDate = conversation.request_date;

                            ctrl.loadEstimation = function () {
                                ctrl.loading = true;

                                // Fetch estimation
                                var parameters = {
                                    experience_id: experienceId,
                                    hosts: ctrl.hosts,
                                    request_date: ctrl.requestDate,
                                    currency: ctrl.userCurrency
                                };
                                ctrl.estimationPromise = requestService.getListAsync('estimations', parameters);
                                ctrl.estimationPromise.then(
                                    function (response) {
                                        var estimation = response.data.meta;

                                        // Set controller attributes from estimation
                                        ctrl.setAttributes(response.data, false);

                                        $timeout(function () {
                                            ctrl.loading = false;
                                        }, 0);

                                    },
                                    null
                                )
                            };
                            ctrl.loadEstimation();
                        }
                    },
                    null
                );
            };
            ctrl.loadData();

            // Update dates when language is changed
            $scope.$on('languageChanged', function (event, language) {
                ctrl.userLanguage = language;
            });

            // Update prices when currency is changed if is not the captain
            $scope.$on('currencyChanged', function (event, currency) {
                if (ctrl.isUserCaptain) return;

                // Get user currency and reload controller data
                ctrl.userCurrency = currency;
                currencyService.getCurrencySymbol(ctrl.userCurrency).then(
                    function (response) {
                        ctrl.userCurrencySymbol = response;

                        if (ctrl.hasBooking) {
                            if (ctrl.loadBooking) ctrl.loadBooking();
                            return;
                        }
                        if (ctrl.loadEstimation) ctrl.loadEstimation();
                    },
                    null
                );
            })
        };
    }
]);