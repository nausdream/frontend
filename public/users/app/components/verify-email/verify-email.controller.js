angular.module('users').controller('verifyEmailController', [
    'translationService', 'requestService', 'userService', 'validationService',
    function (translationService, requestService, userService, validationService) {
        this.$onInit = function () {
            var ctrl = this;
            ctrl.translations = {};
            ctrl.translations.loading = true;
            ctrl.viewName = 'verifyEmail';
            ctrl.email = '';
            ctrl.errors = {};

            var user = userService.get();

            // Load translations
            var pages = ['inputs', 'errors', 'header'];
            translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

            // Send email
            ctrl.send = function () {
                if (ctrl.form.$valid && !ctrl.loading) {
                    ctrl.loading = true;

                    user.attributes.email = ctrl.email;
                    requestService.patchUser(user).then(
                        function (response) {
                            ctrl.verified = true;
                            ctrl.loading = false;
                        },
                        function (response) {
                            // Failure
                            validationService.setBackendErrors(ctrl.form, ctrl.errors, response.data.errors);
                            ctrl.loading = false;
                        }
                    )
                } else {
                    validationService.setFrontendErrors(ctrl.form, ctrl.errors);
                }
            };

            ctrl.validateField = function (field) {
                validationService.validateField(ctrl.form[field], ctrl.errors);
            };

            ctrl.resetField = function (field) {
                if (ctrl.errors) {
                    ctrl.errors[field] = null;
                    ctrl.errors.error = null;
                }
            };
        }
    }]);