angular.module('users').component('verifyEmail', {
    templateUrl: '/users/app/components/verify-email/verify-email.view.html',
    controller: 'verifyEmailController',
    controllerAs: 'ctrl',
    bindings: {
        onClose: '&'
    }
});