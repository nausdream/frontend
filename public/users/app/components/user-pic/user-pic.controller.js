angular
    .module('users')
    .controller('userPicController', ['__env', function (__env) {
        this.$onInit = function () {
            var ctrl = this;

            // Set default role
            if (!ctrl.userRole) ctrl.userRole = 'guest';

            // Placeholders url
            ctrl.placeholders = {
                guest: __env.cloudinaryUrl + '/image/upload/production/assets/img/user-placeholder.png',
                captain: __env.cloudinaryUrl + '/image/upload/production/assets/img/home/captains.png'
            };

            // Set user pic, if not defined revert to placeholder
            if (!ctrl.userPicLink) {
                ctrl.isPlaceholder = true;
                ctrl.photo = ctrl.placeholders[ctrl.userRole];
            } else {
                ctrl.photo = ctrl.userPicLink;
            }
        };
    }]);