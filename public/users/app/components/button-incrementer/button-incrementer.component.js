angular.module('users').component('buttonIncrementer', {
    templateUrl: '/users/app/components/button-incrementer/button-incrementer.view.html',
    controller: 'buttonIncrementerController',
    bindings: {
        'model': '=',
        'min': '<',
        'max': '<',
        'callback': '&'
    }
});