angular.module('users').controller('buttonIncrementerController', [
    '$timeout',
    function ($timeout) {
        this.$onInit = function () {
            var ctrl = this;

            this.addValue = function (val) {
                if ((ctrl.model + val) < ctrl.min) {
                    ctrl.model = ctrl.min;
                    return;
                }
                if ((ctrl.model + val) > ctrl.max) {
                    ctrl.model = ctrl.max;
                    return;
                }
                ctrl.model += val;

                // Force the callback on the next digest cycle
                $timeout(function () {
                    ctrl.callback();
                }, 0);

            }
        };
    }]);