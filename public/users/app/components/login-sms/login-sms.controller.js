angular
    .module('users')
    .controller('loginSmsController', [
        'translationService',
        function (translationService) {
            this.$onInit = function () {
                var vm = this;
                vm.verifyPhone = verifyPhone;
                vm.onlyNumber = /^\d+$/;
                vm.viewName = 'header';

                var pages = ['header'];
                translationService.translate(pages, vm.viewName);

                function verifyPhone() {
                    AccountKit.login('PHONE', {}, function (response) {
                        if (response.status === "PARTIALLY_AUTHENTICATED") {
                            vm.onComplete({response: response});
                        }
                        else if (response.status === "NOT_AUTHENTICATED") {
                            // handle authentication failure
                        }
                        else if (response.status === "BAD_PARAMS") {
                            // handle bad parameters
                        }
                    });
                }
            }
        }]
    );