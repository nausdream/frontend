angular
	.module('users')
	.component('smsAuth', {
		templateUrl: '/users/app/components/login-sms/login.sms.html',
		controller: 'loginSmsController',
		controllerAs: 'smsAuth',
		bindings: {
			onComplete: '&'
		}
	});