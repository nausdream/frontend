angular.module('users').controller('termsOfServiceController', [
    'translationService', '$routeParams', '$location',
    function (translationService, $routeParams, $location) {
        this.$onInit = function () {
            var ctrl = this;

            ctrl.viewName = 'termsOfService';
            ctrl.translations = {};
            ctrl.translations.loading = true;

            ctrl.language = $routeParams.lang;

            // Load translations
            var pages = ['login'];
            translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

            // Define status colors
            ctrl.colors = {
                conversation_new: 'blue',
                conversation_read: 'grey',
                boat_pending: 'orange',
                boat_unfinished: 'grey',
                boat_accepted: 'green',
                boat_rejected: 'red',
                boat_accepted_conditionally: 'orange',
                booking_accepted_captain: 'orange',
                booking_accepted: 'green',
                booking_pending: 'orange',
                booking_rejected: 'red',
                booking_paid: 'green'
            };

            ctrl.goTo = function (path) {
                $location.path('/' + $routeParams.lang + '/' + path);

                ctrl.onClick();
            }
        }
    }]);