angular.module('users').component('termsOfService', {
    templateUrl: '/users/app/components/terms-of-service/terms-of-service.view.html',
    controller: 'termsOfServiceController',
    controllerAs: 'ctrl',
    bindings: {
        onClick: '&'
    }
});