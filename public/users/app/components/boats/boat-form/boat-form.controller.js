angular.module('users').controller('boatFormController', [
    'translationService', 'requestService', 'dateService', '$cookies', '$rootScope', 'mapsService', '$timeout',
    '__env', 'fixedListService', '$q', '$location', 'mathService', 'validationService', 'photoService', 'userService',
    '$routeParams', '$window',
    function (translationService, requestService, dateService, $cookies, $rootScope, mapsService, $timeout,
              __env, fixedListService, $q, $location, mathService, validationService, photoService, userService,
              $routeParams, $window) {
        this.$onInit = function () {
            var ctrl = this;
            ctrl.loading = true;

            ctrl.language = $routeParams.lang;

            // Init translations object
            ctrl.translations = {};
            ctrl.translations.loading = true;
            ctrl.viewName = 'boat';
            ctrl.userName = userService.get().attributes.first_name;

            // Load translations
            var pages = [
                'boat', 'captain_form', 'captain_form_breadcrumb', 'inputs',
                'experience_types', 'boat_types', 'motor_types', 'boat_materials', 'accessories'
            ];
            translationService.translate(pages, ctrl.viewName).then(
                function (response) {
                    ctrl.translations.loading = false
                }, null);

            // Save onBackClick function to avoid overwriting it
            ctrl.onBackClickStepOne = angular.copy(ctrl.onBackClick);

            // Go to a certain step
            ctrl.goToStep = function (step) {
                if (step != 1 && step != 2) step = 1;

                // Set back click functions and form name
                if (step == 1) {
                    ctrl.formName = 'form_boat';

                    ctrl.onBackClick = function () {
                        $location.search('step', null);
                        ctrl.onBackClickStepOne();
                    }
                } else {
                    ctrl.formName = 'form_rules';

                    ctrl.onBackClick = function () {
                        ctrl.goToStep(1);
                    };
                }

                // Set step
                ctrl.step = step;
                $location.search('step', step);
            };

            // Set initial step and keep url in sync with view
            var step = $location.search().step;
            ctrl.goToStep(step);

            // Continue bootstrapping controller
            ctrl.dropdowns = {};
            ctrl.boatYears = mathService.range(1800, new Date().getFullYear(), 1, true);
            ctrl.tempBoat = {
                internal_photo: {},
                external_photo: {},
                sea_photo: {}
            };
            ctrl.tempBoat.experienceTypes = [];
            ctrl.disabled = false;
            ctrl.errors = {};

            // Promises
            var boatTypesPromises = fixedListService.getFixedList('boat-types');
            var boatMaterialsPromise = fixedListService.getFixedList('boat-materials');
            var motorTypesPromise = fixedListService.getFixedList('motor-types');
            var experienceTypesPromise = fixedListService.getFixedList('experience-types');
            var accessoriesPromise = fixedListService.getFixedList('accessories');
            // Boat types
            boatTypesPromises.then(
                function (response) {
                    ctrl.boatTypes = response.data.data;
                },
                function (response) {
                    ctrl.boatTypes = null;
                }
            );
            // Materials
            boatMaterialsPromise.then(
                function (response) {
                    ctrl.boatMaterials = response.data.data;
                },
                function (response) {
                    ctrl.boatMaterials = null;
                }
            );
            // Motor types
            motorTypesPromise.then(
                function (response) {
                    ctrl.motorTypes = response.data.data;
                },
                function (response) {
                    ctrl.motorTypes = null;
                }
            );

            // After the boat has loaded
            ctrl.boatPromise.then(
                function (response) {
                    ctrl.boatId = response.data.data.id;
                    ctrl.boat = response.data.data.attributes;
                    var relationships = response.data.data.relationships;

                    // User can't edit boat if rejected or accepted_conditionally
                    if (ctrl.boat.status == 'rejected' || ctrl.boat.status == 'pending') {
                        $location.path('/' + ctrl.language + '/not-authorized');
                    }

                    // If boat is accepted, we need to patch it with the current data to make everything editable
                    if (ctrl.boat.status == 'accepted') {
                        var boat = angular.copy(ctrl.boat);
                        delete boat.status;
                        delete boat.is_finished;

                        // Send request
                        var promise = requestService.putBoat(ctrl.boatId, boat);
                        promise.then(
                            function (response) {
                                ctrl.boat = response.data.data.attributes;

                                $window.location.reload();
                            },
                            function (response) {
                                // Failure
                                validationService.setBackendErrors(form, ctrl.errors, response.data.errors);
                            }
                        );
                    }
                    else {
                        ctrl.boatName = ctrl.boat.name || ctrl.boat.model || 'boat.new_boat';

                        ctrl.tempBoat.internal_photo.link = ctrl.boat.internal_photo;
                        ctrl.tempBoat.external_photo.link = ctrl.boat.external_photo;
                        ctrl.tempBoat.sea_photo.link = ctrl.boat.sea_photo;

                        // Load experience types into tempBoat model
                        experienceTypesPromise.then(
                            function (response) {
                                ctrl.experienceTypes = response.data.data;
                                // Load experience types into array
                                if (relationships) {
                                    var tempExperienceTypes = relationships.experience_types.data;
                                    // Assign a subset of experience types to tempBoat experienceTypes array.
                                    // The selected one are the ones where the id matches one id of the relationships experience types
                                    ctrl.tempBoat.experienceTypes = ctrl.experienceTypes.filter(function (type) {
                                        return tempExperienceTypes.filter(function (tempType) {
                                                return tempType.id == type.id;
                                            }).length > 0;
                                    });
                                }
                            },
                            function (response) {
                                ctrl.experienceTypes = null;
                            }
                        );

                        // Set checked status on accessories
                        accessoriesPromise.then(
                            function (response) {
                                ctrl.accessories = response.data.data;

                                if (relationships) {
                                    // Set initial checked accessories
                                    var boatAccessories = relationships.accessories;
                                    if (boatAccessories) {
                                        boatAccessories.data.forEach(function (boatAccessory) {
                                            // Find the matching accessory in accessories list, and set its checked attribute to true
                                            ctrl.accessories.filter(function (accessory) {
                                                return accessory.id == boatAccessory.id;
                                            })[0].attributes.checked = true;
                                        })
                                    }
                                }
                            },
                            function (response) {
                                // TODO MANAGE RESPONSE
                            }
                        );

                        ctrl.loading = false;
                    }
                },
                null
            );

            // Trigger map load
            ctrl.triggerMapLoad = function () {
                $q.all([ctrl.boatPromise, mapsService.mapsInitialized]).then(
                    function () {
                        $timeout(ctrl.loadMap, 0);
                    }
                );
            };

            // Patch boat
            ctrl.patchBoat = function (form, requireAll = false) {
                ctrl.patching = true;
                // Build user model
                var boat = {};
                boat.id = ctrl.boatId;
                boat.attributes = angular.copy(ctrl.boat);
                // If you don't delete status, the backend thinks you're trying to change boat status
                delete boat.attributes.status;

                // If every field is required, block requests with empty fields
                var canGoOn = true;
                if (requireAll) {
                    canGoOn = form.$valid;
                } else {
                    // For each form input, take error type (key), and check if type is other than 'required' or 'password_match'.
                    // If the key is, e.g., maxlength, then check what is the field with error. If it's password, go on.
                    // Else, the form can't be submitted
                    angular.forEach(form.$error, function (error, key) {
                        if (-1 == ['required', 'password_match'].indexOf(key)) {
                            error.forEach(function (err) {
                                // If error field (name) is not password or password_match, the form can't be submitted
                                if (-1 == ['password', 'password_match'].indexOf(err.$name)) {
                                    canGoOn = false;
                                }
                            });
                        }
                    });
                }
                if (!canGoOn) return;

                // Send request
                var promise = requestService.putBoat(boat.id, boat.attributes);
                promise.then(
                    function (response) {
                        ctrl.boat = response.data.data.attributes;
                        ctrl.patching = false;
                    },
                    function (response) {
                        // Failure
                        validationService.setBackendErrors(form, ctrl.errors, response.data.errors);
                        ctrl.patching = false;
                    }
                );

                return promise;
            };

            // Patch boat on every field change (auto-save)
            ctrl.patchInput = function (field, resetErrors) {
                ctrl.patching = true;

                // Radio button need its errors to be reset,
                // because it's sending patch on change and not on blur like inputs
                if (resetErrors) {
                    ctrl.resetField(field);
                } else {
                    ctrl.validateField(field);
                }

                // Do not send patch if field is invalid
                if (ctrl.errors[field]) {
                    ctrl.patching = false;
                    return;
                }

                // We need to delete it because otherwise the backend will think we're trying to save it
                delete ctrl.boat.is_finished;

                ctrl.patchBoat(ctrl[ctrl.formName], false);
            };
            ctrl.patchExperienceTypes = function () {
                ctrl.patching = true;
                requestService.patchExperienceTypes(ctrl.boatId, ctrl.tempBoat.experienceTypes).then(
                    function (response) {
                        ctrl.patching = false;
                    }
                );
            };
            ctrl.patchAccessories = function () {
                ctrl.patching = true;

                // Select only accessory with the checked flag
                var accessories = ctrl.accessories.filter(function (accessory) {
                    return !!accessory.attributes.checked;
                });

                requestService.patchAccessories(ctrl.boatId, accessories).then(
                    function (response) {
                        ctrl.patching = false;
                    },
                    function (response) {
                        console.log('Failed to patch accessories.');
                    }
                );
            };
            ctrl.postPhoto = function (file, photoType) {
                if (!file) return;

                // Hide errors on new upload
                delete ctrl.errors.photos;

                ctrl.tempBoat[photoType + '_photo'].loading = true;

                return photoService.getLink(file).then(
                    function (response) {
                        var link = response;
                        requestService.postPhoto(response, 'boat', ctrl.boatId, photoType).then(
                            function (response) {
                                // Photo was patched to boat
                                ctrl.tempBoat[photoType + '_photo'].link = link;
                                ctrl.tempBoat[photoType + '_photo'].loading = false;
                            }
                        );
                    },
                    function (response) {
                        console.log(response);
                    }
                );
            };

            // Toggle modal
            ctrl.toggleSaveModal = function (open = true) {
                ctrl.saving = !ctrl.saving;
            };

            // Check validity and open modal for confirmation
            ctrl.finishEditing = function () {
                // Manual validate certain inputs
                var error = ctrl.validateRulesStep();

                if (!error && ctrl[ctrl.formName].$valid && !ctrl.loading) {
                    ctrl.errors = {};

                    // Open modal
                    ctrl.toggleSaveModal();

                } else {
                    validationService.setFrontendErrors(ctrl[ctrl.formName], ctrl.errors)
                }
            };
            // Save boat and set status to pending
            ctrl.setDone = function () {
                // Finish editing (set is_finished to true)
                ctrl.boat.is_finished = true;
                ctrl.patchBoat(ctrl[ctrl.formName], true).then(
                    function (response) {
                        // Redirect to view
                        $location.search('step', null);
                        $location.path('/' + ctrl.language + '/boats/' + ctrl.boatId);
                    },
                    function (response) {
                        // TODO MANAGE ERROR
                    }
                );
            };

            // Filter restoration year
            ctrl.restorationYearFilter = function (value, index) {
                if (!ctrl.boat) return;

                if (!ctrl.boat.construction_year) return true;

                return value >= ctrl.boat.construction_year;
            };
            // Filter out already selected experience types
            ctrl.notSelectedExperienceTypes = function (experienceType) {
                return ctrl.tempBoat.experienceTypes.filter(function (type) {
                        return experienceType.id == type.id;
                    }).length == 0;
            };
            ctrl.selectedExperienceTypes = function (experienceType) {
                return ctrl.tempBoat.experienceTypes.filter(function (type) {
                        return experienceType.id == type.id;
                    }).length == 0;
            };

            // Clear field errors from ctrl.errors
            ctrl.resetField = function (field) {
                validationService.resetField(field, ctrl.errors, ctrl[ctrl.formName]);
            };
            // Validate field after blur
            ctrl.validateField = function (field) {
                validationService.validateField(ctrl[ctrl.formName][field], ctrl.errors);
            };
            // Perform validation on those inputs that can't be validated by angular
            ctrl.validate = function () {
                var error = false;

                // SPOKEN LANGUAGES
                if (ctrl.tempBoat.experienceTypes.length == 0) {
                    ctrl.errors.experience_types = {code: 'required'};
                    error = true;
                    console.log('error experienceTypes');
                }

                // BOAT TYPE
                if (ctrl.boat.type == null) {
                    ctrl.errors.boat_type = {code: 'required'};
                    error = true;
                    console.log('error boatTypes');
                }

                return error;
            };
            ctrl.validateRulesStep = function () {
                var error = false;

                if (!ctrl.tempBoat.internal_photo.link || !ctrl.tempBoat.external_photo.link || !ctrl.tempBoat.sea_photo.link) {
                    error = true;

                    ctrl.errors.photos = {code: 'required'};
                    console.log('error photos');
                }

                return error;
            };
            // If form is valid, set step value by setting location.search (triggers reload)
            ctrl.goToNextStep = function () {
                // Manual validate certain inputs
                var error = ctrl.validate();

                if (!error && ctrl[ctrl.formName].$valid && !ctrl.loading) {
                    ctrl.errors = {};

                    // Set step
                    ctrl.goToStep(2);

                    // Set back button to go back to step 1
                    ctrl.onBackClick = function () {
                        ctrl.goToStep(1);
                    }
                } else {
                    validationService.setFrontendErrors(ctrl[ctrl.formName], ctrl.errors)
                }
            };

            // Custom inputs
            ctrl.toggleExperienceType = function (experienceType, forceDelete = false) {
                if (ctrl.selectedExperienceType) {
                    // Clear errors on field
                    delete ctrl.errors.experience_types;

                    // Determine if experienceType is already selected
                    var experienceTypeAlreadySelected = ctrl.tempBoat.experienceTypes.filter(function (type) {
                            return type.id == experienceType.id;
                        }).length > 0;

                    var patch = true;

                    if (experienceTypeAlreadySelected) {
                        if (forceDelete) {
                            // Remove
                            ctrl.tempBoat.experienceTypes = ctrl.tempBoat.experienceTypes.filter(function (type) {
                                return type.id != experienceType.id
                            });
                        } else {
                            // Nothing to do, don't patch
                            patch = false;
                        }
                    } else {
                        // Add
                        ctrl.tempBoat.experienceTypes.push(experienceType);
                    }
                    if (patch) ctrl.patchExperienceTypes();
                }
            };

            // Load map
            ctrl.loadMap = function () {
                // DEPARTURE MAP
                // TODO get lat and lng from ip
                var mapCoordinates = {lat: ctrl.boat.lat || 39.211406, lng: ctrl.boat.lng || 9.113607};

                // Set boat coordinates to default if they were not assigned.
                ctrl.boat.lat = mapCoordinates.lat;
                ctrl.boat.lng = mapCoordinates.lng;

                var map = new google.maps.Map(document.getElementById('map'), {
                    center: mapCoordinates,
                    zoomControl: true,
                    streetViewControl: false,
                    mapTypeControl: false,
                    fullscreenControl: false,
                    disableDoubleClickZoom: true,
                    zoom: 8
                });
                var marker = new google.maps.Marker({
                    position: mapCoordinates,
                    draggable: true,
                    map: map
                });

                $timeout(function () {
                    map.panTo(mapCoordinates);
                }, 100);

                // Make map respond to click event: move the marker
                map.addListener('dblclick', function (event) {
                    marker.setPosition(event.latLng);
                    $timeout(function () {
                        map.panTo(event.latLng);
                    }, 100);

                    // Set new boat coordinates
                    mapCoordinates.lat = event.latLng.lat();
                    mapCoordinates.lng = event.latLng.lng();
                    ctrl.boat.lat = event.latLng.lat();
                    ctrl.boat.lng = event.latLng.lng();

                    ctrl.patchBoat(ctrl[ctrl.formName]);
                });
                // Save new coordinates on marker drag
                marker.addListener('dragend', function (event) {
                    // Set new boat coordinates
                    mapCoordinates.lat = event.latLng.lat();
                    mapCoordinates.lng = event.latLng.lng();
                    ctrl.boat.lat = event.latLng.lat();
                    ctrl.boat.lng = event.latLng.lng();

                    ctrl.patchBoat(ctrl[ctrl.formName]);
                });

                $timeout(function () {
                    google.maps.event.trigger(map, 'resize');
                    map.setCenter(mapCoordinates);
                }, 0);
            };
        };
    }
])
;