angular.module('users').component('boatForm', {
    templateUrl: '/users/app/components/boats/boat-form/boat-form.view.html',
    controller: 'boatFormController',
    controllerAs: 'ctrl',
    bindings: {
        boatPromise: '<',
        onBackClick: '&'
    }
});