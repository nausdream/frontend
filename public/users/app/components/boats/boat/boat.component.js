angular.module('users').component('boat', {
    templateUrl: '/users/app/components/boats/boat/boat.view.html',
    controller: 'boatController',
    controllerAs: 'ctrl',
    bindings: {
        boatPromise: '<',
        onBackClick: '&',
        onEdit: '&'
    }
});