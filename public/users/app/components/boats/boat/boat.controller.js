angular.module('users').controller('boatController', [
    'translationService', 'requestService', 'dateService', '$cookies', '$rootScope', 'mapsService', '$timeout', '__env', 'fixedListService', '$q', '$location', '$routeParams',
    function (translationService, requestService, dateService, $cookies, $rootScope, mapsService, $timeout, __env, fixedListService, $q, $location, $routeParams) {
        this.$onInit = function () {
            var ctrl = this;

            ctrl.loading = true;

            // Set language for dates formats
            ctrl.language = $routeParams.lang;

            // Error objects for validation
            ctrl.errors = {};
            ctrl.backendErrors = {};

            // Placeholder
            ctrl.photoPlaceholder = __env.cloudinaryUrl + '/image/upload/production/assets/img/photo-placeholder.png';
            ctrl.textPlaceholder = 'boat.not_filled';

            // Needed here and not inside promises callback,
            // because you need to check if they are empty or not regardless of the presence of relationships
            ctrl.experienceTypes = [];
            ctrl.accessories = [];

            // Toggle the display of warning message about phone and url
            ctrl.warning = false;

            // Init translations object
            ctrl.translations = {};
            ctrl.translations.loading = true;
            ctrl.viewName = 'boat';
            // Load translations
            var pages = [
                'boat', 'experience_types', 'motor_types', 'boat_materials', 'boat_types', 'accessories', 'inputs'
            ];
            translationService.translate(pages, ctrl.viewName).then(
                function () {
                    ctrl.translations.loading = false;
                }
            );

            // Promises
            var accessoriesPromise = fixedListService.getFixedList('accessories');
            var experienceTypesPromise = fixedListService.getFixedList('experience-types');

            ctrl.boatPromise.then(
                function (response) {
                    ctrl.boat = response.data.data.attributes;

                    // Set translation keys on material and motor type
                    ctrl.boat.type = ctrl.boat.type ? 'boat_types.' + ctrl.boat.type : null;
                    ctrl.boat.material = ctrl.boat.material ? 'boat_materials.' + ctrl.boat.material : null;
                    ctrl.boat.motor_type = ctrl.boat.motor_type ? 'motor_types.' + ctrl.boat.motor_type : null;

                    ctrl.boatName = ctrl.boat.name || ctrl.boat.model || 'boat.new_boat';
                    var relationships = response.data.data.relationships;

                    // Define button actions
                    var goToBoatEdit = function () {
                        ctrl.onEdit();
                    };
                    var goToNewBoat = function () {
                        ctrl.loading = true;
                        $location.path('/' + ctrl.language + '/boats/' + response.data.data.id + '/new');
                    };

                    // Define default action for call to action
                    // Initialize action object for cta
                    ctrl.actionObject = {
                        visible: true,
                        translationElement: 'inputs.edit',
                        action: goToBoatEdit
                    };
                    ctrl.actionObject.action = goToBoatEdit;

                    // Set action and view depending on boat status
                    switch (ctrl.boat.status) {
                        case 'rejected':
                            ctrl.actionObject.action = goToNewBoat;
                            ctrl.actionObject.translationElement = 'boat.new_boat_cta';
                            break;
                        case 'pending':
                            ctrl.actionObject.visible = false;
                            ctrl.actionObject.action = null;
                            break;
                    }

                    if (relationships) {
                        // Load accessories
                        if (relationships.accessories) {
                            accessoriesPromise.then(
                                function (response) {
                                    var boatAccessories = relationships.accessories.data;
                                    var accessories = response.data.data;

                                    // Cycle through boat accessories and load them into ctrl.accessories
                                    boatAccessories.forEach(function (boatAccessory) {
                                        var match = accessories.filter(function (accessory) {
                                            return boatAccessory.id == accessory.id;
                                        });
                                        if (match.length > 0) ctrl.accessories.push(match[0]);
                                    });
                                },
                                function (response) {
                                    //?
                                }
                            );
                        } else {
                            // Empty promise
                            accessoriesPromise = $q.when(true);
                        }


                        // Load experience types
                        if (relationships.experience_types) {
                            experienceTypesPromise.then(
                                function (response) {
                                    var boatExperienceTypes = relationships.experience_types.data;
                                    var experienceTypes = response.data.data;

                                    // Cycle through boat experienceTypes and load them into ctrl.experienceTypes
                                    boatExperienceTypes.forEach(function (boatExperienceType) {
                                        var match = experienceTypes.filter(function (experienceType) {
                                            return boatExperienceType.id == experienceType.id;
                                        });
                                        if (match.length > 0) ctrl.experienceTypes.push(match[0]);
                                    });
                                },
                                null
                            );
                        } else {
                            // Empty promise
                            experienceTypesPromise = $q.when(true);
                        }
                    } else {
                        // Assign promise that will be resolved with a 'true' value
                        accessoriesPromise = $q.when(true);
                        experienceTypesPromise = $q.when(true);
                    }
                },
                function (response) {
                    // TODO MANAGE REJECTION
                }
            );

            // Hides loader only when all async request have resolved positively
            $q.all([ctrl.boatPromise, accessoriesPromise, experienceTypesPromise]).then(
                function (response) {
                    ctrl.loading = false;
                }
            );

            // Trigger map load
            ctrl.triggerMapLoad = function () {
                $q.all([ctrl.boatPromise, mapsService.mapsInitialized]).then(
                    function () {
                        $timeout(ctrl.loadMap, 0);
                    }
                );
            };

            // Load map
            ctrl.loadMap = function () {
                // Load map only if not rejected
                if (ctrl.boat.status != 'rejected') {

                    // TODO get lat and lng from ip
                    var mapCoordinates = {lat: ctrl.boat.lat || 39.211406, lng: ctrl.boat.lng || 9.113607};
                    var map = new google.maps.Map(document.getElementById('map'), {
                        center: mapCoordinates,
                        zoomControl: true,
                        streetViewControl: false,
                        mapTypeControl: false,
                        fullscreenControl: false,
                        disableDoubleClickZoom: true,
                        zoom: 8
                    });

                    $timeout(function () {
                        map.panTo(mapCoordinates);
                    }, 100);

                    // Don't create marker if boat position wasn't set
                    // boat.lng is set only if boat.lat is set
                    if (ctrl.boat.lat != null) {
                        var marker = new google.maps.Marker({
                            position: mapCoordinates,
                            draggable: false,
                            map: map
                        });
                    }

                    $timeout(function () {
                        google.maps.event.trigger(map, 'resize');
                        map.setCenter(mapCoordinates);
                    }, 0);
                }
            };

            // Update dates when language is changed
            $rootScope.$on('languageChanged', function (event, language) {
                ctrl.language = language;
            });
        };
    }
]);