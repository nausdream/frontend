angular.module('users').controller('boatsController', [
        'translationService', 'requestService', '$location', 'dateService', 'userService', '__env', '$rootScope', '$cookies', '$routeParams',
        function (translationService, requestService, $location, dateService, userService, __env, $rootScope, $cookies, $routeParams) {
            this.$onInit = function () {
                var ctrl = this;
                ctrl.loading = true;

                // Init translations object
                ctrl.translations = {};
                ctrl.translations.loading = true;
                ctrl.viewName = 'boat';

                // Get user from service
                ctrl.user = userService.get().attributes;

                // Set language for dates formats
                ctrl.language = $routeParams.lang;

                // Define default behaviour for back button url
                ctrl.backButtonUrl = '/' + ctrl.language + '/dashboard';

                // Open accordion by default
                ctrl.open = true;

                // Boat photo placeholder
                ctrl.placeholder = __env.cloudinaryUrl + '/image/upload/production/assets/img/photo-placeholder.png';

                // Load translations
                var pages = [
                    'boat'
                ];
                translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

                // Get correct preview data from boat object
                ctrl.getPreviewData = function (boat) {
                    if (!boat) return;

                    var boatAttributes = boat.attributes;

                    var photo = boatAttributes.sea_photo || ctrl.placeholder;
                    var name = boatAttributes.name || boatAttributes.model || 'boat.new_boat';
                    var date = new Date(boatAttributes.created_at);
                    var status = boatAttributes.status || 'unfinished';
                    return {photo: photo, name: name, date: date, status: status};
                };

                // Go to new boat route
                ctrl.addNewBoat = function () {
                    $location.path('/' + ctrl.language + '/boats/new')
                };

                // Load boats
                var boatsPromise = requestService.getRelatedSingleAsync('captains', ctrl.userId, 'boats', {
                    per_page: 100,
                    editing: true
                });
                boatsPromise.then(
                    function (response) {
                        ctrl.boats = response.data.data;

                        // Set correct preview data
                        ctrl.boats.forEach(function (boat) {
                            boat.previewData = ctrl.getPreviewData(boat);
                        });

                        ctrl.loading = false;
                    },
                    function (response) {
                        ctrl.boats = [];

                        ctrl.loading = false;
                        // TODO MANAGE DIFFERENT RESPONSES
                    }
                );

                // Goes to the previous page
                ctrl.back = function (event, backButtonUrl) {
                    // Prevent parent dom from catching the event
                    event.stopPropagation();

                    // Assign default path to back button
                    if (!backButtonUrl) {
                        backButtonUrl = ctrl.backButtonUrl;
                    }

                    // Change route
                    $location.path(backButtonUrl);
                };

                // Update dates when language is changed
                $rootScope.$on('languageChanged', function (event, language) {
                    ctrl.language = language;
                })
            };
        }
    ]
);