angular.module('users').component('boats', {
    templateUrl: '/users/app/components/boats/boats/boats.view.html',
    controller: 'boatsController',
    controllerAs: 'ctrl',
    bindings: {
        userId: '<',
        onBoatClick: '&',
        showAccordion: '<',
        showBackButton: '<'
    }
});