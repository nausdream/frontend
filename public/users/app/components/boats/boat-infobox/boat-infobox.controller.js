angular.module('users').controller('boatInfoboxController', [
    'translationService', '__env',
    function (translationService, __env) {
        this.$onInit = function () {
            var ctrl = this;
            ctrl.loading = true;

            // Placeholder
            ctrl.photoPlaceholder = __env.cloudinaryUrl + '/image/upload/production/assets/img/photo-placeholder.png';
            ctrl.textPlaceholder = 'boat.not_filled';

            // Init translations object
            ctrl.translations = {};
            ctrl.translations.loading = true;
            ctrl.viewName = 'boatInfoBox';
            // Load translations
            var pages = [
                'boat', 'status'
            ];
            translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

            ctrl.boatPromise.then(
                function (response) {
                    ctrl.boat = response.data.data.attributes;

                    // Prepare data for view
                    ctrl.boat.status = ctrl.boat.status || 'unfinished';
                    ctrl.boat.photo = ctrl.boat.sea_photo || ctrl.photoPlaceholder;
                    ctrl.boatName = ctrl.boat.name || ctrl.boat.model || 'boat.new_boat';

                    // Define action for cta
                    var goToBoatEdit = function () {
                        ctrl.onEdit();
                    };
                    // Define default action for call to action
                    // Initialize action object for cta
                    ctrl.actionObject = {
                        visible: true,
                        translationElement: 'inputs.edit',
                        action: goToBoatEdit,
                        href: '#'
                    };

                    // Hide cta if already editing boat
                    if (ctrl.editing) {
                        ctrl.actionObject.visible = false;
                    }

                    switch (ctrl.boat.status) {
                        case 'accepted_conditionally':
                            ctrl.actionObject.translationElement = 'boat.get_in_touch';
                            ctrl.actionObject.action = null;
                            ctrl.actionObject.href = "mailto:prova@prova.com";
                            break;
                        case 'pending':
                        case 'rejected':
                            ctrl.actionObject.visible = false;
                            break;
                    }
                    ctrl.cta = 'new_boat_cta';

                    ctrl.loading = false;
                },
                function (response) {
                    // TODO MANAGE REJECTION
                }
            );
        };
    }
]);