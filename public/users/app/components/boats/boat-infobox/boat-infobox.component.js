angular.module('users').component('boatInfobox', {
    templateUrl: '/users/app/components/boats/boat-infobox/boat-infobox.view.html',
    controller: 'boatInfoboxController',
    controllerAs: 'ctrl',
    bindings: {
        boatPromise: '<',
        editing: '<',
        onEdit: '&'
    }
});