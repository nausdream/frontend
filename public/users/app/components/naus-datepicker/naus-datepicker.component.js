/**
 * date             => the Date object to be set (require)
 * options          => an option object for the calendar (optional)
 * availabilities   => an array of availabilities as returned by the backend in the included object
 * daysOfWeek       => an array of 0s an 1s, where the index is the day of week (starting from sunday)
 *                     (e.g. [0, 1, 0, 0, 0, 1, 0] is a week where only monday and friday are allowed to be selected)
 */
angular.module('users').component('nausDatepicker', {
    templateUrl: '/users/app/components/naus-datepicker/naus-datepicker.view.html',
    controller: 'nausDatepickerController',
    controllerAs: 'ctrl',
    bindings: {
        date: '=',
        options: '<',
        availabilities: '=',
        daysOfWeek: '='
    }
});