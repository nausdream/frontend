angular.module('users').controller('nausDatepickerController', ['dateService',
    function (dateService) {
        this.$onInit = function () {
            var ctrl = this;

            // Decide which dates should be disabled
            ctrl.dateDisabled = function (data) {
                var date = data.date,
                    mode = data.mode;

                // Return true if not set
                if (!date) return true;

                // If we are not in 'day' view, don't disable anything
                if (mode != 'day') return false;

                // Disable all dates that fall outside day of week. If daysOfWeek is not provided, every day is allowed
                if (!ctrl.daysOfWeek) ctrl.daysOfWeek = [1, 1, 1, 1, 1, 1, 1];
                if (!ctrl.daysOfWeek[date.getDay()]) return true;

                // Disable all dates that falls out availabilities
                // if availabilities are not defined, let any date to be chosen
                // if date is outside availabilities, disallow date selection
                if (!ctrl.availabilities) return false;

                var outOfRange = true;
                ctrl.availabilities.forEach(function (availability) {
                    var dateStart = availability.attributes.date_start;
                    var dateEnd = availability.attributes.date_end;

                    // Convert string to date objects
                    dateStart = dateService.getDateFromString(dateStart);
                    dateEnd = dateService.getDateFromString(dateEnd);

                    // If date is in range return false (= not disabled)
                    if (date >= dateStart && date <= dateEnd) outOfRange = false;
                });

                return outOfRange;
            };

            var data = {date: ctrl.date, mode: 'day'};
            if (ctrl.dateDisabled(data)) {
                ctrl.date = null;
            }

            if (!ctrl.placeholder) ctrl.placeholder = 'experience.select_date';

            if (!ctrl.options) {
                ctrl.options = {
                    minDate: new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
                    showWeeks: false,
                    dateDisabled: ctrl.dateDisabled
                }
            }
        }
    }]);