angular.module('users').controller('rootController', [
        'modalService', '$rootScope', 'pageService', '$location', '$route', '$interval', '$translate', '$cookies',
        'statusCodeService', '$window', '$routeParams', 'validLanguages', '$timeout', 'adwordsService',
        function (modalService, $rootScope, pageService, $location, $route, $interval, $translate, $cookies,
                  statusCodeService, $window, $routeParams, validLanguages, $timeout, adwordsService) {
            var ctrl = this;
            ctrl.isModalOpen = modalService.isOpen;
            ctrl.collapsed = true;
            ctrl.loading = false;

            ctrl.cookieBanner = $cookies.get('cookiePolicyAccepted') == 0;
            console.log($cookies.get('cookiePolicyAccepted'), ctrl.cookieBanner);

            ctrl.language = $cookies.get('language');

            // Hide cookie banner
            ctrl.hideCookieBanner = function() {
                $timeout(function () {
                    ctrl.cookieBanner = false;
                }, 0);

                $cookies.put('cookiePolicyAccepted', 1);
            };

            // Prevent body from scrolling when modal is open
            $rootScope.$on('modalOpened', function () {
                ctrl.isModalOpen = true;
            });
            $rootScope.$on('modalClosed', function () {
                ctrl.isModalOpen = false;
            });

            // Open and close sidebar
            $rootScope.$on('sidebarOpened', function () {
                ctrl.collapsed = false;
            });
            $rootScope.$on('sidebarCollapsed', function () {
                ctrl.collapsed = true;
            });

            // When language starts to change, display loader
            $rootScope.$on('languageChanging', function (event, language) {
                // Update route param lang
                $route.updateParams({lang: language});
                ctrl.loading = true;
            });
            // Stops loading when translation has been done
            $rootScope.$on('languageChanged', function (event, language) {
                ctrl.loading = false;
            });
            // Watch on translate load: refresh translation tables
            $rootScope.$on('customPartialLoaderStructureChanged', function (event, data) {
                if ($translate.use()) {
                    $translate.refresh();
                } else {
                    var token = $interval(function () {
                        if (!$translate.use()) return;
                        $translate.refresh();
                        $interval.cancel(token);
                    }, 10);
                }
            });

            // Set page title and clear every time path change
            $rootScope.$on("pageChanged", function (event, newPage) {
                ctrl.page = newPage;

                // For crawlers (alternative version on pages in different languages)
                $rootScope.alternates = [];
                validLanguages.forEach(function (language) {
                    if (language != $routeParams.lang) {
                        $rootScope.alternates.push(
                            {
                                lang: language,
                                url: $location.absUrl().replace('/' + $routeParams.lang + '/', '/' + language + '/')
                            }
                        )
                    }
                });
            });
            $rootScope.$on("$routeChangeSuccess", function (event) {
                ctrl.page = '';
            });

            // On user logged out, if the current page is private, redirect to home
            $rootScope.$on("userLoggedOut", function (event) {
                if (pageService.getPrivate()) {
                    $location.path('/' + ctrl.language + '/').search({}).hash(null);
                }
            });

            // On route change start
            $rootScope.$on("$routeChangeStart", function (event) {

                // Reset private state to false
                pageService.setPrivate(false);

                // Reset status code to 200
                statusCodeService.set(200);
            });

            // Listen to live chat messages
            ctrl.setLiveChatEvents = function () {
                var olark = $window.olark || {};
                olark('api.chat.onMessageToOperator', function(event) {
                    adwordsService.sendConversion('live-chat');
                });
                olark('api.chat.onOfflineMessageToOperator', function(event) {
                    adwordsService.sendConversion('live-chat');
                });
            };

            // Listen to scroll event on window
            var listenToScrollEvent = function (event) {
                ctrl.hideCookieBanner();
                angular.element($window).off('scroll', listenToScrollEvent);
            };

            angular.element($window).on('scroll', listenToScrollEvent);
        }
    ]
);
