angular.module('users').component('liveChat', {
    templateUrl: '/users/app/components/live-chat/live-chat.view.html',
    controller: 'liveChatController',
    controllerAs: 'ctrl',
    bindings: {
        onLoad: '&'
    }
});