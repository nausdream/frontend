angular.module('users').controller('liveChatController', [
        '$rootScope', '$cookies', '__env', '$scope', '$q', 'translationService', '$filter', "$window",
        function ($rootScope, $cookies, __env, $scope, $q, translationService, $filter, $window) {
            var ctrl = this;

            this.$onInit = function () {

                //if (__env.appEnv == 'production') {
                ctrl.deferredTranslations = $q.defer();

                var translations = {};
                var viewName = 'livechat';
                var pages = ['livechat'];

                // Load translations and fire event when done
                translationService.translate(pages, viewName).then(
                    function () {
                        ctrl.deferredTranslations.resolve();
                    }
                );

                ctrl.setChatText = function (olark) {
                    /* Online Chatbox */
                    olark.configure("locale.welcome_title", $filter('translate')('livechat.welcome_title'));
                    olark.configure("locale.chatting_title", $filter('translate')('livechat.chatting_title'));
                    olark.configure("locale.welcome_message", $filter('translate')('livechat.welcome_message'));
                    olark.configure("locale.chat_input_text", $filter('translate')('livechat.chat_input_text'));
                    olark.configure("locale.ended_chat_message", $filter('translate')('livechat.ended_chat_message'));

                    /* Offline Chatbox */
                    olark.configure("locale.unavailable_title", $filter('translate')('livechat.unavailable_title'));
                    olark.configure("locale.away_message", $filter('translate')('livechat.away_message'));
                    olark.configure("locale.name_input_text", $filter('translate')('livechat.name_input_text'));
                    olark.configure("locale.email_input_text", $filter('translate')('livechat.email_input_text'));
                    olark.configure("locale.phone_input_text", $filter('translate')('livechat.phone_input_text'));
                    olark.configure("locale.offline_note_message", $filter('translate')('livechat.offline_note_message'));
                    olark.configure("locale.send_button_text", $filter('translate')('livechat.send_button_text'));
                    olark.configure("locale.offline_note_thankyou_text", $filter('translate')('livechat.offline_note_thankyou_text'));
                    olark.configure("locale.offline_note_error_text", $filter('translate')('livechat.offline_note_error_text'));

                    /* Pre-Chat Survey */
                    olark.configure("locale.introduction_error_text", $filter('translate')('livechat.introduction_error_text'));
                    olark.configure("locale.introduction_messages", $filter('translate')('livechat.introduction_messages'));
                    olark.configure("locale.introduction_submit_button_text", $filter('translate')('livechat.introduction_submit_button_text'));

                    /* Greeter */
                    olark.configure("WelcomeAssist.welcome_messages", [$filter('translate')('livechat.welcome_messages')]);
                };

                // Add script element to DOM
                ctrl.addScript = function () {
                    ;
                    (function (o, l, a, r, k, y) {
                        if (o.olark)return;
                        r = "script";
                        y = l.createElement(r);
                        r = l.getElementsByTagName(r)[0];
                        y.async = 1;
                        y.src = "//" + a;
                        r.parentNode.insertBefore(y, r);
                        y = o.olark = function () {
                            k.s.push(arguments);
                            k.t.push(+new Date)
                        };
                        y.extend = function (i, j) {
                            y("extend", i, j)
                        };
                        y.identify = function (i) {
                            y("identify", k.i = i)
                        };
                        y.configure = function (i, j) {
                            y("configure", i, j);
                            k.c[i] = j
                        };
                        k = y._ = {s: [], t: [+new Date], c: {}, l: a};
                    })($window, document, "static.olark.com/jsclient/loader.js");
                    /* Add configuration calls below this comment */
                    ctrl.deferredTranslations.promise.then(
                        function () {
                            ctrl.setChatText(olark);
                            olark.identify('2320-724-10-7406');
                        }
                    )

                    // When live chat loads, call onLoad()
                    olark('api.chat.onReady', function () {
                        $window.olark = olark;
                        ctrl.onLoad();
                    });
                };
                // Add script
                ctrl.addScript();
                //}
            };
        }
    ]
);