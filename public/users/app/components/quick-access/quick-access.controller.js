angular.module('users').controller('quickAccessController', [
    'translationService', '$location', '$cookies', '$routeParams',
    function (translationService, $location, $cookies, $routeParams) {
        this.$onInit = function () {
            var ctrl = this;
            ctrl.loading = true;

            ctrl.language = $routeParams.lang;

            // Init translations object
            ctrl.translations = {};
            ctrl.translations.loading = true;
            ctrl.viewName = 'quickAccess';
            // Load translations
            var pages = [
                'dashboard'
            ];
            translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

            // Go to different route
            ctrl.goTo = function (route) {
                $location.path('/' + ctrl.language + '/' + route);
            };

            // Request the numbers of boats, experiences, etc.
            // TODO

            ctrl.loading = false;
        };
    }
]);