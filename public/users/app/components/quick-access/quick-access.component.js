angular.module('users').component('quickAccess', {
    templateUrl: '/users/app/components/quick-access/quick-access.view.html',
    controller: 'quickAccessController',
    controllerAs: 'ctrl',
    bindings: {
        user: '<'
    }
});