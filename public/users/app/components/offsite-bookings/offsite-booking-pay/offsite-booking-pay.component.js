angular.module('users').component('offsiteBookingPay', {
    templateUrl: '/users/app/components/offsite-bookings/offsite-booking-pay/offsite-booking-pay.view.html',
    controller: 'offsiteBookingPayController',
    controllerAs: 'ctrl',
    bindings: {
        bookingPromise: '<',
        payNow: '='
    }
});