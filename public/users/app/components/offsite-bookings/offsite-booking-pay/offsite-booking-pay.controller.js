angular.module('users').controller('offsiteBookingPayController', [
    '__env', 'userService', '$routeParams', '$location', '$translate', 'translationService', '$q', '$timeout',
    '$rootScope', '$filter', 'requestService', '$sce',
    function (__env, userService, $routeParams, $location, $translate, translationService, $q, $timeout,
              $rootScope, $filter, requestService, $sce) {
        this.$onInit = function () {
            var ctrl = this;

            // Init translations object
            ctrl.translations = {};
            ctrl.translations.loading = true;
            ctrl.viewName = 'offsiteBookingPay';

            // Load translations
            var pages = [
                'offsite_booking'
            ];
            translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

            ctrl.language = $routeParams.lang;

            // Listen for translation event
            ctrl.submit = function () {
                ctrl.bookingPromise.then(
                    function (response) {
                        ctrl.bookingId = response.data.data.id;
                        var booking = response.data.data.attributes;

                        ctrl.language = booking.language_guest;

                        if (booking.status != 'pending') {
                            // Redirect to 403
                            $location.path('/' + ctrl.language + '/not-authorized');
                            return;
                        }

                        // Backend response in a wrong format
                        if (!response.data.included) {
                            $location.path('/' + ctrl.language + '/error');
                            return;
                        }
                        ctrl.user = response.data.included[0].attributes;

                        /**
                         * Set all form fields
                         */
                        ctrl.action = $sce.trustAsResourceUrl(__env.paypalAddress);
                        ctrl.seller = __env.paypalSeller;

                        ctrl.successPage = __env.baseUrl + ctrl.language + '/offsite-bookings/' + ctrl.bookingId + '/success';
                        ctrl.failurePage = __env.baseUrl + ctrl.language + '/offsite-bookings/' + ctrl.bookingId + '/failure';
                        ctrl.bookingCurrency = booking.currency_captain;
                        ctrl.notifyUrl = __env.backendUrl + 'v1/ipnlistener-offsite/';
                        // Language to locale
                        var localeMappings = {
                            it: 'it_IT',
                            en: 'en_US'
                        };
                        ctrl.locale = localeMappings[$translate.use()];

                        ctrl.price = booking.fee + '.00';

                        ctrl.experienceTitle = booking.experience_title;

                        $timeout(function () {
                            // Submit form
                            if (ctrl.paypal_form.$valid) {
                                // Submit clicking button
                                $timeout(function () {
                                    angular.element(document.querySelector('#submit').click());
                                }, 0);
                            } else {
                                // Redirect to error 500 page
                                $location.path('/' + ctrl.language + '/error');
                            }
                        }, 0);
                    },
                    null
                );
            };

            ctrl.loadTranslationStrings = function () {
                $translate('offsite_booking.return_to_merchant').then(
                    function (translation) {
                        ctrl.returnToMerchant = translation;

                        ctrl.submit();
                    },
                    function () {
                        // Unregister event listener if already registered
                        if (ctrl.translationListener) ctrl.translationListener();

                        ctrl.translationListener = $rootScope.$on(ctrl.viewName + 'viewHasBeenTranslated', function (event) {
                            ctrl.loadTranslationStrings();
                        })
                    }
                );
            };

            if (ctrl.payNow) ctrl.loadTranslationStrings();
        };
    }]
);