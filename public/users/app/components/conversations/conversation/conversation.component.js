angular.module('users').component('conversation', {
    templateUrl: '/users/app/components/conversations/conversation/conversation.view.html',
    controller: 'conversationController',
    controllerAs: 'ctrl',
    bindings: {
        conversationPromise: '<',
        onBackClick: '&',
        onEdit: '&'
    }
});