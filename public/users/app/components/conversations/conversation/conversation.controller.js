angular.module('users').controller('conversationController', [
    'translationService', 'requestService', 'dateService', '$rootScope', '$cookies', '$routeParams',
    function (translationService, requestService, dateService, $rootScope, $cookies, $routeParams) {
        this.$onInit = function () {
            var ctrl = this;
            ctrl.loading = true;

            // Set language for dates formats
            ctrl.language = $routeParams.lang;

            // Error objects for validation
            ctrl.errors = {};
            ctrl.backendErrors = {};

            // Toggle the display of warning message about phone and url
            ctrl.warning = false;

            // Init translations object
            ctrl.translations = {};
            ctrl.translations.loading = true;
            ctrl.viewName = 'conversation';
            // Load translations
            var pages = [
                'conversation', 'inputs'
            ];
            translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

            ctrl.conversationPromise.then(
                function (response) {
                    ctrl.conversation = response.data.data;
                    var included = response.data.included;
                    // Init messages array grouped by day
                    ctrl.conversation.grouped = [];
                    ctrl.conversation.total_messages = 0;

                    // Set user data
                    var ownRole = ctrl.conversation.attributes.role;
                    ctrl.user = {
                        name: ctrl.conversation.attributes['first_name_' + ownRole],
                        photo: ctrl.conversation.attributes['profile_image_' + ownRole],
                        role: ownRole
                    };
                    var roles = ['guest', 'captain'];
                    // Remove own role from array and take the other one
                    roles.splice(roles.indexOf(ownRole), 1);
                    var contactRole = roles[0];
                    // Set contact data
                    ctrl.contact = {
                        name: ctrl.conversation.attributes['first_name_' + contactRole],
                        photo: ctrl.conversation.attributes['profile_image_' + contactRole]
                    };

                    // Fetch and normalize included objects. Offset dates by user timezones and create message object
                    var date;
                    included.forEach(function (includedObject) {
                        switch (includedObject.type) {
                            case 'messages':
                                // Increase total count by 1
                                ctrl.conversation.total_messages++;

                                // Add each message to group object
                                ctrl.addMessage(includedObject);
                        }
                    });

                    ctrl.loading = false;

                },
                function (response) {
                    // TODO MANAGE REJECTION
                }
            );

            // This variable holds the message typed by the user
            ctrl.newMessage = '';

            // Sends message
            ctrl.send = function () {
                if (!ctrl.form.$valid) return;
                if (ctrl.sendingMessage) return;

                // Prevent the multiple submission of the same message
                ctrl.sendingMessage = true;

                // Reset the phone/mail/url warning message
                ctrl.warning = false;

                var relationship = {role: 'conversation', type: 'conversations', id: ctrl.conversation.id};
                var attributes = {message: ctrl.newMessage};

                requestService.postObject('messages', attributes, relationship).then(
                    function (response) {
                        // Message has been sent
                        ctrl.sendingMessage = false;

                        // Reset input field
                        ctrl.newMessage = '';

                        var message = response.data.data;

                        // Set message role to user role (so that that message is displayed on the right side)
                        message.attributes.sender = ctrl.user.role;

                        // Add message to grouped object (for display purposes)
                        ctrl.addMessage(message);
                    },
                    function (response) {
                        // Message has been sent
                        ctrl.sendingMessage = false;

                        // TODO manage rejection
                    }
                )
            };

            // Add single message to grouped object
            ctrl.addMessage = function (message, date = null) {
                if (date == null) date = new Date(message.attributes.created_at);

                message.day = date.withoutTime();

                // If it's still empty, add first message
                if (ctrl.conversation.grouped.length == 0) {
                    ctrl.conversation.grouped.push({
                        date: message.day,
                        messages: [message]
                    });
                }

                // If not empty, check if a group for the same day already exists
                else {
                    var lastGroup = ctrl.conversation.grouped[ctrl.conversation.grouped.length - 1];
                    if (lastGroup.date.getTime() == message.day.getTime()) {
                        // Day already exists, add the message to the group
                        lastGroup.messages.push(message);
                    }
                    else {
                        // First message with this day, create new group
                        ctrl.conversation.grouped.push({
                            date: message.day,
                            messages: [message]
                        });
                    }
                }
            };

            ctrl.getMessageSenderPhoto = function (sender) {
                if (sender == ctrl.user.role) return ctrl.user.photo;

                return ctrl.contact.photo;
            };

            // Toggle warning based on message content
            ctrl.messageValidation = function () {
                var emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
                var urlRegex = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/g;
                var phoneRegex = /[0-9]{6,256}/g;

                // Runs all the validations
                ctrl.warning =
                    emailRegex.test(ctrl.newMessage) ||
                    urlRegex.test(ctrl.newMessage) ||
                    phoneRegex.test(ctrl.newMessage);
            };

            // Update dates when language is changed
            $rootScope.$on('languageChanged', function (event, language) {
                ctrl.language = language;
            })
        };
    }
]);