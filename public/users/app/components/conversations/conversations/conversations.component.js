angular.module('users').component('conversations', {
    templateUrl: '/users/app/components/conversations/conversations/conversations.view.html',
    controller: 'conversationsController',
    controllerAs: 'ctrl',
    bindings: {
        userId: '<',
        onConversationClick: '&',
        showAccordion: '<',
        showBackButton: '<'
    }
});