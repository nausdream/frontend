angular.module('users').controller('conversationsController', [
        'translationService', 'requestService', '$location', 'dateService', 'userService', '$rootScope', '$cookies', '$routeParams',
        function (translationService, requestService, $location, dateService, userService, $rootScope, $cookies, $routeParams) {
            this.$onInit = function () {
                var ctrl = this;
                ctrl.loading = true;

                // Set language for dates formats
                ctrl.language = $routeParams.lang;

                // Init translations object
                ctrl.translations = {};
                ctrl.translations.loading = true;
                ctrl.viewName = 'conversation';

                // Get user from service
                ctrl.user = userService.get().attributes;

                // Define default behaviour for back button url
                ctrl.backButtonUrl = '/' + ctrl.language + '/dashboard';

                // Open accordion by default
                ctrl.open = true;

                // Load translations
                var pages = [
                    'conversation'
                ];
                translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

                // Get correct preview data from conversation object
                // E.g., captain should see user message, and conversation is read
                // if the user making the request has read it
                ctrl.getPreviewData = function (conversation) {
                    if (!conversation) return;

                    var conversationAttributes = conversation.attributes;

                    var role = conversationAttributes.role;

                    // Sender, unread messages
                    var sender = '';
                    var unread = '';
                    switch (role) {
                        case 'captain':
                            sender = conversationAttributes.first_name_guest;
                            unread = conversationAttributes.unread_messages_captain;
                            break;
                        case 'guest':
                            sender = conversationAttributes.first_name_captain;
                            unread = conversationAttributes.unread_messages_guest;
                            break;
                    }

                    var status = unread > 0 ? 'new' : 'read';
                    // Set date with correct timezone. This works by adding to the fetched date the offset of user timezone in minutes.
                    var date = new Date(conversationAttributes.last_message_date_time);
                    dateService.toUserTimezone(date);

                    // Cut messages and append ellipsis
                    var message = conversationAttributes.last_message_text;
                    if (conversationAttributes.last_message_text) {
                        var ellipsis = '...';
                        message = conversationAttributes.last_message_text.substring(0, 50);
                        if (conversationAttributes.last_message_text.length < 50) {
                            var ellipsis = '';
                        }
                        message += ellipsis;
                    } else {
                        message = ''
                    }

                    return {
                        sender: sender,
                        unread: unread,
                        date: date,
                        status: status,
                        message: message,
                        total: conversationAttributes.total_messages
                    };
                };

                // Load conversations
                var conversationsPromise = requestService.getRelatedSingleAsync('users', ctrl.userId, 'conversations', {per_page: 100});
                conversationsPromise.then(
                    function (response) {
                        ctrl.conversations = response.data.data;

                        // Set correct preview data
                        ctrl.conversations.forEach(function (conversation) {
                            conversation.previewData = ctrl.getPreviewData(conversation);
                        });

                        ctrl.loading = false;
                    },
                    function (response) {
                        ctrl.conversations = [];

                        ctrl.loading = false;
                        // TODO MANAGE DIFFERENT RESPONSES
                    }
                );

                // Goes to the previous page
                ctrl.back = function (event, backButtonUrl) {
                    // Prevent parent dom from catching the event
                    event.stopPropagation();

                    // Assign default path to back button
                    if (!backButtonUrl) {
                        backButtonUrl = ctrl.backButtonUrl;
                    }

                    // Change route
                    $location.path(backButtonUrl);
                }

                // Update dates when language is changed
                $rootScope.$on('languageChanged', function (event, language) {
                    ctrl.language = language;
                })
            };
        }
    ]
);