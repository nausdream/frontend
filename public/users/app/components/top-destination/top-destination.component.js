angular.module('users').component('topDestination', {
    templateUrl: '/users/app/components/top-destination/top-destination.view.html',
    controller: 'topDestinationController',
    controllerAs: 'ctrl',
    transclude: true,
    bindings: {
        destination: '<'
    }
});