angular
    .module('users')
    .controller('topDestinationController',
        ['topDestinations', '$location', '$httpParamSerializer', '$cookies', '$routeParams',
            function (topDestinations, $location, $httpParamSerializer, $cookies, $routeParams) {
                this.$onInit = function () {
                    var ctrl = this;

                    ctrl.styles = ['top-destination-element-button'];
                    ctrl.language = $routeParams.lang;

                    var data = topDestinations.filter(function (obj) {
                        return obj.name == ctrl.destination.translation_key;
                    })[0];

                    console.log(data);

                    var params = {
                        key: data.name,
                        lat: data.lat,
                        lng: data.lng,
                        distance: data.distance,
                        zoom: data.zoom,
                        experience_types: 'trips-and-tours,aperitif,diving',
                        babies: 0,
                        kids: 0,
                        min_length: 1,
                        entire_boat: 0,
                        min_price: 1
                    };

                    var qs = $httpParamSerializer(params);
                    ctrl.url = '/' + ctrl.language + '/experiences\?' + qs;

                    ctrl.go = function () {
                        $location.path('/' + ctrl.language + '/experiences').search(params);
                    }
                };
            }]);