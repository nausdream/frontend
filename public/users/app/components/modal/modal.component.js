angular.module('users').component('modal', {
    templateUrl: '/users/app/components/modal/modal.view.html',
    controller: 'modalController',
    controllerAs: 'ctrl',
    transclude: true,
    bindings: {
        modalTitle: '@',
        onClose: '&'
    }
});