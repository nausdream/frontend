angular
    .module('users')
    .service('FacebookService', [
        "$rootScope", "$q",
        function ($rootScope, $q) {
            var ctrl = this;
            ctrl.login = login;
            ctrl.watchLoginStatus = watchLoginStatus;
            ctrl.logout = logout;
            ctrl.getUserInfo = getUserInfo;

            function login() {
                var deferred = $q.defer();

                FB.getLoginStatus(function (response) {
                    if (response.status === 'connected') {
                        deferred.resolve(response);
                    } else {
                        FB.login(function (response) {
                            $rootScope.$apply(function () {
                                if (response && response.status === 'connected') {
                                    deferred.resolve(response);
                                } else {
                                    deferred.reject(response);
                                }
                            })

                        }, {scope: 'email'})
                    }
                });

                return deferred.promise;
            }

            function watchLoginStatus() {
                FB.Event.subscribe('auth.authResponseChange', function (res) {
                    ctrl.getUserInfo();
                })
            }

            function getUserInfo() {
                FB.api('/me', function (res) {
                    $rootScope.$apply(function () {
                        console.log(res);
                    })
                })
            }


            function logout(callback) {
                FB.logout(function (response) {
                    $rootScope.$apply(function () {
                        console.log(response);
                    })
                })
            }
        }]);