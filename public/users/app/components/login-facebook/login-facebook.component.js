angular
	.module('users')
	.component('loginFacebook', {
		templateUrl: '/users/app/components/login-facebook/login-facebook.view.html',
		controller: 'loginFacebookController',
		controllerAs: 'ctrl',
		bindings: {
			onComplete: '&'
		}
	})