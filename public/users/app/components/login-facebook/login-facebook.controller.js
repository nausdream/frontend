angular
    .module('users')
    .controller('loginFacebookController', [
        '$rootScope', 'FacebookService', 'translationService',
        function ($rootScope, FacebookService, translationService) {
            this.$onInit = function () {
                var ctrl = this;
                ctrl.translations = {};
                ctrl.viewName = 'header';

                var pages = ['header'];
                translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

                ctrl.login = function () {
                    FacebookService.login().then(
                        function (response) {
                            ctrl.onComplete({response: response});
                        },
                        function (response) {
                            console.log(response);
                        })
                };
            };
        }]);