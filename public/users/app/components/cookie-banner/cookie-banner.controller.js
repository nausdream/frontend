angular
    .module('users')
    .controller('cookieBannerController', [
        'translationService', '$cookies',
        function (translationService, $cookies) {
            this.$onInit = function () {
                var ctrl = this;

                ctrl.language = $cookies.get('language');

                // Init translations object
                ctrl.translations = {};
                ctrl.translations.loading = true;
                ctrl.viewName = 'cookieBanner';
                // Load translations
                var pages = [
                    'cookie_banner'
                ];
                translationService.translate(pages, ctrl.viewName, ctrl.language).then(function(response){ctrl.translations.loading = false},null);
            };
        }]
    );