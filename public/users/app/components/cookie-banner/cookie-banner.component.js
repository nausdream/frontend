angular.module('users').component('cookieBanner', {
    templateUrl: '/users/app/components/cookie-banner/cookie-banner.view.html',
    controller: 'cookieBannerController',
    controllerAs: 'ctrl',
    bindings: {
        isVisible: '=',
        onClose: '&'
    }
});