angular.module('users').component('nausDatepickerPopup', {
    templateUrl: '/users/app/components/naus-datepicker-popup/naus-datepicker-popup.view.html',
    controller: 'nausDatepickerController',
    controllerAs: 'ctrl',
    bindings: {
        date: '=',
        options: '<',
        availabilities: '<',
        daysOfWeek: '<',
        isOpen: '=',
        language: '=',
        placeholder: '@'
    }
});