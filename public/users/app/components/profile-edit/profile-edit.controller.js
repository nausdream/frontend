angular.module('users').controller('profileEditController', [
        'translationService', 'requestService', '$location', 'dateService', 'userService', '$rootScope', 'countryService',
        'accountKitService',
        function (translationService, requestService, $location, dateService, userService, $rootScope, countryService,
                  accountKitService) {
            this.$onInit = function () {
                var ctrl = this;
                ctrl.loading = true;

                // Init translations object
                ctrl.translations = {};
                ctrl.translations.loading = true;
                ctrl.viewName = 'profileEdit';

                // Get user from service
                ctrl.user = userService.get();

                ctrl.emailVerified = ctrl.user.attributes.is_mail_activated;
                ctrl.phoneVerified = ctrl.user.attributes.is_phone_activated;

                $rootScope.$on('phoneVerified', function () {
                    ctrl.phoneVerified = true;
                });
                // Account kit phone form submission handler
                ctrl.verifyPhone = function () {
                    if (ctrl.phoneVerified) return;

                    var phone = ctrl.user.attributes.phone;

                    accountKitService.openAK(ctrl.countries, phone, ctrl.user.id);
                };

                // Open accordion
                ctrl.open = true;

                // Load translations
                var pages = [
                    'user'
                ];
                translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

                // Load countries
                countryService.getCountries().then(
                    function (response) {
                        ctrl.countries = response;
                        ctrl.loading = false;
                    }
                )

            };
        }
    ]
);