angular.module('users').component('profileEdit', {
    templateUrl: '/users/app/components/profile-edit/profile-edit.view.html',
    controller: 'profileEditController',
    controllerAs: 'ctrl',
    bindings: {
        user: '<',
        showAccordion: '<',
        showBackButton: '<'
    }
});