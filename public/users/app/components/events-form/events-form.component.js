angular.module('users').component('eventsForm', {
    templateUrl: '/users/app/components/events-form/events-form.view.html',
    controller: 'eventsFormController',
    controllerAs: 'ctrl',
    bindings: {
        onClose: '&'
    }
});