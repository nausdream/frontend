angular.module('users').component('alertWarning', {
    templateUrl: '/users/app/components/alert-warning/alert-warning.view.html',
    controller: 'alertWarningController',
    controllerAs: 'ctrl',
    bindings: {
        alertMessage: '@'
    }
});