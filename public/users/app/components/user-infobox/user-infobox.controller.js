angular.module('users').controller('userInfoboxController', [
    'translationService', '$location', '$routeParams',
    function (translationService, $location, $routeParams) {
        this.$onInit = function () {
            var ctrl = this;
            ctrl.loading = true;

            ctrl.language = $routeParams.lang;

            // Init translations object
            ctrl.translations = {};
            ctrl.translations.loading = true;
            ctrl.viewName = 'userInfoBox';
            // Load translations
            var pages = [
                'user'
            ];
            translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

            // Prepare data for view
            ctrl.role = ctrl.user.attributes.is_captain ? 'captain' : 'guest';
            ctrl.photo = ctrl.user.attributes.profile_photo;
            ctrl.userName = (ctrl.user.attributes.first_name || '') + ' ' + (ctrl.user.attributes.last_name || '');
            if (!ctrl.user.attributes.first_name && !ctrl.user.attributes.last_name) ctrl.userName = 'user.no_name';
            ctrl.cta = 'edit_profile';

            ctrl.loading = false;

            ctrl.goToProfileEdit = function () {
                $location.path('/' + ctrl.language + '/profile/edit')
            }
        };
    }
]);