angular.module('users').component('userInfobox', {
    templateUrl: '/users/app/components/user-infobox/user-infobox.view.html',
    controller: 'userInfoboxController',
    controllerAs: 'ctrl',
    bindings: {
        user: '<'
    }
});