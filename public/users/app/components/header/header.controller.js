angular.module('users').controller('headerController',
    ['requestService', '$rootScope', '$scope', '$location', '$cookies', '__env', 'checkLoginService',
        'userService', 'translationService', 'currencyService', 'languages', 'currencies', 'loginService', '$httpParamSerializer',
        'experienceTypesGroups', 'countryService', 'accountKitService', '$route', 'adwordsService', 'destinations', 'logoService',
        function (requestService, $rootScope, $scope, $location, $cookies, __env, checkLoginService,
                  userService, translationService, currencyService, languages, currencies, loginService, $httpParamSerializer,
                  experienceTypesGroups, countryService, accountKitService, $route, adwordsService, destinations, logoService) {
            this.$onInit = function () {
                var ctrl = this;
                ctrl.page = $location.path().slice(1);
                ctrl.languages = languages;
                ctrl.translations = {};
                ctrl.translations.loading = true;
                ctrl.currencies = currencies;
                ctrl.cloudinaryUrl = __env.cloudinaryUrl;
                ctrl.viewName = 'header';
                ctrl.currentLogo = logoService.get();

                // We can't use routeParams here because the header is outside the routing logic and routeParams
                // may not be defined yet
                ctrl.language = $cookies.get('language');
                ctrl.currency = $cookies.get('currency');

                ctrl.countriesLoaded = false;

                ctrl.supportPhone = __env.supportPhone;
                ctrl.supportPhoneNoSpaces = ctrl.supportPhone.replace(/ /g, '');

                var pages = ['header', 'languages', 'currencies', 'errors'];
                translationService.translate(pages, ctrl.viewName, ctrl.language).then(function(response){ctrl.translations.loading = false},null);

                function checkNotifications(id) {
                    return userService
                        .checkNotifications(id)
                        .then(function (notifications) {
                            ctrl.notifications = notifications.meta;
                        })
                        .catch(function (err) {
                            console.log(err);
                        })
                }

                // Load countries list for AK verification
                var loadCountries = function () {
                    if (!ctrl.user.attributes.is_phone_verified) {
                        countryService.getCountries().then(
                            function (response) {
                                ctrl.countries = response;
                                ctrl.countriesLoaded = true;
                            }
                        );
                    }
                };

                // Set user if logged in
                ctrl.user = userService.get();
                if (ctrl.user) {
                    checkNotifications(ctrl.user.id);
                    loadCountries();
                }

                // Update dates when language is changed
                $scope.$on('languageChanged', function (event, language) {
                    ctrl.language = language;
                });

                $scope.$on('currencyChanged', function (event, currency) {
                    ctrl.currency = currency;
                });

                ctrl.openMenu = function () {
                    $rootScope.$emit('sidebarOpened');
                };
                ctrl.closeMenu = function () {
                    $rootScope.$emit('sidebarCollapsed');
                };


                $rootScope.$on('phoneVerified', function () {
                    ctrl.notifications.is_phone_activated = true;
                    ctrl.phoneVerifiedIsOpen = true;
                });
                $rootScope.$on('errorVerifyingPhone', function (event, response) {
                    console.log(response);
                    ctrl.verifyPhoneError = response.data.errors[0].code;
                    ctrl.errorVerifyPhoneIsOpen = true;
                });

                // Account kit phone form submission handler
                ctrl.verifyPhone = function () {
                    var phone = ctrl.user.attributes.phone;

                    accountKitService.openAK(ctrl.countries, phone, ctrl.user.id);
                };

                // Get header translations
                ctrl.setMenu = function () {
                    // Calculate menu length (for dropdown correct positioning)
                    ctrl.languageObject = {
                        url: '#', sub: {},
                        action: function (language) {
                            ctrl.changeLanguage(language);
                        },
                        login: true,
                        register: true
                    };
                    ctrl.currencyObject = {
                        url: '#', sub: {},
                        action: function (currency) {
                            ctrl.changeCurrency(currency);
                        },
                        login: true,
                        register: true
                    };
                };
                ctrl.setMenu();

                // Return whether or not the menu item should be displayed
                ctrl.isVisible = function (menuItem) {
                    // If not specified, the item should be visible
                    if (!menuItem.hasOwnProperty(ctrl.page)) {
                        return true;
                    }

                    return menuItem[ctrl.page];
                };

                // Change language and currency of the logged user (if user not logged in, just set cookies)
                ctrl.changeLanguage = function (language) {
                    translationService.changeLanguage(language);
                };
                ctrl.changeCurrency = function (currency) {
                    currencyService.changeCurrency(currency);
                };

                // TODO: extract to service
                ctrl.isObjectEmpty = function (obj) {
                    for (var prop in obj) {
                        if (obj.hasOwnProperty(prop))
                            return false;
                    }

                    return JSON.stringify(obj) === JSON.stringify({});
                };

                ctrl.isMenuEmpty = function () {
                    // If ctrl.menu is empty
                    var isEmpty = ctrl.isObjectEmpty(ctrl.menu);
                    if (isEmpty) return true;

                    // Or if any ctrl menu key (eg. experiences, insert boat, ecc) has value false on 'page' key (login/register/ecc)
                    var hasTrueValue = true;
                    for (var property in ctrl.menu) {
                        if (ctrl.menu.hasOwnProperty(property)) {
                            var menuItem = ctrl.menu[property];
                            if (ctrl.isVisible(menuItem)) hasTrueValue = false;
                            //console.log(property + '.' + ctrl.page, ctrl.menu[property][ctrl.page]);
                        }
                    }

                    return hasTrueValue;
                };

                // Go to register page
                ctrl.goToRegisterPage = function () {
                    $location.path('/' + ctrl.language + '/register');
                    ctrl.closeMenu();
                };

                ctrl.loginVisitors = function () {
                    ctrl.guestLoginIsOpen = true;
                }

                ctrl.eventsForm = function () {
                    ctrl.eventsFormIsOpen = true;
                }

                ctrl.goTo = function (path, search, hash) {
                    ctrl.closeMenu();

                    if (!search) search = {};
                    $location.path(path).search(search).hash(hash);
                };

                $rootScope.$on('userLoggedIn', function (event) {
                    ctrl.user = userService.get();
                    loadCountries();
                    checkNotifications(ctrl.user.id);
                    ctrl.closeMenu();
                    ctrl.guestLoginIsOpen = false;
                });

                $rootScope.$on('$routeChangeSuccess', function(event, page){
                    ctrl.currentLogo = logoService.get();
                });
                $rootScope.$on('pageChanged', function(event, page){
                    ctrl.currentLogo = logoService.get();
                });

                ctrl.logout = function () {
                    loginService.logout().then(
                        function () {
                            ctrl.user = null;
                        }, null
                    );
                };

                // User clicks on 'contact us': make phone number appear
                ctrl.togglePhoneNumber = function () {
                    if (!ctrl.isPhoneNumberVisible) {
                        // Send adwords conversion
                        ctrl.convert('call-us');

                        ctrl.isPhoneNumberVisible = true;
                    }
                };

                // Fetch destinations
                ctrl.destinations = destinations;

                ctrl.getParamsFromDestinationName = function (destination) {
                    var data = ctrl.destinations.filter(function (object) {
                        return object.attributes.name == destination;
                    })[0];

                    var params = {
                        locality: destination,
                        lat: data.attributes.lat,
                        lng: data.attributes.lng,
                        distance: data.attributes.distance,
                        zoom: data.attributes.zoom,
                        babies: 0,
                        experience_types: 'trips-and-tours,aperitif,diving',
                        kids: 0,
                        min_length: 1,
                        entire_boat: 0,
                        min_price: 1
                    };

                    if (data.attributes.key) params.key = data.attributes.key;

                    return params;
                };
                ctrl.getLinkFromDestinationName = function (destination) {
                    var qs = $httpParamSerializer(ctrl.getParamsFromDestinationName(destination));

                    return '/' + ctrl.language + '/experiences\?' + qs;
                };
                ctrl.goToDestination = function (destination) {
                    var params = ctrl.getParamsFromDestinationName(destination);

                    if ($location.path() == ('/' + ctrl.language + '/experiences')) $route.reload();

                    $location.path('/' + ctrl.language + '/experiences').search(params);

                    ctrl.closeMenu();
                };

                ctrl.experienceTypes = experienceTypesGroups.filter(function (type) {
                    return type.types;
                });
                ctrl.getParamsFromExperienceType = function (experienceType) {
                    var data = ctrl.experienceTypes.filter(function (object) {
                        return object.name == experienceType;
                    })[0];

                    var params = {
                        key: data.name,
                        experience_types: data.types,
                        lat: 40.976140,
                        lng: 12.270724,
                        distance: 500,
                        zoom: 5,
                        babies: 0,
                        kids: 0,
                        min_length: 1,
                        entire_boat: 0,
                        min_price: 1
                    };

                    return params;
                };
                ctrl.getLinkFromExperienceType = function (experienceType) {
                    var qs = $httpParamSerializer(ctrl.getParamsFromExperienceType(experienceType));

                    return '/' + ctrl.language + '/experiences\?' + qs;
                };
                ctrl.goToExperienceType = function (experienceType) {
                    var params = ctrl.getParamsFromExperienceType(experienceType);

                    if ($location.path() == ( '/' + ctrl.language + '/experiences')) $route.reload();
                    $location.path('/' + ctrl.language + '/experiences').search(params);
                    ctrl.closeMenu();
                };

                // Adwords
                ctrl.convert = adwordsService.sendConversion;
            };
        }
    ]
);