angular
.module('users')
.component('header', {
    templateUrl: '/users/app/components/header/header.view.html',
    controller: 'headerController',
    controllerAs: 'ctrl',
    bindings: {
    }
});