angular.module('users').controller('boatStepController',
    ['mathService', 'requestService', 'fixedListService', 'userService', 'validationService', 'mapsService', '$timeout', '$location', '$q', '$routeParams',
        function (mathService, requestService, fixedListService, userService, validationService, mapsService, $timeout, $location, $q, $routeParams) {
            this.$onInit = function () {
                // Make context accessible inside inner scopes
                var ctrl = this;
                ctrl.loading = true;
                ctrl.formName = 'form_boat';
                // Set disabled (read only form)
                ctrl.disabled = false;
                ctrl.errors = {};
                ctrl.backendErrors = [];
                // To make down-arrow behave correctly
                ctrl.step = 2;
                ctrl.dropdowns = {};
                ctrl.popovers = {};
                ctrl.boatYears = mathService.range(1800, new Date().getFullYear(), 1, true);
                ctrl.userId = userService.get() ? userService.get().id : null;
                ctrl.user = angular.copy(userService.get().attributes);
                ctrl.userRelationships = angular.copy(userService.getRelationships());
                ctrl.tempBoat = {};
                ctrl.tempBoat.experienceTypes = [];

                ctrl.language = $routeParams.lang;

                // Promises
                var boatPromise = requestService.getRelatedSingleAsync('captains', ctrl.userId, 'boats', {editing: true});
                var boatTypesPromises = fixedListService.getFixedList('boat-types');
                var boatMaterialsPromise = fixedListService.getFixedList('boat-materials');
                var motorTypesPromise = fixedListService.getFixedList('motor-types');
                var experienceTypesPromise = fixedListService.getFixedList('experience-types');
                ctrl.boatSetDeferred = $q.defer();

                // Get user boat if present
                boatPromise.then(
                    function (response) {
                        // If there's at least one boat (ie the boat was already created)
                        if (response.data.data.length > 0) {
                            ctrl.boatId = response.data.data[0].id;
                            ctrl.boat = response.data.data[0].attributes;

                            // Map can be loaded
                            ctrl.boatSetDeferred.resolve();

                            // Check if boat is already finished and user is already set
                            if (ctrl.boat.is_finished) {
                                $location.hash(null);
                                $location.path('/' + ctrl.language + '/dashboard');
                            }
                            if (!userService.isUserSet()) {
                                $location.search('step', 1);
                            }

                            // Get boat experience types from boat
                            requestService.getSingleAsync('boats', ctrl.boatId, {editing: true}).then(
                                function (response) {
                                    ctrl.boatRelationships = response.data.data.relationships;

                                    experienceTypesPromise.then(
                                        function (response) {
                                            // Load experience types into array
                                            if (ctrl.boatRelationships) {
                                                var tempExperienceTypes = ctrl.boatRelationships.experience_types.data;
                                                // Assign a subset of experience types to tempBoat experienceTypes array.
                                                // The selected one are the ones where the id matches one id of the relationships experience types
                                                ctrl.tempBoat.experienceTypes = response.data.data.filter(function (type) {
                                                    return tempExperienceTypes.filter(function (tempType) {
                                                            return tempType.id == type.id;
                                                        }).length > 0;
                                                });
                                            }
                                        },
                                        null
                                    );
                                },
                                null
                            );

                            ctrl.loading = false;
                        }
                        else {
                            ctrl.createBoat();
                        }
                    },
                    function (response) {
                        // REDIRECT TO CORRECT VIEW
                    }
                );

                // GET FIXED LISTS
                // Boat types
                boatTypesPromises.then(
                    function (response) {
                        ctrl.boatTypes = response.data.data;
                    },
                    function (response) {
                        ctrl.boatTypes = null;
                    }
                );
                // Materials
                boatMaterialsPromise.then(
                    function (response) {
                        ctrl.boatMaterials = response.data.data;
                    },
                    function (response) {
                        ctrl.boatMaterials = null;
                    }
                );
                // Motor types
                motorTypesPromise.then(
                    function (response) {
                        ctrl.motorTypes = response.data.data;
                    },
                    function (response) {
                        ctrl.motorTypes = null;
                    }
                );

                // Experience types
                experienceTypesPromise.then(
                    function (response) {
                        ctrl.experienceTypes = response.data.data;
                    },
                    function (response) {
                        ctrl.experienceTypes = null;
                    }
                );

                // VALIDATION
                // Manual validation
                ctrl.validate = function () {
                    var error = false;

                    // SPOKEN LANGUAGES
                    if (ctrl.tempBoat.experienceTypes.length == 0) {
                        ctrl.errors.experience_types = {code: 'required'};
                        error = true;
                        console.log('error experienceTypes');
                    }

                    return error;
                };
                ctrl.validateField = function (field) {
                    validationService.validateField(ctrl[ctrl.formName][field], ctrl.errors);
                };
                // Clear field errors from ctrl.errors
                ctrl.resetField = function (field) {
                    validationService.resetField(field, ctrl.errors, ctrl[ctrl.formName]);
                };

                // Custom inputs
                ctrl.toggleExperienceType = function (experienceType, forceDelete = false) {
                    if (ctrl.selectedExperienceType) {
                        // Clear errors on field
                        delete ctrl.errors.experience_types;

                        // Determine if experienceType is already selected
                        var experienceTypeAlreadySelected = ctrl.tempBoat.experienceTypes.filter(function (type) {
                                return type.id == experienceType.id;
                            }).length > 0;

                        var patch = true;

                        if (experienceTypeAlreadySelected) {
                            if (forceDelete) {
                                // Remove
                                ctrl.tempBoat.experienceTypes = ctrl.tempBoat.experienceTypes.filter(function (type) {
                                    return type.id != experienceType.id
                                });
                            } else {
                                // Nothing to do, don't patch
                                patch = false;
                            }
                        } else {
                            // Add
                            ctrl.tempBoat.experienceTypes.push(experienceType);
                        }
                        if (patch) ctrl.patchExperienceTypes();
                    }
                };

                // Filter restoration year
                ctrl.restorationYearFilter = function (value, index) {
                    if (!ctrl.boat) return;

                    if (!ctrl.boat.construction_year) return true;

                    return value >= ctrl.boat.construction_year;
                };
                // Auto edit restoration year when construction year is changed
                ctrl.validateRestorationYear = function () {
                    if (!ctrl.boat.restoration_year) return;

                    ctrl.boat.restoration_year = (!!ctrl.boat.restoration_year && ctrl.boat.restoration_year) < ctrl.boat.construction_year ? ctrl.boat.construction_year : ctrl.boat.restoration_year;
                };

                // Patch boat on every field change (auto-save)
                ctrl.patchInput = function (field, resetErrors) {
                    ctrl.loading = true;

                    // Radio button need its errors to be reset,
                    // because it's sending patch on change and not on blur like inputs
                    if (resetErrors) {
                        ctrl.resetField(field);
                    } else {
                        ctrl.validateField(field);
                    }

                    // Do not send patch if field is invalid
                    if (ctrl.errors[field]) {
                        ctrl.loading = false;
                        return;
                    }

                    ctrl.patchBoat(ctrl[ctrl.formName], false);
                };
                // BOAT Api calls
                ctrl.createBoat = function () {
                    var relationships = {
                        role: 'user',
                        type: 'users',
                        id: ctrl.userId
                    };

                    requestService.postObject('boats', null, relationships).then(
                        function (response) {
                            ctrl.boatId = response.data.data.id;
                            // Set default values for attributes
                            ctrl.boat =
                            {
                                "production_site": null,
                                "model": null,
                                "name": null,
                                "construction_year": null,
                                "restoration_year": null,
                                "type": null,
                                "material": null,
                                "length": null,
                                "has_insurance": false,
                                "motors_quantity": null,
                                "motor_type": null,
                                "docking_place": null,
                                "lat": null,
                                "lng": null,
                                "seats": null,
                                "berths": null,
                                "cabins": null,
                                "toilets": null,
                                "description": null,
                                "rules": null,
                                "indications": null,
                                "events": false,
                                "external_photo": null,
                                "internal_photo": null,
                                "sea_photo": null,
                                "is_luxury": false,
                            };

                            // Map can be loaded
                            ctrl.boatSetDeferred.resolve();

                            ctrl.loading = false;
                        },
                        function (response) {
                            ctrl.loading = false;
                            console.log('Could not create boat.');
                        }
                    );
                };
                ctrl.patchBoat = function (form, requireAll = false) {
                    ctrl.loading = true;
                    // Build user model
                    var boat = {};
                    boat.id = ctrl.boatId;
                    boat.attributes = angular.copy(ctrl.boat);
                    // If you don't delete status, the backend think you're trying to change boat status
                    delete boat.attributes.status;

                    // If every field is required, block requests with empty fields
                    var canGoOn = true;
                    if (requireAll) {
                        canGoOn = form.$valid;
                    } else {
                        // For each form input, take error type (key), and check if type is other than 'required' or 'password_match'.
                        // If the key is, e.g., maxlength, then check what is the field with error. If it's password, go on.
                        // Else, the form can't be submitted
                        angular.forEach(form.$error, function (error, key) {
                            if (-1 == ['required', 'password_match'].indexOf(key)) {
                                error.forEach(function (err) {
                                    // If error field (name) is not password or password_match, the form can't be submitted
                                    if (-1 == ['password', 'password_match'].indexOf(err.$name)) {
                                        canGoOn = false;
                                    }
                                });
                            }
                        });
                    }
                    if (!canGoOn) return;

                    // Send request
                    requestService.putBoat(boat.id, boat.attributes).then(
                        function (response) {
                            ctrl.boat = response.data.data.attributes;
                            ctrl.loading = false;
                        },
                        function (response) {
                            // Failure
                            validationService.setBackendErrors(form, ctrl.errors, response.data.errors);
                            ctrl.loading = false;
                        }
                    );
                };
                ctrl.patchExperienceTypes = function () {
                    ctrl.loading = true;
                    requestService.patchExperienceTypes(ctrl.boatId, ctrl.tempBoat.experienceTypes).then(
                        function (response) {
                            ctrl.loading = false;
                        }
                    );
                };

                // Filter out already selected experience types
                ctrl.notSelectedExperienceTypes = function (experienceType) {
                    return ctrl.tempBoat.experienceTypes.filter(function (type) {
                            return experienceType.id == type.id;
                        }).length == 0;
                };
                ctrl.selectedExperienceTypes = function (experienceType) {
                    return ctrl.tempBoat.experienceTypes.filter(function (type) {
                            return experienceType.id == type.id;
                        }).length == 0;
                };

                // Trigger map load
                ctrl.triggerMapLoad = function () {
                    $q.all([ctrl.boatSetDeferred.promise, mapsService.mapsInitialized]).then(
                        function () {
                            $timeout(ctrl.loadMap, 0);
                        }
                    );
                };
                // Load map
                ctrl.loadMap = function () {
                    // DEPARTURE MAP
                    // TODO get lat and lng from ip
                    ctrl.boat.lat = ctrl.boat.lat || 39.211406;
                    ctrl.boat.lng = ctrl.boat.lng || 9.113607;

                    var mapCoordinates = {lat: ctrl.boat.lat, lng: ctrl.boat.lng};
                    var map = new google.maps.Map(document.getElementById('map'), {
                        center: mapCoordinates,
                        zoomControl: true,
                        streetViewControl: false,
                        mapTypeControl: false,
                        fullscreenControl: false,
                        disableDoubleClickZoom: true,
                        zoom: 8
                    });
                    var marker = new google.maps.Marker({
                        position: mapCoordinates,
                        draggable: true,
                        map: map
                    });

                    $timeout(function () {
                        map.panTo(mapCoordinates);
                    }, 100);

                    // Make map respond to click event: move the marker
                    map.addListener('dblclick', function (event) {
                        marker.setPosition(event.latLng);
                        $timeout(function () {
                            map.panTo(event.latLng);
                        }, 100);

                        // Set new boat coordinates
                        mapCoordinates.lat = event.latLng.lat();
                        mapCoordinates.lng = event.latLng.lng();
                        ctrl.boat.lat = event.latLng.lat();
                        ctrl.boat.lng = event.latLng.lng();

                        ctrl.patchBoat(ctrl[ctrl.formName]);
                    });
                    // Save new coordinates on marker drag
                    marker.addListener('dragend', function (event) {
                        // Set new boat coordinates
                        mapCoordinates.lat = event.latLng.lat();
                        mapCoordinates.lng = event.latLng.lng();
                        ctrl.boat.lat = event.latLng.lat();
                        ctrl.boat.lng = event.latLng.lng();

                        ctrl.patchBoat(ctrl[ctrl.formName]);
                    });

                    $timeout(function () {
                        google.maps.event.trigger(map, 'resize');
                        map.setCenter(mapCoordinates);
                    }, 0);
                };

                // If form is valid, set step value by setting location.search (triggers reload)
                ctrl.goToNextStep = function () {
                    // Manual validate certain inputs
                    var error = ctrl.validate();

                    if (!error && ctrl[ctrl.formName].$valid && !ctrl.loading) {
                        ctrl.errors = {};
                        // Set step
                        $location.hash(null);
                        $location.search('step', ctrl.step + 1);
                    } else {
                        validationService.setFrontendErrors(ctrl[ctrl.formName], ctrl.errors)
                    }
                };
            }
        }]
);