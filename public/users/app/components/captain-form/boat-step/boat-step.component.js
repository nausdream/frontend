angular.module('users').component('boatStep', {
    templateUrl: '/users/app/components/captain-form/boat-step/boat-step.view.html',
    controller: 'boatStepController',
    controllerAs: 'ctrl'
});