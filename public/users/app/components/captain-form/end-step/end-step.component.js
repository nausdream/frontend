angular.module('users').component('endStep', {
    templateUrl: '/users/app/components/captain-form/end-step/end-step.view.html',
    controller: 'endStepController',
    controllerAs: 'ctrl'
});