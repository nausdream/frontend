angular.module('users').controller('endStepController',
    ['requestService', 'userService', 'validationService', '$window', '$location', '$routeParams',
        function (requestService, userService, validationService, $window, $location, $routeParams) {
            this.$onInit = function () {
                // Make context accessible inside inner scopes
                var ctrl = this;
                ctrl.loading = false;
                ctrl.formName = 'form_paypal';
                // Set disabled (read only form)
                ctrl.disabled = false;
                ctrl.errors = {};
                ctrl.backendErrors = [];
                ctrl.userId = userService.get() ? userService.get().id : null;
                ctrl.user = angular.copy(userService.get().attributes);

                ctrl.language = $routeParams.lang;

                // Promises
                var boatPromise = requestService.getRelatedSingleAsync('captains', ctrl.userId, 'boats', {editing: true});

                // If boat is not pending OR paypal account is already set, redirect to dashboard
                if (ctrl.user.paypal_account != null) {
                    console.log('paypal account is set');
                    $location.path('/' + ctrl.language + '/dashboard');
                }
                boatPromise.then(
                    function (response) {
                        // If there's at least one boat (ie the boat was already created)
                        if (response.data.data.length > 0) {
                            var boat = response.data.data[0].attributes;

                            // Check if boat is already finished and user is already set
                            if (!boat.is_finished) {
                                $location.search('step', 2);
                                return;
                            } else if (boat.status != 'pending') {
                                console.log('boat status is not pending');
                                $location.path('/' + ctrl.language + '/dashboard');
                                return;
                            } else if (!userService.isUserSet()) {
                                $location.search('step', 1);
                                return;
                            }

                        } else {
                            $location.search('step', 1);
                            return;
                        }
                    },
                    function (response) {
                        console.log('not authorized');
                        $location.path('/' + ctrl.language + '/dashboard');
                        return;
                    }
                );

                // VALIDATION
                // Manual validation
                ctrl.validate = function () {
                    var error = false;

                    return error;
                };
                ctrl.validateField = function (field) {
                    validationService.validateField(ctrl[ctrl.formName][field], ctrl.errors);
                };
                // Clear field errors from ctrl.errors
                ctrl.resetField = function (field) {
                    validationService.resetField(field, ctrl.errors, ctrl[ctrl.formName]);
                };

                // Patch boat on every field change (auto-save)
                ctrl.patchInput = function (field, resetErrors) {
                    ctrl.loading = true;

                    // Radio button need its errors to be reset,
                    // because it's sending patch on change and not on blur like inputs
                    if (resetErrors) {
                        ctrl.resetField(field);
                    } else {
                        ctrl.validateField(field);
                    }

                    // Do not send patch if field is invalid
                    if (ctrl.errors[field]) {
                        ctrl.loading = false;
                        return;
                    }

                    ctrl.patchUser(ctrl[ctrl.formName], false);
                };
                // USER Api calls
                ctrl.patchUser = function (form, requireAll) {
                    ctrl.loading = true;
                    // Build user model
                    var user = {};
                    user.id = ctrl.userId;
                    user.attributes = angular.copy(ctrl.user);

                    // If every field is required, block requests with empty fields
                    var canGoOn = true;
                    if (requireAll) {
                        canGoOn = form.$valid;
                    } else {
                        // For each form input, take error type (key), and check if type is other than 'required' or 'password_match'.
                        // If the key is, e.g., maxlength, then check what is the field with error. If it's password, go on.
                        // Else, the form can't be submitted
                        angular.forEach(form.$error, function (error, key) {
                            if (-1 == ['required', 'password_match'].indexOf(key)) {
                                error.forEach(function (err) {
                                    // If error field (name) is not password or password_match, the form can't be submitted
                                    if (-1 == ['password', 'password_match'].indexOf(err.$name)) {
                                        canGoOn = false;
                                    }
                                });
                            }
                        });
                    }
                    if (!canGoOn) return;

                    // Send request
                    requestService.patchUser(user).then(
                        function (response) {
                            // Patched! Now get relationships too and store everything in userService
                            userService.save(response.data.data);
                            ctrl.user = angular.copy(userService.get().attributes);
                            ctrl.loading = false;
                            ctrl.emailSent = true;
                        },
                        function (response) {
                            validationService.setBackendErrors(form, ctrl.errors, response.data.errors);
                            ctrl.loading = false;
                        }
                    );
                };

                // Redirect the user to Paypal
                ctrl.goToPaypal = function () {
                    $window.open('https://www.paypal.com');
                }
            }
        }]
);