angular.module('users').component('captainStep', {
    templateUrl: '/users/app/components/captain-form/captain-step/captain-step.view.html',
    controller: 'captainStepController',
    controllerAs: 'ctrl'
});