angular.module('users').controller('captainStepController',
    ['requestService', '$rootScope', 'mathService', 'countryService', 'userService', 'validationService', '$location', '$routeParams',
        function (requestService, $rootScope, mathService, countryService, userService, validationService, $location, $routeParams) {
            this.$onInit = function () {
                // Make context accessible inside inner scopes
                var ctrl = this;
                ctrl.loading = true;
                ctrl.step = 1;
                // Set disabled (read only form)
                ctrl.disabled = false;
                ctrl.errors = {};
                ctrl.backendErrors = {};
                // To make down-arrow behave correctly
                ctrl.dropdowns = {};
                ctrl.popovers = {};
                ctrl.user = {};
                // We need to store field that needs to be manipulated before sending
                ctrl.tempUser = {};
                // Lists
                ctrl.languages = {};
                ctrl.tempUser.languages = [];
                ctrl.captainTypes = [];
                // Load user data into ctrl.user (deep copy)
                ctrl.userId = userService.get() ? userService.get().id : null;

                ctrl.language = $routeParams.lang;

                const PhoneNumber = require('awesome-phonenumber');

                // Get phone string and countries list, set user phone and country code
                ctrl.setUserPhone = function (phone, countries) {
                    if (phone) {
                        var ph = new PhoneNumber(phone);
                        var regionCode = ph.getRegionCode();
                        var significantNumber = ph.getNumber('significant');
                    }

                    if (!regionCode) {
                        regionCode = 'IT';
                    }
                    if (!significantNumber) {
                        significantNumber = '0000000';
                    }

                    ctrl.countries = countries;

                    var selectedCountry = ctrl.countries.filter(function (country) {
                        return country.code.toLowerCase() == regionCode.toLowerCase();
                    });
                    if (selectedCountry) {
                        ctrl.tempUser.selectedCountry = selectedCountry[0];
                    }
                    ctrl.tempUser.phone = significantNumber;
                };

                // STEP RELATED FUNCTIONS
                // Route to proper view (initial, private, company or association)
                ctrl.routeToView = function () {
                    var view = $location.hash() || 'select-type';
                    var setDefault = function () {
                        ctrl.view = '/users/app/components/captain-form/captain-step/views/step-captain.view.html';
                        ctrl.formName = 'form_type';
                    };
                    switch (view) {
                        case 'private':
                            if (ctrl.user.captain_type != 'private') {
                                setDefault();
                                return;
                            }
                            ctrl.view = '/users/app/components/captain-form/captain-step/views/step-captain-private.view.html';
                            ctrl.formName = 'form_private';
                            break;
                        case 'company':
                            if (ctrl.user.captain_type != 'enterprise') {
                                setDefault();
                                return;
                            }
                            ctrl.view = '/users/app/components/captain-form/captain-step/views/step-captain-company.view.html';
                            ctrl.formName = 'form_company';
                            break;
                        case 'sports-association':
                            if (ctrl.user.captain_type != 'association') {
                                setDefault();
                                return;
                            }
                            ctrl.view = '/users/app/components/captain-form/captain-step/views/step-captain-sports-association.view.html';
                            ctrl.formName = 'form_sports_association';
                            break;
                        default:
                            setDefault();
                            break;
                    }
                };
                // Set view
                ctrl.setView = function () {
                    switch (ctrl.user.captain_type) {
                        // Private
                        case 'private':
                            ctrl.view = '/users/app/components/captain-form/captain-step/views/step-captain-private.view.html';
                            ctrl.formName = 'form_private';
                            $location.hash('private');
                            break;
                        // Company
                        case 'enterprise':
                            ctrl.view = '/users/app/components/captain-form/captain-step/views/step-captain-company.view.html';
                            ctrl.formName = 'form_company';
                            $location.hash('company');
                            break;
                        // Sports association
                        case 'association':
                            ctrl.view = '/users/app/components/captain-form/captain-step/views/step-captain-sports-association.view.html';
                            ctrl.formName = 'form_sports_association';
                            $location.hash('sports-association');
                            break;
                        default:
                            break;
                    }
                };

                // Promises
                var userPromise = requestService.getSingleAsync('users', ctrl.userId, {include: 'languages'});
                var spokenLanguagesPromises = requestService.getListAsync('spoken-languages');
                var currenciesPromise = requestService.getListAsync('currencies');
                var captainTypesPromise = requestService.getListAsync('captain-types');
                var boatPromise = requestService.getRelatedSingleAsync('captains', ctrl.userId, 'boats', {editing: true});

                userPromise.then(
                    function (response) {
                        userService.save(response.data.data);
                        userService.saveRelationships(response.data.data.relationships);
                        ctrl.user = angular.copy(userService.get().attributes);
                        ctrl.userRelationships = angular.copy(userService.getRelationships());

                        boatPromise.then(
                            function (response) {
                                // If there's at least one boat (ie the boat was already created)
                                if (response.data.data.length > 0) {
                                    var boat = response.data.data[0].attributes;

                                    // Check if boat is already finished
                                    if (boat.is_finished) {
                                        $location.path('/' + ctrl.language + '/dashboard');
                                        return;
                                    }
                                }

                                // Hide the loader, THEN display view (by calling routeToView)
                                ctrl.loading = false;
                            },
                            null
                        );

                        // Set private/company/association view
                        ctrl.routeToView();

                        spokenLanguagesPromises.then(
                            function (languageListResponse) {
                                var languages = [];
                                languageListResponse.data.data.forEach(function (language) {
                                    languages.push({code: language.attributes.language, id: language.id})
                                });
                                // EN
                                ctrl.selectedLanguage = languages.filter(function (language) {
                                    return language.code == 'en';
                                })[0];
                                ctrl.languages = languages;

                                // Save user languages to tempUser
                                if (ctrl.userRelationships) {
                                    var languagesIds = [];
                                    ctrl.userRelationships.languages.data.forEach(function (language) {
                                        languagesIds.push(language.id);
                                    });
                                    // Save only languages that are also in languagesIds (user relationships)
                                    ctrl.tempUser.languages = ctrl.languages.filter(function (language) {
                                        return -1 != languagesIds.indexOf(language.id)
                                    });
                                }
                            }
                        );

                        // TRANSFORM fields to make them more input friendly
                        // Birth date
                        if (ctrl.user.birth_date) {
                            var date = new Date(ctrl.user.birth_date);
                            ctrl.tempUser.day = date.getDate();
                            ctrl.tempUser.month = date.getMonth();
                            ctrl.tempUser.year = date.getFullYear();
                        } else {
                            ctrl.tempUser.day = null;
                            ctrl.tempUser.month = null;
                            ctrl.tempUser.year = null;
                        }
                        // Password
                        if (ctrl.user.is_set_password) {
                            ctrl.tempUser.password = ctrl.tempUser.password_confirm = '***********';
                        }

                        // Library for parsing and validating of phone numbers
                        countryService.getCountries().then(
                            function (countries) {
                                ctrl.setUserPhone(ctrl.user.phone, countries);
                            }
                        );
                    }
                );

                captainTypesPromise.then(
                    function (response) {
                        response.data.data.forEach(function (type) {
                            var newType = type.attributes.name;
                            newType = newType.replace(/-/, '_');
                            ctrl.captainTypes.push(newType);
                        })
                    },
                    function (response) {
                        console.log('Some problem fetching types. Hardcode some types');
                        ctrl.captainTypes.push('private');
                        ctrl.captainTypes.push('enterprise');
                    }
                );

                // FIXED DATA
                ctrl.sexes = [
                    {code: 'm', alias: 'man'},
                    {code: 'f', alias: 'woman'}
                ];
                ctrl.months = [
                    {code: 'jan', alias: 'january', days: mathService.range(1, 31)},
                    {code: 'feb', alias: 'february', days: mathService.range(1, 29)},
                    {code: 'mar', alias: 'march', days: mathService.range(1, 31)},
                    {code: 'apr', alias: 'april', days: mathService.range(1, 30)},
                    {code: 'may', alias: 'may', days: mathService.range(1, 31)},
                    {code: 'jun', alias: 'june', days: mathService.range(1, 30)},
                    {code: 'jul', alias: 'july', days: mathService.range(1, 31)},
                    {code: 'aug', alias: 'august', days: mathService.range(1, 31)},
                    {code: 'sep', alias: 'september', days: mathService.range(1, 30)},
                    {code: 'oct', alias: 'october', days: mathService.range(1, 31)},
                    {code: 'nov', alias: 'november', days: mathService.range(1, 30)},
                    {code: 'dec', alias: 'december', days: mathService.range(1, 31)}
                ];
                ctrl.getDays = function (month) {
                    if (!month) month = 0;
                    return ctrl.months[month].days;
                };
                ctrl.birthDateYears = mathService.range(1900, new Date().getFullYear() - 18, 1, true);
                // Countries (default: IT)
                ctrl.countries = [{
                    "name": "Italy",
                    "dial_code": "+39",
                    "code": "IT",
                    "flag": __env.cloudinaryUrl + '/image/upload/production/assets/img/countries/it.png'
                }];

                // Currencies
                ctrl.currencies = [
                    {code: 'EUR'}
                ];
                currenciesPromise.then(
                    function (response) {
                        var currencies = [];
                        response.data.data.forEach(function (currency) {
                            currencies.push({code: currency.attributes.name})
                        });
                        ctrl.currencies = currencies;
                    }
                );

                // If form is valid, set step value by setting location.search (triggers reload)
                ctrl.goToNextStep = function () {
                    // Manual validate certain inputs
                    var error = ctrl.validate();

                    if (!error && ctrl[ctrl.formName].$valid && !ctrl.loading && !ctrl.patching) {
                        ctrl.errors = {};
                        // Set step
                        $location.hash(null);
                        $location.search('step', ctrl.step + 1);
                    } else {
                        validationService.setFrontendErrors(ctrl[ctrl.formName], ctrl.errors)
                    }
                };

                // VALIDATION
                // Manual validation
                ctrl.validate = function () {
                    var error = false;

                    // Company or sports association
                    if (ctrl.user.captain_type == 'enterprise' || ctrl.user.captain_type == 'association') {
                        // enterprise_country
                        if (!ctrl.user.enterprise_country) {
                            ctrl.errors.enterprise_country = {code: 'required'};
                            error = true;
                            console.log('error enterprise_country');
                        }
                    }
                    // Private
                    if (ctrl.user.captain_type == 'private') {
                        // SEX
                        if (!ctrl.user.sex) {
                            ctrl.errors.sex = {code: 'required'};
                            ctrl.errors.skipper_sex = {code: 'required'};
                            error = true;
                            console.log('error sex');
                        }
                        // NATIONALITY
                        if (!ctrl.user.nationality) {
                            ctrl.errors.nationality = {code: 'required'};
                            ctrl.errors.skipper_nationality = {code: 'required'};
                            error = true;
                            console.log('error nationality');
                        }
                        // BIRTH DATE
                        if (ctrl.tempUser.day == null || ctrl.tempUser.month == null || ctrl.tempUser.year == null) {
                            ctrl.errors.birth_date = {code: 'required'};
                            ctrl.errors.skipper_birth_date = {code: 'required'};
                            console.log('error birth');
                            error = true;
                        }
                    }

                    // CURRENCY
                    if (!ctrl.user.currency) {
                        ctrl.errors.currency = {code: 'required'};
                        error = true;
                        console.log('error currency');
                    }
                    // SPOKEN LANGUAGES
                    if (ctrl.tempUser.languages.length == 0) {
                        ctrl.errors.spoken_languages = {code: 'required'};
                        ctrl.errors.skipper_spoken_languages = {code: 'required'};
                        error = true;
                        console.log('error languages');
                    }
                    // CELLPHONE
                    if (!ctrl.user.is_phone_activated) {
                        ctrl.errors.phone = {code: 'verify'};
                        console.log('error phone activated');
                        error = true;
                    }

                    return error;
                };
                ctrl.validateField = function (field) {
                    validationService.validateField(ctrl[ctrl.formName][field], ctrl.errors);
                };
                // Clear field errors from ctrl.errors
                ctrl.resetField = function (field) {
                    validationService.resetField(field, ctrl.errors, ctrl[ctrl.formName]);
                };
                ctrl.resetFields = function (fields) {
                    if (ctrl.errors) {
                        fields.forEach(function (field) {
                            ctrl.errors[field] = null;
                        });
                        ctrl.errors.error = null;
                    }
                };

                ctrl.clearField = function (field, user) {
                    if (user) {
                        ctrl.user[field] = null;
                        return;
                    }
                    ctrl.tempUser[field] = null;
                };

                // Add/Remove spoken language
                ctrl.toggleSpokenLanguage = function (language, forceDelete = false) {
                    var index = ctrl.tempUser.languages.indexOf(language);
                    var patch = true;

                    if (index > -1) {
                        if (forceDelete) {
                            // Remove
                            ctrl.tempUser.languages.splice(index, 1);
                        } else {
                            // Nothing to do, don't patch
                            patch = false;
                        }
                    } else {
                        // Add
                        ctrl.tempUser.languages.push(language);

                        // Reset field error
                        delete ctrl.errors.spoken_languages;
                        delete ctrl.errors.skipper_spoken_languages;
                    }
                    if (patch) ctrl.patchLanguages();
                };

                // Account kit
                // phone form submission handler
                ctrl.verifyPhone = function () {
                    if (ctrl.user.is_phone_activated) return;

                    var countryCode = '+' + PhoneNumber.getCountryCodeForRegionCode(ctrl.tempUser.selectedCountry.code);
                    var phoneNumber = ctrl.tempUser.phone;
                    if (countryCode && phoneNumber) {
                        AccountKit.login(
                            'PHONE',
                            {countryCode: countryCode, phoneNumber: phoneNumber}, // will use default values if not specified
                            function (response) {
                                if (response.status === "PARTIALLY_AUTHENTICATED") {
                                    var code = response.code;
                                    var csrf = response.state;
                                    // Send code to server to exchange for access token
                                    requestService.verifyPhone(ctrl.userId, code).then(
                                        function (response) {
                                            // Get user back
                                            ctrl.user.is_phone_activated = true;
                                            ctrl.errors.phone = null;

                                            // Reset user phone
                                            ctrl.setUserPhone(response.data.data.attributes.phone, ctrl.countries);
                                        },
                                        function (response) {
                                            // Failure
                                            validationService.setBackendErrors(ctrl[ctrl.formName], ctrl.errors, response.data.errors);
                                        }
                                    );
                                }
                                else if (response.status === "NOT_AUTHENTICATED") {
                                    // handle authentication failure
                                }
                                else if (response.status === "BAD_PARAMS") {
                                    // handle bad parameters
                                }
                            }
                        );
                    }
                };
                // Set password
                ctrl.setPassword = function (field) {
                    var form = ctrl[ctrl.formName];
                    ctrl.validateField(field);

                    if (ctrl.tempUser.password != ctrl.tempUser.password_confirm) return;
                    if (ctrl.errors.password || ctrl.errors.password_confirm) return;

                    ctrl.patching = true;
                    requestService.setPassword(ctrl.userId, ctrl.tempUser.password).then(
                        function (response) {
                            // Saved! Now store user in userService
                            userService.save(response.data.data);
                            ctrl.user = angular.copy(userService.get().attributes);
                            ctrl.patching = false;
                            ctrl.passwordConfirmed = true;
                        },
                        function (response) {
                            // Failure
                            validationService.setBackendErrors(form, ctrl.errors, response.data.errors);
                            ctrl.patching = false;
                        }
                    );
                };

                // Backend CALLS
                ctrl.patchUser = function (form, requireAll) {
                    ctrl.patching = true;
                    // Build user model
                    var user = {};
                    user.id = ctrl.userId;
                    user.attributes = angular.copy(ctrl.user);

                    // If date is valid
                    if (ctrl.tempUser.day != null && ctrl.tempUser.month != null && ctrl.tempUser.year != null) {
                        user.attributes.birth_date = ctrl.tempUser.year + '-' + (ctrl.tempUser.month + 1) + '-' + ctrl.tempUser.day;
                    }

                    // If every field is required, block requests with empty fields
                    var canGoOn = true;
                    if (requireAll) {
                        canGoOn = form.$valid;
                    } else {
                        // For each form input, take error type (key), and check if type is other than 'required' or 'password_match'.
                        // If the key is, e.g., maxlength, then check what is the field with error. If it's password, go on.
                        // Else, the form can't be submitted
                        angular.forEach(form.$error, function (error, key) {
                            if (-1 == ['required', 'password_match'].indexOf(key)) {
                                error.forEach(function (err) {
                                    // If error field (name) is not password or password_match, the form can't be submitted
                                    if (-1 == ['password', 'password_match'].indexOf(err.$name)) {
                                        canGoOn = false;
                                    }
                                });
                            }
                        });
                    }
                    if (!canGoOn) return;

                    // Send request
                    requestService.patchUser(user).then(
                        function (response) {
                            // Patched! Now get relationships too and store everything in userService
                            userService.save(response.data.data);
                            ctrl.user = angular.copy(userService.get().attributes);
                            ctrl.patching = false;
                        },
                        function (response) {
                            validationService.setBackendErrors(form, ctrl.errors, response.data.errors);
                            ctrl.patching = false;
                        }
                    );
                };
                ctrl.patchLanguages = function () {
                    ctrl.patching = true;

                    requestService.patchUserLanguages(ctrl.userId, ctrl.tempUser.languages).then(
                        function (response) {
                            ctrl.patching = false;
                        },
                        function (response) {
                            // Failure
                            validationService.setBackendErrors(ctrl[ctrl.formName], ctrl.errors, response.data.errors);
                            ctrl.patching = false;
                        }
                    );
                };
                // Patch user on every field change (auto-save)
                ctrl.patchInput = function (field, resetErrors) {
                    ctrl.patching = true;

                    // Radio button need its errors to be reset,
                    // because it's sending patch on change and not on blur like inputs
                    if (resetErrors) {
                        ctrl.resetField(field);
                    } else {
                        ctrl.validateField(field);
                    }

                    // Do not send patch if field is invalid
                    if (ctrl.errors[field]) {
                        ctrl.patching = false;
                        return;
                    }

                    ctrl.patchUser(ctrl[ctrl.formName], false);
                };

                // Every list will be structured like this: {code: 'code', [alias: 'translation alias']}
                ctrl.getAttributeTranslationCode = function (listName, value) {
                    var list = ctrl[listName];
                    var translationAlias = '';

                    list.forEach(function (element) {
                            // Get alias if list has it, else code
                            if (element.code === value) translationAlias = element.alias || element.code;
                        }
                    );

                    if (!translationAlias) translationAlias = list[0].alias || list[0].code;

                    return translationAlias;
                };

                // Popovers controller with checkbox
                ctrl.openPopover = function (popoverName, label = true, $event = null, controllerPopovers = null, objectModel = null) {
                    if (!controllerPopovers) controllerPopovers = ctrl.popovers;
                    if (!objectModel) objectModel = ctrl.user;
                    if (!popoverName) return;

                    // Clicked on label
                    if (label) {
                        // If popover is open, close it without toggling the checkbox
                        if (controllerPopovers[popoverName]) {
                            controllerPopovers[popoverName] = false;
                            return;
                        }
                        // If popover is close, toggle the checkbox and open it
                        objectModel[popoverName] = true;
                        controllerPopovers[popoverName] = true;
                        return;
                    }
                    // Click on checkbox
                    // If popover is close, open it only if checkbox was previously unchecked. In any case, toggle the checkbox
                    if (!controllerPopovers[popoverName]) {
                        if (!objectModel[popoverName]) {
                            controllerPopovers[popoverName] = true;
                        }
                        objectModel[popoverName] = !objectModel[popoverName];
                    }
                    if ($event) $event.stopPropagation();
                };
            }
        }]);