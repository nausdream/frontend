angular.module('users').controller('rulesStepController',
    ['mathService', 'requestService', 'fixedListService', 'userService', 'validationService', 'mapsService', '$location', 'photoService', '$q', '$routeParams',
        function (mathService, requestService, fixedListService, userService, validationService, mapsService, $location, photoService, $q, $routeParams) {
            this.$onInit = function () {
                // Make context accessible inside inner scopes
                var ctrl = this;
                ctrl.loading = true;
                ctrl.formName = 'form_rules';
                // Set disabled (read only form)
                ctrl.disabled = false;
                ctrl.errors = {};
                ctrl.backendErrors = [];
                // To make down-arrow behave correctly
                ctrl.step = 3;
                ctrl.dropdowns = {};
                ctrl.popovers = {};
                ctrl.userId = userService.get() ? userService.get().id : null;
                ctrl.user = angular.copy(userService.get().attributes);
                ctrl.tempBoat = {
                    internal_photo: {},
                    external_photo: {},
                    sea_photo: {}
                };

                ctrl.language = $routeParams.lang;

                // Promises
                var boatPromise = requestService.getRelatedSingleAsync('captains', ctrl.userId, 'boats', {editing: true});
                var accessoriesPromise = fixedListService.getFixedList('accessories');

                // Get user boat if present
                boatPromise.then(
                    function (response) {
                        // If there's at least one boat (ie the boat was already created)
                        if (response.data.data.length > 0) {
                            ctrl.boatId = response.data.data[0].id;
                            ctrl.boat = response.data.data[0].attributes;

                            // Check if boat is already finished and user is already set
                            if (ctrl.boat.is_finished) {
                                $location.hash(null);
                                $location.path('/' + ctrl.language + '/dashboard');
                            }
                            if (!userService.isUserSet()) {
                                $location.search('step', 1);
                            }

                            ctrl.tempBoat.internal_photo.link = ctrl.boat.internal_photo;
                            ctrl.tempBoat.external_photo.link = ctrl.boat.external_photo;
                            ctrl.tempBoat.sea_photo.link = ctrl.boat.sea_photo;

                            // Get boat experience types from boat
                            requestService.getSingleAsync('boats', ctrl.boatId, {editing: true}).then(
                                function (response) {
                                    ctrl.boatRelationships = response.data.data.relationships;

                                    accessoriesPromise.then(
                                        function (response) {
                                            ctrl.accessories = response.data.data;

                                            if (ctrl.boatRelationships) {
                                                // Set initial checked accessories
                                                var boatAccessories = ctrl.boatRelationships.accessories;
                                                if (boatAccessories) {
                                                    boatAccessories.data.forEach(function (boatAccessory) {
                                                        // Find the matching accessory in accessories list, and set its checked attribute to true
                                                        ctrl.accessories.filter(function (accessory) {
                                                            return accessory.id == boatAccessory.id;
                                                        })[0].attributes.checked = true;
                                                    })
                                                }
                                            }

                                            ctrl.loading = false;
                                        },
                                        function (response) {
                                            // TODO MANAGE RESPONSE
                                        }
                                    )
                                },
                                function (response) {
                                    // TODO MANAGE RESPONSE
                                }
                            );

                        } else {
                            // TODO MANAGE ERROR
                        }
                    },
                    function (response) {
                        // TODO MANAGE ERROR
                    }
                );

                // VALIDATION
                // Manual validation
                ctrl.validate = function () {
                    var error = false;

                    if (!ctrl.tempBoat.internal_photo.link || !ctrl.tempBoat.external_photo.link || !ctrl.tempBoat.sea_photo.link) {
                        error = true;

                        ctrl.errors.photos = {code: 'required'};
                        console.log('error photos');
                    }

                    return error;
                };
                ctrl.validateField = function (field) {
                    validationService.validateField(ctrl[ctrl.formName][field], ctrl.errors);
                };
                // Clear field errors from ctrl.errors
                ctrl.resetField = function (field) {
                    validationService.resetField(field, ctrl.errors, ctrl[ctrl.formName]);
                };

                // Patch boat on every field change (auto-save)
                ctrl.patchInput = function (field, resetErrors) {
                    ctrl.loading = true;

                    // Radio button need its errors to be reset,
                    // because it's sending patch on change and not on blur like inputs
                    if (resetErrors) {
                        ctrl.resetField(field);
                    } else {
                        ctrl.validateField(field);
                    }

                    // Do not send patch if field is invalid
                    if (ctrl.errors[field]) {
                        ctrl.loading = false;
                        return;
                    }

                    ctrl.patchBoat(ctrl[ctrl.formName], false);
                };
                // BOAT Api calls
                ctrl.patchBoat = function (form, requireAll = false) {
                    // Prepare async request
                    var deferred = $q.defer();

                    ctrl.loading = true;
                    // Build user model
                    var boat = {};
                    boat.id = ctrl.boatId;
                    boat.attributes = angular.copy(ctrl.boat);
                    // If you don't delete status, the backend think you're trying to change boat status
                    delete boat.attributes.status;

                    // If every field is required, block requests with empty fields
                    var canGoOn = true;
                    if (requireAll) {
                        canGoOn = form.$valid;
                    } else {
                        // For each form input, take error type (key), and check if type is other than 'required' or 'password_match'.
                        // If the key is, e.g., maxlength, then check what is the field with error. If it's password, go on.
                        // Else, the form can't be submitted
                        angular.forEach(form.$error, function (error, key) {
                            if (-1 == ['required', 'password_match'].indexOf(key)) {
                                error.forEach(function (err) {
                                    // If error field (name) is not password or password_match, the form can't be submitted
                                    if (-1 == ['password', 'password_match'].indexOf(err.$name)) {
                                        canGoOn = false;
                                    }
                                });
                            }
                        });
                    }
                    if (!canGoOn) {
                        deferred.reject('error');
                        return deferred.promise;
                    }

                    // Send request
                    requestService.putBoat(boat.id, boat.attributes).then(
                        function (response) {
                            ctrl.boat = response.data.data.attributes;
                            ctrl.loading = false;
                            deferred.resolve(response);
                        },
                        function (response) {
                            // Failure
                            validationService.setBackendErrors(form, ctrl.errors, response.data.errors);
                            ctrl.loading = false;
                            deferred.reject(response);
                        }
                    );

                    return deferred.promise;
                };
                ctrl.postPhoto = function (file, photoType) {
                    if (!file) return;

                    // Hide errors on new upload
                    delete ctrl.errors.photos;

                    ctrl.tempBoat[photoType + '_photo'].loading = true;

                    return photoService.getLink(file).then(
                        function (response) {
                            var link = response;
                            requestService.postPhoto(response, 'boat', ctrl.boatId, photoType).then(
                                function (response) {
                                    // Photo was patched to boat
                                    ctrl.tempBoat[photoType + '_photo'].link = link;
                                    ctrl.tempBoat[photoType + '_photo'].loading = false;
                                }
                            );
                        },
                        function (response) {
                            console.log(response);
                        }
                    );
                };
                ctrl.patchAccessories = function () {
                    ctrl.loading = true;

                    // Select only accessory with the checked flag
                    var accessories = ctrl.accessories.filter(function (accessory) {
                        return !!accessory.attributes.checked;
                    });

                    requestService.patchAccessories(ctrl.boatId, accessories).then(
                        function (response) {
                            ctrl.loading = false;
                        },
                        function (response) {
                            console.log('Failed to patch accessories.');
                        }
                    );
                };


                // If form is valid, set step value by setting location.search (triggers reload)
                ctrl.goToNextStep = function () {
                    // Manual validate certain inputs
                    var error = ctrl.validate();

                    if (!error && ctrl[ctrl.formName].$valid && !ctrl.loading) {
                        ctrl.errors = {};

                        // Finish editing (set is_finished to true)
                        ctrl.boat.is_finished = true;
                        ctrl.patchBoat(ctrl[ctrl.formName], true).then(
                            function (response) {
                                // Set step
                                $location.hash(null);
                                $location.search('step', ctrl.step + 1);
                            },
                            function (response) {
                                // TODO MANAGE ERROR
                            }
                        );


                    } else {
                        validationService.setFrontendErrors(ctrl[ctrl.formName], ctrl.errors)
                    }
                };
            }
        }]
);