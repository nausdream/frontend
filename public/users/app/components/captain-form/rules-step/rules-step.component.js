angular.module('users').component('rulesStep', {
    templateUrl: '/users/app/components/captain-form/rules-step/rules-step.view.html',
    controller: 'rulesStepController',
    controllerAs: 'ctrl'
});