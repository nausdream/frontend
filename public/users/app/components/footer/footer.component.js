angular.module('users').component('footer', {
    templateUrl: '/users/app/components/footer/footer.view.html',
    controller: 'footerController',
    controllerAs: 'ctrl'
});