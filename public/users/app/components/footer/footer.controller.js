angular
    .module('users')
    .controller('footerController', ['translationService', '__env', '$location', '$cookies', '$routeParams',
        function (translationService, __env, $location, $cookies, $routeParams) {
            this.$onInit = function () {
                var ctrl = this;
                ctrl.translations = {};
                ctrl.translations.loading = true;
                ctrl.viewName = 'footer';

                ctrl.language = $routeParams.lang;

                // Load translations
                var pages = ['footer'];
                translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

                // Links
                ctrl.links = {
                    'how_it_works': '',
                    'about_us': '',
                    'follow_us': '',
                    'facebook': 'https://www.facebook.com/nausdream',
                    'instagram': 'https://instagram.com/nausdream/',
                    'twitter': 'https://twitter.com/nausdream',
                    'blog': 'http://blog.nausdream.com',
                    'paypal': __env.cloudinaryUrl + '/image/upload/b_rgb:101c33,q_80/production/assets/img/home/paypal.jpg',
                    'terms_and_conditions': '',
                };

                ctrl.goTo = function (path) {
                    $location.path('/' + ctrl.language + path).search({});
                };
            };
        }]);