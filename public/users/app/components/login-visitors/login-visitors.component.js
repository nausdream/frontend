angular.module('users').controller('LoginVisitorsController',
    ["requestService", "loginService", "translationService",
        function (requestService, loginService, translationService) {
            this.$onInit = function () {
                var vm = this;
                vm.onCompleteSMS = onCompleteSMS;
                vm.onCompleteFacebook = onCompleteFacebook;
                vm.viewName = 'loginVisitors';

                var pages = ['login'];
                translationService.translate(pages, vm.viewName);

                function login(response) {
                    loginService.setCookies(response);
                    loginService.login().then(
                        function () {
                            vm.onComplete();
                            vm.close();
                        },
                        null
                    );
                }

                function onCompleteSMS(response) {
                    var code = response.code;
                    var csrf = response.state;

                    requestService
                        .loginWithPhone(code)
                        .then(function (response) {
                            login(response);
                        })
                        .catch(function (err) {
                            console.log(err);
                        })
                }

                function onCompleteFacebook(response) {
                    var accessToken = response.authResponse.accessToken || '1234';

                    requestService
                        .loginWithFacebook(accessToken)
                        .then(function (response) {
                            login(response);
                        })
                        .catch(function (err) {
                            console.log(err);
                        })
                }
            };
        }
    ]
);

angular
    .module('users')
    .component('loginVisitors', {
        templateUrl: '/users/app/components/login-visitors/login.visitors.html',
        controller: 'LoginVisitorsController',
        controllerAs: 'loginVisitors',
        bindings: {
            close: '&',
            onComplete: '&'
        }
    });