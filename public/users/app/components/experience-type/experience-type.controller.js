angular
    .module('users')
    .controller('experienceTypeController', [
        'experienceTypesGroups', '$location', '$httpParamSerializer', '$cookies', '$routeParams',
        function (experienceTypesGroups, $location, $httpParamSerializer, $cookies, $routeParams) {
            this.$onInit = function () {
                var ctrl = this;

                ctrl.language = $routeParams.lang;

                var data = experienceTypesGroups.filter(function (object) {
                    return object.name == ctrl.experienceTypeObject.title;
                })[0];

                // TODO GEOLOCALIZATION


                var params = {
                    key: data.name,
                    experience_types: data.types,
                    lat: 40.976140,
                    lng: 12.270724,
                    distance: 500,
                    zoom: 5,
                    babies: 0,
                    kids: 0,
                    min_length: 1,
                    entire_boat: 0,
                    min_price: 1
                };

                // Differentiate background between mobile and desktop
                ctrl.image = ctrl.experienceTypeMedia == 'mobile' ? ctrl.experienceTypeObject.image_url_mobile : ctrl.experienceTypeObject.image_url;
                if (!ctrl.image) ctrl.image = ctrl.experienceTypeObject.image_url;

                var qs = $httpParamSerializer(params);
                ctrl.url = '/' + ctrl.language + '/experiences\?' + qs;

                ctrl.go = function () {
                    $location.path('/' + ctrl.language + '/experiences').search(params);
                }
            };
        }]);