angular.module('users').component('experienceType', {
    templateUrl: '/users/app/components/experience-type/experience-type.view.html',
    controller: 'experienceTypeController',
    controllerAs: 'ctrl',
    bindings: {
        experienceTypeObject: '<',
        experienceTypeMedia: '@'
    }
});