angular.module('users').component('experienceEstimationBox', {
    templateUrl: '/users/app/components/experiences/experience-estimation-box/experience-estimation-box.view.html',
    controller: 'experienceEstimationBoxController',
    controllerAs: 'ctrl',
    bindings: {
        experiencePromise: '<',
        adults: '=',
        kids: '=',
        babies: '=',
        experienceDate: '=',
        estimationPromise: '=',
        toggleBookingModal: '&'
    }
});