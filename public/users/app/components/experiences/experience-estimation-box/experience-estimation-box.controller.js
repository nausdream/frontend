angular.module('users').controller('experienceEstimationBoxController', [
    'translationService', '__env', '$location', 'requestService', '$cookies', 'currencyService',
    'userService', '$q', 'mathService', 'fixedListService', 'dateService', 'validationService',
    '$scope', '$routeParams', '$rootScope',
    function (translationService, __env, $location, requestService, $cookies, currencyService,
              userService, $q, mathService, fixedListService, dateService, validationService,
              $scope, $routeParams, $rootScope) {
        this.$onInit = function () {
            var ctrl = this;
            ctrl.loading = true;
            ctrl.language = $routeParams.lang;
            ctrl.currency = $cookies.get('currency');

            ctrl.user = userService.get();

            ctrl.modals = {};
            ctrl.dropdowns = {};

            // Validation bootstrap
            ctrl.errors = {};
            ctrl.formName = 'form';

            // Init translations object
            ctrl.translations = {};
            ctrl.translations.loading = true;
            ctrl.viewName = 'experienceEstimationBox';
            // Load translations
            var pages = [
                'booking', 'inputs', 'errors', 'experience'
            ];
            translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

            // Fetch currencies
            currencyService.getCurrencies().then(
                function (response) {
                    ctrl.currencies = response;
                },
                null
            );

            // Update hosts limits for inputs
            ctrl.updateHosts = function () {
                ctrl.minAdults = Math.max(0, ctrl.minHosts - ctrl.kids - ctrl.babies);
                ctrl.minKids = Math.max(0, ctrl.minHosts - ctrl.adults - ctrl.babies);
                ctrl.minBabies = Math.max(0, ctrl.minHosts - ctrl.adults - ctrl.kids);

                ctrl.maxAdults = ctrl.experienceSeats - ctrl.kids - ctrl.babies;
                ctrl.maxKids = Math.min(ctrl.experienceKids, ctrl.experienceSeats - ctrl.adults - ctrl.babies);
                ctrl.maxBabies = Math.min(ctrl.experienceBabies, ctrl.experienceSeats - ctrl.adults - ctrl.kids);
                ctrl.adultsRange = mathService.range(ctrl.minAdults, ctrl.maxAdults);
            };

            // Reload view
            ctrl.loadEstimation = function (estimationResponse, experienceResponse) {

                var estimation = estimationResponse.meta;

                // Set controller attributes from estimation
                ctrl.setAttributes(estimation);
            };

            // Set min number of hosts from correct period
            ctrl.setMinPersonFromPeriods = function (experienceResponse) {
                // Set min person from correct period
                var correctPeriod;
                var defaultPeriod;
                var nullDatePeriod;
                var periods = experienceResponse.included.filter(function (included) {
                    return included.type == 'periods';
                });
                periods.forEach(function (period) {
                    if (period.attributes.date_start == null) nullDatePeriod = period;
                    if (period.attributes.is_default) defaultPeriod = period;

                    var start = dateService.getDateFromString(period.attributes.date_start);
                    var end = dateService.getDateFromString(period.attributes.date_end);
                    if (ctrl.experienceDate >= start && ctrl.experienceDate <= end) {
                        correctPeriod = period;
                    }
                });
                if (correctPeriod) {
                    ctrl.minHosts = correctPeriod.attributes.min_person;
                    return;
                }
                if (defaultPeriod) {
                    ctrl.minHosts = defaultPeriod.attributes.min_person;
                    return;
                }
                if (nullDatePeriod) {
                    ctrl.minHosts = nullDatePeriod.attributes.min_person;
                    return;
                }
            };

            // Set controller attributes from estimation or booking
            ctrl.setAttributes = function (object) {
                ctrl.priceFrom = object.price_per_person;
                ctrl.pricePerPersonWithoutServices = object.price_per_person_without_services;
                ctrl.servicesPrice = object.services_price;
                ctrl.entireBoat = object.entire_boat;
                ctrl.total = object.price_minus_discount;
                ctrl.totalGuestCurrency = object.price_minus_discount_guest;
                ctrl.priceOnBoard = object.price_on_board;
                ctrl.priceOnBoardGuestCurrency = object.price_on_board_guest;
                ctrl.priceToPay = object.price_to_pay;
                ctrl.priceToPayGuestCurrency = object.price_to_pay_guest;
                ctrl.discount = object.discount;
                ctrl.captainCurrency = object.captain_currency;

                currencyService.getCurrencySymbol(ctrl.captainCurrency).then(
                    function (response) {
                        ctrl.captainCurrencySymbol = response;
                    },
                    null
                );
                currencyService.getCurrencySymbol(ctrl.currency).then(
                    function (response) {
                        ctrl.userCurrencySymbol = response;
                    },
                    null
                );
                ctrl.isSameCurrency = ctrl.captainCurrency == ctrl.currency;

                if (typeof ctrl.experienceDate === 'string') ctrl.experienceDate = dateService.getDateFromString(ctrl.experienceDate);
                ctrl.hosts = ctrl.adults + ctrl.kids + ctrl.babies;
                ctrl.totalPriceWithoutServices = ctrl.pricePerPersonWithoutServices * ctrl.hosts;
            };

            // Set calendar availabilites from experience response
            ctrl.setAvailabilities = function (experienceResponse) {
                if (ctrl.experienceResponse.data.relationships.experience_availabilities) {
                    ctrl.availabilities = angular.copy(experienceResponse.included.filter(function (included) {
                        return included.type == 'experience-availabilities';
                    }));
                }
            };

            // Set calendar available week days from experience response
            ctrl.setAvailableDaysOfWeek = function (experienceResponse) {
                ctrl.availableDaysOfWeek = [];

                var attributes = experienceResponse.data.attributes;

                ctrl.availableDaysOfWeek.push(
                    attributes.sunday,
                    attributes.monday,
                    attributes.tuesday,
                    attributes.wednesday,
                    attributes.thursday,
                    attributes.friday,
                    attributes.saturday
                );
            };

            // Get new prices
            ctrl.requestNewEstimation = function () {
                ctrl.makingRequest = true;

                // Convert date to string
                var requestDate = dateService.getDateString(ctrl.experienceDate);

                var parameters = {
                    experience_id: ctrl.experienceId,
                    hosts: ctrl.adults + ctrl.kids + ctrl.babies,
                    request_date: requestDate,
                    currency: ctrl.currency
                };

                if (ctrl.coupon) parameters.coupon = ctrl.coupon;

                ctrl.estimationPromise = requestService.getListAsync('estimations', parameters);
                ctrl.estimationPromise.then(
                    function (response) {
                        ctrl.loadEstimation(response.data, ctrl.experienceResponse);
                        ctrl.makingRequest = false;
                    },
                    function (response) {
                        validationService.setBackendErrors(ctrl[ctrl.formName], ctrl.errors, response.data.errors);
                        ctrl.makingRequest = false;
                    }
                );
            };

            // Update dates when language is changed
            $scope.$on('languageChanged', function (event, language) {
                ctrl.language = language;
            });

            // Update prices when currency is changed
            $scope.$on('currencyChanged', function (event, currency) {
                ctrl.currency = currency;

                ctrl.requestNewEstimation();
            });

            // Listen for user updates
            $rootScope.$on('userUpdated', function (event, user) {
                ctrl.user = user;
            });

            // Validation
            // Clear field errors from ctrl.errors
            ctrl.resetField = function (field) {
                validationService.resetField(field, ctrl.errors, ctrl[ctrl.formName]);
            };

            // Set new adult: close dropdown, clear errors on field, set ctrl.adults, update new maximums
            // Request new estimation
            ctrl.setAdult = function (newAdults) {
                ctrl.adults = newAdults;
                ctrl.dropdowns.adults = false;
                ctrl.errors.hosts = null;
                ctrl.updateHosts();

                if (!ctrl.areEstimationFieldsValid()) return;

                ctrl.requestNewEstimation();
            };
            // Set new kid or baby and request new estimation
            ctrl.setOtherHost = function () {
                ctrl.errors.hosts = null;
                ctrl.updateHosts();

                if (!ctrl.areEstimationFieldsValid()) return;

                ctrl.requestNewEstimation();
            };

            // Validate field
            ctrl.validateField = function (field) {
                validationService.validateField(ctrl[ctrl.formName][field], ctrl.errors);
            };

            // On blur, validate and then request new estimation
            ctrl.onBlur = function (field) {

                if (field) {
                    ctrl.validateField(field);
                }

                if (!ctrl.areEstimationFieldsValid()) return;

                ctrl.requestNewEstimation();
            };

            // Check if estimation request can be sent
            ctrl.areEstimationFieldsValid = function () {
                // HOSTS
                var hosts = ctrl.adults + ctrl.kids + ctrl.babies;
                if (hosts > ctrl.experienceSeats) return false;
                if (ctrl.kids > ctrl.experienceKids) return false;
                if (ctrl.babies > ctrl.experienceBabies) return false;

                // DATE
                if (!ctrl.experienceDate) return false;
                if (ctrl.errors.experience_date) return false;

                return true;
            };

            // If date changes, request new estimation
            $scope.$watch(
                function () {
                    return ctrl.experienceDate;
                },
                function (newValue, oldValue) {
                    if (!newValue) return;
                    if (typeof newValue.getTime != 'function') return;

                    if (!oldValue || typeof oldValue.getTime != 'function' || newValue.getTime() != oldValue.getTime()) {
                        delete ctrl.errors.experience_date;
                        delete ctrl.errors.error;
                        ctrl.onBlur(null);
                    }
                });

            // LOAD DATA FOR THE FIRST TIME
            var promises = $q.all([ctrl.estimationPromise, ctrl.experiencePromise]);
            promises.then(
                function (responses) {
                    var estimationResponse = responses[0].data;
                    ctrl.experienceResponse = responses[1].data;
                    var experience = ctrl.experienceResponse.data.attributes;
                    ctrl.experienceId = ctrl.experienceResponse.data.id;
                    ctrl.experienceSeats = experience.seats;
                    ctrl.experienceBabies = experience.babies;
                    ctrl.experienceKids = experience.kids;

                    // If experiencePage controller passed undefined adults
                    if (ctrl.adults == null) ctrl.adults = ctrl.experienceSeats - ctrl.kids - ctrl.babies;
                    ctrl.setMinPersonFromPeriods(ctrl.experienceResponse);

                    // Sets max adults kids and babies
                    ctrl.updateHosts();

                    // Set availabilities and days of weeks from experience
                    ctrl.setAvailabilities(ctrl.experienceResponse);
                    ctrl.setAvailableDaysOfWeek(ctrl.experienceResponse);

                    ctrl.loadEstimation(estimationResponse, ctrl.experienceResponse);

                    ctrl.loading = false;
                }, function (response) {
                    if (response.data.errors[0].code == 'experience_date_not_available') {
                        // Load experience anyways
                        ctrl.experiencePromise.then(
                            function (response) {
                                ctrl.experienceResponse = response.data;
                                var experience = ctrl.experienceResponse.data.attributes;
                                ctrl.experienceId = ctrl.experienceResponse.data.id;
                                ctrl.experienceSeats = experience.seats;
                                ctrl.experienceBabies = experience.babies;
                                ctrl.experienceKids = experience.kids;

                                // If experiencePage controller passed undefined adults
                                if (ctrl.adults == null) {
                                    ctrl.adults = ctrl.experienceSeats - ctrl.kids - ctrl.babies;
                                }
                                ctrl.setMinPersonFromPeriods(ctrl.experienceResponse);

                                // Sets max adults kids and babies
                                ctrl.updateHosts();

                                // Set availabilities and days of weeks from experience
                                ctrl.setAvailabilities(ctrl.experienceResponse);
                                ctrl.setAvailableDaysOfWeek(ctrl.experienceResponse);

                                // Get the first available date
                                var selectedDay;
                                ctrl.availabilities.forEach(function (availability) {
                                    var start = dateService.getDateFromString(availability.attributes.date_start);
                                    var end = dateService.getDateFromString(availability.attributes.date_end);

                                    if (selectedDay) return;

                                    // Get availability if experienceDate in in range or less than date_start of availability
                                    if (ctrl.experienceDate >= start && ctrl.experienceDate <= end) {
                                        start = angular.copy(ctrl.experienceDate);
                                        // Take this availability only if it has at least one available day of week
                                        while (start <= end) {
                                            if (ctrl.availableDaysOfWeek[start.getDay()]) {
                                                // We found the correct day
                                                selectedDay = start;
                                                return;
                                            }
                                            else {
                                                // Add one day and try again
                                                start = new Date(start.getTime() + 24 * 60 * 60 * 1000);
                                            }
                                        }
                                    }
                                    if (ctrl.experienceDate <= start && ctrl.experienceDate <= end) {

                                        // Take this availability only if it has at least one available day of week
                                        while (start <= end) {
                                            if (ctrl.availableDaysOfWeek[start.getDay()]) {
                                                // We found the correct day
                                                selectedDay = start;
                                                return;
                                            }
                                            else {
                                                // Add one day and try again
                                                start = new Date(start.getTime() + 24 * 60 * 60 * 1000);
                                            }
                                        }
                                    }
                                });

                                // TODO verify that there is at least one day available

                                // This is already a date Object
                                ctrl.experienceDate = selectedDay;

                                // Now that we have a valid date, we can request a new estimation
                                ctrl.requestNewEstimation();

                                ctrl.loading = false;
                            }
                        );
                    }
                }
            );
        };
    }
]);