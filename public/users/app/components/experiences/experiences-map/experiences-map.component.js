angular
    .module('users')
    .component('experiencesMap', {
        templateUrl: '/users/app/components/experiences/experiences-map/experiences-map.view.html',
        controller: 'experiencesMapController',
        controllerAs: 'ctrl',
        bindings: {
            params: '=',
            listings: '=',
            onParamsUpdate: '&',
            onListingsUpdate: '&',
            currency: '@'
        }
    });