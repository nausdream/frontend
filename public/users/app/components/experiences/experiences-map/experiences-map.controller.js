angular.module('users').controller('experiencesMapController', [
    'translationService', '$location', '$cookies', 'currencyService', '$timeout',
    '$q', 'mapsService', 'topDestinations', 'PriceMarkerFactory', '$routeParams', '$scope',
    function (translationService, $location, $cookies, currencyService, $timeout,
              $q, mapsService, topDestinations, PriceMarkerFactory, $routeParams, $scope) {
        this.$onInit = function () {
            var ctrl = this;

            ctrl.language = $routeParams.lang;

            ctrl.translations = {};
            ctrl.translations.loading = true;

            ctrl.viewName = 'experiences-map';
            var pages = ['experiences', 'experience'];

            // Load translations
            translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

            // Needed to determine if key should be changed
            ctrl.topDestinations = topDestinations;

            // Zoom
            ctrl.triggerWarningLevel = 835;

            // Currency
            if (!ctrl.currency) ctrl.currency = $cookies.get('currency');

            // Init markers array
            ctrl.markers = [];

            // Insert listings markers into map
            ctrl.insertListings = function (listings) {
                // Remove previous markers
                ctrl.markers.forEach(function (marker) {
                    marker.setMap(null);
                    marker = null;
                });

                listings.forEach(function (listing) {
                    var marker = new PriceMarkerFactory(
                        new google.maps.LatLng(listing.attributes.lat, listing.attributes.lng),
                        ctrl.map,
                        listing.attributes.price,
                        ctrl.language + '/experiences/' + listing.attributes.slug_url,
                        ctrl.currency,
                        'listing-' + listing.id
                    );

                    ctrl.markers.push(marker);
                })
            };

            // Load map
            ctrl.loadMap = function () {

                // DEPARTURE MAP
                ctrl.params.lat = parseFloat(ctrl.params.lat);
                ctrl.params.lng = parseFloat(ctrl.params.lng);
                var mapCoordinates = {lat: ctrl.params.lat, lng: ctrl.params.lng};

                // Set default zoom
                if (!ctrl.params.zoom) ctrl.params.zoom = 8;

                // TODO BOH
                var div = document.createElement('div');
                div.id = 'search-map';

                angular.element(document).find('.search-map-container').append(div);

                ctrl.map = new google.maps.Map(div, {
                    center: mapCoordinates,
                    zoomControl: true,
                    streetViewControl: false,
                    mapTypeControl: false,
                    fullscreenControl: false,
                    disableDoubleClickZoom: true,
                    zoom: parseInt(ctrl.params.zoom),
                    gestureHandling: 'greedy',
                    maxZoom: 18
                });

                var reload = () => {
                    var bounds = ctrl.map.getBounds();
                    var center = ctrl.map.getCenter();

                    ctrl.params.lat = center.lat();
                    ctrl.params.lng = center.lng();
                    ctrl.params.zoom = ctrl.map.getZoom();

                    // Distance will be computed as distance from center to north bound
                    var ne = bounds.getNorthEast();

                    ctrl.params.distance = google.maps.geometry.spherical.computeDistanceBetween(
                            new google.maps.LatLng(ne.lat(), ctrl.params.lng),
                            new google.maps.LatLng(ctrl.params.lat, ctrl.params.lng)) / 1000;

                    // Clear locality from params
                    ctrl.params.locality = null;

                    // If there's a key param set, check if it's for a destination or an experience type.
                    // If it's a destination, reset it to null
                    if (!!ctrl.params.key) {
                        let destination = ctrl.topDestinations.filter(function (dest) {
                            return dest.name == ctrl.params.key;
                        });
                        if (destination.length > 0) ctrl.params.key = null;
                    }

                    // Timeout is needed to update binding. Without it,
                    // the params object in parent doesn't get updated immediately but only on the next digest cycle
                    $timeout(() => {
                        ctrl.warningVisible = parseFloat(ctrl.params.distance) > ctrl.triggerWarningLevel;

                        ctrl.onParamsUpdate();
                        if (!ctrl.warningVisible) {
                            ctrl.onListingsUpdate().then(
                                function (response) {
                                    if (!response) return;

                                    ctrl.insertListings(response.data.data);
                                },
                                function (response) {

                                }
                            );
                        }
                    }, 0);
                };

                // When map has loaded, add event listeners
                ctrl.map.addListener('idle', function (event) {
                    // Insert initial listings markers
                    ctrl.insertListings(ctrl.listings);

                    // Experiences can be loaded
                    $timeout(() => {
                        ctrl.warningVisible = parseFloat(ctrl.params.distance) > ctrl.triggerWarningLevel;
                    }, 0);

                    // Listen for page changes from the parent
                    $scope.$on('pageChanged', function (event, listings) {
                        ctrl.insertListings(listings);
                    });

                    // Reload listings
                    ctrl.map.addListener('zoom_changed', function (event) {
                        reload();
                    });
                    ctrl.map.addListener('dragend', function (event) {
                        reload();
                    });
                });

                $timeout(function () {
                    google.maps.event.trigger(ctrl.map, 'resize');
                    ctrl.map.setCenter(mapCoordinates);
                }, 0);
            };

            // Trigger map load
            ctrl.triggerMapLoad = function () {
                $q.all([mapsService.mapsInitialized]).then(
                    function () {
                        $timeout(ctrl.loadMap, 0);
                    }
                );
            };
        };

        this.$onDestroy = function () {
            document.getElementById('search-map').remove();
        }
    }
]);