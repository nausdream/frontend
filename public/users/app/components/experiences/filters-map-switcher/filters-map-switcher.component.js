angular.module('users').component('filtersMapSwitcher', {
    templateUrl: '/users/app/components/experiences/filters-map-switcher/filters-map-switcher.view.html',
    controller: 'filtersMapSwitcherController',
    controllerAs: 'ctrl',
    bindings: {
        params: '=',
        listings: '=',
        onParamsUpdate: '&',
        onListingsUpdate: '&',
        currency: '@',
        filtersOpen: '='
    }
});