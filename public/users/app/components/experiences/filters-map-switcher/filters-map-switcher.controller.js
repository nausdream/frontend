angular.module('users').controller('filtersMapSwitcherController', [
    'translationService', '$location', '$cookies', 'currencyService', '$timeout',
    '$q', 'mapsService',
    function (translationService, $location, $cookies, currencyService, $timeout,
              $q, mapsService) {
        this.$onInit = function () {
            var ctrl = this;

            // Currency
            if (!ctrl.currency) ctrl.currency = $cookies.get('currency');

            // Toggle map
            ctrl.toggleMap = function (open) {
                ctrl.switcherActive = false;
                ctrl.filtersModal = false;

                ctrl.mapModal = open;
            };
            // Toggle filters
            ctrl.toggleFilters = function (open) {
                ctrl.switcherActive = false;
                ctrl.mapModal = false;

                ctrl.filtersModal = open;
            };

            // Close everything
            ctrl.closeEverything = function () {
                // Close map modal
                if (ctrl.mapModal){
                    ctrl.toggleMap(false);
                    return;
                }

                // Close filters modal and reload
                ctrl.toggleFilters(false);
                ctrl.onFiltersUpdate(true);
            };

            // Define behaviour on filters update
            ctrl.onFiltersUpdate = function (loadListings) {
                ctrl.onParamsUpdate();

                if (loadListings) ctrl.onListingsUpdate();
            }
        };
    }
]);