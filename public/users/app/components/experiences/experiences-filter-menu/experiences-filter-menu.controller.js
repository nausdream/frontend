angular.module('users').controller('experiencesFilterMenuController', [
    'translationService', '$location', '$cookies', 'currencyService', '$timeout',
    '$scope', 'mathService', '_', 'locationKeys', 'currencyValuesArray', 'topDestinations',
    function (translationService, $location, $cookies, currencyService, $timeout,
              $scope, mathService, _, locationKeys, currencyValuesArray, topDestinations) {
        this.$onInit = function () {
            var ctrl = this;

            ctrl.loading = true;
            ctrl.translations = {};
            ctrl.translations.loading = true;
            ctrl.mainFilters = true;
            ctrl.otherFilters = false;
            ctrl.viewName = 'experiences';
            ctrl.locationKeys = locationKeys;
            ctrl.adultsRange = mathService.range(1, 16);
            var pages = ['experiences', 'experience_types', 'boat_types', 'inputs'];

            // Load translations
            translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

            ctrl.topDestinations = topDestinations;
            ctrl.currencyValues = currencyValuesArray;

            currencyService.getCurrencySymbol($cookies.get('currency')).then(function (response) {
                ctrl.currencyValue = ctrl.currencyValues.filter(function (object) {
                    return object.name == $cookies.get('currency')
                })[0];

                ctrl.slider = {
                    minValue: ctrl.params.min_price,
                    maxValue: ctrl.params.max_price || ctrl.currencyValue.ceil,
                    options: {
                        autoHideLimitLabels: false,
                        id: 'slider-id',
                        floor: ctrl.currencyValue.floor,
                        ceil: ctrl.currencyValue.ceil,
                        step: ctrl.currencyValue.step,
                        noSwitching: true,
                        minRange: ctrl.currencyValue.minRange,
                        pushRange: true,
                        translate: function (value, sliderId, label) {
                            switch (label) {
                                case 'floor':
                                    return value + response;
                                case 'ceil':
                                    return value + response + '+';
                                default:
                                    return value;
                            }
                        },
                        onEnd: function (id) {
                            ctrl.params.min_price = ctrl.slider.minValue;
                            if (ctrl.currencyValue.ceil == ctrl.slider.maxValue) {
                                ctrl.params.max_price = null;
                            } else {
                                ctrl.params.max_price = ctrl.slider.maxValue;
                            }

                            // Tell parent that params have been updated
                            ctrl.onUpdate();
                        }
                    }
                };
            });

            ctrl.changeLocality = function (locality) {
                ctrl.params.lat = locality.lat;
                ctrl.params.lng = locality.lng;
                ctrl.params.distance = locality.distance;
                ctrl.params.locality = locality.name;
                ctrl.params.zoom = locality.zoom;

                // If there's a key param set, check if it's for a destination or an experience type.
                // If it's a destination, reset it to null
                if (!!ctrl.params.key) {
                    let destination = ctrl.topDestinations.filter(function (dest) {
                        return dest.name == ctrl.params.key;
                    });
                    if (destination.length > 0) ctrl.params.key = null;
                }

                // Tell parent that params have been updated
                ctrl.onUpdate();

                ctrl.dropdowns.locality = false;
            };

            ctrl.changeAdults = function (number) {
                ctrl.params.adults = number;

                // Tell parent that params have been updated
                ctrl.onUpdate();

                ctrl.dropdowns.adults = false;
            };

            ctrl.changeBabies = function () {
                // Tell parent that params have been updated
                ctrl.onUpdate();
            };

            ctrl.changeKids = function () {
                // Tell parent that params have been updated
                ctrl.onUpdate();
            };

            ctrl.changeExperienceTypes = function () {
                ctrl.experience_types = [];

                if (ctrl.params.diving) {
                    ctrl.experience_types.push('diving');
                }
                if (ctrl.params.fishing) {
                    ctrl.experience_types.push('fishing');
                }
                if (ctrl.params.trips_and_tours) {
                    ctrl.experience_types.push('trips-and-tours');
                }
                if (ctrl.params.dinners) {
                    ctrl.experience_types.push('dinners');
                }
                if (ctrl.params.aperitif) {
                    ctrl.experience_types.push('aperitif');
                }
                if (ctrl.params.romantic) {
                    ctrl.experience_types.push('romantic');
                }
                if (ctrl.params.other) {
                    ctrl.experience_types.push('other');
                }
                if (ctrl.params.nights) {
                    ctrl.experience_types.push('nights');
                }
                if (ctrl.params.luxury) {
                    ctrl.experience_types.push('luxury');
                }

                ctrl.params.experience_types = ctrl.experience_types.join(',');

                if (ctrl.params.experience_types.length == 0) ctrl.params.experience_types = null;

                // Tell parent that params have been updated
                ctrl.onUpdate();
            };

            ctrl.changeEntireBoat = function () {
                if (ctrl.params.entire_boat_bool) {
                    ctrl.params.entire_boat = 1;
                } else {
                    ctrl.params.entire_boat = 0;
                }

                // Tell parent that params have been updated
                ctrl.onUpdate();
            };

            ctrl.changeBoatTypes = function () {
                ctrl.boat_types = [];

                if (ctrl.params.catamaran) {
                    ctrl.boat_types.push('catamaran');
                }
                if (ctrl.params.motorboat) {
                    ctrl.boat_types.push('motorboat');
                }
                if (ctrl.params.sailboat) {
                    ctrl.boat_types.push('sailboat');
                }
                if (ctrl.params.rubberboat) {
                    ctrl.boat_types.push('rubberboat');
                }

                ctrl.params.boat_types = ctrl.boat_types.join(',');

                if (ctrl.params.boat_types.length == 0) ctrl.params.boat_types = null;

                // Tell parent that params have been updated
                ctrl.onUpdate();
            };

            ctrl.changeActivityTypes = function () {
                if (ctrl.params.road_stead_bool && !ctrl.params.harbor_bool) {
                    ctrl.params.road_stead = 1;
                }
                if (!ctrl.params.road_stead_bool && ctrl.params.harbor_bool) {
                    ctrl.params.road_stead = 0;
                }
                if ((ctrl.params.road_stead_bool && ctrl.params.harbor_bool) || (!ctrl.params.road_stead_bool && !ctrl.params.harbor_bool)) {
                    ctrl.params.road_stead = null;
                }

                // Tell parent that params have been updated
                ctrl.onUpdate();
            };

            ctrl.changeBoatLength = function (option) {
                if (ctrl.params.option_length == option) {
                    ctrl.params.option_length = null;
                    ctrl.params.min_length = 1;
                    ctrl.params.max_length = null;
                } else {
                    ctrl.params.option_length = option;
                    switch (option) {
                        case 1:
                            ctrl.params.min_length = 1;
                            ctrl.params.max_length = 10;
                            break;
                        case 2:
                            ctrl.params.min_length = 10;
                            ctrl.params.max_length = 20;
                            break;
                        case 3:
                            ctrl.params.min_length = 20;
                            ctrl.params.max_length = null;
                            break;
                    }
                }

                // Tell parent that params have been updated
                ctrl.onUpdate();
            };

            ctrl.changeRulesOnBoard = function () {
                ctrl.rules = [];

                if (ctrl.params.small_animals) {
                    ctrl.rules.push('animal_small');
                }
                if (ctrl.params.average_animals) {
                    ctrl.rules.push('animal_medium');
                }
                if (ctrl.params.big_animals) {
                    ctrl.rules.push('animal_large');
                }
                if (ctrl.params.alcohol) {
                    ctrl.rules.push('alcohol');
                }
                if (ctrl.params.cooking) {
                    ctrl.rules.push('cooking');
                }

                ctrl.params.rules = ctrl.rules.join(',');

                if (ctrl.params.rules.length == 0) ctrl.params.rules = null;


                // Tell parent that params have been updated
                ctrl.onUpdate();
            };

            ctrl.clearFilters = function () {
                // Clear experience type filters
                ctrl.params.diving = false;
                ctrl.params.fishing = false;
                ctrl.params.trips_and_tours = false;
                ctrl.params.dinners = false;
                ctrl.params.aperitif = false;
                ctrl.params.romantic = false;
                ctrl.params.other = false;
                ctrl.params.nights = false;
                ctrl.params.luxury = false;
                ctrl.params.experience_types = null;

                // Clear offer type filter
                ctrl.params.entire_boat_bool = false;
                ctrl.params.entire_boat = 0;

                // Clear boat type filters
                ctrl.params.catamaran = false;
                ctrl.params.motorboat = false;
                ctrl.params.sailboat = false;
                ctrl.params.rubberboat = false;
                ctrl.params.boat_types = null;

                // Clear price filters
                ctrl.slider.minValue = ctrl.currencyValue.floor;
                ctrl.params.min_price = ctrl.slider.minValue;
                ctrl.slider.maxValue = ctrl.currencyValue.ceil;
                ctrl.params.max_price = null;

                // Clear activity type filters
                ctrl.params.road_stead_bool = false;
                ctrl.params.harbor_bool = false;
                ctrl.params.road_stead = null;

                // Clear boat filter
                ctrl.params.option_length = null;
                ctrl.params.min_length = 1;
                ctrl.params.max_length = null;

                // Clear rule filters
                ctrl.params.small_animals = false;
                ctrl.params.average_animals = false;
                ctrl.params.big_animals = false;
                ctrl.params.alcohol = false;
                ctrl.params.cooking = false;
                ctrl.params.rules = null;

                // Tell parent that params have been updated
                ctrl.onUpdate();
            };

            // When other filters are being open, redraw slider
            // This avoid slider not being displayed correctly
            $scope.$watch(
                function () {
                    return ctrl.otherFilters;
                },
                function (newVal, oldVal) {
                    if (newVal != oldVal) {
                        $timeout(function () {
                            $scope.$broadcast('rzSliderForceRender');
                        });
                    }
                }
            )
        };
    }
]);