angular
    .module('users')
    .component('experiencesFilter', {
        templateUrl: '/users/app/components/experiences/experiences-filter-menu/experiences-filter-menu.html',
        controller: 'experiencesFilterMenuController',
        controllerAs: 'ctrl',
        bindings: {
            params: '=',
            onUpdate: '&',
            onClose: '&'
        }
    });