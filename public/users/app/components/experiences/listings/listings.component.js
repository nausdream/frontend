angular.module('users').component('listings', {
    templateUrl: '/users/app/components/experiences/listings/listings.view.html',
    controller: 'listingsController',
    controllerAs: 'ctrl',
    bindings: {
        listings: '<'
    }
});