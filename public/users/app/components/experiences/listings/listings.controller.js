angular
    .module('users')
    .controller('listingsController', [
            '$routeParams', '$location',
            '$cookies', 'currencyService', 'experienceTypesGroups',
            'topDestinations',
            function ($routeParams, $location,
                      $cookies, currencyService, experienceTypesGroups,
                      topDestinations) {
                var ctrl = this;

                ctrl.language = $routeParams.lang;
                ctrl.currency = $cookies.get('currency');

                // Initialize the search key array
                ctrl.experienceTypesGroups = experienceTypesGroups;
                ctrl.topDestinations = topDestinations;

                // Go to experience
                ctrl.goToExperience = function (slugUrl) {
                    $location.path(ctrl.language + '/experiences/' + slugUrl);
                };

                currencyService.getCurrencies().then(
                    function (response) {
                        ctrl.currencies = response;
                    },
                    null
                );
            }
        ]
    );

