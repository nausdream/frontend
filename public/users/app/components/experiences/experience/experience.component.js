angular.module('users').component('experience', {
    templateUrl: '/users/app/components/experiences/experience/experience.view.html',
    controller: 'experienceController',
    controllerAs: 'ctrl',
    bindings: {
        experiencePromise: '<',
        adults: '=',
        kids: '=',
        babies: '=',
        experienceDate: '=',
        toggleBookingModal: '&',
        toggleLoginModal: '&'
    }
});