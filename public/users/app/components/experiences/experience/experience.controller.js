angular.module('users').controller('experienceController',
    ['currencyService', 'fixedListService', 'translationService', 'requestService', '$q', 'mapsService', '$timeout',
        '$rootScope', 'userService', '$cookies', 'seoService', '$routeParams',
        function (currencyService, fixedListService, translationService, requestService, $q, mapsService, $timeout,
                  $rootScope, userService, $cookies, seoService, $routeParams) {
            this.$onInit = function () {
                // Make context accessible inside inner scopes
                var ctrl = this;
                ctrl.loading = true;
                ctrl.translations = {};
                ctrl.translations.loading = true;

                // Define promises
                // Currencies
                var currenciesPromise = currencyService.getCurrencies();
                // Accessories
                var accessoriesPromise = fixedListService.getFixedList('accessories');
                // Load fixed rules
                var fixedRulesPromise = fixedListService.getFixedList('fixed-rules');
                // Load fixed additional services
                var fixedAdditionalServicesPromise = fixedListService.getFixedList('fixed-additional-services');
                // Promise bound to boat promise
                ctrl.boatPromise = $q.defer();
                // Promises to signal when different parts of the page are loaded
                var accessoriesSet = $q.defer();
                var includedSet = $q.defer();
                var rulesSet = $q.defer();

                ctrl.user = userService.get();
                ctrl.language = $routeParams.lang;

                // Lists
                ctrl.includedServices = [];
                ctrl.paidServices = [];
                ctrl.fixedRules = [];
                ctrl.photos = [];

                ctrl.viewName = 'experienceComponent';
                // Load translations
                var pages = ['experience', 'experience_types', 'accessories', 'additional_services', 'rules', 'boat_types'];
                translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

                // Open contact captain modal, if logged in
                ctrl.toggleContactCaptainModal = function (open = true) {
                    if (!ctrl.user || !ctrl.user.attributes) {
                        ctrl.toggleLoginModal({onComplete: ctrl.toggleContactCaptainModal});
                        return;
                    }

                    ctrl.contactCaptainModal = !ctrl.contactCaptainModal;
                };

                // Trigger map load
                ctrl.triggerMapLoad = function () {
                    $q.all([ctrl.boatPromise, mapsService.mapsInitialized]).then(
                        function () {
                            $timeout(ctrl.loadMap, 0);
                        }
                    );
                };

                // Trigger map resize
                ctrl.triggerMapResize = function () {
                    $timeout(
                        function () {
                            google.maps.event.trigger(ctrl.map, 'resize');
                            ctrl.map.setCenter(ctrl.mapCoordinates);
                        }, 0);
                };

                // Load map
                ctrl.loadMap = function () {
                    // TODO get lat and lng from ip
                    ctrl.mapCoordinates = {lat: ctrl.lat || 39.211406, lng: ctrl.lng || 9.113607};
                    ctrl.map = new google.maps.Map(document.getElementById('map'), {
                        center: ctrl.mapCoordinates,
                        zoomControl: true,
                        streetViewControl: false,
                        mapTypeControl: false,
                        fullscreenControl: false,
                        disableDoubleClickZoom: true,
                        zoom: 8
                    });
                    var marker = new google.maps.Marker({
                        position: ctrl.mapCoordinates,
                        draggable: false,
                        map: ctrl.map
                    });

                    $timeout(function () {
                        ctrl.map.panTo(ctrl.mapCoordinates);
                    }, 100);

                    ctrl.triggerMapResize();
                };

                // Open/close map
                ctrl.toggleMap = function () {
                    ctrl.isMapOpen = !ctrl.isMapOpen;

                    ctrl.triggerMapResize();
                };

                // Set included arrays from experience response
                ctrl.setIncluded = function (experienceResponse, fixedAdditionalServices) {
                    var included = experienceResponse.data.included;

                    if (included) {
                        var availabilities = [];
                        var periods = [];

                        included.forEach(function (object) {
                            switch (object.type) {
                                // Fixed and non-fixed services are treated the same, except for the naming part
                                // Fallthrough is intentional
                                case 'fixed-additional-service-prices':
                                    object.attributes.name = fixedAdditionalServices.filter(function (service) {
                                        return service.id == object.relationships.fixed_additional_service.id;
                                    })[0].attributes.name;
                                    object.fixed = true;
                                case 'additional-services':
                                    if (object.attributes.price == 0) {
                                        ctrl.includedServices.push(object);
                                    } else {
                                        ctrl.paidServices.push(object);
                                    }
                                    break;
                                case 'photos':
                                    if (object.attributes.is_cover) ctrl.coverPhoto = object;
                                    else {
                                        ctrl.photos.push(object)
                                    }
                                    break;
                                case 'experience-availabilities':
                                    availabilities.push(object);
                                    break;
                                case 'periods':
                                    periods.push(object);
                                    break;
                            }
                        });
                    }

                    // Signal controller that this part has ended
                    includedSet.resolve();
                };

                // Set experience attributes and relationships
                ctrl.experiencePromise.then(
                    function (response) {
                        ctrl.experienceId = response.data.data.id;
                        var attributes = response.data.data.attributes;

                        // Set attributes from experience
                        ctrl.title = attributes.title;
                        ctrl.location = attributes.departure_port;
                        ctrl.experienceType = 'experience_types.' + attributes.type;
                        ctrl.description = attributes.description;
                        ctrl.departurePort = attributes.departure_port;
                        ctrl.arrivalPort = attributes.arrival_port;
                        ctrl.departureTime = attributes.departure_time;
                        ctrl.arrivalTime = attributes.arrival_time;
                        ctrl.rules = attributes.rules;
                        ctrl.currency = attributes.currency;
                        ctrl.captainName = attributes.captain_first_name;

                        // Get related boat
                        var boatPromise = requestService.getRelatedSingleAsync('experiences', ctrl.experienceId, 'boat');
                        boatPromise.then(
                            function (response) {
                                var boatAttributes = response.data.data.attributes;

                                ctrl.boatName = boatAttributes.name;
                                ctrl.boatType = 'boat_types.' + boatAttributes.type;
                                ctrl.boatModel = boatAttributes.model;
                                ctrl.boatLength = boatAttributes.length;
                                ctrl.dockingPlace = boatAttributes.docking_place;
                                ctrl.lat = boatAttributes.lat;
                                ctrl.lng = boatAttributes.lng;

                                ctrl.boatPromise.resolve(response);
                            },
                            null
                        );
                    },
                    null
                );

                // Set currencies
                currenciesPromise.then(
                    function (response) {
                        ctrl.currencies = response;
                    }
                );

                // Set accessories
                $q.all([accessoriesPromise, ctrl.boatPromise.promise]).then(
                    function (responses) {
                        var accessoriesResponse = responses[0];
                        var boatResponse = responses[1];

                        ctrl.accessories = accessoriesResponse.data.data;

                        var boatAccessories = boatResponse.data.data.relationships.accessories;
                        if (boatAccessories) {
                            boatAccessories.data.forEach(function (boatAccessory) {
                                // Select accessories in the entire accessories list that are also in the boat list
                                // (we assume that every boat accessory is also in the accessories list from the database)
                                ctrl.accessories.filter(function (accessory) {
                                    return accessory.id == boatAccessory.id;
                                })[0].selected = true;
                            });
                        }

                        // Signal controller that this part has ended
                        accessoriesSet.resolve();
                    },
                    null
                );

                // Set fixed rules
                $q.all([fixedRulesPromise, ctrl.experiencePromise]).then(
                    function (responses) {
                        var rulesResponse = responses[0];
                        var experienceResponse = responses[1];

                        var rules = rulesResponse.data.data;
                        var experienceRules = experienceResponse.data.data.relationships.rules;

                        if (experienceRules) {
                            ctrl.fixedRules = experienceRules.data;
                            // Select rules in the experience rules list that are also in the rules list from the rules table
                            ctrl.fixedRules.forEach(function (experienceRule) {
                                experienceRule.name = rules.filter(function (rule) {
                                    return rule.id == experienceRule.id
                                })[0].attributes.name;
                            })
                        }

                        // Signal controller that this part has ended
                        rulesSet.resolve();
                    },
                    null
                );

                // Experience included list
                $q.all([ctrl.experiencePromise, fixedAdditionalServicesPromise]).then(
                    function (responses) {
                        var experienceResponse = responses[0];
                        ctrl.fixedAdditionalServices = responses[1].data.data;

                        ctrl.setIncluded(experienceResponse, ctrl.fixedAdditionalServices);
                    }
                );

                // When everything has been done, finish loading
                $q.all([ctrl.boatPromise.promise, accessoriesSet.promise, includedSet.promise, currenciesPromise, fixedRulesPromise]).then(
                    function () {
                        //TODO: add first name of the captain as ogAuthor
                        //seoService.setOgAuthor();
                        //TODO: add translated experience type as ogSection
                        //seoService.setOgSection();
                        // TODO if cover photo not present set first photo as ogUrl
                        if (ctrl.coverPhoto) {
                            var ogImage = ctrl.coverPhoto.attributes.url;
                        }
                        seoService.setOgImage(ogImage);
                        ctrl.loading = false;
                    },
                    null
                );

                // Set /unset user in controller when he logs in/out
                $rootScope.$on('userLoggedIn', function (event, user) {
                    ctrl.user = user;
                });
                $rootScope.$on('userLoggedOut', function (event) {
                    ctrl.user = null;
                });
            };
        }
    ]
);