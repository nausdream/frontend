/**
 * Created by lucas on 23/08/2017.
 *
 * options object example:
 *      color: 'blue' || 'orange' (def. blue)
 *      shape-square: true || false (def. null)
 *      small: true || false (def. null)
 *      inverse: true || false (def. null)
 *      border-small: true || false (def. null)
 *      bold: true || false (def. null)
 */
angular.module('users').component('nausBtn', {
    templateUrl: '/users/app/components/naus-btn/naus-btn.view.html',
    controller: 'nausBtnController',
    controllerAs: 'ctrl',
    transclude: true,
    bindings: {
        onClick: '&',
        options: '=',
        url: '<',
        customClasses: '<'
    }
});