/**
 * Created by lucas on 23/08/2017.
 */
angular.module('users').controller('nausBtnController',
    function () {
        this.$onInit = function () {
            var ctrl = this;

            // Options
            ctrl.opts = ctrl.options || {};

            if (!ctrl.opts.color) {
                ctrl.opts.color = 'blue';
            }
            if (ctrl.opts.uppercase == null) {
                ctrl.opts.uppercase = true;
            }

            // Url
            ctrl.link = ctrl.url || '#';

            // Action
            ctrl.click = function () {
                if (ctrl.options && ctrl.options.disabled) return;

                ctrl.onClick();
            };

            // CSS Classes
            ctrl.classes =
                [
                    'color-' + ctrl.opts.color,
                    {
                        'shape-square': ctrl.opts.squared,
                        'shape-circle': ctrl.opts.circle,
                        'small': ctrl.opts.small,
                        'inverse': ctrl.opts.inverse,
                        'text-uppercase': ctrl.opts.uppercase
                    }
                ];

            // Concat custom classes
            ctrl.classes = ctrl.classes.concat(ctrl.customClasses);
        }
    });