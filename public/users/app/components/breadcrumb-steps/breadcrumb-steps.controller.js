angular
    .module('users')
    .controller('breadcrumbStepsController',
        ['customPartialLoader', 'translationService',
            function (customPartialLoader, translationService) {
                this.$onInit = function () {
                    var ctrl = this;
                    ctrl.viewName = 'breadcrumb';

                    // Load translations
                    var pages = ['captain_form_breadcrumb'];
                    translationService.translate(pages, ctrl.viewName);
                };
            }
        ]
    );