angular.module('users').component('breadcrumbSteps', {
    templateUrl: '/users/app/components/breadcrumb-steps/breadcrumb-steps.view.html',
    controller: 'breadcrumbStepsController',
    bindings: {
        step: '@',
    }
});