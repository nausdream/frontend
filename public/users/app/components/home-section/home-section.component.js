angular.module('users').component('homeSection', {
    templateUrl: '/users/app/components/home-section/home-section.view.html',
    controller: 'homeSectionController',
    transclude: true,
    bindings: {
        title: '@',
        padding: '@'
    }
});