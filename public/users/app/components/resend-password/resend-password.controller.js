angular.module('users').controller('resendPasswordController', [
        '$rootScope', 'validationService', 'requestService', 'translationService',
        function ($rootScope, validationService, requestService, translationService) {
            this.$onInit = function () {
                // Make context accessible inside inner scopes
                var ctrl = this;
                ctrl.translations = {};
                ctrl.translations.loading = true;
                ctrl.viewName = 'resendPassword';
                ctrl.errors = {};

                // Load translations
                var pages = ['inputs', 'login', 'errors'];
                translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

                // Send email
                ctrl.send = function () {
                    if (ctrl.form.$valid && !ctrl.loading) {
                        ctrl.loading = true;
                        requestService.resendPassword(ctrl.email).then(
                            function (response) {
                                ctrl.success = true;
                                ctrl.loading = false;
                            },
                            function (response) {
                                // Failure
                                validationService.setBackendErrors(ctrl.form, ctrl.errors, response.data.errors);
                                ctrl.loading = false;
                            }
                        );
                    } else {
                        validationService.setFrontendErrors(ctrl.form, ctrl.errors)
                    }
                };

                ctrl.validateField = function (field) {
                    validationService.validateField(ctrl.form[field], ctrl.errors);
                };

                ctrl.resetField = function (field) {
                    validationService.resetField(field, ctrl.errors, ctrl.form);
                };
            }
        }
    ]
);