angular.module('users').component('resendPassword', {
    templateUrl: '/users/app/components/resend-password/resend-password.view.html',
    controller: 'resendPasswordController',
    controllerAs: 'ctrl',
    bindings: {
        email: '@',
        onClose: '&'
    }
});