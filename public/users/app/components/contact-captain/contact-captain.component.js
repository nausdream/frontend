angular.module('users').component('contactCaptain', {
    templateUrl: '/users/app/components/contact-captain/contact-captain.view.html',
    controller: 'contactCaptainController',
    controllerAs: 'ctrl',
    bindings: {
        experiencePromise: '<',
        onClose: '&',
        adults: '=',
        kids: '=',
        babies: '=',
        experienceDate: '='
    }
});