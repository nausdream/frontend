angular.module('users').controller('contactCaptainController', [
    'translationService', '__env', '$location', 'requestService', '$cookies', 'currencyService', '$rootScope',
    'userService', '$q', 'mathService', 'fixedListService', 'dateService', 'validationService',
    '$scope', '$routeParams', 'adwordsService',
    function (translationService, __env, $location, requestService, $cookies, currencyService, $rootScope,
              userService, $q, mathService, fixedListService, dateService, validationService,
              $scope, $routeParams, adwordsService) {
        this.$onInit = function () {
            var ctrl = this;

            ctrl.language = $routeParams.lang;

            ctrl.user = userService.get();

            ctrl.modals = {};
            ctrl.dropdowns = {};

            // Validation bootstrap
            ctrl.errors = {};
            ctrl.formName = 'form';

            // Init translations object
            ctrl.translations = {};
            ctrl.translations.loading = true;
            ctrl.viewName = 'contactCaptain';
            // Load translations
            var pages = [
                'booking', 'inputs', 'errors'
            ];
            translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

            // Update hosts limits for inputs
            ctrl.updateHosts = function () {
                ctrl.maxAdults = ctrl.experienceSeats - ctrl.kids - ctrl.babies;
                ctrl.maxKids = Math.min(ctrl.experienceKids, ctrl.experienceSeats - ctrl.adults - ctrl.babies);
                ctrl.maxBabies = Math.min(ctrl.experienceBabies, ctrl.experienceSeats - ctrl.adults - ctrl.kids);
                ctrl.adultsRange = mathService.range(1, ctrl.maxAdults);
            };

            // Set calendar availabilites from experience response
            ctrl.setAvailabilities = function (experienceResponse) {
                if (experienceResponse.data.relationships.experience_availabilities) {
                    ctrl.availabilities = angular.copy(experienceResponse.included.filter(function (included) {
                        return included.type == 'experience-availabilities';
                    }));
                }
            };

            // Set calendar available week days from experience response
            ctrl.setAvailableDaysOfWeek = function (experienceResponse) {
                ctrl.availableDaysOfWeek = [];

                var attributes = experienceResponse.data.attributes;

                ctrl.availableDaysOfWeek.push(
                    attributes.sunday,
                    attributes.monday,
                    attributes.tuesday,
                    attributes.wednesday,
                    attributes.thursday,
                    attributes.friday,
                    attributes.saturday
                );
            };

            // Toggle warning based on message content
            ctrl.messageValidation = function () {
                var emailRegex = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
                var urlRegex = /[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,4}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/g;
                var phoneRegex = /[0-9]{6,256}/g;

                // Runs all the validations
                ctrl.warning =
                    emailRegex.test(ctrl.message) ||
                    urlRegex.test(ctrl.message) ||
                    phoneRegex.test(ctrl.message);
            };

            // Validation
            // Clear field errors from ctrl.errors
            ctrl.resetField = function (field) {
                validationService.resetField(field, ctrl.errors, ctrl[ctrl.formName]);
            };

            // Set new adult: close dropdown, clear errors on field, set ctrl.adults, update new maximums
            ctrl.setAdult = function (newAdults) {
                ctrl.adults = newAdults;
                ctrl.dropdowns.adults = false;
                ctrl.errors.hosts = null;
                ctrl.updateHosts();
            };
            // Set new kid or baby
            ctrl.setOtherHost = function () {
                ctrl.errors.hosts = null;
                ctrl.updateHosts();
            };

            // Close success/error modals and its own
            ctrl.toggleModals = function (success = false) {
                if (success) {
                    ctrl.onClose();
                    ctrl.modals.success = false;
                    return;
                }

                ctrl.modals.error = false;
            };

            // Validate field
            ctrl.validateField = function (field) {
                validationService.validateField(ctrl[ctrl.formName][field], ctrl.errors);
            };

            // On blur, validate
            ctrl.onBlur = function (field) {
                if (field) {
                    ctrl.validateField(field);
                }
            };

            // Check if estimation request can be sent
            ctrl.areFieldsValid = function () {
                // HOSTS
                var hosts = ctrl.adults + ctrl.kids + ctrl.babies;
                if (hosts > ctrl.experienceSeats) return false;
                if (ctrl.kids > ctrl.experienceKids) return false;
                if (ctrl.babies > ctrl.experienceBabies) return false;

                // DATE
                if (!ctrl.experienceDate) return false;
                if (ctrl.errors.experience_date) return false;

                return true;
            };

            // Send message
            ctrl.sendMessage = function (conversationId) {

                var relationships = {conversation: {data: {type: 'conversations', id: conversationId}}};
                requestService.postSingle('messages', {message: ctrl.message}, relationships).then(
                    function () {
                        // All good! Show success modal
                        ctrl.modals.success = true;

                        // Send adwords conversion
                        adwordsService.sendConversion('chat-captain');
                    },
                    function () {
                        // Something has gone wrong. Validate from BE
                        validationService.setBackendErrors(ctrl[ctrl.formName], ctrl.errors, response.data.errors);
                    }
                )
            };

            // Post booking binding it to a conversation
            ctrl.createConversation = function () {
                var object = {
                    request_date: dateService.getDateString(ctrl.experienceDate),
                    adults: ctrl.adults,
                    kids: ctrl.kids,
                    babies: ctrl.babies
                };

                var relationships = {experience: {data: {type: 'experiences', id: ctrl.experienceId}}};

                // Post new conversation and send message then
                return requestService.postSingle('conversations', object, relationships);
            };

            // Do main action (create conversation and then send message)
            ctrl.do = function () {
                // Check if everything's valid
                if (!ctrl.areFieldsValid()) return;

                // Check form
                if (!ctrl[ctrl.formName].$valid) {
                    validationService.setFrontendErrors(ctrl[ctrl.formName], ctrl.errors);
                    return;
                }

                ctrl.createConversation().then(
                    function (response) {
                        ctrl.sendMessage(response.data.data.id);
                    },
                    function (response) {
                        // Conversation not created: if a previous already exists,
                        // send message
                        if (response.data.errors[0].code == 'conversation_already_exists') {
                            var oldConversationId = response.data.meta.conversation_id;
                            ctrl.sendMessage(oldConversationId);
                        }
                    }
                )
            };

            // LOAD DATA FOR THE FIRST TIME
            ctrl.experiencePromise.then(
                function (response) {
                    ctrl.experienceId = response.data.data.id;
                    var attributes = response.data.data.attributes;
                    ctrl.experienceSeats = attributes.seats;
                    ctrl.experienceKids = attributes.kids;
                    ctrl.experienceBabies = attributes.babies;

                    ctrl.setAvailabilities(response.data);
                    ctrl.setAvailableDaysOfWeek(response.data);
                    ctrl.updateHosts();
                },
                null
            )
        };
    }
]);