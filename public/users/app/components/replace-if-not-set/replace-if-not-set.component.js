angular.module('users').component('replaceIfNotSet', {
    templateUrl: '/users/app/components/replace-if-not-set/replace-if-not-set.view.html',
    controller: 'replaceIfNotSetController',
    controllerAs: 'ctrl',
    bindings: {
        replaceModel: '=',
        replacePlaceholder: '<'
    }
});