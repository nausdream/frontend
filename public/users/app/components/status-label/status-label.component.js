angular.module('users').component('statusLabel', {
    templateUrl: '/users/app/components/status-label/status-label.view.html',
    controller: 'statusLabelController',
    controllerAs: 'ctrl',
    bindings: {
        statusName: '=',
        modelName: '@'
    }
});