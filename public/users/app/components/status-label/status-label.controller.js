angular.module('users').controller('statusLabelController', [
    'translationService',
    function (translationService) {
        this.$onInit = function () {
            var ctrl = this;

            ctrl.viewName = 'status';
            ctrl.translations = {};
            ctrl.translations.loading = true;

            // Load translations
            var pages = ['status'];
            translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

            // Define status colors
            ctrl.colors = {
                conversation_new: 'blue',
                conversation_read: 'grey',
                boat_pending: 'orange',
                boat_unfinished: 'grey',
                boat_accepted: 'green',
                boat_rejected: 'red',
                boat_accepted_conditionally: 'orange',
                booking_accepted_captain: 'orange',
                booking_accepted: 'green',
                booking_pending: 'orange',
                booking_rejected: 'red',
                booking_paid: 'green'
            };
        }
    }]);