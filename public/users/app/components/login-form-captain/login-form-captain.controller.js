angular
    .module('users')
    .controller('loginFormCaptainController', [
            'loginService', '$cookies', '$scope', '$rootScope', '__env', 'validationService',
            function (loginService, $cookies, $scope, $rootScope, __env, validationService) {
                var ctrl = this;
                ctrl.errors = {};
                ctrl.loading = false;
                ctrl.email = '';
                ctrl.password = '';

                if (ctrl.redirected) ctrl.errors.error = {code: 'not_logged_in'};

                ctrl.login = function () {
                    if (ctrl.form.$valid && !ctrl.loading) {
                        ctrl.loading = true;
                        loginService.loginPwd(ctrl.email, ctrl.password).then(
                            function (response) {
                                // Set cookies and then grab user data from Backend
                                loginService.setCookies(response);
                                loginService.login().then(
                                    function (response) {
                                        ctrl.onComplete();
                                    },
                                    function (response) {
                                        console.log('Backend bug. User can authenticate but it cannot do a GET on himself');
                                    }
                                );
                            },
                            function (response) {
                                // Failure
                                validationService.setBackendErrors(ctrl.form, ctrl.errors, response.data.errors);
                                ctrl.loading = false;
                            }
                        );
                    } else {
                        validationService.setFrontendErrors(ctrl.form, ctrl.errors)
                    }
                };

                ctrl.validateField = function (field) {
                    validationService.validateField(ctrl.form[field], ctrl.errors);
                };

                ctrl.resetField = function (field) {
                    validationService.resetField(field, ctrl.errors, ctrl.form);
                };
            }
        ]
    );