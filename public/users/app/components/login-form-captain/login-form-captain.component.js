angular.module('users').component('loginFormCaptain', {
        templateUrl: '/users/app/components/login-form-captain/login-form-captain.view.html',
        controller: 'loginFormCaptainController',
        controllerAs: 'ctrl',
        bindings: {
            onComplete: '&',
            redirected: '<'
        }
    }
);