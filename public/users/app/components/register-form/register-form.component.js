angular.module('users').component('registerForm', {
    templateUrl: '/users/app/pages/register-form/register-form.view.html',
    controller: 'registerFormController',
    controllerAs: 'ctrl',
    bindings: {}
});