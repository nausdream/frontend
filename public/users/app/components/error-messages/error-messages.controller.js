angular.module('users').controller('errorMessagesController',
    ['translationService',
        function (translationService) {
            this.$onInit = function () {
                var ctrl = this;

                if (!ctrl.translationName) ctrl.translationName = ctrl.fieldName;

                // Load translations
                var pages = ['errors'];
                translationService.translate(['errors'], 'error');
            };
        }
    ]
);