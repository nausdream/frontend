angular.module('users').component('errorMessages', {
    templateUrl: '/users/app/components/error-messages/error-messages.view.html',
    controller: 'errorMessagesController',
    bindings: {
        fieldName: '@',
        translationName: '@',
        controllerErrors: '=',
        page: '@'
    }
});