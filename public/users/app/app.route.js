angular
    .module('users')
    .config([
        '$routeProvider',
        function ($routeProvider) {
        $routeProvider
            .when("/:lang/login", {
                templateUrl: "/users/app/pages/login/login-page.view.html",
                controller: "loginPageController",
                controllerAs: "ctrl",
                reloadOnSearch: false
            })
            .when("/:lang/register", {
                templateUrl: "/users/app/pages/register-form/register-form.view.html",
                controller: "registerFormController",
                controllerAs: "ctrl"
            })
            .when("/:lang/new-captain", {
                templateUrl: "/users/app/pages/captain-form/captain-form.view.html",
                controller: "captainFormController",
                controllerAs: "ctrl",
                reloadOnSearch: true,
                middleware: ['logged-in']
            })
            .when("/:lang/dashboard", {
                templateUrl: "/users/app/pages/dashboard/dashboard.view.html",
                controller: "dashboardController",
                controllerAs: "ctrl",
                middleware: ['logged-in'],
                reloadOnSearch: true,
            })
            .when("/:lang/profile/edit", {
                templateUrl: "/users/app/pages/profile-edit/profile-edit-page.view.html",
                controller: "profileEditPageController",
                controllerAs: "ctrl",
                middleware: ['logged-in']
            })
            .when("/:lang/verify-email", {
                templateUrl: "/users/app/pages/email-verified/email-verified.view.html",
                controller: "emailVerifiedController",
                controllerAs: "ctrl"
            })
            .when("/:lang/conversations", {
                templateUrl: "/users/app/pages/conversations/conversations/conversations-page.view.html",
                controller: "conversationsPageController",
                controllerAs: "ctrl",
                middleware: ['logged-in'],
                reloadOnSearch: false
            })
            .when("/:lang/conversations/:conversationId", {
                templateUrl: "/users/app/pages/conversations/conversation/conversation-page.view.html",
                controller: "conversationPageController",
                controllerAs: "ctrl",
                middleware: ['logged-in'],
                reloadOnSearch: false
            })
            .when("/:lang/boats", {
                templateUrl: "/users/app/pages/boats/boats/boats-page.view.html",
                controller: "boatsPageController",
                controllerAs: "ctrl",
                middleware: ['logged-in'],
                reloadOnSearch: false
            })
            .when("/:lang/boats/new", {
                templateUrl: "/users/app/pages/boats/new-boat/new-boat-page.view.html",
                controller: "newBoatPageController",
                controllerAs: "ctrl",
                middleware: ['logged-in'],
                reloadOnSearch: false
            })
            .when("/:lang/boats/:boatId?", {
                templateUrl: "/users/app/pages/boats/boat/boat-page.view.html",
                controller: "boatPageController",
                controllerAs: "ctrl",
                middleware: ['logged-in'],
                reloadOnSearch: false
            })
            .when("/:lang/boats/:boatId?/edit", {
                templateUrl: "/users/app/pages/boats/edit-boat/edit-boat-page.view.html",
                controller: "editBoatPageController",
                controllerAs: "ctrl",
                middleware: ['logged-in'],
                reloadOnSearch: false
            })
            .when("/:lang/experiences/:slugUrl", {
                templateUrl: "/users/app/pages/experiences/experience/experience-page.view.html",
                controller: "experiencePageController",
                controllerAs: "ctrl"
            })
            .when("/:lang/invalid-token", {
                templateUrl: "/users/app/pages/invalid-token/invalid-token.view.html",
                controller: "invalidTokenController",
                controllerAs: "ctrl"
            })
            .when("/:lang/restore-password", {
                templateUrl: "/users/app/pages/restore-password/restore-password-page.view.html",
                controller: "restorePasswordPageController",
                controllerAs: "ctrl"
            })
            .when("/:lang/experiences", {
                templateUrl: "/users/app/pages/experiences/experiences/experiences-page.view.html",
                controller: "experiencesController",
                controllerAs: "ctrl",
                reloadOnSearch: false
            })
            .when("/:lang/bookings", {
                templateUrl: "/users/app/pages/bookings/bookings/bookings-page.view.html",
                controller: "bookingsPageController",
                controllerAs: "ctrl",
                middleware: ['logged-in'],
                reloadOnSearch: false
            })
            .when("/:lang/bookings/:bookingId", {
                templateUrl: "/users/app/pages/bookings/booking/booking-page.view.html",
                controller: "bookingPageController",
                controllerAs: "ctrl",
                middleware: ['logged-in'],
                reloadOnSearch: false
            })
            .when("/:lang/bookings/:bookingId/:action", {
                templateUrl: "/users/app/pages/bookings/booking-pay/booking-pay-page.view.html",
                controller: "bookingPayPageController",
                controllerAs: "ctrl",
                middleware: ['logged-in']
            })
            .when("/:lang/offsite-bookings/:bookingId/confirm", {
                templateUrl: "/users/app/pages/offsite-bookings/offsite-booking-confirm/offsite-booking-confirm-page.view.html",
                controller: "offsiteBookingConfirmPageController",
                controllerAs: "ctrl"
            })
            .when("/:lang/offsite-bookings/:bookingId/pay", {
                templateUrl: "/users/app/pages/offsite-bookings/offsite-booking-pay/offsite-booking-pay-page.view.html",
                controller: "offsiteBookingPayPageController",
                controllerAs: "ctrl"
            })
            .when("/:lang/offsite-bookings/:bookingId/:action", {
                templateUrl: "/users/app/pages/offsite-bookings/offsite-booking-callback/offsite-booking-callback-page.view.html",
                controller: "offsiteBookingCallbackPageController",
                controllerAs: "ctrl"
            })
            .when("/:lang/not-authorized", {
                templateUrl: "/users/app/pages/not-authorized/not-authorized.view.html",
                controller: "notAuthorizedController",
                controllerAs: "ctrl"
            })
            .when("/:lang/error", {
                templateUrl: "/users/app/pages/error/error.view.html",
                controller: "errorController",
                controllerAs: "ctrl"
            })
            .when("/:lang/how-it-works-captain", {
                templateUrl: "/users/app/pages/support/howitworks-captain.view.html",
                controller: "supportController",
                controllerAs: "ctrl"
            })
            .when("/:lang/how-it-works-guest", {
                templateUrl: "/users/app/pages/support/howitworks-guest.view.html",
                controller: "supportController",
                controllerAs: "ctrl"
            })
            .when("/:lang/assistance", {
                templateUrl: "/users/app/pages/support/assistance.view.html",
                controller: "supportController",
                controllerAs: "ctrl"
            })
            // Team page
            /*
            .when("/:lang/contacts", {
                templateUrl: "/users/app/pages/support/contacts.view.html",
                controller: "supportController",
                controllerAs: "ctrl"
            })
            */
            .when("/:lang/terms", {
                templateUrl: "/users/app/pages/support/terms.view.html",
                controller: "supportController",
                controllerAs: "ctrl"
            })
            .when("/:lang/cookies", {
                templateUrl: "/users/app/pages/support/cookies.view.html",
                controller: "supportController",
                controllerAs: "ctrl"
            })
            .when("/:lang/privacy", {
                templateUrl: "/users/app/pages/support/privacy.view.html",
                controller: "supportController",
                controllerAs: "ctrl"
            })
            .when("/:lang/not-found", {
                templateUrl: "/users/app/pages/not-found/not-found.view.html",
                controller: "notFoundController",
                controllerAs: "ctrl"
            })
            .when("/:lang?/", {
                templateUrl: "/users/app/pages/home/home.view.html",
                controller: "homeController",
                controllerAs: "ctrl",
                reloadOnSearch: false,
                middleware: ['home-lang']
            })
            .otherwise({
                redirectTo: '/not-found'
            });
    }]);