angular.module('users').controller('notFoundController',
    ['seoService', '$rootScope', 'translationService', 'requestService', '__env', '$window', 'statusCodeService', '$routeParams', '$location',
        function (seoService, $rootScope, translationService, requestService, __env, $window, statusCodeService, $routeParams, $location) {
            // Set status code
            statusCodeService.set(404);

            // Make context accessible inside inner scopes
            var ctrl = this;
            ctrl.translations = {};
            ctrl.translations.loading = true;
            ctrl.viewName = 'not-found';
            var pages = ['not_found'];

            ctrl.language = $routeParams.lang;

            // Set SEO
            seoService.setMetaTranslations(ctrl.viewName);

            // Load translations
            translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);
            
            ctrl.backHome = function () {
                $location.path('/' + ctrl.language + '/');
            }
        }
    ]
);