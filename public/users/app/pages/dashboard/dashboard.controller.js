angular
    .module('users')
    .controller('dashboardController', [
            'requestService', 'userService', 'translationService', '$location','$routeParams',
            function (requestService, userService, translationService, $location, $routeParams) {
                var ctrl = this;
                ctrl.loading = true;

                ctrl.language = $routeParams.lang;

                // Select single conversation
                ctrl.selectConversation = function (id) {
                    ctrl.makingRequest = true;

                    $location.path('/' + ctrl.language + '/conversations/' + id);
                };
                // Select single boat
                ctrl.selectBoat = function (id) {
                    ctrl.makingRequest = true;

                    $location.path('/' + ctrl.language + '/boats/' + id);
                };
                // Select single boat
                ctrl.selectBooking = function (id) {
                    ctrl.makingRequest = true;

                    $location.path('/' + ctrl.language + '/bookings/' + id);
                };

                // Individual loaders
                ctrl.loaders = {
                    boats: true,
                    experiences: true,
                    conversations: true,
                    bookings: true
                };

                // Init translations object
                ctrl.translations = {};
                ctrl.translations.loading = true;
                ctrl.viewName = 'dashboard';

                // Load user
                ctrl.user = angular.copy(userService.get());
                ctrl.isCaptain = ctrl.user.attributes.is_captain;
                // Finish loading
                ctrl.loading = false;

                // Load translations
                var pages = [
                    'user', 'conversation'
                ];
                translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

                // Init promises
                var perPage = 50;

                // Load boats and experiences
                if (ctrl.user.attributes.is_captain) {
                    var boatsPromise = requestService.getRelatedSingleAsync('captains', ctrl.user.id, 'boats', {
                        editing: true,
                        per_page: perPage
                    });
                    var experiencesPromise = requestService.getRelatedSingleAsync('captains', ctrl.user.id, 'experiences', {
                        editing: true,
                        per_page: perPage
                    });

                    boatsPromise.then(
                        function (response) {
                            ctrl.boats = response.data.data;
                            ctrl.loaders.boats = false;
                        },
                        function (response) {
                            // TODO Manage backend bug
                        }
                    );

                    experiencesPromise.then(
                        function (response) {
                            ctrl.experiences = response.data.data;
                            ctrl.loaders.experiences = false;
                        },
                        function (response) {
                            // TODO Manage backend bug
                        }
                    );
                }

                var conversationsPromise = requestService.getRelatedSingleAsync('users', ctrl.user.id, 'conversations', {per_page: perPage});
                var bookingsPromise = requestService.getRelatedSingleAsync('captains', ctrl.user.id, 'bookings', {per_page: perPage});

                // Load conversations
                conversationsPromise.then(
                    function (response) {
                        ctrl.conversations = response.data.data;
                        ctrl.loaders.conversations = false;
                    },
                    function (response) {
                        // TODO Manage backend bug
                    }
                );

                // Load bookings
                bookingsPromise.then(
                    function (response) {
                        ctrl.bookings = response.data.data;
                        ctrl.loaders.bookings = false;
                    },
                    function (response) {
                        // TODO Manage backend bug
                    }
                )


            }
        ]
    );