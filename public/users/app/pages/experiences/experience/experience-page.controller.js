angular.module('users').controller('experiencePageController',
    ['$routeParams', 'requestService', 'seoService', '$cookies', 'dateService', 'userService', '$rootScope', '$location',
        'adwordsService', '$window', 'translationService',
        function ($routeParams, requestService, seoService, $cookies, dateService, userService, $rootScope, $location,
        adwordsService, $window, translationService) {
            // Make context accessible inside inner scopes
            var ctrl = this;
            ctrl.loading = true;

            var userCurrency = $cookies.get('currency');
            var slugUrl = $routeParams.slugUrl;

            ctrl.user = userService.get();

            // Load translations
            ctrl.viewName = 'experiencePage';
            var pages = ['experience', 'experience_types', 'accessories', 'additional_services', 'rules', 'boat_types', 'booking', 'inputs', 'footer'];
            translationService.translate(pages, ctrl.viewName);

            // Closed by default
            ctrl.bookingModal = false;
            ctrl.loginModal = false;
            ctrl.contactCaptainModal = false;

            // TODO Get experience date from url
            ctrl.adults = $location.search().adults;
            if (ctrl.adults) ctrl.adults = parseInt(ctrl.adults);
            ctrl.kids = parseInt($location.search().kids) || 0;
            ctrl.babies = parseInt($location.search().babies) || 0;
            ctrl.experienceDate = new Date(new Date().getTime() + 24 * 60 * 60 * 1000);

            // Build new estimation promise
            ctrl.buildEstimationPromise = function () {
                // Convert date to string
                var requestDate = dateService.getDateString(ctrl.experienceDate);

                var hosts = ctrl.adults ? (ctrl.adults + ctrl.kids + ctrl.babies) : null;

                var parameters = {
                    experience_id: ctrl.experienceId,
                    hosts: hosts,
                    request_date: requestDate,
                    currency: userCurrency
                };

                return requestService.getListAsync('estimations', parameters);
            };

            // Open booking popup component
            ctrl.toggleBookingModal = function (open = true) {
                // If user is not logged in, open login modal instead
                if (open) {
                    if (!ctrl.user || !ctrl.user.attributes) {
                        ctrl.toggleLoginModal(ctrl.toggleBookingModal);
                        return;
                    }
                }

                ctrl.bookingModal = !ctrl.bookingModal;
            };

            // Close success/errors modals
            ctrl.toggleModals = function (open = true, success = false) {
                if (success) {
                    ctrl.modals.success = !ctrl.modals.success;

                    // When closing success modal, close booking popup too
                    if (!open && ctrl.onClose) {
                        ctrl.onClose({update: true, newBookingId: ctrl.newBookingId});
                    }

                    return;
                }
                ctrl.modals.error = !ctrl.modals.error;
            };

            // Open login modal if user is not logged in
            ctrl.toggleLoginModal = function (onComplete) {
                if (onComplete) {
                    ctrl.onComplete = onComplete;
                }

                ctrl.loginModal = !ctrl.loginModal;
            };

            var seoPromise = requestService.getExperienceFromSlugUrl(slugUrl);
            seoPromise.then(
                function (response) {
                    var attributes = response.data.data.attributes;
                    ctrl.experienceId = response.data.data.relationships.experience.data.id;
                    // Set meta tags
                    seoService.setMetaTitle(attributes.title);
                    seoService.setMetaDescription(attributes.description);
                    seoService.setOgType('article');

                    let userWidth = $window.innerWidth;
                    let device = 'mobile';
                    if (userWidth >= 992) device = 'desktop';
                    else if (userWidth > 480 && userWidth < 992) device = 'tablet';
                    // Create experience promise to be passed to experience component
                    ctrl.experiencePromise = requestService.getSingleAsync('experiences', ctrl.experienceId,
                        {device: device});

                    // Track google conversion
                    ctrl.experiencePromise.then(
                        function () {
                            adwordsService.sendConversion('view-experience');
                        },
                        null
                    );

                    ctrl.estimationPromise = ctrl.buildEstimationPromise();

                    ctrl.loading = false;
                },
                null
            );

            // Set /unset user in controller when he logs in/out
            $rootScope.$on('userLoggedIn', function (event, user) {
                ctrl.user = user;
            });
            $rootScope.$on('userLoggedOut', function (event) {
                ctrl.user = null;
            })
        }
    ]
);