angular
    .module('users')
    .controller('experiencesController', [
            '$window', 'translationService',
            '$routeParams', 'loadingService', 'requestService', '__env', '$location', '$filter',
            '$cookies', 'currencyService', '$q', '$timeout', '$scope', 'mathService', 'seoService', 'experienceTypesGroups',
            'topDestinations', '_', '$route', 'pageService', '$anchorScroll',
            function ($window, translationService,
                      $routeParams, loadingService, requestService, __env, $location, $filter,
                      $cookies, currencyService, $q, $timeout, $scope, mathService, seoService, experienceTypesGroups,
                      topDestinations, _, $route, pageService, $anchorScroll) {
                var ctrl = this;
                ctrl.translations = {};
                ctrl.translations.loading = true;
                ctrl.viewName = 'experiences';
                var pages = ['experiences', 'experience_types', 'boat_types', 'inputs', 'footer'];
                ctrl.listings = [];

                // Default switcher position to FILTERS
                ctrl.filtersOpen = true;

                // Define canceler resolver to destroy old call to listings api
                var canceler = $q.defer();

                ctrl.language = $routeParams.lang;
                ctrl.currency = $cookies.get('currency');

                // Set seo attributes
                seoService.setMetaTranslations(ctrl.viewName);

                // Load translations
                translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

                // Initialize the search key array
                ctrl.experienceTypesGroups = experienceTypesGroups;
                ctrl.topDestinations = topDestinations;


                let adults, max_length, max_price, road_stead;

                if ($location.search().adults) {
                    adults = parseInt($location.search().adults);
                } else {
                    adults = null;
                }

                if ($location.search().max_length) {
                    max_length = parseInt($location.search().max_length);
                } else {
                    max_length = null;
                }

                if ($location.search().max_price) {
                    max_price = parseInt($location.search().max_price);
                } else {
                    max_price = null;
                }

                if ($location.search().road_stead) {
                    road_stead = $location.search().road_stead;
                } else {
                    road_stead = null;
                }

                ctrl.params = {
                    lat: $location.search().lat,
                    lng: $location.search().lng,
                    distance: $location.search().distance,
                    adults: adults,
                    babies: parseInt($location.search().babies),
                    kids: parseInt($location.search().kids),
                    min_length: parseInt($location.search().min_length),
                    max_length: max_length,
                    experience_types: $location.search().experience_types,
                    entire_boat: $location.search().entire_boat,
                    boat_types: $location.search().boat_types,
                    min_price: $location.search().min_price,
                    max_price: max_price,
                    road_stead: road_stead,
                    rules: $location.search().rules,
                    currency: ctrl.currency,
                    language: ctrl.language,
                    device: 'mobile_search',
                    locality: $location.search().locality,
                    key: $location.search().key,
                    zoom: $location.search().zoom
                };

                // Set device for right photo resolution
                ctrl.setDevice = function () {
                    let userWidth = $window.innerWidth;
                    if (userWidth <= 480) ctrl.params.device = 'mobile_search';
                    else if (userWidth >= 992) ctrl.params.device = 'desktop_search';
                    else if (userWidth > 480 && userWidth < 992) ctrl.params.device = 'tablet_search';
                };
                ctrl.setDevice();

                // Function to get the right page keys attribute from search key
                ctrl.getSearchPageAttributes = function (searchKey) {
                    if (!searchKey) return null;

                    // Search one of the two lists for an occurrence, return the first that matches
                    var experienceTypeAttributes = ctrl.experienceTypesGroups.filter(function (object) {
                        return object.name == searchKey
                    })[0];
                    if (experienceTypeAttributes) {
                        return experienceTypeAttributes;
                    }

                    var topDestinationAttributes = ctrl.topDestinations.filter(function (object) {
                        return object.name == searchKey
                    })[0];
                    if (topDestinationAttributes) return topDestinationAttributes;
                };

                var experience_types = $location.search().experience_types;

                if (experience_types) {
                    experience_types = experience_types.split(',');

                    if (_.includes(experience_types, 'diving')) {
                        ctrl.params.diving = true;
                    }

                    if (_.includes(experience_types, 'fishing')) {
                        ctrl.params.fishing = true;
                    }

                    if (_.includes(experience_types, 'trips-and-tours')) {
                        ctrl.params.trips_and_tours = true;
                    }

                    if (_.includes(experience_types, 'dinners')) {
                        ctrl.params.dinners = true;
                    }

                    if (_.includes(experience_types, 'aperitif')) {
                        ctrl.params.aperitif = true;
                    }

                    if (_.includes(experience_types, 'romantic')) {
                        ctrl.params.romantic = true;
                    }

                    if (_.includes(experience_types, 'other')) {
                        ctrl.params.other = true;
                    }

                    if (_.includes(experience_types, 'nights')) {
                        ctrl.params.nights = true;
                    }

                    if (_.includes(experience_types, 'luxury')) {
                        ctrl.params.luxury = true;
                    }
                }

                ctrl.params.entire_boat_bool = ctrl.params.entire_boat == 1;

                var boat_types = $location.search().boat_types;

                if (boat_types) {
                    boat_types = boat_types.split(',');

                    if (_.includes(boat_types, 'catamaran')) {
                        ctrl.params.catamaran = true;
                    }

                    if (_.includes(boat_types, 'motorboat')) {
                        ctrl.params.motorboat = true;
                    }

                    if (_.includes(boat_types, 'sailboat')) {
                        ctrl.params.sailboat = true;
                    }

                    if (_.includes(boat_types, 'rubberboat')) {
                        ctrl.params.rubberboat = true;
                    }
                }

                if (ctrl.params.road_stead == 1) {
                    ctrl.params.road_stead_bool = true;
                    ctrl.params.harbor_bool = false;
                }
                if (ctrl.params.road_stead == 0) {
                    ctrl.params.road_stead_bool = false;
                    ctrl.params.harbor_bool = true;
                }
                if (ctrl.params.road_stead == null) {
                    ctrl.params.road_stead_bool = false;
                    ctrl.params.harbor_bool = false;
                }

                if (ctrl.params.min_length == 1 && ctrl.params.max_length == 10) {
                    ctrl.params.option_length = 1;
                }
                if (ctrl.params.min_length == 10 && ctrl.params.max_length == 20) {
                    ctrl.params.option_length = 2;
                }
                if (ctrl.params.min_length == 20 && ctrl.params.max_length == null) {
                    ctrl.params.option_length = 3;
                }
                if (ctrl.params.min_length == 1 && ctrl.params.max_length == null) {
                    ctrl.params.option_length = null;
                }

                var rules = $location.search().rules;

                if (rules) {
                    rules = rules.split(',');

                    if (_.includes(rules, 'animal_small')) {
                        ctrl.params.small_animals = true;
                    }

                    if (_.includes(rules, 'animal_medium')) {
                        ctrl.params.average_animals = true;
                    }

                    if (_.includes(rules, 'animal_large')) {
                        ctrl.params.big_animals = true;
                    }

                    if (_.includes(rules, 'alcohol')) {
                        ctrl.params.alcohol = true;
                    }

                    if (_.includes(rules, 'cooking')) {
                        ctrl.params.cooking = true;
                    }
                }


                // Set page class (to make CSS respond to different pages)
                pageService.set('search-page');

                $scope.$on('currencyChanged', function (event, newCurrency) {
                    $route.reload();
                });

                // Load listings using ctrl params
                ctrl.loadListings = function (nextPage, scrollToTop) {
                    if (ctrl.meta && ctrl.params.page > ctrl.meta.total_pages) {
                        return;
                    }

                    // Replace old listings
                    ctrl.listings = [];

                    if (!nextPage) {
                        ctrl.params.page = 1;
                        ctrl.meta = null;
                    }
                    ctrl.loadingListings = true;
                    canceler.resolve();
                    canceler = $q.defer();

                    if (scrollToTop || 1) {
                        $timeout(function () {
                            $anchorScroll('load-listings-scroll-point')
                        }, 0);
                    }

                    // Copy params object
                    let params = angular.copy(ctrl.params);
                    delete params.zoom;

                    let listingsPromise = requestService.getListings(params, canceler);
                    listingsPromise
                        .then(
                            function (response) {
                                if (!response) return;

                                ctrl.meta = response.data.meta;
                                ctrl.listings = response.data.data;
                                ctrl.loadingListings = false;

                                $scope.$broadcast('pageChanged', ctrl.listings);

                                if (ctrl.meta.total_elements == 0) return;
                            },
                            function (err) {
                                ctrl.loadingListings = false;
                                throw err;
                            });

                    return listingsPromise;
                };

                // Update $location.search() from params
                ctrl.updateSearch = function () {
                    // Map and locality
                    $location.search('lat', ctrl.params.lat);
                    $location.search('lng', ctrl.params.lng);
                    $location.search('distance', ctrl.params.distance);
                    $location.search('locality', ctrl.params.locality);
                    $location.search('zoom', ctrl.params.zoom);

                    // Prices
                    $location.search('min_price', ctrl.params.min_price);
                    $location.search('max_price', ctrl.params.max_price);

                    // Guests
                    $location.search('adults', ctrl.params.adults);
                    $location.search('babies', ctrl.params.babies);
                    $location.search('kids', ctrl.params.kids);

                    // Miscs
                    $location.search('experience_types', ctrl.params.experience_types);
                    $location.search('entire_boat', ctrl.params.entire_boat);
                    $location.search('boat_types', ctrl.params.boat_types);
                    $location.search('road_stead', ctrl.params.road_stead);
                    $location.search('max_length', ctrl.params.max_length);
                    $location.search('min_length', ctrl.params.min_length);
                    $location.search('rules', ctrl.params.rules);

                    // Control custom header
                    $location.search('key', ctrl.params.key);
                };

                ctrl.closeMobileFilter = function () {
                    ctrl.filtersModalOpen = false;
                    ctrl.loadListings();
                };

                // Pagination
                ctrl.params.per_page = 10;
                ctrl.params.page = 1;
                ctrl.pagesToDisplay = 6;

                // Initial load
                ctrl.loadListings();
            }
        ]
    );

