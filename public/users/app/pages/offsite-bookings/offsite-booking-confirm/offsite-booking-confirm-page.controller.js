angular.module('users').controller('offsiteBookingConfirmPageController', [
    '__env', 'userService', '$routeParams', '$location', '$translate', 'translationService', '$q', '$timeout',
    'pageService', 'requestService',
    function (__env, userService, $routeParams, $location, $translate, translationService, $q, $timeout,
              pageService, requestService) {
        var ctrl = this;

        ctrl.language = $routeParams.lang;

        pageService.set('offsite-booking-confirm-page');

        // Init translations object
        ctrl.translations = {};
        ctrl.translations.loading = true;
        ctrl.viewName = 'offsiteBookingConfirm';

        // Load translations
        var pages = [
            'offsite_booking'
        ];
        translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

        ctrl.supportMail = __env.supportEmail;
        ctrl.supportPhone = __env.supportPhone;

        ctrl.language = $routeParams.lang;

        // Submit form
        ctrl.pay = function () {
            ctrl.paying = true;
        };

        // Get booking
        ctrl.bookingPromise = requestService.getSingleAsync('offsite-bookings', $routeParams.bookingId);

        ctrl.bookingPromise.then(
            function (response) {
                ctrl.user = response.data.included[0].attributes;
            },
            null
        )
    }]
);