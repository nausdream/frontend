angular.module('users').controller('offsiteBookingCallbackPageController', [
    '__env', 'userService', '$routeParams', '$location', '$translate', 'translationService', '$q', '$timeout',
    '$rootScope', '$filter', 'requestService',
    function (__env, userService, $routeParams, $location, $translate, translationService, $q, $timeout,
              $rootScope, $filter, requestService) {
        var ctrl = this;

        // Init translations object
        ctrl.translations = {};
        ctrl.translations.loading = true;
        ctrl.viewName = 'offsiteBookingPayPage';

        ctrl.language = $routeParams.lang;

        // Load translations
        var pages = [
            'booking'
        ];
        translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

        // Get booking id from routeParams
        ctrl.bookingId = $routeParams.bookingId;

        // Try to pay again
        ctrl.payAgain = function () {
            $location.path('/' + ctrl.language + '/offsite-bookings/' + ctrl.bookingId + '/pay');
        };

        // Go to homepage
        ctrl.backToHome = function () {
            $location.path('/' + ctrl.language + '/');
        };

        // Determine component behaviour based on route
        switch ($routeParams.action) {
            case 'success':
                ctrl.success = true;
                break;
            case 'failure':
                ctrl.failure = true;
                break;
        }
    }]
);