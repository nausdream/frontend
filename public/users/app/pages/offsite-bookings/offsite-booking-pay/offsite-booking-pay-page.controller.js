angular.module('users').controller('offsiteBookingPayPageController', [
    '__env', 'userService', '$routeParams', '$location', '$translate', 'translationService', '$q', '$timeout',
    'pageService', 'requestService', '$sce', '$rootScope',
    function (__env, userService, $routeParams, $location, $translate, translationService, $q, $timeout,
              pageService, requestService, $sce, $rootScope) {
        var ctrl = this;
        ctrl.paying = true;

        // Init translations object
        ctrl.translations = {};
        ctrl.translations.loading = true;
        ctrl.viewName = 'offsiteBookingPay';

        // Load translations
        var pages = [
            'offsite_booking'
        ];
        translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

        // Get booking
        ctrl.bookingPromise = requestService.getSingleAsync('offsite-bookings', $routeParams.bookingId);
    }]
);