// Define controller
angular
    .module('users')
    .controller('registerFormController',
        ['requestService', '$cookies', '$scope', '$rootScope', '__env', 'validationService', '$location', 'translationService',
            function (requestService, $cookies, $scope, $rootScope, __env, validationService, $location, translationService) {
                var ctrl = this;
                ctrl.loading = false;
                ctrl.viewName = 'register';
                ctrl.translations = {};
                ctrl.translations.loading = true;
                ctrl.errors = {};
                ctrl.captain = {
                    name: '',
                    email: '',
                    phone: '',
                    dockingPlace: ''
                };
                ctrl.success = false;

                // Load translations
                var pages = ['inputs', 'register', 'errors'];
                translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

                ctrl.register = function () {
                    if (ctrl.form.$valid && !ctrl.loading) {
                        ctrl.loading = true;
                        requestService.registerCaptain(ctrl.captain).then(
                            function (response) {
                                // Success
                                ctrl.success = true;
                                ctrl.loading = false;
                            },
                            function (response) {
                                // Failure
                                validationService.setBackendErrors(ctrl.form, ctrl.errors, response.data.errors);
                                ctrl.loading = false;
                            }
                        );
                    } else {
                        validationService.setFrontendErrors(ctrl.form, ctrl.errors)
                    }
                };

                ctrl.validateField = function (field) {
                    validationService.validateField(ctrl.form[field], ctrl.errors);
                };

                ctrl.resetField = function (field) {
                    validationService.resetField(field, ctrl.errors, ctrl.form);
                };
            }]);