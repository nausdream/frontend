angular.module('users').controller('bookingsPageController', [
        '$location', 'userService', '$routeParams',
        function ($location, userService, $routeParams) {
            var ctrl = this;

            ctrl.language = $routeParams.lang;

            // Select single booking
            ctrl.select = function (id) {
                ctrl.makingRequest = true;

                $location.path('/' + ctrl.language + '/bookings/' + id);
            };

            ctrl.user = userService.get();
        }
    ]
);