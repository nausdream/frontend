angular.module('users').controller('bookingPageController', [
        '$routeParams', '$location', 'requestService', 'userService',
        function ($routeParams, $location, requestService, userService) {
            var ctrl = this;

            ctrl.action = $location.search();

            ctrl.language = $routeParams.lang;

            // When user clicks on back button in a booking, changes route
            ctrl.bookingBackButton = function () {
                ctrl.loading = true;

                $location.path('/' + ctrl.language + '/bookings');
            };

            ctrl.bookingPromise = requestService.getSingleAsync('bookings', $routeParams.bookingId);
            ctrl.user = userService.get();
        }
    ]
);