angular.module('users').controller('bookingPayPageController', [
    '__env', 'userService', '$routeParams', '$location', '$translate', 'translationService', '$q', '$timeout',
    '$rootScope', '$filter', 'requestService',
    function (__env, userService, $routeParams, $location, $translate, translationService, $q, $timeout,
              $rootScope, $filter, requestService) {
        var ctrl = this;

        // Init translations object
        ctrl.translations = {};
        ctrl.translations.loading = true;
        ctrl.viewName = 'bookingPayPage';

        ctrl.language = $routeParams.lang;

        // Load translations
        var pages = [
            'booking'
        ];
        translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

        // Get booking id from routeParams
        ctrl.bookingId = $routeParams.bookingId;
        ctrl.bookingPromise = requestService.getSingleAsync('bookings', ctrl.bookingId);

        // Go back to booking view
        ctrl.backToBooking = function () {
            $location.path('/' + ctrl.language + '/bookings/' + ctrl.bookingId);
        };

        // Try to pay again
        ctrl.payAgain = function () {
            $location.path('/' + ctrl.language + '/bookings/' + ctrl.bookingId + '/pay');
        };

        // Determine component behaviour based on route
        switch ($routeParams.action) {
            case 'pay':
                ctrl.paying = true;
                break;
            case 'success':
                ctrl.success = true;
                break;
            case 'failure':
                ctrl.failure = true;
                break;
            case 'accept':
            case 'reject':
            case 'change':
                $location.path('/' + ctrl.language + '/bookings/' + ctrl.bookingId).search($routeParams.action);
                break;
            default:
                $location.path('/' + ctrl.language + '/bookings/' + ctrl.bookingId);
        }
    }]
);