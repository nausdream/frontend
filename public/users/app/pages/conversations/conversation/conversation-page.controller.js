angular.module('users').controller('conversationPageController', [
        '$routeParams', '$location', 'requestService',
        function ($routeParams, $location, requestService) {
            var ctrl = this;

            ctrl.language = $routeParams.lang;

            // When user clicks on back button in a conversation, changes route
            ctrl.conversationBackButton = function () {
                ctrl.loading = true;

                $location.path('/' + ctrl.language + '/conversations');
            };

            ctrl.conversationPromise = requestService.getSingleAsync('conversations', $routeParams.conversationId);
        }
    ]
);