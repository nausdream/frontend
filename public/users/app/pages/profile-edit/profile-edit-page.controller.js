angular.module('users').controller('profileEditPageController', [
        '$location', 'userService', '$routeParams',
        function ($location, userService, $routeParams) {
            var ctrl = this;

            ctrl.language = $routeParams.lang;

            // Select single conversation
            ctrl.select = function (id) {
                ctrl.makingRequest = true;

                $location.path('/' + ctrl.language + '/conversations/' + id);
            };

            ctrl.user = userService.get();
        }
    ]
);