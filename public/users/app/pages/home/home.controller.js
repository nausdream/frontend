angular
    .module('users')
    .controller('homeController', [
            'seoService', '$scope', '$rootScope', 'loadingService', 'requestService', '$filter', '__env', 'translationService',
            '$location', 'pageService', '$routeParams', 'destinationAreas', 'destinations',
            function (seoService, $scope, $rootScope, loadingService, requestService, $filter, __env, translationService,
                      $location, pageService, $routeParams, destinationAreas, destinations) {
                // Make context accessible inside inner scopes
                var ctrl = this;
                ctrl.cloudinaryUrl = __env.cloudinaryUrl + '/image/upload/';
                ctrl.cloudinaryFolder = 'production/assets/img/home/';
                ctrl.viewName = 'home';
                ctrl.translations = {};
                ctrl.translations.loading = true;

                ctrl.language = $routeParams.lang;

                // Load translations
                var pages = ['home', 'header', 'footer', 'default_seo', 'inputs', 'errors', 'register'];
                translationService.translate(pages, ctrl.viewName).then(function (response) {
                    seoService.setMetaTranslations(ctrl.viewName);
                    ctrl.translations.loading = false;
                }, null);

                // Set page class (to make header respond to different pages)
                pageService.set('home');

                // Load destination areas and destinations
                ctrl.destinationAreas = [];
                ctrl.destinations = [];
                // Set top destination areas
                angular.forEach(destinationAreas, function (area) {
                    this.push({id: area.id, name: area.attributes.name, destinations: []});
                }, ctrl.destinationAreas);
                var destinationAreaId;
                var destinationAreaName;
                angular.forEach(destinations, function (destination) {
                    destinationAreaId = destination.relationships.destination_area.data.id;
                    destinationAreaName = $filter('filter')(ctrl.destinationAreas, {id: destinationAreaId})[0].name;

                    this.push(
                        {
                            id: destination.id,
                            name: destination.attributes.name,
                            area_id: destinationAreaId,
                            area_name: destinationAreaName,
                            key: destination.attributes.key,
                            coords: {
                                lat: destination.attributes.lat,
                                lng: destination.attributes.lng,
                                distance: destination.attributes.distance,
                                zoom: destination.attributes.zoom
                            }
                        }
                    )
                }, ctrl.destinations);

                ctrl.destinationsLoaded = true;

                // Carousel
                ctrl.carousel = {
                    variableWidth: true,
                    arrows: false,
                    infinite: false
                };

                ctrl.topDestinationsCarousel = {
                    cagliari: {
                        translation_key: 'cagliari_villasimius',
                        image_url: ctrl.cloudinaryUrl + 'q_100/' + ctrl.cloudinaryFolder + 'villasimius.jpg',
                        url: ''
                    },
                    amalfi: {
                        translation_key: 'amalfi_islands',
                        image_url: ctrl.cloudinaryUrl + 'q_100/' + ctrl.cloudinaryFolder + 'amalfi.jpg',
                        url: ''
                    },
                    costa_smeralda: {
                        translation_key: 'costa_smeralda',
                        image_url: ctrl.cloudinaryUrl + 'q_100/' + ctrl.cloudinaryFolder + 'costa_smeralda.jpg',
                        url: ''
                    }
                };
                ctrl.partnersCarousel = ['logo-costa-crociere', 'logo-eden-viaggi'];

                // Experience types
                ctrl.experienceTypes = {
                    adventure: {
                        title: 'adventure',
                        description: 'adventure_description',
                        image_url: ctrl.cloudinaryUrl + 'q_35/c_scale,w_1020/c_crop,w_680,h_680,x_100/v1501164757/' + ctrl.cloudinaryFolder + 'adventure.jpg',
                        url: '',
                        color: ''
                    },
                    relaxing_boat_trips: {
                        title: 'relaxing_boat_trips',
                        description: 'relaxing_boat_trips_description',
                        image_url: ctrl.cloudinaryUrl + 'q_35/c_scale,w_1400,h_1002/c_crop,w_1400,h_734,y_250/v1501164718/' + ctrl.cloudinaryFolder + 'relaxing-boat-trips.jpg',
                        image_url_mobile: ctrl.cloudinaryUrl + 'q_35/c_scale,h_832/c_crop,w_732,h_732,x_260,y_100/v1501164718/' + ctrl.cloudinaryFolder + 'relaxing-boat-trips.jpg',
                        url: '',
                        color: ''
                    },
                    food_and_drinks: {
                        title: 'food_and_drinks',
                        description: 'food_and_drinks_description',
                        image_url: ctrl.cloudinaryUrl + 'q_50/c_scale,w_960/c_crop,w_760,h_380,x_110,y_170/v1501164733/' + ctrl.cloudinaryFolder + 'food-and-drinks.jpg',
                        image_url_mobile: ctrl.cloudinaryUrl + 'q_50/c_scale,h_732/c_crop,w_732,h_732,x_200/v1501164733/' + ctrl.cloudinaryFolder + 'food-and-drinks.jpg',
                        url: '',
                        color: ''
                    },
                    romantic_getaways: {
                        title: 'romantic_getaways',
                        description: 'romantic_getaways_description',
                        image_url: ctrl.cloudinaryUrl + 'q_35/c_scale,h_800/c_crop,w_732,h_732,x_400/v1501164755/' + ctrl.cloudinaryFolder + 'romantic-getaways.jpg',
                        image_url_mobile: ctrl.cloudinaryUrl + 'q_35/c_scale,h_732/c_crop,w_732,h_732,x_200/v1501164755/' + ctrl.cloudinaryFolder + 'romantic-getaways.jpg',
                        url: '',
                        color: ''
                    },
                    weekends_and_vacations: {
                        title: 'weekends_and_vacations',
                        description: 'weekends_and_vacations_description',
                        image_url: __env.cloudinaryUrl + 'q_35/c_scale,h_732/c_crop,w_732,h_732,x_100/v1501164664/' + ctrl.cloudinaryFolder + 'weekends-and-vacations.jpg',
                        image_url_mobile: __env.cloudinaryUrl + 'q_35/c_scale,h_732/c_crop,w_732,h_732,x_100/v1501164664/' + ctrl.cloudinaryFolder + 'weekends-and-vacations.jpg',
                        url: '',
                        color: ''
                    },
                    boat_and_breakfast: {
                        title: 'boat_and_breakfast',
                        description: 'boat_and_breakfast_description',
                        image_url: ctrl.cloudinaryUrl + 'q_60/c_scale,w_760/c_crop,h_380,w_760,y_42/v1501164592/' + ctrl.cloudinaryFolder + 'boat-and-breakfast.jpg',
                        image_url_mobile: ctrl.cloudinaryUrl + 'q_60/c_scale,h_732/c_crop,w_732,h_732,x_50/v1501164592/' + ctrl.cloudinaryFolder + 'boat-and-breakfast.jpg',
                        url: '',
                        color: ''
                    },
                    luxury: {
                        title: 'luxury',
                        description: 'luxury_description',
                        image_url: ctrl.cloudinaryUrl + 'luxury',
                        image_url_mobile: ctrl.cloudinaryUrl + 'luxury',
                        url: '',
                        color: ''
                    }
                };

                // ABOUT US Carousel
                ctrl.feedbacks = [
                    {
                        text: 'feedback_1_text',
                        pic: 'immacolata',
                        name: 'feedback_1_name',
                        role: 'feedback_1_role',
                        id: 0
                    },
                    {
                        text: 'feedback_2_text',
                        pic: 'anna',
                        name: 'feedback_2_name',
                        role: 'feedback_2_role',
                        id: 1
                    },
                    {
                        text: 'feedback_3_text',
                        pic: 'stefano',
                        name: 'feedback_3_name',
                        role: 'feedback_3_role',
                        id: 2
                    }
                ];

                // Prefooter
                ctrl.animateCounter = function (counter, to, duration) {
                    var count = new CountUp(counter, 0, to, 0, duration);
                    count.start(function () {
                        //Set final number
                        var element = $('#' + counter);
                        var finalText = element.text();
                        element.text(finalText + '+');
                    });
                };

                // Search bar dropdown
                ctrl.toggleSearchSelect = function (isOpen) {
                    ctrl.isSelectOpen = isOpen;
                };
                // Return translated area name
                ctrl.selectGroupByAreaName = function (item) {
                    return item.area_name;
                };

                ctrl.eventsForm = function () {
                    ctrl.eventsFormIsOpen = true;
                };

                // Go to selected locality
                ctrl.explore = function () {
                    if (!ctrl.selectedDestination) {
                        return;
                    }
                    var dest = ctrl.selectedDestination;

                    var params = {
                        locality: dest.name,
                        lat: dest.coords.lat,
                        lng: dest.coords.lng,
                        distance: dest.coords.distance,
                        experience_types: 'trips-and-tours,aperitif,diving',
                        zoom: dest.coords.zoom,
                        babies: 0,
                        kids: 0,
                        min_length: 1,
                        entire_boat: 0,
                        min_price: 1
                    };

                    if (dest.key) params.key = dest.key;

                    $location.path('/' + ctrl.language + '/experiences').search(params);
                }
            }
        ]
    );