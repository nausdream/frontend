angular.module('users').controller('boatsPageController', [
        '$location', 'userService',  '$routeParams',
        function ($location, userService, $routeParams) {
            var ctrl = this;

            ctrl.language = $routeParams.lang;

            // Select single boat
            ctrl.select = function (id) {
                ctrl.makingRequest = true;

                $location.path('/' + ctrl.language + '/boats/' + id);
            };

            ctrl.user = userService.get();
        }
    ]
);