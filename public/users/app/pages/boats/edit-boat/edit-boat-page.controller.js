angular.module('users').controller('editBoatPageController', [
        '$routeParams', '$location', 'requestService',
        function ($routeParams, $location, requestService) {
            var ctrl = this;

            var boatId = $routeParams.boatId;
            ctrl.boatPromise = requestService.getSingleAsync('boats', boatId, {editing: true});

            ctrl.language = $routeParams.lang;

            // When user clicks on back button in a boat edit, goes back to boat view
            ctrl.boatBackButton = function () {
                ctrl.loading = true;

                $location.path('/' + ctrl.language + '/boats/' + boatId);
            };
        }
    ]
);