angular.module('users').controller('newBoatPageController', [
        'requestService', 'userService', '$location', '$routeParams',
        function (requestService, userService, $location, $routeParams) {
            var ctrl = this;

            ctrl.language = $routeParams.lang;

            // Create new boat
            var relationships = {
                role: 'user',
                id: userService.get().id,
                type: 'users'
            };
            requestService.postObject('boats', {}, relationships).then(
                function (response) {
                    $location.replace();
                    $location.path('/' + ctrl.language + '/boats/' + response.data.data.id + '/edit').search('step', 1);
                },
                null
            );
        }
    ]
);