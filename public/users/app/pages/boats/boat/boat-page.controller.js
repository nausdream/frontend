angular.module('users').controller('boatPageController', [
        '$routeParams', '$location', 'requestService',
        function ($routeParams, $location, requestService) {
            var ctrl = this;

            ctrl.language = $routeParams.lang;

            // When user clicks on back button in a boat, changes route
            ctrl.boatBackButton = function () {
                ctrl.loading = true;

                $location.path('/' + ctrl.language + '/boats');
            };

            // When user clicks on edit button in a boat
            ctrl.edit = function () {
                ctrl.loading = true;

                $location.path('/' + ctrl.language + '/boats/' + $routeParams.boatId + '/edit');
            };

            ctrl.boatPromise = requestService.getSingleAsync('boats', $routeParams.boatId, {editing: true});
        }
    ]
);