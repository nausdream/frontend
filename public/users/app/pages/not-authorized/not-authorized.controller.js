angular
    .module('users')
    .controller('notAuthorizedController', ['seoService', '$rootScope', 'translationService', 'requestService', '__env', 'customPartialLoader', '$window',
        function (seoService, $rootScope, translationService, requestService, __env, customPartialLoader, $window) {
            // Make context accessible inside inner scopes
            var ctrl = this;
            ctrl.translations = {};
            ctrl.translations.loading = true;
            ctrl.viewName = 'not-authorized';
            var pages = ['not_authorized'];

            seoService.setMetaTranslations(ctrl.viewName);

            // Load translations
            translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

            ctrl.backHome = function () {
                $window.location.href = '/';
            }
        }
    ]);