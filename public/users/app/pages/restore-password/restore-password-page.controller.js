angular.module('users').controller('restorePasswordPageController', [
        '$rootScope', 'validationService', 'requestService', 'translationService', '$location', '$routeParams',
        function ($rootScope, validationService, requestService, translationService, $location, $routeParams) {
            // Make context accessible inside inner scopes
            var ctrl = this;
            ctrl.translations = {};
            ctrl.translations.loading = true;
            ctrl.viewName = 'restorePassword';
            ctrl.errors = {};

            // Get token from params
            ctrl.token = $location.search().token_restore;
            // Get language
            ctrl.language = $routeParams.lang;

            // Load translations
            var pages = ['inputs', 'login', 'errors'];
            translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

            // Send new password
            ctrl.send = function () {
                if (ctrl.form.$valid && !ctrl.loading) {
                    ctrl.loading = true;

                    ctrl.data = {password: ctrl.password, token: ctrl.token};

                    requestService.resetPassword(ctrl.data).then(
                        function (response) {
                            ctrl.success = true;
                            ctrl.loading = false;
                        },
                        function (response) {
                            // Failure
                            validationService.setBackendErrors(ctrl.form, ctrl.errors, response.data.errors);
                            ctrl.loading = false;
                        }
                    );
                } else {
                    validationService.setFrontendErrors(ctrl.form, ctrl.errors)
                }
            };

            ctrl.goToLogin = function () {
                $location.path('/' + ctrl.language + '/login').search({});
            };

            ctrl.validateField = function (field) {
                validationService.validateField(ctrl.form[field], ctrl.errors);
            };

            ctrl.resetField = function (field) {
                if (ctrl.errors) {
                    ctrl.errors[field] = null;
                    ctrl.errors.error = null;
                }
            };
        }
    ]
);