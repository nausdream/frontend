angular.module('users').controller('emailVerifiedController', [
        'translationService',
        function (translationService) {
            var ctrl = this;

            ctrl.translations = {};
            ctrl.translations.loading = true;
            ctrl.viewName = 'emailVerified';

            // Load translations
            var pages = ['header'];
            translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);
        }
    ]
);