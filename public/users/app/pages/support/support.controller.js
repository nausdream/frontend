angular
    .module('users')
    .controller('supportController', [
        'seoService', 'translationService', 'customPartialLoader', '$rootScope', '$location', '$cookies', '$routeParams', '__env',
        function (seoService, translationService, customPartialLoader, $rootScope, $location, $cookies, $routeParams, __env) {
            // Make context accessible inside inner scopes
            var ctrl = this;
            ctrl.translations = {};
            ctrl.translations.loading = true;

            ctrl.language = $routeParams.lang;

            var actualRoute = $location.path(); // '/Home'
            var pages;
            if (actualRoute == '/' + ctrl.language + '/how-it-works-captain') {
                ctrl.viewName = 'support';
                pages = ['support'];
            }
            if (actualRoute == '/' + ctrl.language + '/how-it-works-guest') {
                ctrl.viewName = 'support_guests';
                pages = ['support_guests'];
            }
            if (actualRoute == '/' + ctrl.language + '/assistance') {
                ctrl.viewName = 'assistance';

                ctrl.supportPhone = __env.supportPhone;
                pages = ['assistance'];
            }
            /*
             if (actualRoute == '/contacts') {
             ctrl.viewName = 'contacts';
             pages = ['contacts'];
             }
             */
            if (actualRoute == '/' + ctrl.language + '/terms') {
                ctrl.viewName = 'terms';
                pages = ['terms'];
            }
            if (actualRoute == '/' + ctrl.language + '/cookies') {
                ctrl.viewName = 'cookies';
                pages = ['cookies'];
            }
            if (actualRoute == '/' + ctrl.language + '/privacy') {
                ctrl.viewName = 'privacy';
                pages = ['privacy'];
            }

            // Load translations
            translationService.translate(pages, ctrl.viewName).then(function (response) {
                seoService.setMetaTranslations(ctrl.viewName, false);

                ctrl.translations.loading = false
            }, null);
        }]);