angular.module('users').controller('captainFormController', [
    'requestService', '$location', '$q', 'translationService', '$rootScope', 'validationService', 'userService',
    function (requestService, $location, $q, translationService, $rootScope, validationService, userService) {
        // Make context accessible inside inner scopes
        var ctrl = this;
        ctrl.loading = true;
        ctrl.translations = {};
        ctrl.translations.loading = true;
        ctrl.viewName = 'captainForm';
        ctrl.maxStepValue = 4;
        // Set disabled (read only form)
        ctrl.disabled = false;
        ctrl.errors = {};
        ctrl.backendErrors = {};
        // To make down-arrow behave correctly
        ctrl.dropdowns = {};
        ctrl.popovers = {};
        ctrl.user = {};
        // We need to store field that needs manipulating before sending
        ctrl.tempUser = {};
        // Load user data into ctrl.user (deep copy)
        ctrl.userId = userService.get() ? userService.get().id : null;
        ctrl.user = angular.copy(userService.get().attributes);

        // Load translations
        var pages = [
            'captain_form', 'inputs', 'errors', 'countries', 'spoken_languages', 'currencies',
            'months', 'boat_types', 'boat_materials', 'captain_types', 'motor_types', 'experience_types', 'accessories'
        ];
        translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

        // STEPS RELATED FUNCTIONS
        // Get search 'step' parameter. If not valid or out of range, set it to one (and trigger page reload)
        ctrl.setCtrlStep = function () {
            var step = $location.search().step;

            // TODO Check if a finished boat exists. Redirect to dashboard

            // Avoid out of range
            if (!step || isNaN(step) || (step < 1) || (step > ctrl.maxStepValue)) {
                step = 1;
                $location.search('step', step);
            } else {

            }
            ctrl.step = step;

            // View has been loaded
            ctrl.loading = false;
        };
        ctrl.setCtrlStep();
    }
]);