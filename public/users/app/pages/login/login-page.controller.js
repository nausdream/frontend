angular
    .module('users')
    .controller('loginPageController', [
            'loginService', '$cookies', '$scope', '$rootScope', '__env', 'validationService', '$location',
        'translationService', '$routeParams', 'pageService',
            function (loginService, $cookies, $scope, $rootScope, __env, validationService, $location,
                      translationService, $routeParams, pageService) {
                var ctrl = this;
                ctrl.viewName = 'loginPage';
                ctrl.translations = {};
                ctrl.translations.loading = true;
                ctrl.loading = true;

                ctrl.language = $routeParams.lang;

                pageService.set('login');

                // Load translations
                var pages = ['errors', 'inputs', 'login'];
                translationService.translate(pages, ctrl.viewName).then(function(response){ctrl.translations.loading = false},null);

                // Check if user has been redirected here from another route (by logged-in middleware)
                var from = $location.search().from;
                if (from) {
                    // Redirect to previous view
                    ctrl.onComplete = function () {
                        // Clear search before redirecting
                        $location.search({});

                        $location.path(from);
                    };

                    // Show only captain login
                    ctrl.captainLogin = from == ctrl.language + '/new-captain';

                    ctrl.loading = false;
                } else {
                    // Redirect to home
                    ctrl.onComplete = function () {
                        $location.path('/' + ctrl.language + '/');
                    };
                }

                // Check if user wants to log as captain
                if ($location.hash() == 'captain') ctrl.captainLogin = true;
                $location.hash(null);

                ctrl.loading = false;
            }
        ]
    );