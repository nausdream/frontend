angular.module('users').filter('userDateTime', ['dateService', '$filter', function (dateService, $filter) {
    return function (value, language, convert = true) {
        // Fetch format from service
        var format = dateService.getFormat('dateTime', language);

        // If date not provided, defaults to today
        if (!value) return $filter('date')(new Date(), format);

        // If string instead of date is provided, convert it to date object
        if (typeof value == "string")  value = dateService.getDateFromString(value);

        // Create new object before converting to user timezone
        var newDate = new Date(value.getTime());

        // If convert is set to true, offset date to user timezone
        if (convert) {
            dateService.toUserTimezone(newDate);
        }

        return $filter('date')(newDate, format);
    };
}]);