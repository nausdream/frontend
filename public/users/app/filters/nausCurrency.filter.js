angular.module('users').filter('nausCurrency', ['currencyService', function (currencyService) {
    return function (value, currency, currencies) {
        if (value == null) return '';
        if (!currency || !currencies) return value;

        return value + currencies[currency];
    }
}]);