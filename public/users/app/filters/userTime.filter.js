angular.module('users').filter('userTime', ['dateService', '$filter', function (dateService, $filter) {
        return function (value, language, convert = true) {
            // Fetch format from service
            var format = dateService.getFormat('time', language);

            // If date not provided, defaults to today
            if (!value) return $filter('date')(new Date(), format);

            // If string instead of date is provided, convert it to date object
            if (typeof value == "string") {
                // User can pass either a date/time in one of these formats:
                // yyyy-mm-dd hh:mm:ss
                // yyyy-mm-dd format
                // hh:mm:ss
                var splitDate = value.split(':');
                if (splitDate[0].length < 3) {
                    // User passed a time string
                    value = new Date();
                    value.setHours(splitDate[0], splitDate[1], splitDate[2]);
                } else {
                    // User passed a date string
                    value = dateService.getDateFromString(value);
                }
            }

            // Create new object before converting to user timezone
            var newDate = new Date(value.getTime());

            // If convert is set to true, offset date to user timezone
            if (convert) {
                dateService.toUserTimezone(newDate);
            }

            return $filter('date')(newDate, format);
        };
    }]
);