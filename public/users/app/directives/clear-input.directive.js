/**
 * When passed event is fired, clear provided variable
 */
angular.module('users').directive('clearInput', function () {
    return {
        scope: {
            ciInput: '='
        },
        link: function (scope, elem, attrs) {
            var event = attrs.ciEvent;

            elem.on(event, function () {
                scope.ciInput = null;
                scope.$apply();
            });
        }
    }
});