/**
 * Created by lucas on 19/07/2017.
 */
angular.module('users').directive('affix', [
    '$window', '$document', '$timeout',
    function ($window, $document, $timeout) {
        return {
            restrict: 'A',
            scope: {
                isAffixed: '='
            },
            link: function (scope, element, attrs) {

                // Make parent height at least as big as element's
                element.parent().css('minHeight', element.height() + 'px');

                function applyAffix() {
                    // The element should stick to the top
                    let affixTop = $window.pageYOffset >= attrs.affixTop;

                    // The element should stick to the bottom
                    let affixBottom =
                        element.height() >=
                        (angular.element($document).height() - attrs.affixBottom) -
                        ($window.pageYOffset) - attrs.affixTopPosition - parseInt(element.css('marginBottom'));

                    // If either one of them is true
                    scope.isAffixed = !!(affixTop || affixBottom);

                    // Make parent height at least as big as element's
                    element.parent().css('minHeight', element.height() + 'px');

                    // Affix top
                    if (affixTop) {
                        element.addClass('affix-top');
                        element.css('top', attrs.affixTopPosition + 'px');
                    } else {
                        element.removeClass('affix-top');
                        element.css('top', '');
                    }

                    // Affix bottom (takes precedence over affix top)
                    if (affixBottom) {
                        element.addClass('affix-bottom');
                        element.css('bottom', ($window.pageYOffset + $window.innerHeight - (angular.element($document).height() - attrs.affixBottom)) + 'px');
                        element.css('top', '');
                    } else {
                        element.removeClass('affix-bottom');
                        element.css('bottom', '');
                    }

                    scope.$apply();
                }

                // Wait for scroll event
                angular.element($window).on("scroll", function () {
                    applyAffix();
                });

                // Watch height change
                function watch(){

                    var h = element.height();

                    if( h != scope.elementHeight ){

                        scope.elementHeight = h;
                        $timeout( applyAffix, 0 );
                    }
                    $timeout( watch, 350 );
                }
                watch();
            }
        }
    }]);