angular
    .module('users')
    .directive('slickHover', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs) {
                var slider = angular.element(element);

                $timeout(function () {
                    // Acquisisco la larghezza dello slider
                    var sliderWidth = slider.width();

                    // Quando il mouse entra allora scatta il controllo
                    slider.on('mouseover', function (e) {
                        /*
                         * Immagino lo slider diviso in 4 quadranti, questo per permettere uno
                         * spostamento solo quando il mouse si trova agli estremi
                         */
                        // Se il mouse si trova nel quarto quadrante allora va avanti
                        console.log(sliderWidth * 0.75);

                        if (e.pageX >= (sliderWidth * (5 / 6))) {
                            console.log("4 quadrante x ", e.pageX); //debug
                            slider.slick('getSlick').slickNext(); // Metodo della libreria slick --> http://kenwheeler.github.io/slick/ (è mappata in angular dalla directive che avete installato)
                        }
                        ;

                        // Se il mouse si trova nel primo quadrante allora torna indietro
                        if (e.pageX <= (sliderWidth * (1 / 2))) {
                            console.log("1 quadrante x ", e.pageX);
                            slider.slick('getSlick').slickPrev();
                        }
                        ;
                    })
                }, 1000);

                // Quando il mouse lascia il carosello, torna al primo elemento (ricarica la lista)
                slider.on('mouseleave', function () {
                    slider.slick('getSlick').slickGoTo(0);
                })
            }
        }
    }]);