angular.module('users').directive("preventHref", function () {
    return function (scope, element) {
        angular.element(element).bind('click', function(event){
            event.preventDefault();
        })
    }
});