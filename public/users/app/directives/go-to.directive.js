/**
 * Attach click event to element based on href property
 * (in order to avoid too much login on the translation element)
 */
angular.module('users').directive("goTo", ['$location', function ($location) {
    return {
        restrict: 'A',
        link: function (scope, element, attrs) {
            // Bind click event to element
            angular.element(element)
                .bind('click', function (event) {
                    // Prevent <a> event to be triggered
                    event.preventDefault();
                    // Changes url
                    $location.url(attrs.href);
                    // Tell angular to run the digest cycle. It doesn't work without this line
                    scope.$apply();
                })
        }
    }
}]);