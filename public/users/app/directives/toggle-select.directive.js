angular.module('users').directive('toggleSelect', function() {
    return {
        require: 'uiSelect',
        link: function(scope, element, attrs, $select) {
            var toggler = angular.element(document.getElementById(attrs.toggleId));

            toggler.on('click', function (event) {
                event.preventDefault();
                event.stopPropagation();
                scope.$apply(function() {
                    $select.toggle(event);
                });
            })
        }
    }
});