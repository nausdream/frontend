angular
    .module('users')
    .directive('experienceTypeColor', ['experienceColors', function (experienceColors) {
        return {
            restrict: 'A',
            scope: {
                experienceTypeColor: '='
            },
            link: function (scope, element, attrs) {
                var element = angular.element(element);

                scope.$watch(
                    function () {
                        return scope.experienceTypeColor;
                    },
                    function (val) {
                        if (experienceColors[val]){
                            element.addClass(experienceColors[val]);
                        }
                    }
                );
            }
        }
    }]);