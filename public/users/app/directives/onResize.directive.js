/**
 * Created by lucas on 02/08/2017.
 */
angular.module('users').directive('onResize', ['$window', function ($window) {
        return {
            restrict: 'A',
            scope: {
                onResize: '&'
            },
            link: function (scope, element, attrs) {
                angular.element($window).on('resize', function () {
                    scope.onResize();
                });
            }
        }
    }]
);