angular.module('users').directive('uiSelectGroupString', [
    '$translate', '$rootScope',
    function ($translate, $rootScope) {
    return {
        scope: {destinations: '='},
        link: function (scope, element, attrs) {
            // Create array of unique areas
            var uniqueAreas = [];
            scope.destinations.forEach(function (dest) {
                if (-1 == uniqueAreas.indexOf(dest.area_name)) uniqueAreas.push(dest.area_name);
            });

            function setTranslations(uniqueAreasNames, DOMAreas) {
                angular.forEach(uniqueAreasNames, function (value, key) {
                    $translate('home.'+value).then(
                        function (translation) {
                            DOMAreas[key].innerHTML = '<span class="group-label">'+translation+'</span>';
                        }
                    )
                });
            }

            // Replace destination key
            angular.element(document).ready(
                function () {
                    // Select all areas from DOM
                    var areas = $('#select-choices').find('.ui-select-choices-group-label');

                    // Translate text
                    if ($translate.use()) {
                        setTranslations(uniqueAreas, areas);
                    }

                    // Listen for language changes
                    $rootScope.$on('languageChanged', function () {
                        // Translate text
                        setTranslations(uniqueAreas, areas);
                    });
                }
            );
        }
    }
}]);