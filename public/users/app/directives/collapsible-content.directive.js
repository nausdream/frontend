/**
 * Created by lucas on 06/07/2017.
 *
 * Add 'read-more / read-less' functionality to a div
 */
angular.module('users').directive('collapsibleContent', ['$compile', function ($compile) {
    return {
        restrict: 'A',
        scope: {
          collapsibleContentExpanded: '='
        },
        link: function (scope, elem, attrs) {
            angular.element(elem).addClass('collapsible-content');

            // Collapse by default
            angular.element(elem).addClass('collapsed');
            scope.collapsibleContentExpanded = false;

            // Expand element
            scope.open = function () {
                scope.collapsibleContentExpanded = true;
                angular.element(elem).addClass('expanded');
                angular.element(elem).removeClass('collapsed');
            };

            // Collapse element
            scope.close = function () {
                scope.collapsibleContentExpanded = false;
                angular.element(elem).addClass('collapsed');
                angular.element(elem).removeClass('expanded');
            };

            var readMoreLess = angular.element(
                '<h5 class="bold light-blue text-right pointer" ng-hide="collapsibleContentExpanded" ng-click="open()">{{"experience.read_more" | translate}}</h5>' +
                '<h5 class="bold light-blue text-right pointer" ng-show="collapsibleContentExpanded" ng-click="close()">{{"experience.read_less" | translate}}</h5>');
            elem.after(readMoreLess);

            $compile(readMoreLess)(scope);
        }
    }
}]);