angular.module('users').directive('passwordMatch', function () {

    return {
        require: 'ngModel',
        scope: {
            passwordMatch: '<'
        },
        link: function (scope, elem, attrs, field) {
            field.$validators.password_match = function(modelValue, viewValue) {
                return viewValue == scope.passwordMatch;
            };
        }
    }
});