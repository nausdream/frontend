// Reset time, keeping only the day
Date.prototype.withoutTime = function () {
    var d = new Date(this);
    d.setHours(0, 0, 0, 0);
    return d;
};

'use strict';
angular.module('users', ['shared', 'ngRoute.middleware', 'ui.select', 'ngSanitize', 'slickCarousel', 'countUpModule',
    'scroll-trigger', 'pascalprecht.translate', 'angular.filter', 'underscore', 'rzModule']);

// MIDDLEWARES
angular.module('users').config(['$middlewareProvider', '$translateProvider',
    function ($middlewareProvider, $translateProvider) {
        $middlewareProvider.map({
            // If user is not logged in, redirect to login page
            'logged-in': [
                '$rootScope', 'checkLoginService', '$location', 'pageService',
                function loggedIn($rootScope, checkLoginService, $location, pageService) {
                    var middleware = this;

                    var language = middleware.params.lang;

                    // If user is not logged in, redirect to login, passing it the desired route
                    checkLoginService.getLoginPromise().then(
                        function (response) {
                            // Tell the pageService that this page is private
                            pageService.setPrivate(true);

                            middleware.next();
                        },
                        function () {
                            // Redirect to login page
                            middleware.redirectTo('/' + language + '/login', {from: $location.url().substring(1)});
                        }
                    );
                }
            ],

            // Define language routing logic
            'route-lang': [
                'validLanguages', '$cookies', 'checkLoginService', '$route', 'translationService', '$translate', '__env', '$location',
                function (validLanguages, $cookies, checkLoginService, $route, translationService, $translate, __env, $location) {
                    var middleware = this;
                    // Get route language
                    var routeLanguage = middleware.params.lang;
                    // Get cookie language
                    var language = $cookies.get('language');

                    // Route language is not valid
                    if (routeLanguage && -1 == validLanguages.indexOf(routeLanguage)) {

                        // If cookie language is valid, redirect to that language route
                        if (!language) {
                            $translateProvider.determinePreferredLanguage();

                            language = $translate.preferredLanguage();
                        }

                        $location.path('/' + language + '/not-found').search({});
                        return;
                    }

                    // User is going to homepage
                    if (!routeLanguage) {
                        middleware.next();
                    }
                    // User is going to another page
                    else {
                        // Login
                        checkLoginService.getLoginPromise().then(
                            // Logged in
                            function () {
                                middleware.next();
                            },
                            // Not logged in
                            function () {
                                // If they're equal and translate library has already a language set, don't change
                                if (routeLanguage != language || $translate.use() == null) {
                                    translationService.changeLanguage(routeLanguage, false)
                                }

                                middleware.next();
                            }
                        );
                    }
                }],

            'home-lang': [
                'validLanguages', '$cookies', '$translate', 'translationService', 'checkLoginService', '$rootScope',
                function (validLanguages, $cookies, $translate, translationService, checkLoginService, $rootScope) {
                    var middleware = this;
                    // Get language from routeParams, if present
                    var routeLanguage = middleware.params.lang;
                    // Get cookie language
                    var cookieLanguage = $cookies.get('language');

                    checkLoginService.getLoginPromise().then(
                        // Logged in
                        function () {
                            $rootScope.$emit('languageChanging', cookieLanguage);
                            $rootScope.$emit('languageChanged');

                            middleware.next();
                        },
                        // Not logged in
                        function () {
                            var language = routeLanguage;

                            // If routeLanguage is not valid
                            if (-1 == validLanguages.indexOf(language)) {
                                $rootScope.$emit('languageChanging', cookieLanguage);
                                $rootScope.$emit('languageChanged');

                                language = cookieLanguage;
                            }

                            // We only change language if routeLanguage and cookieLanguage differ
                            if (language != cookieLanguage) {
                                translationService.changeLanguage(routeLanguage, false)
                            }

                            // Go on
                            middleware.next();
                        }
                    );
                }
            ]
        });

        $middlewareProvider.global(['route-lang']);
    }]);

// Bootstrap translateProvider
angular.module('users').config(['$translateProvider', 'validLanguages', function ($translateProvider, validLanguages) {
    let mappings = {};
    validLanguages.forEach(function (language) {
        mappings[language + '_*'] = language;
    });
    mappings['*'] = window.__env.fallbackLanguage;

    $translateProvider
    // Map any locale to correct language (eg. en_US/en_UK are both mapped to en)
        .registerAvailableLanguageKeys(
            validLanguages,
            mappings
        )
        .addInterpolation('$translateMessageFormatInterpolation')
        // Make directives work in translations too
        .usePostCompiling(true)
        // Set fallback
        //.fallbackLanguage(window.__env.fallbackLanguage)
        // Protect from XSS
        .useSanitizeValueStrategy('sanitizeParameters');

    $translateProvider
        .useLoader('customPartialLoader', {
            urlTemplate: __env.apiTranslationsUrl + "translations?page={part}&language={lang}"
        });

    $translateProvider.determinePreferredLanguage();
}]);

// Perform initial tasks
angular.module('users').run(
    ['$rootScope', '$translate', 'loginService', '__env', 'translationService', 'currencyService', '$cookies', '$window',
        'FacebookService', '$location', 'requestService',
        function ($rootScope, $translate, loginService, __env, translationService, currencyService, $cookies, $window,
                  FacebookService, $location, requestService) {
            // Init facebook
            $window.fbAsyncInit = function () {
                // Executed when the SDK is loaded
                FB.init({
                    /*
                     The app id of the web app;
                     To register a new app visit Facebook App Dashboard
                     ( https://developers.facebook.com/apps/ )
                     */
                    appId: __env.fbAppId,

                    /*
                     Adding a Channel File improves the performance
                     of the javascript SDK, by addressing issues
                     with cross-domain communication in certain browsers.
                     */
                    channelUrl: 'app/channel.html',

                    /*
                     Set if you want to check the authentication status
                     at the start up of the app
                     */
                    status: true,

                    /*
                     Enable cookies to allow the server to access
                     the session
                     */
                    cookie: true,

                    /* Parse XFBML */
                    xfbml: true,

                    /* Tell Facebook which version we're using */
                    version: __env.fbAppVersion
                });

                FacebookService.watchLoginStatus();
            };
            (function (d) {
                // load the Facebook javascript SDK
                var js,
                    id = 'facebook-jssdk',
                    ref = d.getElementsByTagName('script')[0];
                if (d.getElementById(id)) {
                    return;
                }
                js = d.createElement('script');
                js.id = id;
                js.async = true;
                js.src = "https://connect.facebook.net/en_US/sdk.js";
                ref.parentNode.insertBefore(js, ref);

            }(document));

            // Set currency cookie by default
            var currency = $cookies.get('currency') || __env.fallbackCurrency;
            currencyService.changeCurrency(currency, false, false);

            // LOGIN
            var language = $cookies.get('language') || $translate.preferredLanguage() || __env.fallbackLanguage;
            // Change language
            translationService.changeLanguage(language, false);

            // Check if there's a token in the GET parameters
            var token = $location.search().token;
            if (token) {
                // Send token to backend
                requestService.verifyToken(token).then(
                    function (response) {
                        // Token is valid, save csrf and access token
                        loginService.setCookies(response);
                        // Save user data to userService
                        loginService.login().then(
                            function (response) {
                                // Finish loading
                                $location.search({'token': null});
                            },
                            function (response) {
                                // Finish loading
                                $location.path('/' + language + '/not-authorized').search('token', null);
                            }
                        );
                    },
                    function (response) {
                        // Tries to login anyways, maybe the user was already logged in
                        loginService.login().then(
                            function (response) {
                                // Finish loading
                                $location.search({'token': null});
                            },
                            function () {
                                // User is not logged in
                                // If the user wanted to go to the new-captain page, let him get the token again.
                                // Else, go to login page
                                $location.search('token', null);

                                if ($location.path() == ('/' + language + '/new-captain')) {
                                    $location.path('/' + language + '/invalid-token');
                                } else {
                                    $location.path('/' + language + '/login').search({
                                        'redirect': true,
                                        'from': $location.url().substring(1)
                                    });
                                }
                            }
                        )
                    }
                );
            }
            // There's no token
            else {
                loginService.login();
            }

            // Set cookie policy cookie
            if (!$cookies.get('cookiePolicyAccepted')) $cookies.put('cookiePolicyAccepted', 0);

            console.log('fine run');
        }]
);

angular.module('users').config(['uiSelectConfig', function (uiSelectConfig) {
    uiSelectConfig.theme = 'bootstrap';
}]);

// If response (from Backend) is an error, redirect to right error page
angular.module('users').factory('httpResponseInterceptor', [
    '$q', '$location', 'errorHandlingService', '$routeParams',
    function ($q, $location, errorHandlingService, $routeParams) {
        return {
            responseError: function (rejection) {
                var language = $routeParams.lang;

                // Check if request url contains api url
                if (-1 == rejection.config.url.indexOf(__env.apiUrl)) {
                    // Error is coming from another resource (typically an external resource)
                    return $q.reject(rejection);
                }

                // Check if we need to ignore a specific error (500 error can't be ignored)
                // Ignoring an error means that the controller is going to manage it in its own way,
                // instead of directly redirecting the user to the error pages
                if (rejection.status == 500) {
                    $location.path('/' + language + '/error');
                    return $q.reject(rejection);
                }
                if (!rejection.data) {
                    return;
                }
                if (errorHandlingService.canBeIgnored(rejection.data.errors[0].code)) {
                    return $q.reject(rejection);
                }

                // Error is coming from the backend
                switch (rejection.status) {
                    case 403:
                        $location.path('/' + language + '/not-authorized');
                        return $q.reject(rejection);
                    case 404:
                        $location.path('/' + language + '/not-found');
                        return $q.reject(rejection);
                    default:
                        return $q.reject(rejection);
                }
            }
        };
    }]);
angular.module('users').config([
    '$httpProvider',
    function ($httpProvider) {
        $httpProvider.interceptors.push('httpResponseInterceptor');
    }]);