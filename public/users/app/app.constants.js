// Constants
angular.module('users').constant('experienceColors', {
    'trips-and-tours': 'light-blue',
    'luxury': 'light-blue',
    'romantic': 'light-blue',
    'dinners': 'light-blue',
    'aperitif': 'light-blue',
    'diving': 'light-blue',
    'fishing': 'light-blue',
    'other': 'light-blue'
});
angular.module('users').constant('locationKeys', [
    {
        name: 'south_sardinia',
        lat: 39.4731,
        lng: 9.0280,
        distance: 100,
        zoom: 8
    }, {
        name: 'north_sardinia',
        lat: 40.7918,
        lng: 8.9730,
        distance: 100,
        zoom: 8
    }, {
        name: 'campania',
        lat: 40.5559,
        lng: 14.9236,
        distance: 150,
        zoom: 8
    }, {
        name: 'lazio',
        lat: 41.7311,
        lng: 12.2863,
        distance: 130,
        zoom: 8
    }, {
        name: 'phuket',
        lat: 7.95193,
        lng: 98.3381,
        distance: 60,
        zoom: 8
    }, {
        name: 'koh_samui',
        lat: 9.79362,
        lng: 99.904137,
        distance: 70,
        zoom: 9
    }]);
angular.module('users').constant('experienceTypesGroups', [
    {
        name: 'adventure',
        headerKey: 'adventure_header',
        subKey: 'adventure_sub',
        backgroundUrl: 'https://res.cloudinary.com/naustrip/image/upload/q_69/v1496735609/production/assets/img/search/adventure.jpg',
        types: 'diving,fishing,trips-and-tours'
    }, {
        name: 'relaxing_boat_trips',
        headerKey: 'trip_header',
        subKey: 'trip_sub',
        backgroundUrl: 'https://res.cloudinary.com/naustrip/image/upload/q_69/v1496735608/production/assets/img/search/trips-and-tours.jpg',
        types: 'trips-and-tours,other'
    }, {
        name: 'food_and_drinks',
        headerKey: 'eat_header',
        subKey: 'eat_sub',
        backgroundUrl: 'https://res.cloudinary.com/naustrip/image/upload/q_69/v1496735608/production/assets/img/search/eat.jpg',
        types: 'dinners,aperitif'
    }, {
        name: 'romantic_getaways',
        headerKey: 'romantic_header',
        subKey: 'romantic_sub',
        backgroundUrl: 'https://res.cloudinary.com/naustrip/image/upload/q_69/v1496735607/production/assets/img/search/romantic.jpg',
        types: 'romantic'
    }, {
        name: 'boat_and_breakfast',
        headerKey: 'night_header',
        subKey: 'night_sub',
        backgroundUrl: 'https://res.cloudinary.com/naustrip/image/upload/q_69/v1496735609/production/assets/img/search/night-aboard.jpg',
        types: 'nights'
    }, {
        name: 'default',
        headerKey: 'header',
        subKey: 'sub',
        backgroundUrl: 'https://res.cloudinary.com/naustrip/image/upload/q_69/v1496970254/production/assets/img/search/default.jpg'
    }]);
angular.module('users').constant('topDestinations', [
    {
        name: 'cagliari_villasimius',
        headerKey: 'cagliari_top_header',
        subKey: 'cagliari_top_sub',
        backgroundUrl: 'https://res.cloudinary.com/naustrip/image/upload/q_69/v1496970213/production/assets/img/search/sardinia.jpg',
        lat: 39.35134,
        lng: 9.34819,
        distance: 90,
        zoom: 9
    }, {
        name: 'amalfi_islands',
        headerKey: 'amalfi_top_header',
        subKey: 'amalfi_top_sub',
        backgroundUrl: 'https://res.cloudinary.com/naustrip/image/upload/q_69/v1496970212/production/assets/img/search/amalfi.jpg',
        lat: 40.63515,
        lng: 14.29737,
        distance: 150,
        zoom: 8
    }, {
        name: 'costa_smeralda',
        headerKey: 'costa_top_header',
        subKey: 'costa_top_sub',
        backgroundUrl: 'https://res.cloudinary.com/naustrip/image/upload/q_69/v1496970213/production/assets/img/search/sardinia.jpg',
        lat: 40.8781647,
        lng: 9.4821412,
        distance: 70,
        zoom: 9
    }]);
angular.module('users').constant('currencyValuesArray', [
    {
        name: 'THB',
        floor: 50,
        ceil: 40000,
        step: 400,
        minRange: 400,
    }, {
        name: 'EUR',
        floor: 5,
        ceil: 1000,
        step: 10,
        minRange: 10,
    }, {
        name: 'USD',
        floor: 5,
        ceil: 1000,
        step: 10,
        minRange: 10,
    }]);
angular.module('users').constant('validLanguages', ['en', 'it']);
angular.module('users').constant('destinationAreas', [
    {
        "type": "destination-areas",
        "id": 1,
        "attributes": {
            "name": "mediterranean_sea"
        }
    },
    {
        "type": "destination-areas",
        "id": 2,
        "attributes": {
            "name": "caribbean"
        }
    },
    {
        "type": "destination-areas",
        "id": 3,
        "attributes": {
            "name": "south_east_asia"
        }
    }
]);
angular.module('users').constant('destinations', [
    {
        "type": "destinations",
        "id": "1",
        "attributes": {
            "name": "south_sardinia",
            "lat": 39.4731,
            "lng": 9.0280,
            "distance": 100,
            "zoom": 8
        },
        "relationships": {
            "destination_area": {
                "data": {
                    "id": "1",
                    "type": "destination-areas"
                }
            }
        }
    },
    {
        "type": "destinations",
        "id": "2",
        "attributes": {
            "name": "north_sardinia",
            "lat": 40.7918,
            "lng": 8.9730,
            "distance": 100,
            "zoom": 8
        },
        "relationships": {
            "destination_area": {
                "data": {
                    "id": "1",
                    "type": "destination-areas"
                }
            }
        }
    },
    {
        "type": "destinations",
        "id": "5",
        "attributes": {
            "name": "campania",
            "lat": 40.5559,
            "lng": 14.9236,
            "distance": 150,
            "zoom": 8
        },
        "relationships": {
            "destination_area": {
                "data": {
                    "id": "1",
                    "type": "destination-areas"
                }
            }
        }
    },
    {
        "type": "destinations",
        "id": "6",
        "attributes": {
            "name": "lazio",
            "lat": 41.7311,
            "lng": 12.2863,
            "distance": 130,
            "zoom": 8
        },
        "relationships": {
            "destination_area": {
                "data": {
                    "id": "1",
                    "type": "destination-areas"
                }
            }
        }
    },
    {
        "type": "destinations",
        "id": "9",
        "attributes": {
            "name": "phuket",
            "lat": 7.95193,
            "lng": 98.3381,
            "distance": 60,
            "zoom": 8
        },
        "relationships": {
            "destination_area": {
                "data": {
                    "id": "3",
                    "type": "destination-areas"
                }
            }
        }
    },
    {
        "type": "destinations",
        "id": "10",
        "attributes": {
            "name": "koh_samui",
            "lat": 9.51202,
            "lng": 100.014,
            "distance": 70,
            "zoom": 9
        },
        "relationships": {
            "destination_area": {
                "data": {
                    "id": "3",
                    "type": "destination-areas"
                }
            }
        }
    }
]);
angular.module('users').constant('currencyCodes', {
    "ALL": "Lek",
    "AFN": "؋",
    "ARS": "$",
    "AWG": "ƒ",
    "AUD": "$",
    "AZN": "ман",
    "BSD": "$",
    "BBD": "$",
    "BYN": "Br",
    "BZD": "BZ$",
    "BMD": "$",
    "BOB": "$b",
    "BAM": "KM",
    "BWP": "P",
    "BGN": "лв",
    "BRL": "R$",
    "BND": "$",
    "KHR": "៛",
    "CAD": "$",
    "KYD": "$",
    "CLP": "$",
    "CNY": "¥",
    "COP": "$",
    "CRC": "₡",
    "HRK": "kn",
    "CUP": "₱",
    "CZK": "Kč",
    "DKK": "kr",
    "DOP": "RD$",
    "XCD": "$",
    "EGP": "£",
    "SVC": "$",
    "EUR": "€",
    "FKP": "£",
    "FJD": "$",
    "GHS": "¢",
    "GIP": "£",
    "GTQ": "Q",
    "GGP": "£",
    "GYD": "$",
    "HNL": "L",
    "HKD": "$",
    "HUF": "Ft",
    "ISK": "kr",
    "INR": "",
    "IDR": "Rp",
    "IRR": "﷼",
    "IMP": "£",
    "ILS": "₪",
    "JMD": "J$",
    "JPY": "¥",
    "JEP": "£",
    "KZT": "лв",
    "KPW": "₩",
    "KRW": "₩",
    "KGS": "лв",
    "LAK": "₭",
    "LBP": "£",
    "LRD": "$",
    "MKD": "ден",
    "MYR": "RM",
    "MUR": "₨",
    "MXN": "$",
    "MNT": "₮",
    "MZN": "MT",
    "NAD": "$",
    "NPR": "₨",
    "ANG": "ƒ",
    "NZD": "$",
    "NIO": "C$",
    "NGN": "₦",
    "NOK": "kr",
    "OMR": "﷼",
    "PKR": "₨",
    "PAB": "B/.",
    "PYG": "Gs",
    "PEN": "S/.",
    "PHP": "₱",
    "PLN": "zł",
    "QAR": "﷼",
    "RON": "lei",
    "RUB": "₽",
    "SHP": "£",
    "SAR": "﷼",
    "RSD": "Дин.",
    "SCR": "₨",
    "SGD": "$",
    "SBD": "$",
    "SOS": "S",
    "ZAR": "R",
    "LKR": "₨",
    "SEK": "kr",
    "CHF": "CHF",
    "SRD": "$",
    "SYP": "£",
    "TWD": "NT$",
    "THB": "฿",
    "TTD": "TT$",
    "TRY": "",
    "TVD": "$",
    "UAH": "₴",
    "GBP": "£",
    "USD": "$",
    "UYU": "$U",
    "UZS": "лв",
    "VEF": "Bs",
    "VND": "₫",
    "YER": "﷼",
    "ZWD": "Z$"
});



