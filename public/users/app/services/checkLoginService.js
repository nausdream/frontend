angular.module('users').service('checkLoginService',
    function () {
        var ctrl = this;

        // Promise that gets resolved when user logs in
        ctrl.getLoginPromise = function () {
            return ctrl.promise;
        };

        ctrl.setLoginPromise = function (promise) {
            ctrl.promise = promise;
        }
    }
);