angular.module('users').service('statusCodeService',
    ['$rootScope',
        function ($rootScope) {
            var ctrl = this;

            $rootScope.statusCode = '200';

            ctrl.set = function (code) {
                $rootScope.statusCode = code;
            };
        }]);