angular.module('users').service('loginService',
    ['$cookies', '$rootScope', 'translationService', 'currencyService', 'requestService', '$q', '$http', 'userService', 'checkLoginService', '__env', '$translate',
        function ($cookies, $rootScope, translationService, currencyService, requestService, $q, $http, userService, checkLoginService, __env, $translate) {
            var ctrl = this;

            // If user has a JWT, load his/her data into userService
            ctrl.login = function () {
                var deferred = $q.defer();

                // Make this promise available to other controllers and services
                checkLoginService.setLoginPromise(deferred.promise);

                // If userService has already a user, return it without going forward
                if (userService.get() != null && userService.get().attributes) {
                    deferred.resolve(userService.get());
                    return deferred.promise;
                }

                // Check if user is logged in
                var accessToken = $cookies.get('access_token');
                if (!accessToken) {
                    ctrl.unSetCookies();
                    deferred.reject('No JWT');
                    return deferred.promise;
                }
                // Access token is set, get user ID from JWT
                var tmp = accessToken.substr(accessToken.indexOf(".") + 1);
                var userDataEncoded = tmp.substr(0, tmp.indexOf("."));
                var userData = JSON.parse(atob(userDataEncoded));
                var userId = userData.sub;

                // Get user and save his/her data
                requestService.getSingleAsync('users', userId, {include: 'languages'}).then(
                    function (response) {
                        var language = response.data.data.attributes.language;
                        var currency = response.data.data.attributes.currency;

                        // Save user in userService
                        userService.save(response.data.data);
                        // Set language: don't patch user
                        translationService.changeLanguage(language, false);
                        // Set currency: don't patch user and don't reload page
                        currencyService.changeCurrency(currency, false, false);

                        // Resolve promise with logged user
                        deferred.resolve(response);

                        // Emit userLoggedIn event
                        $rootScope.$emit('userLoggedIn', response.data.data);
                    },
                    function (response) {
                        if (response.status == 403 || response.status == 404) {
                            // Unset backend cookies
                            ctrl.logout();
                        }
                        deferred.reject(response);
                    }
                );

                return deferred.promise;
            };

            ctrl.setCookies = function (response) {
                // Set access_token and csrf
                $cookies.put('access_token', response.data.meta.access_token);
                $cookies.put('csrf_token', response.data.meta.csrf_token);
            };

            ctrl.unSetCookies = function () {
                // Unset access_token and csrf
                $cookies.remove('access_token');
                $cookies.remove('csrf_token');
            };

            // Login captain
            ctrl.loginPwd = function (email, password) {
                var requestBody = {meta: {email: email, password: password}};

                return $http.post(__env.apiUrl + 'auth/password', requestBody);
            };

            // Logout
            ctrl.logout = function () {
                var promise = requestService.postLogout();
                promise.then(
                    function () {
                        ctrl.unSetCookies();
                        userService.delete();

                        // Make this promise available to other controllers and services
                        var deferred = $q.defer();
                        checkLoginService.setLoginPromise(deferred.promise);
                        deferred.reject();

                        // Emit userLoggedOut event
                        $rootScope.$emit('userLoggedOut');
                    },
                    function () {
                        ctrl.unSetCookies();
                        userService.delete();

                        // Make this promise available to other controllers and services
                        var deferred = $q.defer();
                        checkLoginService.setLoginPromise(deferred.promise);
                        deferred.reject();

                        // Emit userLoggedOut event
                        $rootScope.$emit('userLoggedOut');
                    }
                );
                return promise;
            }
        }
    ]);