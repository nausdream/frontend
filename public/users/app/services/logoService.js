/**
 * Get/Set the current page
 */
angular.module('users').service('logoService', ['$rootScope', function ($rootScope) {
    var logo = 'logo_poli';

    this.get = function () {
        return logo;
    };

    $rootScope.$on('$routeChangeSuccess', function () {
        logo = 'logo_poli';
    });

    $rootScope.$on('pageChanged', function (event, page) {
        if (page === 'home') {
            logo = 'logo_mono_white';
        }
    });
}]);