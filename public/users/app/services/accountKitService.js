/**
 * Created by lucas on 12/06/2017.
 */
angular.module('users').service('accountKitService', ['$rootScope', 'requestService', 'userService',
    function ($rootScope, requestService, userService) {
        var ctrl = this;

        var openPopup = function (countryCode, phoneNumber, userId) {
            AccountKit.login(
                'PHONE',
                {countryCode: countryCode, phoneNumber: phoneNumber}, // will use default values if not specified
                function (response) {
                    if (response.status === "PARTIALLY_AUTHENTICATED") {
                        console.log(response);
                        var code = response.code;
                        var csrf = response.state;
                        // Send code to server to exchange for access token
                        requestService.verifyPhone(userId, code).then(
                            function (response) {
                                // Save user
                                userService.save(response.data.data);

                                // Notify rootscope
                                $rootScope.$emit('phoneVerified');
                            },
                            function (response) {
                                // Notify rootscope
                                $rootScope.$emit('errorVerifyingPhone', response);
                            }
                        );
                    }
                    if (response.status === "NOT_AUTHENTICATED") {
                        // handle authentication failure
                        console.log(response);
                    }
                    if (response.status === "BAD_PARAMS") {
                        // handle bad parameters
                        console.log(response);
                    }
                }
            );
        };

        ctrl.openAK = function (countries, phone, userId) {
            if (!countries) return;
            if (!userId) return;

            if (phone) {
                console.log('phone provided');
                var PhoneNumber = require('awesome-phonenumber');
                var ph = new PhoneNumber(phone);
                var regionCode = ph.getRegionCode();
                var significantNumber = ph.getNumber('significant');
                if (!regionCode) {
                    regionCode = 'IT';
                }
                if (!significantNumber) {
                    significantNumber = '0000000';
                }

                var selectedCountry = countries.filter(function (country) {
                    return country.code.toLowerCase() == regionCode.toLowerCase();
                });
                if (selectedCountry) {
                    var country = selectedCountry[0];
                }

                var countryCode = '+' + PhoneNumber.getCountryCodeForRegionCode(country.code);
                var phoneNumber = significantNumber;

                if (countryCode && phoneNumber) {
                    openPopup(countryCode, phoneNumber, userId);
                }
            }
            else {
                console.log('phone not provided');
                openPopup(null, null, userId);
            }
        }
    }]
);