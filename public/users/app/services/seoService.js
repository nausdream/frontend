angular.module('users').service('seoService',
    ['$rootScope', 'requestService', '$q', 'customPartialLoader', '$filter', 'translationService', '__env', '$location', '$translate', '$cookies',
        function ($rootScope, requestService, $q, customPartialLoader, $filter, translationService, __env, $location, $translate, $cookies) {
            var ctrl = this;
            $rootScope.metaDescription = '';
            $rootScope.metaKeywords = '';
            $rootScope.metaTitle = '';
            $rootScope.ogUrl = '';
            $rootScope.ogType = '';
            $rootScope.ogImage = '';
            $rootScope.ogAuthor = '';
            $rootScope.ogSection = '';
            ctrl.deferredSeo = $q.defer();

            var translations = {};
            translations.loading = true;
            var viewName = 'default-seo';
            var pages = ['default_seo'];

            if (customPartialLoader.isPartLoaded(viewName, __env.fallbackLanguage)) {
                ctrl.deferredSeo.resolve();
            } else {
                $rootScope.$on(viewName + 'viewHasBeenTranslated', function (event) {
                    ctrl.deferredSeo.resolve();
                });
            }

            ctrl.language = $cookies.get('language');
            // Load translations and fire event when done
            translationService.translate(pages, viewName, ctrl.language);

            ctrl.metaDescription = function () {
                return $rootScope.metaDescription;
            };
            ctrl.metaKeywords = function () {
                return $rootScope.metaKeywords;
            };
            ctrl.metaTitle = function () {
                return $rootScope.metaTitle;
            };
            ctrl.ogUrl = function () {
                return $rootScope.ogUrl;
            };
            ctrl.ogType = function () {
                return $rootScope.ogType;
            };
            ctrl.ogImage = function () {
                return $rootScope.ogImage;
            };
            ctrl.ogAuthor = function () {
                return $rootScope.ogAuthor;
            };
            ctrl.ogSection = function () {
                return $rootScope.ogSection;
            };
            ctrl.reset = function () {
                $rootScope.metaDescription = '';
                $rootScope.metaKeywords = '';
                $rootScope.metaTitle = '';
                $rootScope.ogUrl = '';
                $rootScope.ogType = '';
                $rootScope.ogImage = '';
                $rootScope.ogAuthor = '';
                $rootScope.ogSection = '';
            };
            ctrl.setMetaDescription = function (newMetaDescription) {
                if (!newMetaDescription) {
                    ctrl.deferredSeo.promise.then(
                        function () {
                            $translate('default_seo.meta_description').then(
                                function (translation) {
                                    $rootScope.metaDescription = translation;
                                },
                                null
                            );
                        }
                    )
                }
                $rootScope.metaDescription = newMetaDescription;
            };
            ctrl.setMetaTitle = function (newMetaTitle) {
                if (!newMetaTitle) {
                    ctrl.deferredSeo.promise.then(
                        function () {
                            $translate('default_seo.meta_title').then(
                                function (translation) {
                                    $rootScope.metaTitle = translation;
                                },
                                null
                            );
                        }
                    )
                }
                $rootScope.metaTitle = newMetaTitle;
            };
            ctrl.appendMetaKeywords = function (newKeywords) {
                for (var key in newKeywords) {
                    if ($rootScope.metaKeywords === '') {
                        $rootScope.metaKeywords += newKeywords[key].name;
                    } else {
                        $rootScope.metaKeywords += ', ' + newKeywords[key].name;
                    }
                }
            };
            ctrl.setOgUrl = function (newOgUrl) {
                if (!newOgUrl) {
                    $rootScope.ogUrl = __env + $location.path();
                }
                $rootScope.ogUrl = newOgUrl;
            };
            ctrl.setOgType = function (newOgType) {
                if (!newOgType) {
                    $rootScope.ogType = 'website';
                }
                $rootScope.ogType = newOgType;
            };
            ctrl.setOgImage = function (newOgImage) {
                if (!newOgImage) {
                    $rootScope.ogImage = 'https://res.cloudinary.com/naustrip/image/upload/v1490639778/production/assets/img/home/img-prefooter.jpg';
                }
                $rootScope.ogImage = newOgImage;
            };
            ctrl.setOgAuthor = function (newOgAuthor) {
                if (!newOgAuthor) {
                    $rootScope.ogAuthor = 'Nausdream.com';
                }
                //TODO: aggiungere nome del capitano
                $rootScope.ogAuthor = 'Nausdream.com';
            };
            ctrl.setOgSection = function (newOgSection) {
                if (!newOgSection) {
                    $rootScope.ogSection = '';
                }
                $rootScope.ogSection = newOgSection;
            };
            ctrl.getExperienceFromSlugUrl = function (slugUrl, params) {
                return requestService.getExperienceFromSlugUrl(slugUrl, params);
            };
            ctrl.setMetaTitleAndDescription = function (viewName, default_meta) {
                if (!default_meta) {
                    $translate(viewName + '.meta_title').then(
                        function (translation) {
                            var metaTitle = translation;
                            ctrl.setMetaTitle(metaTitle);
                        },
                        null
                    );
                    $translate(viewName + '.meta_description').then(
                        function (translation) {
                            var metaDescription = translation;
                            ctrl.setMetaDescription(metaDescription);
                        },
                        null
                    );
                } else {
                    ctrl.setMetaTitle(null);
                    ctrl.setMetaDescription(null);
                }
                ctrl.setOgUrl();
                ctrl.setOgType();
                ctrl.setOgImage();
                ctrl.setOgAuthor();
                ctrl.setOgSection();
            };
            ctrl.setMetaTranslations = function (viewName, default_meta = true) {
                if (translationService.arePagesLoaded([viewName], $translate.use())) {
                    ctrl.setMetaTitleAndDescription(viewName, default_meta);
                } else {
                    $rootScope.$on(viewName + 'viewHasBeenTranslated', function (event) {
                        ctrl.setMetaTitleAndDescription(viewName, default_meta);
                    });
                }
            }
        }]);