angular.module('users').service('currencyService',
    ['$cookies', '__env', 'requestService', '$rootScope', '$route', 'fixedListService', '$q', 'checkLoginService', 'userService', 'currencyCodes',
        function ($cookies, __env, requestService, $rootScope, $route, fixedListService, $q, checkLoginService, userService, currencyCodes) {
            var ctrl = this;
            ctrl.currencies = $q.defer();

            // Get currencies
            ctrl.currencies.resolve(currencyCodes);

            // Set cookie if not already set
            ctrl.bootstrap = function () {
                var currency = $cookies.get('currency');

                if (!currency) {
                    // Cookie is set: tell $translate service to use this currency
                    $cookies.put("currency", __env.fallbackCurrency);
                }
            };

            // Change language. Don't patch user if we are in the bootstrap of the app
            ctrl.changeCurrency = function (currency, reloadPage, patchUser = true) {
                // Set cookie
                $cookies.put("currency", currency);

                // User is logged in
                if (patchUser) {
                    checkLoginService.getLoginPromise().then(
                        function (response) {
                            userService.setAttribute('currency', currency);
                            requestService.patchUser(userService.get()).then(
                                function (response) {
                                    /**
                                     *  Why broadcast? Because we can catch the event on child scopes
                                     *  and unregister the events as they are being destroyed.
                                     *  So, if for example we do a $route.reload() in a component,
                                     *  we need to unregister the event as soon as the component is destroyed.
                                     *  But this is the standard behaviour
                                     *  if we listen to the event on the component $scope instead
                                     */
                                    $rootScope.$broadcast('currencyChanged', currency);

                                    if (!reloadPage) return;
                                    $route.reload();
                                }
                            );
                        },
                        function (response) {
                            // Emit event on $rootScope even if the user is not really logged in
                            $rootScope.$broadcast('currencyChanged', currency);
                        }
                    );
                } else {
                    // Emit event on $rootScope
                    $rootScope.$broadcast('currencyChanged', currency);
                }
            };

            // Get a symbol given a currency
            ctrl.getCurrencySymbol = function (currency) {
                var deferred = $q.defer();

                ctrl.currencies.promise.then(
                    function (response) {
                        deferred.resolve(response[currency]);
                    },
                    function (response) {
                        deferred.reject(response)
                    }
                );

                return deferred.promise;
            };

            // Get list of currencies
            ctrl.getCurrencies = function () {
                return ctrl.currencies.promise;
            }
        }]);