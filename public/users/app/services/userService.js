/**
 * CRU user API
 * CREATE/UPDATE -> save()
 * READ -> get()
 */

angular.module('users').service('userService', ['$http', '$rootScope', function ($http, $rootScope) {
    var ctrl = this;
    var user = null;
    var included = {};
    var relationships = {};

    ctrl.save = function (newUser) {
        user = newUser;

        $rootScope.$emit('userUpdated', user);
    };

    ctrl.saveRelationships = function (newRelationships) {
        relationships = newRelationships;
    };
    ctrl.getRelationships = function () {
        return relationships;
    };

    ctrl.get = function () {
        return user;
    };

    // Reset user
    ctrl.delete = function () {
        user = undefined;
        relationships = undefined;
        included = undefined;
    };

    ctrl.getIncluded = function () {
        return included;
    };

    ctrl.setIncluded = function (newIncluded) {
        included = newIncluded;
    };

    ctrl.setAttribute = function (name, value) {
        user.attributes[name] = value
    };

    // Get array of user attributes and set them to user
    // attributes = [{name: 'Luca'}, {type: 'private'}]
    ctrl.setAttributes = function (attributes) {
        angular.forEach(attributes, function (value, key) {
            ctrl.setAttribute(key, value);
        });
    };

    // Determine if all user fields have already been set
    // TODO CHECK OPTIONAL FIELDS TOO
    // TODO IF PARTNER, CHECK OTHER FIELDS TOO
    ctrl.isUserSet = function (usr, requirePassword = false, requireNumber = false, checkOptionalFields = false) {
        if (!usr) usr = user.attributes;

        var isUserSet = true;

        var required = ['first_name', 'last_name', 'email', 'phone', 'currency'];
        var trueFields = [];
        if (requirePassword) trueFields.push('is_set_password');
        if (requireNumber) trueFields.push('is_phone_activated');

        // PRIVATE
        if (usr.captain_type == 'private') {
            required = required.concat([
                'sex', 'birth_date', 'nationality'
            ]);

            // IS SKIPPER
            if (!usr.is_skipper) {
                required = required.concat(['first_name_skipper', 'last_name_skipper']);
            }
        }
        // COMPANY OR SPORTS ASSOCIATION
        else {
            required = required.concat([
                'enterprise_address', 'enterprise_city', 'enterprise_country',
                'enterprise_name', 'enterprise_zip_code'
            ]);
            trueFields.push('skippers_have_license');

            // Card price check
            if (usr.has_card) required = required.concat(['card_price']);
        }

        // Return false if any of the required fields is missing
        required.forEach(function (field) {
            if (usr[field] == null){
                console.log(field + ' is required');
                isUserSet = false;
            }
        });

        // Return false if any of the boolean which have to be true is false
        trueFields.forEach(function (field) {
            if (!usr[field]) {
                console.log(field + ' must be true');
                isUserSet = false
            }
        });

        // User is set
        return isUserSet;
    }

    ctrl.checkNotifications = function(id){
        return $http({
            method: 'GET',
            url: __env.apiUrl + 'users/' + id + '/notifications'
        })
        .then(function(response){
            return response.data;
        }, function(err){
            throw err;
        })
    }
}]);