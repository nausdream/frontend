/**
 * Created by lucas on 13/07/2017.
 */
angular
    .module('users')
    .factory('PriceMarkerFactory', [
        'mapsService', '$location', '$timeout', 'currencyService', '$filter',
        function (mapsService, $location, $timeout, currencyService, $filter) {
            // Create custom marker (OverlayView)
            function PriceMarker(latlng, map, price, url, currency, listingElementId) {
                this.latlng = latlng;
                this.price = price;
                this.url = url;
                this.currency = currency;
                this.listingElementId = listingElementId;
                this.setMap(map);
            }

            mapsService.mapsInitialized.then(
                function () {
                    PriceMarker.prototype = new google.maps.OverlayView();

                    // This gets called everytime the marker is draw
                    PriceMarker.prototype.draw = function () {

                        var self = this;

                        var div = this.div;
                        let width = 56;
                        let height = 50;

                        if (!div) {

                            // Marker box
                            div = this.div = document.createElement('div');

                            // Listing container
                            let listing = angular.element(document.getElementById(self.listingElementId));

                            div.className = 'price-marker';
                            div.style.width = width + 'px';
                            div.style.height = height + 'px';

                            // Respond to mouseover
                            google.maps.event.addDomListener(div, "mouseenter", function (event) {
                                div.classList.add('active');

                                // Mark hover listing
                                listing.addClass('listing-hover');
                            });
                            google.maps.event.addDomListener(div, "mouseleave", function (event) {
                                div.classList.remove('active');

                                // Reset hover listing
                                listing.removeClass('listing-hover');
                            });

                            /* Respond to click.
                             * We don't use the event 'click' because it can lead to not wanted behaviours
                             * (like dragging the map and going to the esperience when the user releases the mouse)
                             */
                            google.maps.event.addDomListener(div, "mousedown", function (event) {
                                self.mousePressed = true;

                                // Disallow map drag
                                event.stopPropagation();
                            });
                            google.maps.event.addDomListener(div, "mouseup", function (event) {
                                if (self.mousePressed) {
                                    $timeout(function () {
                                        $location.path(self.url);
                                    }, 0);
                                }
                                self.mousePressed = false;
                            });

                            // On listing mouseenter, make marker active
                            listing.on("mouseenter", function () {
                                div.classList.add('active');
                            });
                            // On listing mouseenter, clear marker active
                            listing.on("mouseleave", function () {
                                div.classList.remove('active');
                            });

                            // Price tag
                            var price = document.createElement('span');
                            price.className = 'price-marker-price text-center h5 bold';

                            currencyService.getCurrencies().then(
                                function (response) {
                                    self.currencies = response;

                                    price.innerHTML = $filter('nausCurrency')(self.price, self.currency, self.currencies);

                                    div.appendChild(price);

                                    var panes = self.getPanes();
                                    panes.overlayImage.appendChild(div);
                                },
                                null
                            );
                        }

                        var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

                        if (point) {
                            div.style.left = (point.x - width / 2) + 'px';
                            div.style.top = (point.y - (height + 8)) + 'px';
                        }
                    };

                    PriceMarker.prototype.remove = function () {
                        if (this.div) {
                            this.div.parentNode.removeChild(this.div);
                            this.div = null;
                        }
                    };
                }, null
            );

            return PriceMarker;
        }]
    );