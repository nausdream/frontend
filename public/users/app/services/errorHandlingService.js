angular.module('users').service('errorHandlingService',
    function () {
        var ctrl = this;

        var ignoreErrors = [
            'incorrect_password',
            'user_not_captain_or_partner',
            'user_not_captain_or_partner_id_match',
            'user_not_captain_or_partner_id_match_password_already_set',
            'booking_already_exists',
            'conversation_already_exists',
            'coupon_not_found',
            'not_authorized_coupon',
            'not_authorized_coupon_experience',
            'not_authorized_coupon_area',
            'booking_not_possible',
            'email_not_found',
            'user_has_already_password',
            'no_account_relation_with_jwt',
            'token_not_found',
            'token_tampered_with',
            'invalid_csrf_token',
            'token_expired'
        ];

        // Returns true if error has to be ignored
        ctrl.canBeIgnored = function (error) {
            return ignoreErrors.indexOf(error) != -1;
        }
    }
);