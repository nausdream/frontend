angular.module('users').service('adwordsService', [
    '$window', '__env',
    function ($window, __env) {
        // Conversion labels
        var google_conversion_label = {
            'call-us': "wiPRCK-W_nIQ7MzRwQM",
            'chat-captain': "HsC4CPnf53IQ7MzRwQM",
            'live-chat': "movICNOK4XIQ7MzRwQM",
            'booking': "NLogCJHa53IQ7MzRwQM",
            'view-experience': "hvScCM6D3HIQ7MzRwQM"
        };
        // Basic settings for AdWords Conversion
        var googleTrackConversion = function (conversionLabel) {
            let params = {
                google_conversion_id: 942958188,
                google_conversion_language: "en",
                google_conversion_format: "3",
                google_conversion_color: "ffffff",
                google_conversion_label: google_conversion_label[conversionLabel],
                google_conversion_value: 0,
                google_remarketing_only: false
            };

            if (typeof $window.google_trackConversion == 'function'){
                $window.google_trackConversion(params);
            }
        };
        return {
            sendConversion: function (conversion) {
                if (__env.appEnv == 'production') {
                    googleTrackConversion(conversion);
                }
                else {
                    console.log('Conversion:', conversion);
                }
            }
        };
    }
]);