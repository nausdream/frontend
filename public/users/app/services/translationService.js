angular.module('users').service('translationService', [
    '$cookies', '$rootScope', '$translate', 'requestService', '$http', 'userService', 'customPartialLoader',
    'checkLoginService', 'localeService', '$routeParams', 'validLanguages', '$q',
    function ($cookies, $rootScope, $translate, requestService, $http, userService, customPartialLoader,
              checkLoginService, localeService, $routeParams, validLanguages, $q) {
        var ctrl = this;
        ctrl.parts = {};
        validLanguages.forEach(function (language) {
            ctrl.parts[language] = [];
        });

        // Change language
        ctrl.changeLanguage = function (language, patchUser = true) {
            // Language change has started
            $rootScope.$emit('languageChanging', language);

            // Set cookie
            $cookies.put("language", language);

            // Change app language
            $translate.use(language).then(
                function () {
                    // Broadcast event to $rootScope and children
                    $rootScope.$broadcast('languageChanged', language);
                }
            );
            // Change locale (for datepicker)
            localeService.setLocale(language);

            // Update rootScope var (for html tag)
            $rootScope.language = language;

            // If user is logged in
            if (patchUser) {
                checkLoginService.getLoginPromise().then(
                    function (response) {
                        userService.setAttribute('language', language);
                        requestService.patchUser(userService.get()).then();
                    },
                    function (response) {

                    }
                );
            }
        };

        // Check if pages are loaded
        ctrl.arePagesLoaded = function (pages, language) {
            if (!language) language = $routeParams.lang;

            var loaded = true;

            pages.forEach(function (page) {
                if (!customPartialLoader.isPartLoaded(page, language)) {
                    loaded = false;
                }
            });

            return loaded;
        };

        // Translate view with its parts and emit message when done
        ctrl.translateView = function (viewName, pages, language) {
            if (!language) language = $routeParams.lang;

            var deferred = $q.defer();

            // When
            let viewTranslated = $rootScope.$on('$translateLoadingSuccess', function () {
                if (ctrl.arePagesLoaded(pages)) {
                    $rootScope.$emit(viewName + 'viewHasBeenTranslated');

                    deferred.resolve();
                    viewTranslated();
                }
            });

            // Stop loading when translations are ready
            // If translations are not loaded yet, then listen for completion event
            if (!ctrl.arePagesLoaded(pages)) {
                customPartialLoader.addPart(pages.join());
            } else {
                $rootScope.$emit(viewName + 'viewHasBeenTranslated');

                return $translate.refresh();
            }

            return deferred.promise;
        };

        /**
         * Take pages, viewName, and optional language.
         * Translate all the pages and return resolved promise
         * @param pages
         * @param viewName
         * @param language
         */
        ctrl.translate = function (pages, viewName, language) {
            if (!language) language = $routeParams.lang || $cookies.get('language');

            var deferred = $q.defer();

            $translate.use(language).then(
                function () {
                    let toLoad = [];
                    pages.forEach(function (page) {
                        if (ctrl.parts[language].indexOf(page) == -1){
                            toLoad.push(page);
                            ctrl.parts[language].push(page);
                        }
                    });

                    ctrl.translateView(viewName, toLoad, language).then(
                        function () {
                            deferred.resolve();
                        }
                    )
                }, null
            );

            return deferred.promise;
        }
    }
]);