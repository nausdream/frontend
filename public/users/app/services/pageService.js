/**
 * Get/Set the current page
 */
angular.module('users').service('pageService', ['$rootScope', function ($rootScope) {
    var ctrl = this;
    var page = '';
    var isPrivate = false;

    ctrl.get = function () {
        return page;
    };

    // Fire event when new page is set
    ctrl.set = function (newPage) {
        page = newPage;

        $rootScope.$emit('pageChanged', newPage);
    };

    // Check if a page is private (can't be viewed without being logged in)
    ctrl.setPrivate = function (newPrivateValue) {
        isPrivate = newPrivateValue;
    };
    // Get page private status
    ctrl.getPrivate = function () {
        return isPrivate;
    }
}]);