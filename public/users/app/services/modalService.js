/**
 * This service set the open-modal class on body element
 */
angular.module('users').service('modalService', ['$rootScope', function ($rootScope) {
        var isOpen = false;
        var count = 0;

        // Toggle modal. If there is or there was more than one modal, don't fire the open/close event
        var toggle = function (open = true) {
            if (open) {
                count += 1;

                if (count != 1) return;

                isOpen = open;
                $rootScope.$emit('modalOpened');
            } else {
                count -= 1;

                if (count != 0) return;

                isOpen = open;
                $rootScope.$emit('modalClosed');
            }
        };

        return {
            isOpen: isOpen,
            toggle: toggle
        };
    }]
);