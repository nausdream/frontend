angular.module('users').service('fixedAdditionalServicesService', ['$q', 'fixedListService',
    function ($q, fixedListService) {
        var ctrl = this;
        ctrl.fixedAdditionalServicesDeferred = $q.defer();

        fixedListService.getFixedList('fixed-additional-services').then(
            function (response) {
                ctrl.fixedAdditionalServicesDeferred.resolve(response.data.data);
            },
            null
        );

        // Get fixed additional services prices with names included within the attributes
        ctrl.getFixedAdditionalServicePricesWithNames = function (fixedAdditionalServicePrices) {
            var deferred = $q.defer();

            // Don't directly modify provided object
            var list = angular.copy(fixedAdditionalServicePrices);

            // When the list of fixed service has been fetched
            ctrl.fixedAdditionalServicesDeferred.then(
                function (response) {

                    // Set the name of every element of the provided services list
                    // to be equal to the one from the fetched list of fixed additional services
                    list.forEach(function (price) {
                        price.attributes.name = response.filter(function (service) {
                            return service.id == price.relationships.fixed_additional_service.id
                        })[0].attributes.name;
                    });

                    // Resolve promise with the new list
                    deferred.resolve(list);
                }
            );

            return deferred.promise;
        }
    }]
);