<?php

?>

<script>(function (window) {
        window.__env = window.__env || {};

        window.__env.apiUrl = <?php echo "\"" . env('BACKEND_URL') . env('API_VERSION') . "\""?>;
        window.__env.backendUrl = <?php echo "\"" . env('BACKEND_URL') . "\""?>;
        window.__env.supportEmail = <?php echo "\"" . env('SUPPORT_EMAIL') . "\""?>;
        window.__env.supportPhone = <?php echo "\"" . env('SUPPORT_PHONE') . "\""?>;
        window.__env.apiTranslationsUrl = <?php echo "\"" . env('API_TRANSLATIONS_URL') . "\""?>;
        window.__env.cloudinaryUrl = <?php echo "\"" . env('CLOUDINARY_URL') . "\""?>;
        window.__env.baseUrl = <?php echo "\"" . env('APP_URL') . "\"" ?>;
        window.__env.mapsApiKey = <?php echo "\"" . env('MAPS_API_KEY') . "\"" ?>;
        window.__env.fbAppId = <?php echo "\"" . env('FB_APP_ID') . "\"" ?>;
        window.__env.fbAppVersion = <?php echo "\"" . env('FB_APP_VERSION') . "\"" ?>;
        window.__env.fallbackCurrency = <?php echo "\"" . env('FALLBACK_CURRENCY') . "\"" ?>;
        window.__env.fallbackLanguage = <?php echo "\"" . env('FALLBACK_LANGUAGE') . "\"" ?>;
        window.__env.paypalAddress = <?php echo "\"" . env('PAYPAL_ADDRESS') . "\"" ?>;
        window.__env.paypalSeller = <?php echo "\"" . env('PAYPAL_SELLER') . "\"" ?>;
        window.__env.appEnv = <?php echo "\"" . env('APP_ENV') . "\"" ?>;

    }(this));</script>