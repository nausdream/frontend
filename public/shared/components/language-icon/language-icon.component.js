angular.module('shared').component('languageIcon', {
    templateUrl: '/shared/components/language-icon/language-icon.view.html',
    controller: 'languageIconController',
    bindings: {
        datatype: '@',
        popover: '@',
        dropdown: '@',
        color: '@',
        language: '='
    }
});