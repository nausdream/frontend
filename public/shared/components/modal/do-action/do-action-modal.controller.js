angular
    .module('shared')
    .controller('doActionModalController',
        ['$uibModalInstance', 'items',
        function ($uibModalInstance, items) {
        this.$onInit = function () {
            var ctrl = this;
            ctrl.items = items;
            // Admin text for accepted_conditionally and reject
            ctrl.adminText = '';

            ctrl.do = function () {
                $uibModalInstance.close({actionDone: true, text: ctrl.adminText});
            };

            ctrl.close = function () {
                $uibModalInstance.close(false);
            };
        };
    }]);
