angular.module('shared').service('validationModalService', [
    '$uibModal',
    function ($uibModal) {
    return {
        open: function (message) {$uibModal.open({
            animation: true,
            component: 'validationModalComponent',
            resolve: {
                message: function () {
                    return message;
                }
            }
        })}
    }
}]);