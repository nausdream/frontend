angular.module('shared').component('validationModalComponent', {
    templateUrl: '/shared/components/modal/validation-modal/validation-modal.view.html',
    controller: 'validationModalController',
    controllerAs: 'ctrl',
    bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    }
});