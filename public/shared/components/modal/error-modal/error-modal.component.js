angular.module('shared').component('errorModalComponent', {
    templateUrl: '/shared/components/modal/error-modal/error-modal.view.html',
    controller: 'errorModalController',
    controllerAs: 'ctrl',
    bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    }
});