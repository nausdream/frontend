angular.module('shared').service('errorModalService',
    ['$uibModal',
    function ($uibModal) {
    return {
        open: function (response) {$uibModal.open({
            animation: true,
            component: 'errorModalComponent',
            resolve: {
                items: function () {
                    return {
                        status: response.status,
                        errors: response.data.errors,
                        request: response.config.url
                    }
                }
            }
        })}
    }
}]);