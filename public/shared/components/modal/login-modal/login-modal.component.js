angular.module('shared').component('loginModalComponent', {
    templateUrl: '/shared/components/modal/login-modal/login-modal.view.html',
    controller: 'loginModalController',
    controllerAs: 'ctrl',
    bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    }
});