angular.module('shared').service('loginModalService',
    ['$uibModal',
    function ($uibModal) {
    return {
        open: function (item) {$uibModal.open({
            animation: true,
            component: 'loginModalComponent',
            resolve: {
                item: function () {
                    return item;
                }
            }
        })}
    }
}]);