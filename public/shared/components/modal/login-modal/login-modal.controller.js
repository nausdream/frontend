angular
    .module('shared')
    .controller('loginModalController',
        ['$uibModalInstance', 'item', 'requestService', '__env', '$window',
        function ($uibModalInstance, item, requestService, __env, $window) {
        this.$onInit = function () {
            var ctrl = this;

            ctrl.item = item;

            ctrl.loginAsUser = function () {
                requestService.createAdminToken(ctrl.item.user_id)
                    .then(function (response) {
                        ctrl.meta = response.data.meta;
                        $window.open(__env.appUrl + '?token=' + ctrl.meta.token, '_blank');
                        $uibModalInstance.close(false);
                    }, function (err) {
                        $uibModalInstance.close(false);
                        throw err;
                    });
            };

            ctrl.close = function () {
                $uibModalInstance.close(false);
            };
        };
    }]);
