angular.module('shared').component('successModalComponent', {
    templateUrl: '/shared/components/modal/success-modal/success-modal.view.html',
    controller: 'successModalController',
    controllerAs: 'ctrl',
    bindings: {
        resolve: '<',
        close: '&',
        dismiss: '&'
    }
});