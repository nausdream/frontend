angular.module('shared').service('successModalService', [
    '$uibModal',
    function ($uibModal) {
        return {
            open: function (requestData) {
                $uibModal.open({
                    animation: true,
                    component: 'successModalComponent',
                    resolve: {
                        items: function () {
                            return {
                                requestData: requestData
                            }
                        }
                    }
                })
            }
        }
    }]);