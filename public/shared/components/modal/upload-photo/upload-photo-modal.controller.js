angular
    .module('shared')
    .controller('uploadPhotoModalController', [
        '$uibModalInstance', 'requestService', 'photoService',
        function ($uibModalInstance, requestService, photoService) {
        var ctrl = this;

        ctrl.close = function () {
            $uibModalInstance.close(ctrl.newPhotoLink);
        };

        ctrl.upload = function (file) {
            ctrl.loading = true;

            var fileType = file["type"];
            var validImageTypes = ["image/gif", "image/jpeg", "image/png"];

            // Not an image
            if (validImageTypes.indexOf(fileType) === -1) {
                ctrl.loading = false;
            } else {
                var putSignedUrl;
                var getSignedUrl;
                // Receive signed url from backend
                requestService.getPresignedUrl().then(
                    function (response) {
                        putSignedUrl = response.data.data.attributes.put_url;
                        getSignedUrl = response.data.data.attributes.get_url;

                        // Upload photo to s3 and receive url
                        return photoService.uploadToS3(putSignedUrl, file)
                            .then(function (response) {
                                ctrl.loading = false;
                                ctrl.newPhotoLink = getSignedUrl;
                            });
                    }
                );
            }
        };
    }]);
