angular
    .module('shared')
    .controller('listActionsController',
        ['$uibModal', 'requestService', 'errorModalService', 'successModalService', '$route', '$location',
            function ($uibModal, requestService, errorModalService, successModalService, $route, $location) {
                this.$onInit = function () {
                    var ctrl = this;

                    ctrl.template = '/shared/components/list-actions/views/' + ctrl.type + '.view.html';


                    // ACTIONS
                    ctrl.accept = function () {
                        var modalInstance = $uibModal.open({
                            animation: true,
                            templateUrl: '/shared/components/modal/do-action/views/accept-modal.view.html',
                            controller: 'doActionModalController',
                            controllerAs: 'ctrl',
                            resolve: {
                                items: function () {
                                    return {
                                        modelType: ctrl.type
                                    };
                                }
                            }
                        });
                        modalInstance.result.then(function (data) {
                            if (data.actionDone) {
                                // Change boat status to 'accepted'
                                ctrl.changeStatus('accepted');
                            }
                        });
                    };
                    ctrl.reject = function () {
                        var modalInstance = $uibModal.open({
                            animation: true,
                            templateUrl: '/shared/components/modal/do-action/views/reject-modal.view.html',
                            controller: 'doActionModalController',
                            controllerAs: 'ctrl',
                            resolve: {
                                items: function () {
                                    return {
                                        modelType: ctrl.type
                                    };
                                }
                            }
                        });
                        modalInstance.result.then(function (data) {
                            if (data.actionDone) {
                                // Change boat status to 'rejected'
                                ctrl.changeStatus('rejected', data.text);
                            }
                        });
                    };
                    ctrl.suspend = function () {
                        var modalInstance = $uibModal.open({
                            animation: true,
                            templateUrl: '/shared/components/modal/do-action/views/suspend-modal.view.html',
                            controller: 'doActionModalController',
                            controllerAs: 'ctrl',
                            resolve: {
                                items: function () {
                                    return {
                                        modelType: ctrl.type
                                    };
                                }
                            }
                        });
                        modalInstance.result.then(function (data) {
                            if (data.actionDone) {
                                // Change boat status to 'accepted'
                                ctrl.changeStatus('accepted_conditionally', data.text);
                            }
                        });
                    };
                    ctrl.freeze = function () {
                        var modalInstance = $uibModal.open({
                            animation: true,
                            templateUrl: '/shared/components/modal/do-action/views/freeze-modal.view.html',
                            controller: 'doActionModalController',
                            controllerAs: 'ctrl',
                            resolve: {
                                items: function () {
                                    return {
                                        modelType: ctrl.type
                                    };
                                }
                            }
                        });
                        modalInstance.result.then(function (data) {
                            if (data.actionDone) {
                                // Change boat status to 'accepted'
                                ctrl.changeStatus('freezed');
                            }
                        });
                    };
                    ctrl.changeStatus = function (status, adminText) {
                        var requestBody = {
                            'status': status,
                            'admin_text': adminText
                        };

                        var success = function () {
                            ctrl.model.attributes.status = status;
                            successModalService.open();
                        };

                        var failure = function (response) {
                            errorModalService.open(response, 'Change status to ' + status);
                        };

                        switch (ctrl.type) {
                            case 'boat':
                                requestService.putBoat(ctrl.model.id, requestBody).then(
                                    success,
                                    failure
                                );
                                break;
                            case 'experience':
                                requestService.putExperience(ctrl.model.id, requestBody).then(
                                    success,
                                    failure
                                );
                                break;
                        }

                    };

                    ctrl.deletePage = function () {
                        requestService.deleteSingle(ctrl.model.id, 'pages').then(
                            function () {
                                $route.reload();
                            },
                            function (response) {
                                errorModalService.open(response);
                            }
                        );
                    }

                    // Add new experience tied to a boat
                    ctrl.newExperience = function () {
                        requestService.postObject(
                            'experiences',
                            null,
                            {role: 'boat', type: 'boats', id: ctrl.model.id}
                        ).then(
                            function (response) {
                                let id = response.data.data.id;

                                $location.path('/experiences/' + id);
                            },
                            function (response) {
                                errorModalService.open(response);
                            }
                        )
                    }
                };
            }]);