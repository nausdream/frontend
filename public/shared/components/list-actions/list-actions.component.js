angular.module('shared').component('listActions', {
    templateUrl: '/shared/components/list-actions/list-actions.view.html',
    controller: 'listActionsController',
    controllerAs: 'ctrl',
    bindings: {
        model: '=',
        type: '@'
    }
});