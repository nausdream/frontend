angular
    .module('shared')
    .controller('datePickerController', ['dateService', function (dateService) {
        this.$onInit = function () {
            // Make controller accessible from inner scopes
            var ctrl = this;

            ctrl.addDays = function (date, days = 1) {
                return dateService.addDays(date, days);
            };
            ctrl.removeDays = function (date, days = 1) {
                return dateService.removeDays(date, days);
            };

            ctrl.today = function () {
                ctrl.date = new Date();
            };

            ctrl.clear = function () {
                ctrl.date = null;
            };

            ctrl.inlineOptions = {
                showWeeks: true
            };

            // Disable overlapping days
            ctrl.disabled = function (data) {
                // data = {date: someDateObject, mode: string}
            };

            // Open/close popup
            ctrl.toggle = function () {
                ctrl.popup.opened = !ctrl.popup.opened;
            };

            ctrl.setDate = function (year, month, day) {
                ctrl.date = new Date(year, month, day);
            };

            ctrl.formats = ['yyyy-MM-dd', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
            ctrl.format = ctrl.formats[0];
            ctrl.altInputFormats = ['M!/d!/yyyy'];

            ctrl.popup = {
                opened: false
            };

            var tomorrow = new Date();
            tomorrow.setDate(tomorrow.getDate() + 1);
            var afterTomorrow = new Date();
            afterTomorrow.setDate(tomorrow.getDate() + 1);
            ctrl.events = [
                {
                    date: tomorrow,
                    status: 'full'
                },
                {
                    date: afterTomorrow,
                    status: 'partially'
                }
            ];
        };
    }]);
