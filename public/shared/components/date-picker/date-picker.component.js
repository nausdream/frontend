angular.module('shared').component('datePicker', {
    templateUrl: '/shared/components/date-picker/date-picker.view.html',
    controller: 'datePickerController',
    bindings: {
        date: '=',
        text: '@',
        mindate: '=',
        maxdate: '=',
        required: '<',
        disabled: '<',
    }
});