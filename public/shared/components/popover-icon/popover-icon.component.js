angular.module('shared').component('popoverIcon', {
    templateUrl: '/shared/components/popover-icon/popover-icon.view.html',
    controller: 'popoverIconController',
    bindings: {
        icontype: '@',
        iconcolor: '@',
        template: '@',
        position: '@'
    }
});