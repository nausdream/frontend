angular
    .module('shared')
    .controller('popoverIconController', [
        '$scope',
        function ($scope) {
        this.$onInit = function () {
            // Get parent scope
            this.parent = $scope.$parent;

            // Sets default position
            if (!this.position) {
                this.position = 'bottom-right';
            }

            this.popoverTemplate = '/shared/components/popover-icon/views/' + this.template + '.view.html';
        };
    }]);