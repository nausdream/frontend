angular.module('shared').component('notificationIcon', {
    templateUrl: '/shared/components/notification-icon/notification-icon.view.html',
    controller: 'notificationIconController',
    bindings: {
        datatype: '@',
        popover: '@',
        dropdown: '@',
        color: '@'
    }
});