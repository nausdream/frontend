'use strict';
// Define app constants
var env = {};

/**
 * Define polyfills to provide ES5 & ES6 compatibility
 */

// Object.copy
// Import variables if present (from env.js)
if (window) {
    // IE
    if (typeof Object.assign != 'function') {
        Object.assign = function (target, varArgs) { // .length of function is 2
            'use strict';
            if (target == null) { // TypeError if undefined or null
                throw new TypeError('Cannot convert undefined or null to object');
            }

            var to = Object(target);

            for (var index = 1; index < arguments.length; index++) {
                var nextSource = arguments[index];

                if (nextSource != null) { // Skip over if undefined or null
                    for (var nextKey in nextSource) {
                        // Avoid bugs when hasOwnProperty is shadowed
                        if (Object.prototype.hasOwnProperty.call(nextSource, nextKey)) {
                            to[nextKey] = nextSource[nextKey];
                        }
                    }
                }
            }
            return to;
        };
    }
    Object.assign(env, window.__env);
}
// Filter
if (!Array.prototype.filter) {
    Array.prototype.filter = function (fun/*, thisArg*/) {
        'use strict';

        if (this === void 0 || this === null) {
            throw new TypeError();
        }

        var t = Object(this);
        var len = t.length >>> 0;
        if (typeof fun !== 'function') {
            throw new TypeError();
        }

        var res = [];
        var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
        for (var i = 0; i < len; i++) {
            if (i in t) {
                var val = t[i];

                // NOTE: Technically this should Object.defineProperty at
                //       the next index, as push can be affected by
                //       properties on Object.prototype and Array.prototype.
                //       But that method's new, and collisions should be
                //       rare, so use the more-compatible alternative.
                if (fun.call(thisArg, val, i, t)) {
                    res.push(val);
                }
            }
        }

        return res;
    };
}

angular.module('shared', ['ngRoute', 'ngCookies', 'ngFileUpload', 'ngAnimate', 'angularCSS', 'ui.bootstrap', 'ngTouch']);

// Set __env global constant
angular.module('shared').constant('__env', env);
angular.module('shared').constant('experienceDurations', [
    {
        "type": "experience-durations",
        "id": "1",
        "attributes": {
            "name": "up-to-one-day"
        }
    },
    {
        "type": "experience-durations",
        "id": "2",
        "attributes": {
            "name": "more-than-one-day"
        }
    }
]);

// Set JWT in request header on every request
angular.module('shared').factory('httpRequestInterceptor', ['$cookies', function ($cookies) {
    return {
        request: function (config) {

            // Allow requests to explicitly omit JWT by sending a jwt=no_jwt param
            if (config.params) {
                if (config.params.jwt == 'no_jwt') {
                    // If params are not cleared, Amazon S3 request fails
                    config.params = undefined;

                    return config;
                }
            }

            config.headers['Authorization'] = 'Bearer ' + $cookies.get('access_token');
            return config;
        }
    };
}]);
angular.module('shared').config(['$httpProvider', function ($httpProvider) {
    $httpProvider.defaults.withCredentials = true;
    $httpProvider.interceptors.push('httpRequestInterceptor');
    $httpProvider.defaults.headers.put = {'Content-Type': 'application/vnd.api+json'};
    $httpProvider.defaults.headers.post = {'Content-Type': 'application/vnd.api+json'};
    $httpProvider.defaults.headers.patch = {'Content-Type': 'application/vnd.api+json'};
    $httpProvider.defaults.headers.delete = {'Content-Type': 'application/vnd.api+json'};
}]);

// Get languages and currencies from API server
var languages = [];
var languagesMappings = {};
var currencies = [];
angular.module('shared').run(['requestService', function (requestService) {
    // Get languages
    requestService.getLanguages().then(
        function (response) {
            response.data.data.forEach(function (language) {
                languages.push(language.attributes.language);
                languagesMappings[language.attributes.language + '_*'] = language.attributes.language;
            })
        }
    );

    // Get currencies
    requestService.getCurrencies().then(
        function (response) {
            response.data.data.forEach(function (currency) {
                currencies.push(currency.attributes.name);
            })
        }
    );
}]);
angular.module('shared').constant('languages', languages);
angular.module('shared').constant('currencies', currencies);

angular.module('shared').config(['$cookiesProvider', function ($cookiesProvider) {
    // Set expiration date (2069)
    $cookiesProvider.defaults.expires = new Date(100 * 365 * 24 * 60 * 60 * 1000);
}]);

angular.module('shared').config(['$locationProvider', '$routeProvider', function ($locationProvider, $routeProvider) {
    // Strip out #! from route
    $locationProvider.hashPrefix('');
    if (!!(window.history && history.pushState)) {
        $locationProvider.html5Mode({
            enabled: true,
            rewriteLinks: false
        });
    }
    else {
        // IE9 FIX
        window.location.hash = '/';
    }

    // Make routes case insensitive
    $routeProvider.caseInsensitiveMatch = true;
}]);

angular.module('shared').filter('roundup', function () {
    return function (value) {
        return Math.ceil(value);
    };
});