angular.module('shared').service('statusService', function () {
    // Set status array
    this.bookingStatus = {
        paid: "Paid",
        pending: "Pending",
        cancelled: "Cancelled",
        new: "New",
        accepted: "Accepted",
        rejected: "Rejected",
    };
    this.experienceStatus = {
        new: "Not completed yet",
        accepted: "Approvato",
        accepted_conditionally: "Rifiutato con riserva",
        rejected: "Rifiutato",
        pending: "In sospeso",
        freezed: "Congelato"
    };
    this.boatStatus = {
        new: "Nuova",
        accepted: "Approvata",
        rejected: "Rifiutata",
        pending: "In sospeso",
        accepted_conditionally: "Rifiutato con riserva"
    };
    this.captainStatus = {
        new: "Nuovo",
        accepted: "Approvato",
        rejected: "Rifiutato",
        pending: "In sospeso"
    };
    this.userStatus = {
        new: "Nuovo",
        accepted: "Approvato",
        rejected: "Rifiutato",
        pending: "In sospeso"
    };
    this.conversationStatus = {
        new: "New",
        accepted: "Accepted",
        rejected: "Rejected",
        pending: "Pending",
        paid: "Paid",
        cancelled: "Cancelled"
    };
    this.couponStatus = {
        expired: "Scaduto",
        consumed: "Usato"
    };
    this.eventStatus = {
        new: "Nuova",
        completed: "Completata"
    };

    // Get status names
    this.getBookingStatusName = function getBookingStatusName(status) {
        return this.bookingStatus[status];
    };
    this.getExperienceStatusName = function getExperienceStatusName(status) {
        return this.experienceStatus[status];
    };
    this.getBoatStatusName = function getBoatStatusName(status) {
        return this.boatStatus[status];
    };
    this.getCaptainStatusName = function getCaptainStatusName(status) {
        return this.captainStatus[status];
    };
    this.getUserStatusName = function getUserStatusName(status) {
        return this.userStatus[status];
    };
    this.getConversationStatusName = function getConversationStatusName(status) {
        return this.conversationStatus[status];
    };
    this.getCouponStatusName = function getCouponStatusName(status) {
        if (!status) {return 'Attivo'}
        return this.couponStatus[status];
    };
    this.getEventStatusName = function getCouponStatusName(status) {
        return this.eventStatus[status];
    };
});