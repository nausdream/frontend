angular.module('shared').service('photoService', ['$http', '__env', '$q', 'requestService', function ($http, __env, $q, requestService) {

    // Make this available inside inner scopes
    var srvc = this;

    this.getLinkFromVersion = function (userId, boatId, version, photoType) {
        return __env.cloudinaryBaseUrl
            + 'v'
            + version
            + '/users/'
            + userId + "/"
            + boatId + "/"
            + photoType;
    };

    this.uploadToS3 = function (signedUrl, file) {
        return $http.put(signedUrl, file, {headers: {'Content-Type': file.type}, params: {jwt: 'no_jwt'}});
    };

    // Return link from file
    this.getLink = function (file) {
        var deferred = $q.defer();

        var fileType = file["type"];
        var validImageTypes = ["image/gif", "image/jpeg", "image/png"];

        // Not an image
        if (validImageTypes.indexOf(fileType) === -1) {
            deferred.reject('Not an image');
        } else {
            var putSignedUrl;
            var getSignedUrl;
            // Receive signed url from backend
            requestService.getPresignedUrl().then(
                function (response) {
                    putSignedUrl = response.data.data.attributes.put_url;
                    getSignedUrl = response.data.data.attributes.get_url;

                    // Upload photo to s3 and receive url
                    srvc.uploadToS3(putSignedUrl, file)
                        .then(
                            function (response) {
                                deferred.resolve(getSignedUrl);
                            },
                            function (response) {
                                deferred.reject(response);
                            });
                },
                function (response) {
                    deferred.reject(response);
                }
            );
        }

        return deferred.promise;
    }
}]);