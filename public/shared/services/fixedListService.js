angular.module('shared').service('fixedListService', ['$q', 'requestService', function($q, requestService) {
    var service = this;

    /**
     * Return static data list in a promise, calling the backend just once
     * @param datatype
     * @returns {*}
     */
    this.getFixedList = function (datatype) {

        // If the same fixed data was already fetched, don't do it again
        if (!service[datatype]) {
            service[datatype] = $q.defer();

            requestService.getListAsync(datatype).then(
                function (response) {
                    service[datatype].resolve(response);
                },
                function (response) {
                    service[datatype].reject(response);
                }
            );
        }

        return service[datatype].promise;
    };
}]);