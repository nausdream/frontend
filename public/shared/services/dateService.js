/**
 * Created by lucas on 17/02/2017.
 */
angular.module('shared').service('dateService', ['$cookies', function ($cookies) {

    // Define formats
    var formats = {
        date: {
            en: 'MM/dd/yyyy',
            it: 'dd/MM/yyyy'
        },
        time: {
            en: 'hh:mm a',
            it: 'HH:mm'
        },
        dateTime: {
            en: 'MM/dd/yyyy hh:mm a',
            it: 'dd/MM/yyyy HH:mm'
        }
    };

    /**
     * Get format from a list or revert to default
     *
     * formatType = 'date' | 'time' | 'dateTime'
     *
     * @param formatType
     * @param language
     * @returns {*}
     */
    this.getFormat = function (formatType = 'dateTime', language) {
        if (language == undefined) language = $cookies.get('language');

        var list = formats[formatType];

        // If wrong formatType was provided, revert to dateTime
        if (!list) list = formats.dateTime;

        var format = list[language];

        // If language is missing, revert to english
        if (!format) format = list.en;

        return format;
    };

    // Return date in this string format: YYYY-MM-DD
    this.getDateString = function (date) {
        if (!date) return null;

        var mm = date.getMonth() + 1; // getMonth() is zero-based
        var dd = date.getDate();

        return [date.getFullYear(),
            (mm>9 ? '-' : '-0') + mm,
            (dd>9 ? '-' : '-0') + dd
        ].join('');
    };

    // Add and subtract arbitrary number of days
    this.addDays = function (date, days = 1) {
        if (date) {
            return new Date(date.getTime() + 86400000 * days);
        }
    };
    this.removeDays = function (date, days = 1) {
        if (date) {
            return new Date(date.getTime() - 86400000 * days);
        }
    };

    // Take a date and convert it to the user timezone
    this.toUserTimezone = function (date) {
        date.setTime(date.getTime() - (new Date().getTimezoneOffset() * 60 * 1000));
        return date;
    };

    // Convert string in yyyy-MM-dd to JS Date Object
    this.getDateFromString = function (dateString) {
        return new Date(new Date(dateString).toUTCString().substr(0, 25));
    }
}]);