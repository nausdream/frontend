angular.module('shared').service('mathService', function () {
    // TODO: Fetch data from API and let the backend do the pagination
    // TODO define angular global for perPage
    this.range = function (min = 0, max, step = 1, reverse) {
        if (!max) return null;
        var range = [];

        if (reverse) {
            for (var i = max; i>= min; i-=step){
                range.push(i);
            }
        } else {
            for (var i = min; i<= max; i+=step){
                range.push(i);
            }
        }
        return range;
    };
});