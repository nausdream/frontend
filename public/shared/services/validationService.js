angular.module('shared').service('validationService', ['$filter', function ($filter) {
    var ctrl = this;

    this.setBackendErrors = function (form, controllerErrors, backendErrors) {
        backendErrors.forEach(function (error) {
            if (error.title) {
                // If form has an input named error.title
                if (form[error.title]) {
                    form[error.title].$setValidity(error.code, false);
                    form[error.title].$setTouched();
                }
                controllerErrors[error.title] = {code: error.code, backend: true};
            } else {
                controllerErrors['error'] = {code: error.code, backend: false};
            }
        });
    };

    this.setFrontendErrors = function (form, controllerErrors) {
        // For each form input
        angular.forEach(form, function (field, key) {
            // If input is invalid
            if (typeof field === 'object' && field.hasOwnProperty('$modelValue')) {
                ctrl.validateField(field, controllerErrors)
            }
        });
    };

    // Set errors on field
    this.validateField = function (field, errors) {
        var fieldName = field.$name;

        field.$setTouched();

        // Check if there are backend errors
        var fieldHasBackendError = false;
        if (errors[fieldName]) {
            if (errors[fieldName].backend) fieldHasBackendError = true;
        }

        // If there are backend errors, leave the field validity as it is
        if (field.$invalid && !fieldHasBackendError) {
            angular.forEach(field.$error, function (value, key) {
                this[fieldName] = {code: key, backend: false};
            }, errors);
        }
    };

    // Remove errors from field
    this.resetField = function (fieldName, errors, form) {
        if (!errors) return;
        if (!errors[fieldName]) return;

        if (errors[fieldName].backend) {
            var field = form[fieldName];
            angular.forEach(field.$error, function (error, key) {
                field.$setValidity(key, true);
            });
            field.$setPristine();
        }

        delete errors[fieldName];
        delete errors['error'];
    }
}]);