angular.module('shared').service('staticDataService',
    ['$http', 'staticData', 'requestService', 'experienceDurations',
        function ($http, staticData, requestService, experienceDurations) {
            var ctrl = this;

            // Return list of materials
            this.getMaterialList = function (callback) {
                requestService.getList(function (data) {
                    // Get material names from staticData material object
                    var materials = [];
                    data.forEach(function (element) {
                        materials.push({
                            id: element.id,
                            name: element.attributes.name,
                            text: staticData.materials[element.attributes.name]
                        });
                    });
                    callback(materials);
                }, 'boat-materials');
            };

            // Return list of boat types
            this.getBoatTypeList = function (callback) {
                requestService.getList(function (data) {
                    // Get boat types from staticData boat_type object
                    var types = [];
                    data.forEach(function (element) {
                        types.push({
                            id: element.id,
                            name: element.attributes.name,
                            text: staticData.boat_types[element.attributes.name]
                        });
                    });
                    callback(types);
                }, 'boat-types');
            };

            // Return list of motor types
            this.getMotorTypeList = function (callback) {
                requestService.getList(function (data) {
                    // Get motor types from staticData motor_type object
                    var types = [];
                    data.forEach(function (element) {
                        types.push({
                            id: element.id,
                            name: element.attributes.name,
                            text: staticData.motor_types[element.attributes.name]
                        });
                    });
                    callback(types);
                }, 'motor-types');
            };

            // Return experience type list
            this.getExperienceTypeList = function (callback) {
                requestService.getList(function (data) {
                    // Get fixed rules from staticData fixed rules object
                    var list = [];
                    data.forEach(function (element) {
                        list.push({id: element.id, name: element.attributes.name});
                    });
                    callback(list);
                }, 'experience-types');
            };

            // Return accessories list
            this.getAccessoryList = function (callback) {
                requestService.getList(function (data) {
                    // Get fixed rules from staticData fixed rules object
                    var list = [];
                    data.forEach(function (element) {
                        list.push({
                            id: element.id,
                            name: element.attributes.name,
                            text: staticData.accessories[element.attributes.name]
                        });
                    });
                    callback(list);
                }, 'accessories');
            };

            // Return fixed_rules list
            this.getFixedRuleList = function (callback) {
                // Get fixed rules from API server
                requestService.getList(function (data) {
                    var list = [];
                    data.forEach(function (element) {
                        list.push({id: element.id, name: element.attributes.name});
                    });
                    callback(list);
                }, 'fixed-rules');
            };

            // Return fixed_additional_service list
            this.getFixedAdditionalServiceList = function (callback) {
                requestService.getList(function (data) {
                    // Get fixed rules from staticData fixed rules object
                    var list = [];
                    data.forEach(function (element) {
                        list.push({id: element.id, attributes: {name: element.attributes.name}});
                    });
                    callback(list);
                }, 'fixed-additional-services');
            };

            // Return experience duration list
            this.getExperienceDurationList = function (callback) {
                var experience_durations = [];
                experienceDurations.forEach(function (element) {
                    experience_durations.push({
                        id: element.id,
                        name: element.attributes.name,
                        text: staticData.experience_durations[element.attributes.name]
                    });
                });
                callback(experience_durations);
            };

            // Return fishing_partition list
            this.getFishingPartitionList = function (callback) {
                // Get fixed rules from API server
                requestService.getList(function (data) {
                    var list = [];
                    data.forEach(function (element) {
                        list.push({
                            id: element.id,
                            attributes: {
                                name: element.attributes.name,
                                text: staticData.fishing_partitions[element.attributes.name]
                            }
                        });
                    });
                    callback(list);
                }, 'fishing-partitions');
            };

            // Return fishing_type list
            this.getFishingTypeList = function (callback) {
                // Get fixed rules from API server
                requestService.getList(function (data) {
                    var list = [];
                    data.forEach(function (element) {
                        list.push({id: element.id, attributes: {name: element.attributes.name}});
                    });
                    callback(list);
                }, 'fishing-types');
            };

            // Return foods list
            // Dirty way to get keys with values.
            // TODO: refactor
            this.getFoodList = function (callback) {
                var foods = [];
                for (var food in staticData.foods) {
                    if (staticData.foods.hasOwnProperty(food)) {
                        foods.push({name: food, text: staticData.foods[food]});
                    }
                }
                callback(foods);
            }
        }]);