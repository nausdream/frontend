// Google async initializer needs global function, so we use $window
angular.module('shared')
    .service('mapsService', ['$window', '$q', '__env', '$cookies', function ($window, $q, __env, $cookies) {

        var apiKey = __env.mapsApiKey;
        var language = $cookies.get('language');
        //Google's url for async maps initialization accepting callback function
        var asyncUrl = "https://maps.googleapis.com/maps/api/js?key=" + apiKey + "&libraries=geometry&language=" + language + "&callback=",
            mapsDefer = $q.defer();

        //Callback function - resolving promise after maps successfully loaded
        $window.googleMapsInitialized = mapsDefer.resolve;

        //Async loader
        var asyncLoad = function (asyncUrl, callbackName) {
            var script = document.createElement('script');
            script.type = 'text/javascript';
            script.setAttribute('class', 'map-script');
            script.src = asyncUrl + callbackName;
            document.body.appendChild(script);
        };
        //Start loading google maps
        asyncLoad(asyncUrl, 'googleMapsInitialized');

        //Usage: mapsService.mapsInitialized.then(callback)
        return {
            mapsInitialized: mapsDefer.promise
        };
    }]);