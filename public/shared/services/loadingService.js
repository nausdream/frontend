angular.module('shared').service('loadingService', ['$rootScope', '$translatePartialLoader', '__env', function ($rootScope, $translatePartialLoader, __env) {
    var ctrl = this;

    /**
     * Attach event listener on rootScope. When the desired pages in desired languages are loaded,
     * the controller has finished loading.
     * @param pages
     * @param desiredLanguage
     * @param ctrl
     */
    ctrl.waitForTranslation = function (pages, ctrl, desiredLanguage = __env.fallbackLanguage, viewName) {
        $rootScope.$on('$translateLoadingEnd', function (event, language) {
            // Check if parts have been loaded
            var loaded = true;
            pages.forEach(function (page) {
                if (!$translatePartialLoader.isPartLoaded(page, desiredLanguage)) {
                    loaded = false;
                }
            });

            if (loaded && ctrl) {
                ctrl.loading = false;
            }
        });
    }
}]);