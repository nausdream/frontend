angular.module('shared').service('requestService',
    ['$cookies', '$http', '__env', '$q', 'dateService',
    function ($cookies, $http, __env, $q, dateService) {
    var ctrl = this;
    // TODO: heavy refactor to reduce code repetition and improve usability
    this.logout = function (route = '') {
        $http.post(__env.apiUrl + 'auth/logout', {meta: ctrl.meta()}).then(
            function (response) {
                $cookies.remove('access_token');
                $cookies.remove('csrf_token');
                window.location = __env.baseUrl + route;
            },
            function (response) {
                $cookies.remove('access_token');
                $cookies.remove('csrf_token');
                window.location = __env.baseUrl + route;
            }
        )
    };

    /**
     * @deprecated
     * @param callback
     * @param dataType
     * @param page
     * @param perPage
     */
    this.getList = function (callback, dataType, page = 1, perPage = 6) {
        $http
            .get(__env.apiUrl + dataType + '?page=' + page + '&per_page=' + perPage)
            .then(function (response) {
                var count = null;
                if (response.data.meta) {
                    count = response.data.meta.total_elements;
                }
                var data = response.data.data;
                // Pass data to the callback function
                callback(data, count);
            });
    };

    this.getListAsync = function (dataType, params, canceler) {
        if (!canceler) canceler = $q.defer();

        return $http({
            method: 'GET',
            url: __env.apiUrl + dataType,
            params: params,
            timeout: canceler.promise
        });
    };

    this.getListings = function (params, canceler) {
        return $http({
            method: 'GET',
            url: __env.apiUrl + 'listings',
            params: params,
            timeout: canceler.promise
        })
    };

    /**
     * @deprecated
     * @param callback
     * @param dataType
     * @param id
     * @param editing
     * @param parameters
     */
    // Get single record
    this.getSingle = function (callback, dataType, id, editing = true, parameters) {
        var url = __env.apiUrl + dataType + '/' + id;
        // Append other parameters to request string. Parameters is an object like this
        // {someKey: "someValue", otherKey: "otherValue"}
        if (parameters) {
            for (var key in parameters) {
                if (parameters.hasOwnProperty(key)) {
                    url += "&" + key + "=" + parameters[key];
                }
            }
        }

        $http
            .get(url)
            .then(function (response) {
                    // Pass data to the callback function
                    callback(response.data);
                },
                function (response) {
                    callback(response.data);
                });
    };
    // Get single record async
    this.getSingleAsync = function (dataType, id, params) {
        var url = __env.apiUrl + dataType + '/' + id + '?';
        // Append other params to request string. Parameters is an object like this
        // {someKey: "someValue", otherKey: "otherValue"}
        url = ctrl.addParametersToUrl(url, params);

        return $http.get(url);
    };

    // Get single record with editing true
    this.getSingleEditing = function (callback, dataType, id) {
        $http
            .get(__env.apiUrl + dataType + '/' + id + '?editing=true')
            .then(function (response) {
                    // Pass data to the callback function
                    callback(response.data);
                },
                function (response) {
                    callback(response.data);
                });
    };

    // Get related list
    this.getRelatedList = function (callback, parentType, parentId, childType, params = '') {
        $http
            .get(__env.apiUrl + parentType + '/' + parentId + '/' + childType + params)
            .then(function (response) {
                    // Pass data to the callback function
                    callback(response.data);
                },
                function (response) {
                    callback(response.data);
                });
    };

    // Get related list in editing mode
    this.getRelatedListEditing = function (callback, parentType, parentId, childType) {
        $http
            .get(__env.apiUrl + parentType + '/' + parentId + '/' + childType + '?editing=true')
            .then(function (response) {
                    // Pass data to the callback function
                    callback(response.data);
                },
                function (response) {
                    callback(response.data);
                });
    };

    /**
     * Post single object. Relationship object has a role, type and id attribute
     * @param dataType
     * @param attributes
     * @param relationships
     * @returns {HttpPromise}
     */
    this.postObject = function (dataType, attributes, relationships) {
        var requestBody = {
            data: {
                type: dataType,
                attributes: attributes
            },
            meta: ctrl.meta()
        };

        // Set relationships body
        if (relationships) {
            var relationshipsBody = {};
            relationshipsBody[relationships.role] = {
                data: {
                    type: relationships.type,
                    id: relationships.id
                }
            };
            requestBody.data.relationships = relationshipsBody;
        }

        return $http.post(__env.apiUrl + dataType, requestBody);
    };

    /** Patch single object */
    this.patchObject = function (dataType, id, attributes) {
        var requestBody = {
            data: {
                type: dataType,
                id: id,
                attributes: attributes
            },
            meta: ctrl.meta()
        };

        return $http({
            method: 'PATCH',
            url: __env.apiUrl + dataType + '/' + id,
            data: requestBody
        });
    };

    /**
     * @deprecated
     * @param callback
     * @param parentType
     * @param parentId
     * @param childType
     */
    this.getRelatedSingle = function (callback, parentType, parentId, childType) {
        $http
            .get(__env.apiUrl + parentType + '/' + parentId + '/' + childType + '?editing=true')
            .then(function (response) {
                    // Pass data to the callback function
                    callback(response.data);
                },
                function (response) {
                    callback(response.data);
                });
    };
    this.getRelatedSingleAsync = function (parentType, parentId, childType, params) {
        var url = __env.apiUrl + parentType + '/' + parentId + '/' + childType + '?';
        // Append other params to request string. Params is an object like this
        // {someKey: "someValue", otherKey: "otherValue"}
        url = ctrl.addParametersToUrl(url, params);

        return $http.get(url);
    };

    // Post single
    this.postSingle = function (dataType, attributes, relationships) {
        var requestBody = {
            data: {
                type: dataType,
                attributes: attributes
            },
            meta: ctrl.meta()
        };

        if (relationships) requestBody.data.relationships = relationships;

        return $http.post(__env.apiUrl + dataType, requestBody);
    };

    // DELETE
    this.deleteSingle = function (modelId, modelType) {
        return $http({
            method: 'DELETE',
            url: __env.apiUrl + modelType + '/' + modelId,
            data: {meta: ctrl.meta()}
        });
    };
    this.deleteCollection = function (models, modelType) {
        var promises = [];
        models.forEach(function (model) {
            promises.push(ctrl.deleteSingle(model.id, modelType));
        });

        return $q.all(promises);
    };


    // BOAT
    this.putBoat = function (id, data) {
        var requestBody = {
            data: {
                type: 'boats',
                id: id,
                attributes: data
            },
            meta: ctrl.meta()
        };

        return $http.put(__env.apiUrl + 'boats/' + id, requestBody);
    };

    // BOAT RELATIONSHIPS
    this.patchExperienceTypes = function (id, data) {
        var requestBody = {
            data: [],
            meta: ctrl.meta()
        };

        // Prepare request body data array
        data.forEach(function (experienceType) {
            requestBody.data.push({type: 'experience-types', id: experienceType.id});
        });

        return $http.patch(__env.apiUrl + 'boats/' + id + '/relationships/experience-types', requestBody);
    };
    this.patchAccessories = function (id, data) {
        var requestBody = {
            data: [],
            meta: ctrl.meta()
        };

        // Prepare request body data array
        data.forEach(function (accessory) {
            requestBody.data.push({type: 'accessories', id: accessory.id});
        });

        return $http.patch(__env.apiUrl + 'boats/' + id + '/relationships/accessories', requestBody);
    };

    // PHOTOS
    this.postPhotos = function (photos, relationshipType, relationshipId) {
        var promises = [];

        photos.forEach(function (photo) {
            promises.push(ctrl.postPhoto(photo.attributes.link, relationshipType, relationshipId));
        });

        return $q.all(promises);
    };
    this.postPhoto = function (url, relationshipType, relationshipId, photoType) {
        var requestBody = {
            data: {
                type: 'photos',
                attributes: {
                    link: url
                }
            },
            meta: ctrl.meta()
        };

        var relationship;
        switch (relationshipType) {
            case 'boat':
                relationship = {
                    boat: {
                        data: {
                            type: 'boats',
                            id: relationshipId
                        }
                    }
                };
                requestBody.data.attributes.photo_type = photoType;
                break;
            case 'experience':
                relationship = {
                    experience: {
                        data: {
                            type: 'experiences',
                            id: relationshipId
                        }
                    }
                };
                break;
            case 'user':
                relationship = {
                    user: {
                        data: {
                            type: 'users',
                            id: relationshipId
                        }
                    }
                };
                break;
        }

        requestBody.data.relationships = relationship;

        return $http.post(__env.apiUrl + 'photos', requestBody);
    };
    this.deletePhoto = function (id) {
        return $http({
            method: 'DELETE',
            url: __env.apiUrl + 'photos/' + id,
            data: {meta: ctrl.meta()}
        });
    };
    this.getPresignedUrl = function () {
        var requestBody = {data: {type: "signed-urls"}, meta: ctrl.meta()};
        return $http.post(__env.apiUrl + 'signed-urls', requestBody);
    };

    this.meta = function () {
        return {
            csrf_token: $cookies.get('csrf_token')
        }
    };

    // EXPERIENCE
    this.putExperience = function (id, data) {
        var requestBody = {
            data: {
                type: 'experiences',
                id: id,
                attributes: data
            },
            meta: ctrl.meta()
        };

        return $http.put(__env.apiUrl + 'experiences/' + id, requestBody);
    };
    // EXPERIENCE RELATIONSHIPS
    // Fixed rules
    this.patchFixedRules = function (experienceId, rules) {
        // Construct request body
        var requestBody = {data: [], meta: ctrl.meta()};

        rules.forEach(function (rule) {
            requestBody.data.push({type: 'fixed-rules', id: rule.id})
        });

        // Make API call
        return $http.patch(__env.apiUrl + 'experiences/' + experienceId + '/relationships/fixed-rules', requestBody);
    };
    // Fixed additional services
    this.postFixedAdditionalServices = function (experienceId, services) {
        // Construct multiple API Calls
        var promises = [];

        services.forEach(function (service) {
            var requestBody = {meta: ctrl.meta()};
            requestBody.data =
            {
                type: 'fixed-additional-service-prices',
                attributes: {
                    per_person: service.attributes.per_person,
                    price: service.attributes.price
                },
                relationships: {
                    service: {
                        data: {
                            type: "fixed-additional-services",
                            id: service.id
                        }
                    },
                    experience: {
                        data: {
                            type: "experiences",
                            id: experienceId
                        }
                    }
                }
            };
            // Add API call to promises
            promises.push($http.post(__env.apiUrl + 'fixed-additional-service-prices', requestBody));
        });

        // Resolve promises together
        return $q.all(promises);
    };
    this.patchFixedAdditionalServices = function (experienceId, services) {
        // Construct multiple API Calls
        var promises = [];

        var requestBody = {meta: ctrl.meta()};
        requestBody.data = [];
        services.forEach(function (service) {
            requestBody.data.push(
                {
                    type: 'fixed-additional-service-prices',
                    attributes: {
                        per_person: service.attributes.per_person,
                        price: service.attributes.price
                    },
                    relationships: {
                        service: {
                            data: {
                                type: 'fixed-additional-services',
                                id: service.id
                            }
                        }
                    }
                }
            );
        });
        // Add API call to promises
        promises.push($http.patch(__env.apiUrl + 'experiences/' + experienceId + '/fixed-additional-service-prices', requestBody));
        // Resolve promises together
        return $q.all(promises);
    };
    this.deleteFixedAdditionalServices = function (services) {
        // Construct multiple API Calls
        var promises = [];

        services.forEach(function (service) {
            // Add API call to promises
            promises.push(
                $http({
                    method: 'DELETE',
                    url: __env.apiUrl + 'fixed-additional-service-prices/' + service.attributes.price_id,
                    data: {meta: ctrl.meta()}
                }));
        });

        // Resolve promises together
        return $q.all(promises);
    };
    // Additional services
    this.postAdditionalServices = function (experienceId, services) {
        // Construct multiple API Calls
        var promises = [];

        services.forEach(function (service) {
            var requestBody = {meta: ctrl.meta()};
            requestBody.data =
            {
                type: 'additional-services',
                attributes: {
                    name: service.attributes.name,
                    per_person: service.attributes.per_person,
                    price: service.attributes.price
                },
                relationships: {
                    experience: {
                        data: {
                            type: "experiences",
                            id: experienceId
                        }
                    }
                }
            };
            // Add API call to promises
            promises.push($http.post(__env.apiUrl + 'additional-services', requestBody));
        });

        // Resolve promises together
        return $q.all(promises);
    };
    this.patchAdditionalServices = function (services) {
        // Construct multiple API Calls
        var promises = [];

        services.forEach(function (service) {
            var requestBody = {meta: ctrl.meta()};
            requestBody.data =
            {
                id: service.id,
                type: 'additional-services',
                attributes: {
                    name: service.attributes.name,
                    per_person: service.attributes.per_person,
                    price: service.attributes.price
                }
            };
            // Add API call to promises
            promises.push($http.patch(__env.apiUrl + 'additional-services/' + service.id, requestBody));
        });

        // Resolve promises together
        return $q.all(promises);
    };
    this.deleteAdditionalServices = function (services) {
        // Construct multiple API Calls
        var promises = [];

        services.forEach(function (service) {
            // Add API call to promises
            promises.push(
                $http({
                    method: 'DELETE',
                    url: __env.apiUrl + 'additional-services/' + service.id,
                    data: {meta: ctrl.meta()}
                }));
        });

        // Resolve promises together
        return $q.all(promises);
    };
    // Availabilities
    this.postAvailabilities = function (experienceId, availabilities) {
        // Construct multiple API Calls
        var promises = [];

        availabilities.forEach(function (availability) {
            var requestBody = {meta: ctrl.meta()};
            requestBody.data =
            {
                type: 'experience-availabilities',
                attributes: {
                    date_start: dateService.getDateString(availability.attributes.date_start),
                    date_end: dateService.getDateString(availability.attributes.date_end)
                },
                relationships: {
                    experience: {
                        data: {
                            type: "experiences",
                            id: experienceId
                        }
                    }
                }
            };
            // Add API call to promises
            promises.push($http.post(__env.apiUrl + 'experience-availabilities', requestBody));
        });

        // Resolve promises together
        return $q.all(promises);
    };
    this.postAvailability = function (experienceId, availability) {
        var requestBody = {meta: ctrl.meta()};
        requestBody.data =
        {
            type: 'experience-availabilities',
            attributes: {
                date_start: dateService.getDateString(availability.attributes.date_start),
                date_end: dateService.getDateString(availability.attributes.date_end)
            },
            relationships: {
                experience: {
                    data: {
                        type: "experiences",
                        id: experienceId
                    }
                }
            }
        };

        return $http.post(__env.apiUrl + 'experience-availabilities', requestBody);
    };
    this.patchAvailabilities = function (availabilities) {
        // Construct multiple API Calls
        var promises = [];

        availabilities.forEach(function (availability) {
            var requestBody = {meta: ctrl.meta()};
            requestBody.data =
            {
                id: availability.id,
                type: 'experience-availabilities',
                attributes: {
                    date_start: dateService.getDateString(availability.attributes.date_start),
                    date_end: dateService.getDateString(availability.attributes.date_end)
                }
            };
            // Add API call to promises
            promises.push($http.patch(__env.apiUrl + 'experience-availabilities/' + availability.id, requestBody));
        });

        // Resolve promises together
        return $q.all(promises);
    };
    this.deleteAvailabilities = function (ids) {
        // Construct multiple API Calls
        var promises = [];

        ids.forEach(function (id) {
            // Add API call to promises
            promises.push(
                $http({
                    method: 'DELETE',
                    url: __env.apiUrl + 'experience-availabilities/' + id,
                    data: {meta: ctrl.meta()}
                }));
        });

        // Resolve promises together
        return $q.all(promises);
    };
    // Periods with prices
    this.postPeriods = function (experienceId, periods) {
        // Construct multiple API Calls
        var promises = [];

        periods.forEach(function (period) {
            var requestBody = {meta: ctrl.meta()};
            requestBody.data =
            {
                type: 'periods',
                attributes: {
                    date_start: dateService.getDateString(period.attributes.date_start),
                    date_end: dateService.getDateString(period.attributes.date_end),
                    entire_boat: period.attributes.entire_boat,
                    price: period.attributes.price,
                    min_person: period.attributes.min_person
                },
                relationships: {
                    experience: {
                        data: {
                            type: "experiences",
                            id: experienceId
                        }
                    }
                }
            };
            // Add API call to promises
            promises.push($http.post(__env.apiUrl + 'periods', requestBody)
                .then(function (response) {
                    console.log(response);
                    console.log(period);
                    // Add new prices
                    promises.push(ctrl.postPrices(response.data.data.id, [period.single_person_price]));
                    promises.push(ctrl.postPrices(response.data.data.id, period.newPrices));
                })
            );
        });

        // Resolve promises together
        return $q.all(promises);
    };
    this.patchPeriods = function (periods) {
        // Construct multiple API Calls
        var promises = [];

        periods.forEach(function (period) {
            var requestBody = {meta: ctrl.meta()};
            var dateStart, dateEnd;
            requestBody.data =
            {
                id: period.id,
                type: 'periods',
                attributes: {
                    date_start: dateService.getDateString(period.attributes.date_start),
                    date_end: dateService.getDateString(period.attributes.date_end),
                    entire_boat: period.attributes.entire_boat,
                    price: period.attributes.price,
                    min_person: period.attributes.min_person,
                    is_default: period.attributes.is_default
                }
            };
            // Add API call to promises
            promises.push($http.patch(__env.apiUrl + 'periods/' + period.id, requestBody).then(
                function () {
                    // Edit period prices
                    return ctrl.deletePrices(period.deletedPrices)
                        .then(
                            null,
                            function (response) {
                                return $q.reject(response);
                            }
                        )
                        .then(
                            function () {
                                return ctrl.patchPrices([period.single_person_price]);
                            },
                            function (response) {
                                return $q.reject(response);
                            }
                        )
                        .then(
                            function () {
                                return ctrl.patchPrices(period.oldPrices);
                            },
                            function (response) {
                                return $q.reject(response);
                            }
                        )
                        .then(
                            function () {
                                return ctrl.postPrices(period.id, period.newPrices);
                            },
                            function (response) {
                                return $q.reject(response);
                            }
                        );
                },
                function (response) {
                    return $q.reject(response);
                }
            ));
        });

        // Resolve promises together
        return $q.all(promises);
    };

    // Periods only
    this.postPeriod = function (experienceId, period) {
        var requestBody = {meta: ctrl.meta()};
        requestBody.data =
        {
            type: 'periods',
            attributes: {
                date_start: dateService.getDateString(period.attributes.date_start),
                date_end: dateService.getDateString(period.attributes.date_end),
                entire_boat: period.attributes.entire_boat,
                price: period.attributes.price,
                min_person: period.attributes.min_person
            },
            relationships: {
                experience: {
                    data: {
                        type: "experiences",
                        id: experienceId
                    }
                }
            }
        };

        return $http({
            method: 'POST',
            url: __env.apiUrl + 'periods',
            data: requestBody
        });
    };
    this.patchPeriod = function (period) {
        var requestBody = {meta: ctrl.meta()};

        requestBody.data =
        {
            id: period.id,
            type: 'periods',
            attributes: {
                date_start: dateService.getDateString(period.attributes.date_start),
                date_end: dateService.getDateString(period.attributes.date_end),
                entire_boat: period.attributes.entire_boat,
                price: period.attributes.price,
                min_person: period.attributes.min_person,
                is_default: period.attributes.is_default
            }
        };

        return $http({
            method: 'PATCH',
            url: __env.apiUrl + 'periods/' + period.id,
            data: requestBody
        });
    };
    this.deletePeriods = function (periods) {
        // Construct multiple API Calls
        var promises = [];

        periods.forEach(function (periodId) {
            // Add API call to promises
            promises.push(
                $http({
                    method: 'DELETE',
                    url: __env.apiUrl + 'periods/' + periodId,
                    data: {meta: ctrl.meta()}
                }));
        });

        // Resolve promises together
        return $q.all(promises);
    };
    this.deletePeriod = function (periodId) {
        return $http({
            method: 'DELETE',
            url: __env.apiUrl + 'periods/' + periodId,
            data: {meta: ctrl.meta()}
        });
    };

    // Prices
    this.postPrices = function (periodId, prices) {
        // Construct multiple API Calls
        var promises = [];
        prices.forEach(function (price) {
            var requestBody = {meta: ctrl.meta()};
            requestBody.data =
            {
                type: 'prices',
                attributes: {
                    price: price.attributes.price,
                    person: price.attributes.person
                },
                relationships: {
                    period: {
                        data: {
                            type: "periods",
                            id: periodId
                        }
                    }
                }
            };
            // Add API call to promises
            promises.push($http.post(__env.apiUrl + 'prices', requestBody));
        });

        // Resolve promises together
        return $q.all(promises);
    };
    this.postPrice = function (periodId, price) {
        var requestBody = {meta: ctrl.meta()};
        requestBody.data =
        {
            type: 'prices',
            attributes: {
                price: price.attributes.price,
                person: price.attributes.person
            },
            relationships: {
                period: {
                    data: {
                        type: "periods",
                        id: periodId
                    }
                }
            }
        };
        // Add API call to promises
        return $http.post(__env.apiUrl + 'prices', requestBody);
    };
    this.patchPrices = function (prices) {
        // Construct multiple API Calls
        var promises = [];

        prices.forEach(function (price) {
            var requestBody = {meta: ctrl.meta()};
            requestBody.data =
            {
                id: price.id,
                type: 'prices',
                attributes: {
                    price: price.attributes.price
                }
            };
            // Add API call to promises
            promises.push($http.patch(__env.apiUrl + 'prices/' + price.id, requestBody));
        });

        // Resolve promises together
        return $q.all(promises);
    };
    this.patchPrice = function (price) {
        var requestBody = {meta: ctrl.meta()};
        requestBody.data =
        {
            id: price.id,
            type: 'prices',
            attributes: {
                price: price.attributes.price
            }
        };

        return $http({
            method: 'PATCH',
            url: __env.apiUrl + 'prices/' + price.id,
            data: requestBody
        })
    };
    this.deletePrices = function (pricesIds) {
        var promises = [];

        pricesIds.forEach(function (id) {
            promises.push(
                $http({
                    method: 'DELETE',
                    url: __env.apiUrl + 'prices/' + id,
                    data: {meta: ctrl.meta()}
                }));
        });

        return $q.all(promises);
    };
    this.deletePrice = function (priceId) {
        return $http({
            method: 'DELETE',
            url: __env.apiUrl + 'prices/' + priceId,
            data: {meta: ctrl.meta()}
        });
    };
    // Seo
    this.postSeo = function (experienceId, seo) {
        var requestBody = {
            data: {
                type: 'seos',
                attributes: {
                    description: seo.description,
                    title: seo.title
                },
                relationships: {
                    experience: {
                        data: {
                            type: 'experiences',
                            id: experienceId
                        }
                    }
                }
            },
            meta: ctrl.meta()
        };

        return $http.post(__env.apiUrl + 'seos', requestBody)
    };
    this.getSlugUrl = function (title) {
        return $http.get(__env.apiUrl + 'slugurls?title=' + title);
    };
    this.getExperienceFromSlugUrl = function (slugUrl, params = {}) {
        var url = __env.apiUrl + 'slugurls/seo';

        params.slug_url = slugUrl;

        return $http({
            method: 'GET',
            url: url,
            params: params
        });
    };
    // Steps
    this.postStep = function (experienceId, step, message) {
        var requestBody = {
            data: {
                type: 'steps',
                attributes: {
                    step: step,
                    message: message
                },
                relationships: {
                    experience: {
                        data: {
                            type: 'experiences',
                            id: experienceId
                        }
                    }
                }
            },
            meta: ctrl.meta()
        };

        return $http.post(__env.apiUrl + 'steps', requestBody)
    }
    // Languages list
    this.getLanguages = function () {
        return $http.get(__env.apiUrl + "languages");
    };
    // Currencies list
    this.getCurrencies = function () {
        return $http.get(__env.apiUrl + "currencies");
    };

    // Register captain
    this.registerCaptain = function (captain) {
        var requestBody = {
            data: {
                type: 'users',
                attributes: {
                    'first_name': captain.name,
                    'phone': captain.phone,
                    'docking_place': captain.dockingPlace,
                    'email': captain.email,
                    'language': $cookies.get('language'),
                    'currency': $cookies.get('currency'),
                }
            },
            meta: ctrl.meta()
        };

        return $http.post(__env.apiUrl + 'captains', requestBody);
    };
    // Patch user
    this.patchUser = function (user) {
        var requestBody = {
            data: {
                type: 'users',
                id: user.id,
                attributes: user.attributes
            },
            meta: ctrl.meta()
        };

        return $http.patch(__env.apiUrl + 'users/' + user.id, requestBody)
    };
    // Patch user language
    this.patchUserLanguages = function (userId, languages) {
        var requestBody = {
            data: [],
            meta: ctrl.meta()
        };

        // Build request body
        languages.forEach(function (language) {
            requestBody.data.push({id: language.id, type: 'languages'});
        });

        return $http.patch(__env.apiUrl + 'users/' + userId + '/relationships/languages', requestBody)
    };
    // Set password
    this.setPassword = function (userId, password) {
        var meta = ctrl.meta();
        meta.password = password;
        return $http.post(__env.apiUrl + 'users/' + userId + '/password', {meta: meta});
    };
    this.resendPassword = function (email) {
        return $http({
            method: 'POST',
            url: __env.apiUrl + 'auth/restore',
            data: {meta: {email: email}}
        });
    };
    this.resetPassword = function (data) {
        return $http({
            method: 'POST',
            url: __env.apiUrl + 'auth/reset',
            data: {meta: data}
        });
    };

    // Pages
    this.postPage = function (name) {
        var requestBody = {
            data: {
                type: 'pages',
                attributes: {
                    name: name,
                }
            },
            meta: ctrl.meta()
        };

        return $http.post(__env.apiUrl + 'pages', requestBody)
    };

    // Translations
    this.postTranslation = function (sentenceId, translation, language) {
        var requestBody = {
            data: {
                id: sentenceId,
                type: 'sentences',
                attributes: {
                    text: translation,
                    language: language
                }
            },
            meta: ctrl.meta()
        };

        return $http.post(__env.apiUrl + 'sentences', requestBody)
    };

    this.postSentence = function (pageId, name, text) {
        var requestBody = {
            data: {
                type: 'sentences',
                attributes: {
                    name: name,
                    text: text
                },
                relationships: {
                    page: {
                        data: {
                            id: pageId,
                            type: 'pages'
                        }
                    }
                }
            },
            meta: ctrl.meta()
        };

        return $http.post(__env.apiUrl + 'sentences', requestBody)
    };

    // Mail token
    this.verifyToken = function (token) {
        return $http.post(__env.apiUrl + 'auth/token', {meta: {token: token}});
    };
    this.resendToken = function (email) {
        return $http.post(__env.apiUrl + 'resend/token', {meta: {email: email}});
    };

    // Account kit
    this.verifyPhone = function (userId, code) {
        var meta = ctrl.meta();
        meta.code = code;
        var requestBody = {
            data: {
                id: userId
            },
            meta: meta
        };

        return $http
            .post(__env.apiUrl + 'verify/accountkit', requestBody)
            .catch(function (err) {
                throw err;
            });
    };

    this.loginWithPhone = function (code, name, email) {
        return $http({
            method: 'POST',
            url: __env.apiUrl + 'auth/accountkit',
            data: {
                meta: {
                    name: name,
                    email: email,
                    language: $cookies.get('language'),
                    currency: $cookies.get('currency'),
                    code: code
                }
            }
        }).then(function (response) {
            return response;
        }, function (err) {
            throw err;
        })
    };

    this.loginWithFacebook = function (code) {
        return $http({
            method: 'POST',
            url: __env.apiUrl + 'auth/facebook',
            data: {
                meta: {
                    language: $cookies.get('language'),
                    currency: $cookies.get('currency'),
                    access_token: code
                }
            }
        })
            .then(function (response) {
                return response;
            }, function (err) {
                throw err;
            })
    }

    this.postLogout = function () {
        var meta = ctrl.meta();
        return $http({
            method: 'POST',
            url: __env.apiUrl + 'auth/logout',
            data: {
                meta: meta
            }
        }).then(function (response) {
            return response;
        }, function (err) {
            throw err;
        })
    };

    this.addParametersToUrl = function (url, parameters) {
        if (parameters) {
            for (var key in parameters) {
                if (parameters.hasOwnProperty(key)) {
                    url += "&" + key + "=" + parameters[key];
                }
            }
        }

        return url;
    }

    // PATCH BOOKING
    this.patchBooking = function (id, attributes) {
        var requestBody = {
            data: {
                type: 'bookings',
                id: id,
                attributes: attributes
            },
            meta: ctrl.meta()
        };

        return $http.patch(__env.apiUrl + 'bookings/' + id, requestBody)
    }

    // POST CONTACTS
    this.postContacts = function (contacts) {
        return $http({
            method: 'POST',
            url: __env.apiUrl + 'contacts',
            data: {
                email: contacts.email,
                text: contacts.text
            }
        })
            .then(function (response) {
                return response;
            }, function (err) {
                throw err;
            })
    };

    // PATCH MESSAGE IN CONVERSATIONS
    this.patchMessage = function (messageId, text) {
        var requestBody = {
            meta: ctrl.meta(),
            data: {
                type: "messages",
                id: messageId,
                attributes: {
                    message: text
                },
            }
        };

        return $http({
            method: 'PATCH',
            url: __env.apiUrl + 'messages/' + messageId,
            data: requestBody
        });
    };

    /**
     * Create a new user token for admin
     * @returns {HttpPromise}
     */
    this.createAdminToken = function (userId) {
        var requestBody = {
            meta: {
                csrf_token: $cookies.get('csrf_token'),
                user_id: userId
            }
        };
        return $http.post(__env.apiUrl + 'admin-token', requestBody);
    };

}]);