angular.module('shared').service('countryService', [
    'requestService', '__env', '$q',
    function (requestService, __env, $q) {
        var ctrl = this;
        ctrl.fetched = false;
        ctrl.deferred = $q.defer();

        ctrl.getCountries = function () {
            if (ctrl.fetched) return ctrl.deferred.promise;
            requestService.getListAsync('countries').then(
                function (response) {
                    ctrl.fetched = true;
                    var countries = [];
                    response.data.data.forEach(function (country) {
                        country = country.attributes;
                        countries.push(
                            {
                                code: country.name,
                                flag: __env.cloudinaryUrl + '/image/upload/production/assets/img/countries/' + country.name.toLowerCase() + '.png'
                            }
                        )
                    });
                    ctrl.deferred.resolve(countries);
                },
                function (response) {
                    ctrl.deferred.reject()
                }
            );
            return ctrl.deferred.promise;
        };
    }]);