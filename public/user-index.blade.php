<!DOCTYPE html>
<html lang="@{{language}}" data-ng-app="users">
<head>
    <meta name="fragment" content="!">
    <!-- SEO Metas -->
    <title ng-bind="metaTitle"></title>
    <meta name="description" content="@{{metaDescription}}">
    <meta name="keywords" content="@{{metaKeywords}}">

    <meta property="og:url" content="@{{ogUrl}}"/>
    <meta property="fb:app_id" content="{{ env('FB_APP_ID') }}"/>
    <meta property="og:type" content="@{{ogType}}"/>
    <meta property="og:title" content="@{{metaTitle}}"/>
    <meta property="og:description" content="@{{metaDescription}}"/>
    <meta property="og:image" content="@{{ogImage}}"/>
    <meta property="article:author" content="@{{ogAuthor}}"/>
    <meta property="article:section" content="@{{ogSection}}"/>

    <meta name="prerender-status-code" content="@{{statusCode}}">

    <!-- Crawler alternate languages -->
    <link ng-repeat="alternate in alternates" rel="alternate" ng-attr-hreflang="@{{alternate.lang}}"
          ng-href="@{{alternate.url}}"/>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    @include('favicon')
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,600&amp;subset=latin-ext" rel="stylesheet"
          type="text/css">
    <!-- CSS -->
    <link type="text/css" rel="stylesheet" href="{{ elixir('users/assets/css/style.css') }}">
    <!-- TO AVOID ANGULAR # ON ADDRESS -->
    <base href="/">
    <?php include env('USER_APP_FOLDER') . "/env.php"; ?>

</head>
<body class="users-site"
      ng-class="[{'sidebar-open': !ctrl.collapsed, 'modal-open': ctrl.isModalOpen,
      'loading': ctrl.loading}, ctrl.page]"
      ng-controller="rootController as ctrl"
      ng-click="ctrl.hideCookieBanner()">
<!-- Header -->
<header></header>

<!-- Content -->
<div id="content" data-ng-view autoscroll></div>
<!--Global Loader-->
<div class="global-loader loader-alt inside-parent"></div>

<!--Cookie Policy Banner -->
<cookie-banner is-visible="ctrl.cookieBanner" on-close="ctrl.hideCookieBanner()"></cookie-banner>

<!--Adwords-->
<script type="text/javascript" src="https://www.googleadservices.com/pagead/conversion_async.js" async defer
        charset="utf-8"></script>

<!--SMS LOGIN-->
<script>
    AccountKit_OnInteractive = function () {
        AccountKit.init(
                {
                    appId: __env.fbAppId,
                    state: "12345",
                    version: __env.fbAppVersion,
                    fbAppEventsEnabled: true
                }
        );
    };
</script>
<!-- HTTPS required. HTTP will give a 403 forbidden response -->
<script src="https://sdk.accountkit.com/en_US/sdk.js" async></script>

<!--GOOGLE ANALYTICS-->
<script type="text/javascript">
    if (__env.appEnv == 'production') {
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments)
                    }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                    m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-57038398-2', 'auto');
        ga('set', 'anonymizeIp', true);
        ga('send', 'pageview');
    }
</script>

<!-- App -->
<script src="{{ elixir('users/assets/js/libraries.js') }}"></script>
<script src="{{ elixir('users/assets/js/app.js') }}"></script>

<!--CREATED BY FACEBOOK IF NOT ALREADY PRESENT (minor speed up)-->
<div id="fb-root"></div>
</body>
</html>