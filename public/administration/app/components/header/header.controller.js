angular
    .module('administration')
    .controller('headerController', ['iconService', 'requestService', function (iconService, requestService) {
        this.profilePic = '/shared/assets/img/ragazza.jpg';

        this.logout = function () {
            requestService.logout('/login');
        };
    }]);