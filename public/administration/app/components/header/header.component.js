angular.module('administration').component('header', {
    templateUrl: '/administration/app/components/header/header.view.html',
    controller: 'headerController'
});