angular
    .module('administration')
    .controller('couponsListController',
        ['statusService', '$scope',
        function (statusService, $scope) {
        // Make context accessible inside other scopes
        var ctrl = this;
        ctrl.loading = true;

        // Fetch data from service
        requestService.getList(function (data, totalItems) {
            ctrl.loading = false;
            ctrl.totalItems = totalItems;
            ctrl.data = data;
        }, 'coupons');

        // Set table properties
        this.columns = [
            {name: 'code', heading: 'Codice'},
            {name: 'amount', heading: '%'},
            {name: 'date', heading: 'Data'},
            {name: 'assigned_to', heading: 'Assegnato a'},
            {name: 'country', heading: 'Località'},
            {name: 'status', heading: 'Stato'},
            {name: 'actions', heading: 'Azioni'}
        ];

        // Set top icons
        this.icons = [
            {type: 'world', template: 'search-coupon-by-country'},
            {type: 'boat', template: 'boats'},
            {type: 'filter', template: 'filters-coupon'},
            {type: 'search', template: 'search-coupon'}
        ];

        this.controlIconsColor = 'navy-blue';

        this.getStatusName = function (status) {
            return statusService.getCouponStatusName(status);
        };

        // Search coupon
        $scope.searchString = '';
        $scope.searchCoupon = function (text) {
            console.log('Text: ' + text)
        };

        // Pagination
        this.currentPage = 1;
        this.perPage = 6;
        this.pagesToDisplay = 5;
        this.boundaryLinks = true;
        this.directionLinks = false;

        // Request new data
        this.pageChanged = function () {
            requestService.getList(function (data) {
                ctrl.data = data;
            }, 'coupons', this.currentPage);
        }
    }]);