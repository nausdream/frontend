angular.module('administration').controller('uploadPhotoController', ['requestService', 'photoService',
    function (requestService, photoService) {
        this.$onInit = function () {
            var ctrl = this;

            // Differentiate between new photo and old
            ctrl.newPhoto = ctrl.newPhoto == 'true';

            ctrl.upload = function (file) {
                if (!file) return;

                ctrl.loading = true;

                var fileType = file["type"];
                var validImageTypes = ["image/gif", "image/jpeg", "image/png"];

                // Not an image
                if (validImageTypes.indexOf(fileType) === -1) {
                    ctrl.loading = false;
                    ctrl.onComplete({link: null});
                } else {
                    var putSignedUrl;
                    var getSignedUrl;
                    // Receive signed url from backend
                    requestService.getPresignedUrl().then(
                        function (response) {
                            putSignedUrl = response.data.data.attributes.put_url;
                            getSignedUrl = response.data.data.attributes.get_url;

                            // Upload photo to s3 and receive url
                            photoService.uploadToS3(putSignedUrl, file)
                                .then(function (response) {
                                    ctrl.loading = false;
                                    ctrl.newPhotoLink = getSignedUrl;

                                    ctrl.onComplete({link: getSignedUrl})
                                });
                        }
                    );
                }
            };
        }
    }]);