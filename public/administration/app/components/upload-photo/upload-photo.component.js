angular.module('administration').component('uploadPhoto', {
    templateUrl: '/administration/app/components/upload-photo/upload-photo.view.html',
    controller: 'uploadPhotoController',
    controllerAs: 'ctrl',
    bindings: {
        onClose: '&',
        onComplete: '&',
        onDelete: '&',
        newPhoto: '@'
    }
});