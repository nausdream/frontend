angular
    .module('administration')
    .controller('usersListController', [
        'statusService', '$scope', 'requestService', '$uibModal', '$q', '$location', '$timeout',
        function (statusService, $scope, requestService, $uibModal, $q, $location, $timeout) {
            // Make context accessible inside other scopes
            var ctrl = this;
            ctrl.loading = true;

            var deferred = $q.defer();

            // Filters
            ctrl.searchFilter = $location.search().filter || '';

            let userTypes = $location.search().types || '';
            userTypes = userTypes.split(',');
            ctrl.filterUsers = userTypes.indexOf('users') != -1;
            ctrl.filterCaptains = userTypes.indexOf('captains') != -1;
            ctrl.filterPartners = userTypes.indexOf('partners') != -1;

            // Pagination
            ctrl.currentPage = parseInt($location.search().page) || 1;
            ctrl.perPage = 12;
            ctrl.pagesToDisplay = 5;
            ctrl.boundaryLinks = true;
            ctrl.directionLinks = false;

            // Set table properties
            this.columns = [
                {name: 'name', heading: 'Name'},
                {name: 'date', heading: 'Subscription date'},
                {name: 'email', heading: 'E-mail'},
                {name: 'country', heading: 'Location'},
                {name: 'actions', heading: 'Actions'}
            ];

            // Set top icons
            this.icons = [
                {type: 'filter', template: 'filters-user'},
                {type: 'search', template: 'search-user'}
            ];

            this.controlIconsColor = 'navy-blue';

            // Request new data
            ctrl.pageChanged = function () {
                $location.search('page', ctrl.currentPage);
            };

            // Set modal
            ctrl.loginAsUser = function (userId) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: '/shared/components/modal/login-modal/login-modal.view.html',
                    controller: 'loginModalController',
                    controllerAs: 'ctrl',
                    resolve: {
                        item: function () {
                            return {
                                user_id: userId
                            };
                        }
                    }
                });
                modalInstance.result.then(function (data) {
                    // if (data.actionDone) {
                    //     if (!ctrl.isFormValid(ctrl.step1) || !ctrl.isFormValid(ctrl.step2) || !ctrl.isFormValid(ctrl.step3) || !ctrl.isFormValid(ctrl.step4)) {
                    //         validationModalService.open();
                    //         return;
                    //     }
                    //     // Set loading
                    //     ctrl.loading = true;
                    //     // Status is set to null because backend expects status to be present only when changing experience status
                    //     delete ctrl.experience.status;
                    //     // Send PUT to save experience data, then send PUT to change status. Wait for data save before the second PUT
                    //     ctrl.put().then(
                    //         function () {
                    //             // Change experience status to 'accepted'
                    //             ctrl.changeStatus('accepted');
                    //         },
                    //         function (response) {
                    //             ctrl.errorCallback(response);
                    //         }
                    //     );
                    // }
                });
            };

            // Load users with current params
            ctrl.load = function () {
                ctrl.loading = true;

                deferred.resolve();
                deferred = $q.defer();

                let params = {
                    page: ctrl.currentPage,
                    per_page: ctrl.perPage,
                };

                let userTypes = $location.search().types || '';
                if (userTypes.length > 0) params.type = userTypes;
                if (ctrl.searchFilter.length > 0) params.filter = ctrl.searchFilter;

                let usersPromise = requestService.getListAsync('users', params, deferred);
                usersPromise
                    .then(
                        function (response) {
                            ctrl.data = response.data.data;
                            ctrl.loading = false;
                            ctrl.totalItems = response.data.meta.total_elements;
                        },
                        function (response) {
                            ctrl.loading = false;
                        });
            };

            // When user changes filter
            ctrl.changeFilter = function () {
                $timeout(
                    function () {
                        if (ctrl.searchFilter.length > 0) {
                            $location.search('filter', ctrl.searchFilter);
                        } else {
                            $location.search('filter', null);
                        }
                        // Start again from page 1
                        $location.search('page', null);
                    },
                    1500
                )
            };

            // When user filters by type
            ctrl.changeType = function () {
                let userTypes = [];

                if (ctrl.filterUsers) userTypes.push('users');
                if (ctrl.filterCaptains) userTypes.push('captains');
                if (ctrl.filterPartners) userTypes.push('partners');

                userTypes = userTypes.join();

                if (userTypes.length > 0) {
                    $location.search('types', userTypes);
                } else {
                    $location.search('types', null);
                }

                // Start again from page 1
                $location.search('page', null);
            };

            // Initial load
            ctrl.load();
        }]);