angular
    .module('administration')
    .controller('captainsListController',['statusService', '$scope', function (statusService, $scope) {
        // Make context accessible inside other scopes
        var ctrl = this;
        ctrl.loading = true;

        // Fetch data from service
        requestService.getList(function (data, totalItems) {
            ctrl.loading = false;
            ctrl.totalItems = totalItems;
            ctrl.data = data;
        }, 'captains');

        // Set table properties
        this.columns = [
            {name: 'pic', heading: ''},
            {name: 'name', heading: 'Nome'},
            {name: 'date', heading: 'Data'},
            {name: 'email', heading: 'E-mail'},
            {name: 'country', heading: 'Località'},
            {name: 'status', heading: 'Stato'},
            {name: 'actions', heading: 'Azioni'}
        ];

        // Set top icons
        this.icons = [
            {type: 'world', template: 'search-captain-by-country'},
            {type: 'boat', template: 'boats'},
            {type: 'filter', template: 'filters-captain'},
            {type: 'search', template: 'search-captain'}
        ];

        this.controlIconsColor = 'navy-blue';

        this.getStatusName = function (status) {
            return statusService.getCaptainStatusName(status);
        };

        // Search captain
        $scope.searchString = '';
        $scope.searchCaptain = function (text) {
            console.log('Text: ' + text)
        };

        // Pagination
        this.currentPage = 1;
        this.perPage = 6;
        this.pagesToDisplay = 5;
        this.boundaryLinks = true;
        this.directionLinks = false;

        // Request new data
        this.pageChanged = function () {
            requestService.getList(function (data) {
                ctrl.data = data;
            }, 'captains', this.currentPage);
        }
    }]);