angular.module('administration').controller('rootController', [
        'modalService', '$rootScope',
        function (modalService, $rootScope) {
            var ctrl = this;
            ctrl.isModalOpen = modalService.isOpen;

            // Prevent body from scrolling when modal is open
            $rootScope.$on('modalOpened', function () {
                ctrl.isModalOpen = true;
            });
            $rootScope.$on('modalClosed', function () {
                ctrl.isModalOpen = false;
            });
        }
    ]
);
