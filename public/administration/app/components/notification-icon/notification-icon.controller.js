angular
    .module('administration')
    .controller('notificationIconController', [
        'iconService',
        function (iconService) {
            this.$onInit = function () {
                // Fetch icons data from iconService
                var data = iconService.getIconsData(this.datatype);
                this.iconName = data.icon || this.datatype;
                this.text = data.text;

                this.count = data.count;
                this.name = data.name;

                if (this.popover) {
                    // Set popover template
                    this.popoverTemplate = '/administration/app/components/notification-icon/views/popover-list.view.html';
                    // Create popover list
                    this.popoverData = {
                        experience: iconService.getIconsData('experience'),
                        captain: iconService.getIconsData('captain'),
                        boat: iconService.getIconsData('boat'),
                        hosts: iconService.getIconsData('hosts'),
                        chat: iconService.getIconsData('chat'),
                        mail: iconService.getIconsData('mail')
                    };
                }
            };
        }]);