angular
    .module('administration')
    .controller('conversationsListController', [
        'statusService', 'requestService',
        function (statusService, requestService) {
            // Make context accessible inside other scopes
            var ctrl = this;
            ctrl.loading = true;

            // Pagination
            ctrl.currentPage = 1;
            ctrl.perPage = 12;
            ctrl.boundaryLinks = true;
            ctrl.directionLinks = false;

            // Fetch conversations from service
            ctrl.requestData = function (page) {
                ctrl.loading = true;

                requestService.getListAsync('conversations', {page: page, per_page: ctrl.perPage}).then(
                    function (response) {
                        ctrl.totalItems = response.data.meta.total_elements;
                        ctrl.pagesToDisplay = response.data.meta.total_pages;
                        ctrl.data = response.data.data;
                        ctrl.loading = false;
                    },
                    null
                );
            };
            ctrl.requestData(ctrl.currentPage);

            // Set table properties
            ctrl.columns = [
                {name: 'users', heading: 'Users'},
                {name: 'date', heading: 'Date'},
                {name: 'alert', heading: 'Alerts'},
                {name: 'status', heading: 'Booking Status'},
                {name: 'actions', heading: 'Actions'}
            ];

            // Set top icons
            ctrl.icons = [
                {type: 'search', template: 'search-conversation'}
            ];

            ctrl.controlIconsColor = 'navy-blue';

            ctrl.getStatusName = function (status) {
                return statusService.getConversationStatusName(status);
            };
        }]
    );