angular
    .module('administration')
    .controller('sidebarController', function () {
        this.places = [
            //{name: 'home', url: '/'},
            {name: 'hosts', url: 'users'},
            {name: 'boat', url: 'boats'},
            {name: 'experience', url: 'experiences'},
            // {name: 'hosts', url: 'users'},
            {name: 'chat', url: 'conversations'},
            {name: 'booking-offsite', url: 'bookings-offsite'},
            {name: 'eden-voucher', url: 'eden-vouchers/new'}
            // {name: 'booking', url: 'bookings'},
            // {name: 'coupon', url: 'coupons'}
        ];
    });