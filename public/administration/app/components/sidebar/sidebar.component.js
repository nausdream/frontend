angular.module('administration').component('sidebar', {
    templateUrl: '/administration/app/components/sidebar/sidebar.view.html',
    controller: 'sidebarController'
});