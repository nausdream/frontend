angular
    .module('administration')
    .controller('pathIconController',[
        'iconService', '$rootScope',
        function (iconService, $rootScope) {
        this.$onInit = function () {

            // Fetch icons data from iconService
            var data = iconService.getIconsData(this.datatype);

            this.name = data.name;
            this.url = data.url || data.icon;
            this.dropdownData = data.dropdownData;
            this.isMouseOverItem = false;
            this.hasActiveChild = false;

            // Determine if a child is active (i.e. current route url is the same as child url)
            this.isActiveChild = function isActiveChild(item) {
                return item.url == $rootScope.currentUrl;
            };

            // Determine if the current icon has an active child
            this.hasActiveChild = function hasActiveChild(){
                for (var li in this.dropdownData) {
                    if (this.isActiveChild(this.dropdownData[li])) return true;
                }
            };
        };
    }]);