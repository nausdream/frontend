angular.module('administration').component('pathIcon', {
    templateUrl: '/administration/app/components/sidebar/components/path-icon/path-icon.view.html',
    controller: 'pathIconController',
    bindings: {
        datatype: '@'
    }
});