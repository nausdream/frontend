/**
 * Created by lucas on 10/07/2017.
 */
angular.module('administration').controller('bookingOffsiteController', [
    'fixedListService', 'requestService', '$translatePartialLoader', '$translate', '$routeParams', '$route',
    function (fixedListService, requestService, $translatePartialLoader, $translate, $routeParams, $route) {
        // Make context accessible inside other scopes
        var ctrl = this;
        ctrl.loading = true;

        // Load translations
        $translatePartialLoader.addPart('boat_types');
        $translatePartialLoader.addPart('languages');
        $translatePartialLoader.addPart('currencies');
        $translate.refresh();

        // Get booking id
        ctrl.id = $routeParams.id;

        // Field values
        ctrl.booking = {
            'first_name_guest': null,
            'last_name_guest': null,
            'address_guest': null,
            'city_guest': null,
            'phone_guest': null,
            'email_guest': null,
            'vat_guest': null,
            'experience_date': new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
            'arrival_date': new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
            'experience_title': null,
            'experience_description': null,
            'boat_type': 'sailboat',
            'departure_port': null,
            'arrival_port': null,
            'departure_time': new Date(),
            'arrival_time': new Date(),
            'adults': 2,
            'kids': 0,
            'babies': 0,
            'included_services': null,
            'excluded_services': null,
            'drinks': null,
            'dishes': null,
            'price': 1,
            'fee': 1,
            'expiration_date': new Date(new Date().getTime() + 8 * 24 * 60 * 60 * 1000),
            'language_guest': 'en',
            'currency_guest': 'USD',
            'language_captain': 'en',
            'currency_captain': 'USD',
            'first_name_captain': null,
            'last_name_captain': null,
            'email_captain': null,
            'phone_captain': null,
            'without_payment': false
        };

        // Set booking as paid
        ctrl.setPaid = function () {
            requestService.patchObject('offsite-bookings', ctrl.id, {status: 'paid'}).then(
                function () {
                    $route.reload();
                },
                null
            );
        };

        // Boat types
        fixedListService.getFixedList('boat-types').then(
            function (response) {
                ctrl.boat_types = response.data.data;
            },
            function (response) {

            }
        );

        // Languages
        fixedListService.getFixedList('languages').then(
            function (response) {
                ctrl.languages = response.data.data;
            },
            function (response) {

            }
        );

        // Currencies
        fixedListService.getFixedList('currencies').then(
            function (response) {
                ctrl.currencies = response.data.data;
            },
            function (response) {

            }
        );

        // Get offsite booking
        requestService.getSingleAsync('offsite-bookings', ctrl.id).then(
            function (response) {
                ctrl.booking = response.data.data.attributes;

                // Transform departure/arrival time
                var splitDepartureDate = ctrl.booking.departure_time.split(':');
                ctrl.booking.departure_time = new Date();
                ctrl.booking.departure_time.setHours(splitDepartureDate[0], splitDepartureDate[1], splitDepartureDate[2]);
                var splitArrivalDate = ctrl.booking.arrival_time.split(':');
                ctrl.booking.arrival_time = new Date();
                ctrl.booking.arrival_time.setHours(splitArrivalDate[0], splitArrivalDate[1], splitArrivalDate[2]);

                ctrl.loading = false;
            },
            function (response) {
                ctrl.loading = false;
            }
        )
    }]);