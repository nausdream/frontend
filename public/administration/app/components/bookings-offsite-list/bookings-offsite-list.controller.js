angular
    .module('administration')
    .controller('bookingsOffsiteListController', [
        'statusService', '$scope', 'requestService',
        function (statusService, $scope, requestService) {
            // Make context accessible inside other scopes
            var ctrl = this;
            ctrl.loading = true;

            // Pagination
            ctrl.currentPage = 1;
            ctrl.perPage = 6;
            ctrl.boundaryLinks = true;
            ctrl.directionLinks = false;

            // Fetch conversations from service
            ctrl.requestData = function (page) {
                ctrl.loading = true;

                requestService.getListAsync('offsite-bookings', {page: page, per_page: ctrl.perPage}).then(
                    function (response) {
                        ctrl.totalItems = response.data.meta.total_elements;
                        ctrl.pagesToDisplay = response.data.meta.total_pages;
                        ctrl.data = response.data.data;
                        ctrl.loading = false;
                    },
                    function () {
                        ctrl.loading = false;
                    }
                );
            };
            ctrl.requestData(ctrl.currentPage);

            // Set table properties
            this.columns = [
                {name: 'id', heading: 'ID'},
                {name: 'date', heading: 'Experience Date'},
                {name: 'title', heading: 'Title'},
                {name: 'guest', heading: 'Guest'},
                {name: 'captain', heading: 'Captain'},
                {name: 'status', heading: 'Status'},
                {name: 'actions', heading: 'Actions'}
            ];

            // Set top icons
            this.icons = [
                {type: 'search', template: 'search-booking'}
            ];

            this.controlIconsColor = 'navy-blue';

            this.getStatusName = function (status) {
                return statusService.getBookingStatusName(status);
            };

            // Pagination
            this.currentPage = 1;
            this.perPage = 6;
            this.pagesToDisplay = 5;
            this.boundaryLinks = true;
            this.directionLinks = false;

            // Request new data
            this.pageChanged = function () {
                requestService.getList(function (data) {
                    ctrl.data = data;
                }, 'bookings', this.currentPage);
            }
        }]);