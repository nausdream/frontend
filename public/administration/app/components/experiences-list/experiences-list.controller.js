angular
    .module('administration')
    .controller('experiencesListController', [
        'statusService', '$scope', 'requestService', '$q', '$location', '$timeout',
        function (statusService, $scope, requestService, $q, $location, $timeout) {
            // Make context accessible inside other scopes
            var ctrl = this;
            ctrl.loading = true;

            var deferred = $q.defer();

            // Filters
            ctrl.searchFilter = $location.search().filter || '';

            // Pagination
            this.currentPage = parseInt($location.search().page) || 1;
            this.perPage = 10;
            this.pagesToDisplay = 5;
            this.boundaryLinks = true;
            this.directionLinks = false;

            // Set table properties
            this.columns = [
                {name: 'title', heading: 'Titolo'},
                {name: 'date', heading: 'Data Inserimento'},
                {name: 'dockingPlace', heading: 'Località d\'ormeggio'},
                {name: 'captain', heading: 'Capitano'},
                {name: 'status', heading: 'Stato'},
                {name: 'actions', heading: 'Azioni'}
            ];

            // Set top icons
            this.icons = [
                {type: 'world', template: 'search-experience-by-country'},
                {type: 'boat', template: 'boats'},
                {type: 'filter', template: 'filters-experience'},
                {type: 'search', template: 'search-experience'}
            ];

            this.controlIconsColor = 'navy-blue';

            // When user changes filter
            ctrl.changeFilter = function () {
                $timeout(
                    function () {
                        if (ctrl.searchFilter.length > 0) {
                            $location.search('filter', ctrl.searchFilter);
                        } else {
                            $location.search('filter', null);
                        }
                        // Start again from page 1
                        $location.search('page', null);
                    },
                    1500
                )
            };

            this.getStatusName = function (status, isFinished) {
                if (!isFinished) status = 'new';
                return statusService.getExperienceStatusName(status);
            };

            // Reload with new page
            this.pageChanged = function () {
                $location.search('page', ctrl.currentPage);
            };

            // Load experiences with current params
            ctrl.load = function () {
                ctrl.loading = true;

                deferred.resolve();
                deferred = $q.defer();

                let params = {
                    page: ctrl.currentPage,
                    per_page: ctrl.perPage,
                    editing: true
                };

                if (ctrl.searchFilter.length > 0) params.filter = ctrl.searchFilter;

                let experiencesPromise = requestService.getListAsync('experiences', params, deferred);
                experiencesPromise.then(
                    function (response) {
                        ctrl.data = response.data.data;
                        ctrl.loading = false;
                        ctrl.totalItems = response.data.meta.total_elements;
                    },
                    function (response) {
                        ctrl.loading = false;
                    }
                );
            };

            // Initial load
            ctrl.load();
        }]);