/**
 * Created by lucas on 10/07/2017.
 */
angular.module('administration').controller('edenVoucherNewController', [
    'fixedListService', 'requestService', '$translatePartialLoader', '$translate', 'dateService',
    'errorModalService', 'validationService',
    function (fixedListService, requestService, $translatePartialLoader, $translate, dateService,
              errorModalService, validationService) {
        // Make context accessible inside other scopes
        var ctrl = this;

        // Load translations
        $translatePartialLoader.addPart('languages');
        $translate.refresh();

        ctrl.bootstrap = function () {
            // Field rules
            ctrl.fields = {
                'village_name': {
                    required: true,
                    max: 150
                },
                'assistant_name': {
                    required: true,
                    max: 300
                },
                'village_phone': {
                    required: true,
                    max: 150
                },
                'village_mail': {
                    required: true,
                    max: 150
                },
                'voucher_number': {
                    required: true
                },
                'contact_name': {
                    required: true,
                    max: 300
                },
                'guest_name': {
                    required: true,
                    max: 300
                },
                'room_number': {
                    required: true,
                    max: 20
                },
                'guests': {
                    max: 40,
                    required: true
                },
                'experience_title': {
                    required: true,
                    max: 300
                },
                'tour_number': {
                    required: true
                },
                'experience_date': {
                    required: true,
                    min: new Date(),
                    max: 100
                },
                'departure_port': {
                    required: true,
                    max: 100
                },
                'departure_time': {
                    required: true,
                    max: 150
                },
                'arrival_time': {
                    required: true,
                    max: 150
                },
                'guest_language': {
                    required: true
                }
            };

            // Field values
            ctrl.booking = {
                'village_name': null,
                'assistant_name': null,
                'village_phone': null,
                'village_mail': null,
                'voucher_number': null,
                'contact_name': null,
                'guest_name': null,
                'room_number': null,
                'guests': null,
                'experience_title': null,
                'tour_number': null,
                'experience_date': new Date(),
                'departure_port': null,
                'departure_time': new Date(),
                'arrival_time': new Date(),
                'guest_language': 'en'
            };
        };
        ctrl.bootstrap();


        // Languages
        fixedListService.getFixedList('languages').then(
            function (response) {
                ctrl.languages = response.data.data;
            },
            function (response) {

            }
        );

        // Post
        ctrl.post = function () {
            ctrl.loading = true;

            if (!ctrl.form.$valid) {
                validationService.setFrontendErrors(ctrl.form, {});
                return;
            }

            let attributes = angular.copy(ctrl.booking);

            // Transform attributes to meet Backend specifications
            attributes.departure_time = attributes.departure_time.toLocaleTimeString('en-US', { hour12: false });
            attributes.arrival_time = attributes.arrival_time.toLocaleTimeString('en-US', { hour12: false });
            attributes.experience_date = dateService.getDateString(attributes.experience_date);

            requestService.postSingle('eden-vouchers', attributes).then(
                function (response) {
                    ctrl.loading = false;
                    ctrl.successModal = true;
                },
                function (error) {
                    // If not a backend error, create error manually
                    if (!error.data || !error.data.errors) {
                        error = {
                            status: error.status,
                            config: {url: 'GENERATING NEW EDEN VOUCHER'},
                            data: {
                                errors: [{code: null, detail: 'There was some error', title: 'New Eden Voucher'}]
                            }
                        };
                    }

                    ctrl.loading = false;
                    errorModalService.open(error)
                }
            )
        };

        // Go to route
        ctrl.reload = function () {
            ctrl.bootstrap();
            ctrl.successModal = false;
        }
    }]);