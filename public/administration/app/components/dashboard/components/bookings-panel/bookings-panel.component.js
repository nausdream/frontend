angular.module('administration').component('bookingsPanel', {
    templateUrl: '/administration/app/components/dashboard/components/bookings-panel/bookings-panel.view.html',
    controller: 'bookingsPanelController'
});