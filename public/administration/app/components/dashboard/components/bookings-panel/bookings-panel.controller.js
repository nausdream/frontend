angular
    .module('administration')
    .controller('bookingsPanelController', [
        'iconService', 'statusService',
        function (iconService, statusService) {
        this.status = {
            open: true
        };

        this.title = 'Ultime prenotazioni';

        // Make context accessible inside other scopes
        var ctrl = this;

        // Fetch data from service
        requestService.getList(function (data) {
            ctrl.bookings = data;
        }, 'bookings');

        this.arrowIcon = iconService.getUrl('arrow');
        this.plusIcon = iconService.getUrl('plus');
        this.minusIcon = iconService.getUrl('minus');

        this.getStatusName = function (status) {
            return statusService.getBookingStatusName(status);
        }
    }]);