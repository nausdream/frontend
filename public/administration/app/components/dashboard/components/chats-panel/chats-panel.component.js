angular.module('administration').component('chatsPanel', {
    templateUrl: '/administration/app/components/dashboard/components/chats-panel/chats-panel.view.html',
    controller: 'chatsPanelController'
});