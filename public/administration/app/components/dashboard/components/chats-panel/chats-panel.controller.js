angular
    .module('administration')
    .controller('chatsPanelController',[
        'iconService',
        function (iconService) {
        this.status = {
            open: true
        };

        this.title = 'Chat Capitani&Ospiti';

            // Make context accessible inside other scopes
            var ctrl = this;

            // Fetch data from service
            requestService.getList(function (data) {
                    ctrl.chats = data;
            }, 'conversations');

        this.previewIcon = iconService.getUrl('preview');
        this.editIcon = iconService.getUrl('edit');
        this.arrowIcon = iconService.getUrl('arrow');
        this.plusIcon = iconService.getUrl('plus');
        this.minusIcon = iconService.getUrl('minus');
    }]);