angular.module('administration').component('messagesPanel', {
    templateUrl: '/administration/app/components/dashboard/components/messages-panel/messages-panel.view.html',
    controller: 'messagesPanelController'
});