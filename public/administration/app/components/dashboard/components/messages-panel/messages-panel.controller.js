angular
    .module('administration')
    .controller('messagesPanelController',
        ['iconService',
        function (iconService) {
        this.status = {
            open: true
        };

        this.title = 'Messaggi';

        // Make context accessible inside other scopes
        var ctrl = this;

        // Fetch data from service
        requestService.getList(function (data) {
            ctrl.messages = data;
        }, 'messages');

        // Set icons
        this.messagesIcon = iconService.getUrl('messages');
        this.addMessagesIcon = iconService.getUrl('plus');
        this.searchIcon = iconService.getUrl('search');

        // Set current view (list/single user messages)
        this.defaultView = '/administration/app/components/dashboard/components/messages-panel/views/messages-list.view.html';
        this.view = this.defaultView;
    }]);