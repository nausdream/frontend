/**
 * Pass photo link and type of user (guest | captain).
 * If photo if null, get placeholder based on user role. If role is not defined, get user placeholder
 */

angular.module('administration').component('userPic', {
    templateUrl: '/users/app/components/user-pic/user-pic.view.html',
    controller: 'userPicController',
    controllerAs: 'ctrl',
    bindings: {
        userPicLink: '@',
        userRole: '@'
    }
});