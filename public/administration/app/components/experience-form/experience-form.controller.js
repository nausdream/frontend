angular
    .module('administration')
    .controller('experienceFormController',
        ['staticDataService', 'requestService', '$location', '$routeParams', '$uibModal', '__env', 'errorModalService',
            'successModalService', 'validationModalService', '$q', 'mapsService', '$scope', '$translatePartialLoader', '$translate',
            '$timeout',
            function (staticDataService, requestService, $location, $routeParams, $uibModal, __env, errorModalService,
                      successModalService, validationModalService, $q, mapsService, $scope, $translatePartialLoader, $translate,
                      $timeout) {
                // Make context accessible inside other scopes
                var ctrl = this;

                // Set initial view
                ctrl.loading = true;
                // Set disabled (read only form)
                ctrl.disabled = true;
                ctrl.safeMode = false;

                // Store open/closed popovers
                ctrl.popovers = {};

                // Set experience loaded promise
                ctrl.experienceLoaded = $q.defer();

                // Load translations
                $translatePartialLoader.addPart('experience_types');
                $translatePartialLoader.addPart('rules');
                $translatePartialLoader.addPart('additional_services');
                $translatePartialLoader.addPart('fishing_types');
                $translate.refresh();

                // Initialize data
                ctrl.departure_time = new Date();
                ctrl.arrival_time = new Date();
                // Experience types
                staticDataService.getExperienceTypeList(function (data) {
                    ctrl.experience_types_list = data;
                });
                // Durations
                staticDataService.getExperienceDurationList(function (data) {
                    ctrl.durations = data;

                    ctrl.getExperienceDurationTextFromQuantity = function (days) {
                        if (!days) {
                            return "Seleziona durata dell'attività";
                        }
                        if (days <= 1) {
                            return ctrl.durations.filter(function (element) {
                                return element.name == 'up-to-one-day';
                            })[0].text
                        }
                        return ctrl.durations.filter(function (element) {
                            return element.name == 'more-than-one-day';
                        })[0].text
                    };

                    ctrl.getExperienceDurationTextFromName = function (name) {
                        if (!name) {
                            return "Seleziona durata dell'attività";
                        }
                        return ctrl.durations.filter(function (element) {
                            return element.name == name;
                        })[0].text
                    };
                });
                // Fishing partitions
                staticDataService.getFishingPartitionList(function (data) {
                    ctrl.fishing_partitions = data;

                    ctrl.getFishingPartitionTextFromName = function (name) {
                        if (!name) {
                            return 'Error';
                        }
                        return ctrl.fishing_partitions.filter(function (element) {
                            return element.attributes.name == name;
                        })[0].text
                    };
                });
                // Fishing types
                staticDataService.getFishingTypeList(function (data) {
                    ctrl.fishing_types = data;

                    ctrl.getFishingTypeTextFromName = function (name) {
                        if (!name) {
                            return 'Error';
                        }
                        return ctrl.fishing_types.filter(function (element) {
                            return element.attributes.name == name;
                        })[0].text
                    };
                });
                // Foods
                staticDataService.getFoodList(function (data) {
                    ctrl.foods = data;
                });

                ctrl.departure_coordinates = {lat: 0, lng: 0};
                ctrl.destination_coordinates = {lat: 0, lng: 0};
                ctrl.days_list = [
                    {name: 'monday', text: 'Lunedì'},
                    {name: 'tuesday', text: 'Martedì'},
                    {name: 'wednesday', text: 'Mercoledì'},
                    {name: 'thursday', text: 'Giovedì'},
                    {name: 'friday', text: 'Venerdì'},
                    {name: 'saturday', text: 'Sabato'},
                    {name: 'sunday', text: 'Domenica'}
                ];
                ctrl.selectedDay = ctrl.days_list[0];

                // Get ID
                ctrl.id = $routeParams.id;

                // TODO: increase efficiency by looping over included just once
                // Fetch data (experience, captain, boat) from service
                // Experience
                ctrl.loadExperienceData = function () {
                    requestService.getSingleAsync('experiences', ctrl.id, {editing: true}).then(
                        function (response) {
                            // Set experience
                            ctrl.experience = response.data.data.attributes;
                            ctrl.experienceRelationships = response.data.data.relationships;
                            ctrl.experienceIncluded = response.data.included;

                            // Set disabled for inputs
                            ctrl.canEdit = ctrl.experience.status == 'pending' || ctrl.experience.status == 'accepted' || ctrl.experience.status == null;
                            ctrl.slugEditDisabled = ctrl.disabled || ctrl.experience.status == null;

                            // Set foods checkboxes
                            ctrl.foods.forEach(function (food) {
                                if (ctrl.experience[food.name]) {
                                    food.checked = true;
                                }
                            });

                            // Set deposit checkbox
                            ctrl.deposit = ctrl.experience.deposit > 0;

                            // Get SEO
                            requestService.getRelatedSingle(
                                function (data) {
                                    if (data) {
                                        ctrl.experience.slug_url = data.data.attributes.slug_url;
                                        ctrl.experience.seo_description = data.data.attributes.description;
                                        ctrl.experience.seo_title = data.data.attributes.title;
                                    }
                                },
                                'experiences', ctrl.id, 'seo'
                            );

                            // Set map and marker position
                            ctrl.departure_coordinates.lat = ctrl.experience.departure_lat || 0;
                            ctrl.departure_coordinates.lng = ctrl.experience.departure_lng || 0;
                            ctrl.destination_coordinates.lat = ctrl.experience.destination_lat || 0;
                            ctrl.destination_coordinates.lng = ctrl.experience.destination_lng || 0;

                            // Set dates: yy-mm-dd are not meaningful but required to parse date (we would only need time)
                            if (ctrl.experience.departure_time) ctrl.departure_time = new Date('1970-01-01 ' + ctrl.experience.departure_time);
                            if (ctrl.experience.arrival_time) ctrl.arrival_time = new Date('1970-01-01 ' + ctrl.experience.arrival_time);

                            // Set days
                            ctrl.days = [];
                            ctrl.days_list.forEach(function (day) {
                                ctrl.experience[day.name] ? ctrl.days.push(ctrl.days_list.filter(function (element) {
                                    return element.name == day.name;
                                })[0]) : null;
                            });
                            // Adds or remove day from list
                            ctrl.toggleDay = function (day, adding = true) {
                                if (!day) return;
                                var isAlreadySelected = ctrl.days.filter(function (element) {
                                    return element.name == day.name;
                                })[0];
                                // Remove
                                if (!adding) {
                                    ctrl.days = ctrl.days.filter(function (element) {
                                        return element.name != day.name;
                                    });
                                    ctrl.experience[day.name] = false;
                                }
                                // Add
                                else {
                                    if (isAlreadySelected) return;

                                    ctrl.days.push(day);
                                    ctrl.experience[day.name] = true;
                                }

                                ctrl.patchExperience();
                            };

                            // Fixed rules
                            staticDataService.getFixedRuleList(function (fixed_rules) {
                                ctrl.fixed_rules_list = fixed_rules;
                                if (ctrl.experienceRelationships) {
                                    // Set 'selected' flag on rules
                                    if (ctrl.experienceRelationships.rules) {
                                        ctrl.experienceRelationships.rules.data.forEach(function (rule) {
                                            ctrl.fixed_rules_list.filter(function (element) {
                                                return element.id == rule.id;
                                            })[0].checked = true;
                                        });
                                    }
                                }
                            });
                            // Set kids and babies
                            ctrl.kids = {
                                checked: ctrl.experience.kids > 0
                            };
                            ctrl.babies = {
                                checked: ctrl.experience.babies > 0
                            };

                            // Fixed additional services
                            staticDataService.getFixedAdditionalServiceList(function (fixed_additional_services) {
                                // We need this to track old and new services, because we can't POST all services.
                                // Instead, we need to POST new services and PATCH the old ones
                                ctrl.experienceFixedAdditionalServices = [];

                                ctrl.allFixedAdditionalServices = fixed_additional_services;
                                if (ctrl.experienceIncluded) {
                                    // Set 'selected' flag on services
                                    ctrl.experienceIncluded.forEach(function (included) {
                                        var service = ctrl.allFixedAdditionalServices.filter(function (element) {
                                            return included.type == 'fixed-additional-service-prices'
                                                && element.id == included.relationships.fixed_additional_service.id;
                                        })[0];
                                        if (service) {
                                            service.attributes.price_id = included.id;
                                            service.checked = true;
                                            service.attributes.currency = included.attributes.currency;
                                            service.attributes.price = included.attributes.price;
                                            service.attributes.per_person = included.attributes.per_person;

                                            ctrl.experienceFixedAdditionalServices.push(service);
                                        }
                                    });
                                }
                            });

                            // Additional services
                            ctrl.editedAdditionalServices = [];
                            if (ctrl.experienceIncluded) {
                                ctrl.editedAdditionalServices = ctrl.experienceIncluded.filter(function (element) {
                                    return element.type == 'additional-services';
                                });
                                // Avoid copying values by reference
                                ctrl.originalAdditionalServices = JSON.parse(JSON.stringify(ctrl.editedAdditionalServices));
                            }

                            // Availabilities
                            ctrl.editedAvailabilities = [];
                            if (ctrl.experienceIncluded) {
                                ctrl.editedAvailabilities = ctrl.experienceIncluded.filter(function (element) {
                                    if (element.type == 'experience-availabilities') {
                                        // Convert strings to date format
                                        element.attributes.date_start = new Date(element.attributes.date_start);
                                        element.attributes.date_end = new Date(element.attributes.date_end);
                                        return true;
                                    }
                                });
                                // Avoid copying values by reference
                                ctrl.originalAvailabilities = JSON.parse(JSON.stringify(ctrl.editedAvailabilities));
                            }

                            // Periods
                            ctrl.periods = [];
                            ctrl.deletedPeriods = [];
                            if (ctrl.experienceIncluded) {
                                ctrl.periods = ctrl.experienceIncluded.filter(function (element) {
                                    return element.type == 'periods';
                                });
                            }

                            // Prices
                            ctrl.periods.forEach(function (period) {
                                // Transform dates in Date format
                                if (period.attributes.date_start)
                                    period.attributes.date_start = new Date(period.attributes.date_start);
                                if (period.attributes.date_start)
                                    period.attributes.date_end = new Date(period.attributes.date_end);

                                // Fetch prices
                                requestService.getRelatedListEditing(function (data) {
                                    period.included = data.data;
                                    period.prices = [];
                                    // Set single price object
                                    period.single_person_price = {attributes: {person: 1}};
                                    var single_person_price = period.included.filter(function (price) {
                                        return price.attributes.person == 1;
                                    })[0];
                                    if (single_person_price) {
                                        period.single_person_price.id = single_person_price.id;
                                        period.single_person_price.attributes.price = single_person_price.attributes.price;
                                    }

                                    // Delete single price from prices array
                                    period.included.forEach(function (price) {
                                        if (price.attributes.person != 1) {
                                            period.prices.push(price)
                                        }
                                    });
                                    delete period.included;

                                    // Track deleted prices
                                    period.deletedPrices = [];
                                }, 'periods', period.id, 'prices');
                            });

                            // Create empty period for fixed price, or use the first with date_start = null
                            ctrl.setFixedPeriod(ctrl.experience.is_fixed_price);

                            // Photos
                            ctrl.experience.photos = [];
                            requestService.getRelatedListEditing(
                                function (data) {
                                    data.data.forEach(
                                        function (photo) {
                                            ctrl.experience.photos.push(photo);
                                        }
                                    )
                                },
                                'experiences', ctrl.id, 'photos'
                            );
                            // Track updates
                            ctrl.updatedPhotos = [];

                            // Watch field changes
                            $scope.$watchGroup(
                                [
                                    function () {
                                        return ctrl.departure_time;
                                    },
                                    function () {
                                        return ctrl.arrival_time;
                                    }
                                ],
                                function (newCollection, oldCollection) {
                                    var toBePatched = false;
                                    // Dates
                                    if (newCollection[0] > oldCollection[0] ||
                                        newCollection[0] < oldCollection[0]) toBePatched = true;
                                    if (newCollection[1] > oldCollection[1] ||
                                        newCollection[1] < oldCollection[1]) toBePatched = true;

                                    if (toBePatched) ctrl.patchExperience();
                                }
                            );

                            // Experience has loaded
                            ctrl.loading = false;

                            ctrl.experienceLoaded.resolve();
                        },
                        null
                    );
                };

                // Trigger map load when DOM has been created
                ctrl.triggerMapLoad = function () {
                    $q.all([ctrl.experienceLoaded.promise, mapsService.mapsInitialized]).then(
                        function () {
                            ctrl.loadMaps();
                        },
                        null
                    )
                };

                // Boat
                requestService.getRelatedSingle(function (data) {
                    ctrl.boat = data.data.attributes;
                    ctrl.boat.id = data.data.id;

                    // Captain
                    requestService.getRelatedSingle(function (data) {
                        ctrl.captain = data.data.attributes;
                    }, 'boats', ctrl.boat.id, 'captain');

                }, 'experiences', ctrl.id, 'boat');

                // Go to step
                ctrl.step = function (step) {
                    $location.search('step', step);
                };

                // Date change function
                ctrl.changeDate = function (dateToChange) {
                    if (typeof dateToChange.getMonth !== 'function') return;
                    return dateToChange.toLocaleTimeString('en-US',
                        {
                            hour12: false,
                            hour: "numeric",
                            minute: "numeric",
                            second: "numeric"
                        });
                };

                // Inject MAP script into DOM
                ctrl.loadMaps = function () {
                    // DEPARTURE MAP
                    ctrl.departure_map = new google.maps.Map(document.getElementById('departure_map'), {
                        center: ctrl.departure_coordinates,
                        zoom: 8
                    });
                    ctrl.departure_marker = new google.maps.Marker({
                        position: ctrl.departure_coordinates,
                        draggable: !ctrl.disabled,
                        map: ctrl.departure_map
                    });

                    // Make map respond to click event: move the marker
                    ctrl.departure_map.addListener('dblclick', function (event) {
                        if (!ctrl.disabled) {
                            ctrl.departure_marker.setPosition(event.latLng);
                            window.setTimeout(function () {
                                ctrl.departure_map.panTo(event.latLng);
                            }, 100);

                            // Set new boat coordinates
                            ctrl.departure_coordinates.lat = event.latLng.lat();
                            ctrl.departure_coordinates.lng = event.latLng.lng();
                            ctrl.experience.departure_lat = event.latLng.lat();
                            ctrl.experience.departure_lng = event.latLng.lng();

                            ctrl.patchExperience();
                        }
                    });
                    // Save new coordinates on marker drag
                    ctrl.departure_marker.addListener('dragend', function (event) {
                        // Set new boat coordinates
                        ctrl.departure_coordinates.lat = event.latLng.lat();
                        ctrl.departure_coordinates.lng = event.latLng.lng();
                        ctrl.experience.departure_lat = event.latLng.lat();
                        ctrl.experience.departure_lng = event.latLng.lng();

                        ctrl.patchExperience();
                    });

                    // ARRIVAL MAP
                    ctrl.arrival_map = new google.maps.Map(document.getElementById('arrival_map'), {
                        center: ctrl.destination_coordinates,
                        zoom: 8
                    });
                    ctrl.arrival_marker = new google.maps.Marker({
                        position: ctrl.destination_coordinates,
                        draggable: !ctrl.disabled,
                        map: ctrl.arrival_map
                    });

                    // Make map respond to click event: move the marker
                    ctrl.arrival_map.addListener('dblclick', function (event) {
                        if (!ctrl.disabled) {
                            ctrl.arrival_marker.setPosition(event.latLng);
                            window.setTimeout(function () {
                                ctrl.arrival_map.panTo(event.latLng);
                            }, 100);

                            // Set new boat coordinates
                            ctrl.destination_coordinates.lat = event.latLng.lat();
                            ctrl.destination_coordinates.lng = event.latLng.lng();
                            ctrl.experience.destination_lat = event.latLng.lat();
                            ctrl.experience.destination_lng = event.latLng.lng();

                            ctrl.patchExperience();
                        }
                    });
                    // Save new coordinates on marker drag
                    ctrl.arrival_marker.addListener('dragend', function (event) {
                        // Set new boat coordinates
                        ctrl.destination_coordinates.lat = event.latLng.lat();
                        ctrl.destination_coordinates.lng = event.latLng.lng();
                        ctrl.experience.destination_lat = event.latLng.lat();
                        ctrl.experience.destination_lng = event.latLng.lng();

                        ctrl.patchExperience();
                    });

                    ctrl.departure_marker.setPosition(ctrl.departure_coordinates);
                    window.setTimeout(function () {
                        ctrl.departure_map.panTo(ctrl.departure_coordinates);
                    }, 100);
                    ctrl.arrival_marker.setPosition(ctrl.destination_coordinates);
                    window.setTimeout(function () {
                        ctrl.arrival_map.panTo(ctrl.destination_coordinates);
                    }, 100);

                    // Fix not loading map issue
                    setTimeout(
                        function () {
                            google.maps.event.trigger(ctrl.departure_map, 'resize');
                            ctrl.departure_map.setCenter(ctrl.departure_coordinates);

                        }, 100);
                    setTimeout(
                        function () {
                            google.maps.event.trigger(ctrl.arrival_map, 'resize');
                            ctrl.arrival_map.setCenter(ctrl.destination_coordinates);
                        }, 100);
                };

                // PERIODS AND PRICES

                // Hide period according to is_fixed_price attributes
                ctrl.periodsFilter = function (value) {
                    if (ctrl.experience.is_fixed_price) {
                        return value.attributes.date_start == null;
                    }

                    return true;
                };
                // Add new price to period
                ctrl.addPrice = function (period, price) {
                    if (!price.attributes) {
                        return;
                    }
                    if (!price.attributes.hasOwnProperty('person') && !price.attributes.hasOwnProperty('price')) {
                        return;
                    }
                    if ((isNaN(parseFloat(price.attributes.person)) || !isFinite(price.attributes.person)) ||
                        (isNaN(parseFloat(price.attributes.price)) || !isFinite(price.attributes.price))) {
                        return;
                    }

                    if (!period.prices) period.prices = [];

                    // Remove price with same person if present
                    var prices = period.prices.filter(function (oldPrice) {
                        return oldPrice.attributes.person == price.attributes.person;
                    });
                    if (prices.length > 0) var priceForSameNumberOfPeople = prices[0];

                    // Remove old price first
                    if (priceForSameNumberOfPeople) {
                        ctrl.loading = true;

                        requestService.deletePrice(priceForSameNumberOfPeople.id).then(
                            function () {
                                // Delete price from view
                                period.prices = period.prices.filter(function (oldPrice) {
                                    return oldPrice.attributes.person != price.attributes.person;
                                });

                                // Post new price
                                requestService.postPrice(period.id, price).then(
                                    function (response) {
                                        // Add price to view
                                        period.prices.push({
                                            id: response.data.data.id,
                                            attributes: {person: price.attributes.person, price: price.attributes.price}
                                        });

                                        ctrl.loading = false;
                                    },
                                    function (response) {
                                        ctrl.errorCallback(response);
                                        ctrl.loading = false;
                                    }
                                );
                            },
                            function (response) {
                                ctrl.errorCallback(response);
                                ctrl.loading = false;
                            }
                        );

                    }
                    // Just add new price
                    else {
                        ctrl.loading = true;

                        // Post new price
                        requestService.postPrice(period.id, price).then(
                            function (response) {
                                // Add price to view
                                period.prices.push({
                                    id: response.data.data.id,
                                    attributes: {person: price.attributes.person, price: price.attributes.price}
                                });

                                ctrl.loading = false;
                            },
                            function (response) {
                                ctrl.errorCallback(response);
                                ctrl.loading = false;
                            }
                        );
                    }
                };
                // Remove price from period
                ctrl.removePrice = function (period, oldPrice) {

                    ctrl.loading = true;

                    requestService.deletePrice(oldPrice.id).then(
                        function () {
                            // Delete price from view
                            period.prices = period.prices.filter(function (price) {
                                return price.attributes.person != oldPrice.attributes.person;
                            });

                            ctrl.loading = false;
                        },
                        function (response) {
                            ctrl.errorCallback(response);
                            ctrl.loading = false;
                        }
                    );
                };
                // Add new period to periods
                ctrl.addPeriod = function (period) {
                    if (!period.attributes) return;
                    if (!period.attributes.date_start || !period.attributes.date_end) return;

                    ctrl.loading = true;

                    // Set default attributes
                    // Every period needs to have a price for one person
                    period.attributes.min_person = 1;
                    period.attributes.price = 1;
                    period.attributes.entire_boat = true;
                    period.single_person_price = {attributes: {person: 1}};

                    // Post period
                    requestService.postPeriod(ctrl.id, period).then(
                        function (response) {
                            period.id = response.data.data.id;
                            ctrl.loading = false;
                        },
                        function (response) {
                            ctrl.loading = false;
                            ctrl.errorCallback(response);
                        }
                    );

                    ctrl.periods.push(period);
                };
                // Remove period from periods
                ctrl.removePeriod = function (period) {
                    if (period) {
                        ctrl.loading = true;

                        if (ctrl.periods.length == 1) {
                            ctrl.errorCallback(
                                {
                                    status: 'NOT ALLOWED',
                                    config: {url: 'DELETING PERIOD'},
                                    data: {
                                        errors: [{
                                            code: 'Period',
                                            detail: 'You cannot delete the last period',
                                            title: 'Period'
                                        }]
                                    }
                                }
                            );

                            return;
                        }

                        requestService.deletePeriod(period.id).then(
                            function (response) {
                                ctrl.loading = false;

                                ctrl.periods = ctrl.periods.filter(function (element) {
                                    return period.id != element.id;
                                });
                            },
                            function (response) {
                                ctrl.errorCallback(response);
                                ctrl.loading = false;
                            }
                        );
                    }
                };
                // Set or remove period for fixed price
                ctrl.setFixedPeriod = function (isFixedPrice) {
                    if (isFixedPrice) {
                        ctrl.loading = true;

                        var fixedPeriod;
                        var fixedPeriods = ctrl.periods.filter(function (period) {
                            return period.attributes.date_start == null;
                        });
                        if (fixedPeriods.length > 0) {
                            fixedPeriod = fixedPeriods[0];
                            return;
                        }
                        else {
                            fixedPeriod = {
                                attributes: {
                                    price: 1,
                                    min_person: 1,
                                    entire_boat: true,
                                    is_default: false
                                }
                            };
                        }

                        // Get default period if present and copy its attributes
                        var defaultPeriods = ctrl.periods.filter(function (period) {
                            return period.attributes.is_default;
                        });
                        if (defaultPeriods.length > 0) var defaultPeriod = defaultPeriods[0];
                        if (defaultPeriod) {
                            fixedPeriod = angular.copy(defaultPeriod);
                            fixedPeriod.attributes.date_start = null;
                            fixedPeriod.attributes.date_end = null;
                            fixedPeriod.attributes.is_default = false;
                        }

                        fixedPeriod.prices = [];

                        var postPeriod = requestService.postPeriod(ctrl.id, fixedPeriod);
                        postPeriod.then(
                            function (response) {
                                fixedPeriod.id = response.data.data.id;
                                ctrl.periods.push(fixedPeriod);
                                var promises = [];

                                if (defaultPeriod) {
                                    // Add price for one person to prices array
                                    var prices = defaultPeriod.prices;

                                    // Copy prices from default period
                                    prices.forEach(function (price) {
                                        promises.push(
                                            requestService.postPrice(fixedPeriod.id, price).then(
                                                function (response) {
                                                    price.id = response.data.data.id;

                                                    fixedPeriod.prices.push(price);
                                                },
                                                null
                                            )
                                        );
                                    });

                                    // Copy price for one person
                                    promises.push(
                                        requestService.postPrice(fixedPeriod.id, fixedPeriod.single_person_price).then(
                                            function (response) {
                                                fixedPeriod.single_person_price.id = response.data.data.id;
                                            },
                                            null
                                        )
                                    );
                                }

                                $q.all(promises).then(
                                    function (responses) {
                                        ctrl.loading = false;
                                    },
                                    function (responses) {
                                        ctrl.loading = false;

                                        responses.forEach(function (response) {
                                            ctrl.errorCallback(response);
                                        });
                                    }
                                )
                            },
                            null
                        );
                    }
                    else {
                        ctrl.loading = true;
                        // Delete period with null dates
                        var fixedPeriods = ctrl.periods.filter(function (period) {
                            return period.attributes.date_start == null;
                        });
                        if (fixedPeriods.length > 0) var fixedPeriod = fixedPeriods[0];

                        var deferred = $q.defer();
                        // If there's a fixed period, delete id
                        if (fixedPeriod) {
                            requestService.deletePeriod(fixedPeriod.id).then(
                                function () {
                                    ctrl.periods = ctrl.periods.filter(function (period) {
                                        return period.attributes.date_start != null;
                                    });
                                    ctrl.loading = false;
                                    deferred.resolve();
                                },
                                function (response) {
                                    ctrl.loading = false;
                                    ctrl.errorCallback(response)
                                }
                            );
                        } else {
                            deferred.resolve()
                        }
                        deferred.promise.then(
                            function (response) {
                                // If there's not a period, create one with default dates
                                if (ctrl.periods.length == 0) {
                                    var nextMonth = new Date();
                                    nextMonth.setMonth(nextMonth.getMonth() + 1);
                                    var firstPeriod = {
                                        attributes: {
                                            is_default: true,
                                            date_start: new Date(),
                                            date_end: nextMonth
                                        }
                                    };

                                    ctrl.addPeriod(firstPeriod);
                                }
                            },
                            null
                        )
                    }
                };
                // Set default period
                ctrl.setDefaultPeriod = function (period) {
                    ctrl.periods.forEach(function (item) {
                        if (period.id != item.id) {
                            item.attributes.is_default = false;
                        }
                    });
                    ctrl.patchPeriod(period);
                };
                // Patch period
                ctrl.patchPeriod = function (period) {
                    ctrl.loading = true;

                    if (!period.id) {
                        requestService.postPeriod(ctrl.id, period).then(
                            function (response) {
                                period.id = response.data.data.id;
                                ctrl.loading = false;
                            },
                            function (response) {
                                ctrl.loading = false;
                                ctrl.errorCallback(response);
                            }
                        );

                        return;
                    }

                    requestService.patchPeriod(period).then(
                        function () {
                            ctrl.loading = false;
                        },
                        function (response) {
                            ctrl.loading = false;
                            ctrl.errorCallback(response);
                        }
                    );
                };
                // Patch single price
                ctrl.patchPrice = function (price, period = null) {
                    ctrl.loading = true;

                    if (!price.id) {
                        // Post new price
                        requestService.postPrice(period.id, price).then(
                            function (response) {
                                // Add price to view
                                price.id = response.data.data.id;

                                ctrl.loading = false;
                            },
                            function (response) {
                                ctrl.errorCallback(response);
                                ctrl.loading = false;
                            }
                        );
                    } else {
                        // Patch old price
                        requestService.patchPrice(price).then(
                            function (response) {
                                ctrl.loading = false;
                            },
                            function (response) {
                                ctrl.errorCallback(response);
                                ctrl.loading = false;
                            }
                        )
                    }
                };

                // AVAILABILITIES
                // Add new availability to editedAvailabilities
                ctrl.addAvailability = function (availability) {
                    if (!availability.attributes || !availability.attributes.date_start || !availability.attributes.date_end) {
                        return;
                    }
                    ctrl.loading = true;

                    requestService.postAvailability(ctrl.id, availability).then(
                        function (response) {
                            var newAvailability = {
                                id: response.data.data.id,
                                attributes: angular.copy(availability.attributes)
                            };

                            ctrl.editedAvailabilities.push(newAvailability);
                            ctrl.loading = false;

                            availability.attributes.date_start = null;
                            availability.attributes.date_end = null;
                            availability.id = null;
                        },
                        function (response) {
                            ctrl.errorCallback(response);
                            ctrl.loading = false;

                            availability.attributes.date_start = null;
                            availability.attributes.date_end = null;
                            availability.id = null;
                        }
                    );
                };
                // Remove availability from editedAvailabilities
                ctrl.removeAvailability = function (index, id) {
                    ctrl.loading = true;

                    ctrl.editedAvailabilities.splice(index, 1);

                    requestService.deleteAvailabilities([id]).then(
                        function () {
                            ctrl.loading = false;
                        },
                        function (response) {
                            ctrl.errorCallback(response);
                            ctrl.loading = false;
                        }
                    );
                };
                // Patch availability
                ctrl.patchAvailability = function (availability) {
                    ctrl.loading = true;

                    requestService.patchAvailabilities([availability]).then(
                        function () {
                            ctrl.loading = false;
                        },
                        function (response) {
                            ctrl.errorCallback(response);
                            ctrl.loading = false;
                        }
                    );
                };

                // ADDITIONAL SERVICES
                // Add new additional_service to editedAdditionalServices
                ctrl.addAdditionalService = function (additional_service) {
                    if (!additional_service.attributes.name) {
                        return;
                    }

                    var newAdditionalService = angular.copy(additional_service);
                    ctrl.editedAdditionalServices.push(newAdditionalService);

                    ctrl.loading = true;

                    requestService.postAdditionalServices(ctrl.id, [additional_service]).then(
                        function (response) {
                            ctrl.loading = false;

                            newAdditionalService.id = response[0].data.data.id
                        },
                        function (response) {
                            ctrl.errorCallback(response);
                            ctrl.loading = false;
                        }
                    );
                };
                // Remove additional_service from editedAdditionalServices
                ctrl.removeAdditionalService = function (additional_service) {
                    ctrl.editedAdditionalServices = ctrl.editedAdditionalServices.filter(function (element) {
                        return element.attributes.name != additional_service.attributes.name
                    });

                    ctrl.loading = true;
                    requestService.deleteAdditionalServices([additional_service]).then(
                        function () {
                            ctrl.loading = false;
                        },
                        function (response) {
                            ctrl.errorCallback(response);
                            ctrl.loading = false;
                        }
                    );
                };
                // Check service
                ctrl.checkService = function (service, fromLabel) {
                    if (!ctrl.disabled) {
                        // If clicking on the label, just open popover and select checkbox
                        if (fromLabel) {
                            if (service.checked) {
                                return;
                            }
                            else {
                                service.checked = true;

                                service.attributes.per_person = true;
                                service.attributes.price = 0;
                            }
                        }
                        // Clicking on checkbox
                        else {
                            if (service.checked) {
                                // Open popover
                                $timeout(function () {
                                    ctrl.popovers[service.attributes.name] = true;
                                }, 0);

                                service.attributes.per_person = true;
                                service.attributes.price = 0;
                            }
                        }

                        ctrl.patchFixedAdditionalServices();
                    }
                };
                // Patch additional service
                ctrl.patchAdditionalService = function (service) {
                    if (!service.id) return;
                    ctrl.loading = true;

                    requestService.patchAdditionalServices([service]).then(
                        function () {
                            ctrl.loading = false;
                        },
                        function (response) {
                            ctrl.errorCallback(response);
                            ctrl.loading = false;
                        }
                    );
                };


                // Open photo modal
                ctrl.editPhoto = function (index) {
                    if (ctrl.disabled) return;

                    ctrl['photoModal' + index] = true;
                };
                // Set photo link
                ctrl.updatePhoto = function (index, link) {
                    ctrl.loading = true;

                    if (!link) {
                        ctrl.photoErrorModal = true;
                        ctrl.loading = false;
                        return;
                    }

                    requestService.deletePhoto(ctrl.experience.photos[index].id).then(
                        function () {
                            var photo = {attributes: {link: link}};
                            requestService.postPhotos([photo], 'experience', ctrl.id).then(
                                function (response) {
                                    if (!ctrl.experience.photos[index]) ctrl.experience.photos[index] = {attributes: {}};

                                    ctrl.experience.photos[index].id = response[0].data.id;
                                    ctrl.experience.photos[index].attributes.link = response[0].data.attributes.link;
                                    ctrl.updatedPhotos.push(ctrl.experience.photos[index]);

                                    ctrl['photoModal' + index] = false;
                                    ctrl.loading = false;
                                },
                                function (response) {
                                    ctrl.errorCallback(response);
                                    ctrl.loading = false;
                                }
                            );
                        },
                        function (response) {
                            ctrl.errorCallback(response);

                            ctrl.loading = false;
                        }
                    );


                };
                // Add new photo
                ctrl.addPhoto = function (link) {
                    ctrl.loading = true;

                    if (!link) {
                        ctrl.photoErrorModal = true;
                        ctrl.loading = false;
                        return;
                    }

                    var photo = {
                        attributes: {link: link}
                    };
                    requestService.postPhotos([photo], 'experience', ctrl.id).then(
                        function (response) {

                            photo = {
                                id: response[0].data.id,
                                attributes: {link: response[0].data.attributes.link}
                            };

                            ctrl.experience.photos.push(photo);
                            ctrl.updatedPhotos.push(photo);

                            ctrl['photoModal' + (ctrl.experience.photos.length - 1)] = false;
                            ctrl.loading = false;
                        },
                        function (response) {
                            ctrl.errorCallback(response);
                            ctrl.loading = false;
                        }
                    );
                };
                // Delete photo
                ctrl.deletePhoto = function (index) {
                    requestService.deletePhoto(ctrl.experience.photos[index].id);

                    ctrl.experience.photos.splice(index, 1);
                    ctrl['photoModal' + index] = false;
                };
                // Get slug url from Seo title
                ctrl.getSlugUrl = function () {
                    if (ctrl.experience.seo_title) {
                        requestService.getSlugUrl(ctrl.experience.seo_title)
                            .then(
                                function (response) {
                                    ctrl.experience.slug_url = response.data.meta.slug_url;
                                }
                            );
                    }
                };

                // ADMIN ACTIONS
                // Set error callback on actions
                ctrl.errorCallback = function (response) {
                    ctrl.loadExperienceData();
                    errorModalService.open(response);
                };

                // Start editing
                ctrl.startEditing = function () {
                    var experience = angular.copy(ctrl.experience);
                    delete experience.status;
                    delete experience.is_finished;

                    // Prepare attributes
                    if (experience.deposit == 0) delete experience.deposit;

                    requestService.putExperience(ctrl.id, experience).then(
                        function () {
                            ctrl.loadExperienceData();

                            // Enable input editing and hide edit button
                            ctrl.disabled = false;
                            ctrl.canEdit = false;

                            // Reset events and draggable marker
                            ctrl.loadMaps();
                        },
                        function (response) {
                            ctrl.errorCallback(response);
                        }
                    )
                };

                // Change status
                ctrl.changeStatus = function (status, adminText) {
                    var requestBody = {
                        'status': status,
                        'admin_text': adminText
                    };

                    var success = function () {
                        successModalService.open();
                        ctrl.loadExperienceData();
                    };

                    requestService.putExperience(ctrl.id, requestBody).then(
                        success,
                        function (response) {
                            errorModalService.open(response, 'Change status to ' + status);
                        });
                };

                // Set MODALS
                ctrl.toggleSaveModal = function () {
                    // Reset errors
                    ctrl.formError = false;
                    ctrl.part1Error = false;
                    ctrl.part2Error = false;
                    ctrl.part3Error = false;
                    ctrl.part4Error = false;

                    ctrl.saveModal = !ctrl.saveModal;
                };
                ctrl.save = function () {
                    // Manual validate
                    if (!ctrl.manualValidation()) return;

                    // Form validation
                    if (!ctrl.isFormValid(ctrl.step1)) {
                        ctrl.part1Error = true;
                        ctrl.formError = true;
                    }
                    if (!ctrl.isFormValid(ctrl.step2)) {
                        ctrl.part2Error = true;
                        ctrl.formError = true;
                    }
                    if (!ctrl.isFormValid(ctrl.step3)) {
                        ctrl.part3Error = true;
                        ctrl.formError = true;
                    }
                    if (!ctrl.isFormValid(ctrl.step4)) {
                        ctrl.part4Error = true;
                        ctrl.formError = true;
                    }
                    if (ctrl.formError) return;

                    ctrl.loading = true;

                    delete ctrl.experience.status;
                    // If experience is still not completed, save it
                    if (!ctrl.experience.is_finished) {
                        ctrl.experience.is_finished = true;
                    }
                    // If experience is already completed, just patch it
                    else {
                        delete ctrl.experience.is_finished;
                    }

                    // Patch experience
                    ctrl.patchExperience(ctrl.experience.is_finished).then(
                        function () {
                            ctrl.loadExperienceData();
                            // Close accept modal
                            ctrl.toggleSaveModal();
                            if (ctrl.canEdit) {
                                // Open Seo Modal
                                ctrl.toggleSeoModal();
                            }
                            // Just accept
                            else {
                                ctrl.changeStatus('accepted');
                            }
                        },
                        null
                    )
                };
                ctrl.saveSeoAndAccept = function () {
                    ctrl.toggleSeoModal();

                    // Save seo
                    ctrl.postSeo().then(
                        function () {
                            if (!ctrl.manualValidation()) return;
                            ctrl.changeStatus('accepted');
                        },
                        function (response) {
                            ctrl.loading = false;
                            ctrl.errorCallback(response)
                        }
                    );
                };
                ctrl.toggleSeoModal = function () {
                    ctrl.seoModal = !ctrl.seoModal;
                };
                ctrl.reject = function () {
                    var modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: '/shared/components/modal/do-action/views/reject-modal.view.html',
                        controller: 'doActionModalController',
                        controllerAs: 'ctrl',
                        resolve: {
                            items: function () {
                                return {
                                    modelType: 'EXPERIENCE'
                                };
                            }
                        }
                    });
                    modalInstance.result.then(function (data) {
                            if (data.actionDone) {
                                // Change experience status to 'rejected'
                                ctrl.changeStatus('rejected', data.text);
                            }
                        },
                        function (response) {
                            ctrl.errorCallback(response);
                        });
                };
                ctrl.suspend = function () {
                    var modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: '/shared/components/modal/do-action/views/suspend-modal.view.html',
                        controller: 'doActionModalController',
                        controllerAs: 'ctrl',
                        resolve: {
                            items: function () {
                                return {
                                    modelType: 'EXPERIENCE'
                                };
                            }
                        }
                    });
                    modalInstance.result.then(function (data) {
                        if (data.actionDone) {
                            if (!ctrl.isFormValid(ctrl.step1) || !ctrl.isFormValid(ctrl.step2) || !ctrl.isFormValid(ctrl.step3) || !ctrl.isFormValid(ctrl.step4)) {
                                validationModalService.open();
                                return;
                            }
                            // Status is set to null because backend expects status to be present only when changing experience status
                            delete ctrl.experience.status;
                            // Send PUT to save experience data, then send PUT to change status. Wait for data save before the second PUT
                            ctrl.put().then(
                                function () {
                                    // Change experience status to 'accepted'
                                    ctrl.changeStatus('accepted_conditionally', data.text);
                                },
                                function (response) {
                                    ctrl.errorCallback(response);
                                }
                            );
                        }
                    });
                };
                ctrl.suspendStep = function (step) {
                    var modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: '/shared/components/modal/do-action/views/suspend-modal.view.html',
                        controller: 'doActionModalController',
                        controllerAs: 'ctrl',
                        resolve: {
                            items: function () {
                                return {
                                    modelType: 'EXPERIENCE'
                                };
                            }
                        }
                    });
                    modalInstance.result.then(function (data) {
                        if (data.actionDone) {
                            requestService.postStep(ctrl.id, step, data.text).then(
                                function () {
                                    successModalService.open();
                                },
                                function (response) {
                                    errorModalService.open(response);
                                }
                            );
                        }
                    });
                };
                ctrl.freeze = function () {
                    var modalInstance = $uibModal.open({
                        animation: true,
                        templateUrl: '/shared/components/modal/do-action/views/freeze-modal.view.html',
                        controller: 'doActionModalController',
                        controllerAs: 'ctrl',
                        resolve: {
                            items: function () {
                                return {
                                    modelType: 'EXPERIENCE'
                                };
                            }
                        }
                    });
                    modalInstance.result.then(
                        function (data) {
                            if (data.actionDone) {
                                // Change experience status to 'accepted_conditionally'
                                ctrl.changeStatus('freezed', data.text);
                            }
                        },
                        function (response) {
                            ctrl.errorCallback(response);
                        }
                    );
                };

                // API CALLS
                ctrl.patchFixedRules = function () {
                    ctrl.loading = true;
                    // Fixed rules
                    var fixedRules = ctrl.fixed_rules_list.filter(function (rule) {
                        return rule.checked;
                    });
                    requestService.patchFixedRules(ctrl.id, fixedRules).then(
                        function () {
                            ctrl.loading = false;
                        },
                        function (response) {
                            ctrl.errorCallback(response);
                            ctrl.loading = false;
                        }
                    );
                };
                ctrl.patchFixedAdditionalServices = function () {
                    ctrl.loading = true;
                    // Services that were already present: do a PATCH
                    var additionalServicesToPatch = ctrl.allFixedAdditionalServices.filter(function (service) {
                        return service.checked;
                    });
                    requestService.patchFixedAdditionalServices(ctrl.id, additionalServicesToPatch).then(
                        function () {
                            ctrl.loading = false;
                        },
                        function (response) {
                            ctrl.errorCallback(response);
                            ctrl.loading = false;
                        }
                    );
                };
                ctrl.patchAdditionalServices = function () {
                    ctrl.loading = true;

                    requestService.patchAdditionalServices(ctrl.editedAdditionalServices).then(
                        function () {
                            ctrl.loading = false;
                        },
                        function (response) {
                            ctrl.errorCallback(response);
                            ctrl.loading = false;
                        }
                    );
                };
                ctrl.postSeo = function () {
                    return requestService.postSeo(ctrl.id, {
                        description: ctrl.experience.seo_description,
                        title: ctrl.experience.seo_title
                    });
                };
                ctrl.postPhotos = function () {
                    var promises = [];
                    if (ctrl.updatedPhotos) {
                        promises.push(requestService.deleteCollection(ctrl.updatedPhotos, 'photos').then(
                            function () {
                                requestService.postPhotos(ctrl.updatedPhotos, 'experience', ctrl.id);
                            }
                        ));
                    }

                    return $q.all(promises);
                };
                ctrl.patchExperience = function (isFinished) {
                    ctrl.loading = true;

                    var deferred = $q.defer();

                    var experience = angular.copy(ctrl.experience);

                    // Prepare attributes
                    if (experience.deposit == 0) experience.deposit = null;
                    delete experience.is_finished;
                    if (isFinished) experience.is_finished = true;
                    delete experience.status;

                    // Convert Date object into string times (e.g. 19:00:00)
                    experience.departure_time = ctrl.departure_time.toLocaleTimeString('en-US', {hour12: false});
                    experience.arrival_time = ctrl.arrival_time.toLocaleTimeString('en-US', {hour12: false});

                    requestService.putExperience(ctrl.id, experience).then(
                        function (response) {
                            deferred.resolve();
                            ctrl.loading = false;
                        },
                        function (response) {
                            deferred.reject();
                            ctrl.errorCallback(response);
                            ctrl.loading = false;
                        }
                    )

                    return deferred.promise;
                };

                // Check if form can be submitted
                ctrl.isFormValid = function (form) {
                    return form.$valid;
                };

                // Perform manual validation on some objects
                ctrl.manualValidation = function () {
                    // Not enough photos
                    if (ctrl.experience.photos.length < 2) {
                        ctrl.errorCallback(
                            {
                                status: 'NOT ALLOWED',
                                config: {url: 'SAVING EXPERIENCE'},
                                data: {
                                    errors: [{
                                        code: 'Photos',
                                        detail: 'There need to be at least 2 photos',
                                        title: 'Photos'
                                    }]
                                }
                            }
                        );

                        return false;
                    }


                    if (ctrl.periods && !ctrl.experience.is_fixed_price) {
                        var defaultPeriods = ctrl.periods.filter(function (period) {
                            return period.attributes.is_default
                        });
                        if (!defaultPeriods[0]) {
                            // You can't save, no default period
                            ctrl.errorCallback(
                                {
                                    status: 'NOT ALLOWED',
                                    config: {url: 'SAVING EXPERIENCE'},
                                    data: {
                                        errors: [{
                                            code: 'Period',
                                            detail: 'There is no default period',
                                            title: 'Period'
                                        }]
                                    }
                                }
                            )

                            return false;
                        }
                    }

                    return true;
                };

                // Load everything
                ctrl.loadExperienceData();
            }]
    );