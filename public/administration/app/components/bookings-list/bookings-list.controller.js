angular
    .module('administration')
    .controller('bookingsListController',[
        'statusService', '$scope',
        function (statusService, $scope) {
        // Make context accessible inside other scopes
        var ctrl = this;
        ctrl.loading = true;

        // Fetch data from service
        requestService.getList(function (data, totalItems) {
            ctrl.loading = false;
            ctrl.totalItems = totalItems;
            ctrl.data = data;
        }, 'bookings');

        // Set table properties
        this.columns = [
            {name: 'id', heading: 'ID'},
            {name: 'date', heading: 'Data'},
            {name: 'experience', heading: 'Esperienza'},
            {name: 'captain', heading: 'Capitano'},
            {name: 'status', heading: 'Stato'},
            {name: 'actions', heading: 'Azioni'}
        ];

        // Set top icons
        this.icons = [
            {type: 'search', template: 'search-booking'}
        ];

        this.controlIconsColor = 'navy-blue';

        this.getStatusName = function (status) {
            return statusService.getBookingStatusName(status);
        };

        // Search booking
        $scope.searchString = '';
        $scope.searchBooking = function (text) {
            console.log('Text: ' + text)
        };

        // Pagination
        this.currentPage = 1;
        this.perPage = 6;
        this.pagesToDisplay = 5;
        this.boundaryLinks = true;
        this.directionLinks = false;

        // Request new data
        this.pageChanged = function () {
            requestService.getList(function (data) {
                ctrl.data = data;
            }, 'bookings', this.currentPage);
        }
    }]);