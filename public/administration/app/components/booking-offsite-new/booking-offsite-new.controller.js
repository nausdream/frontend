/**
 * Created by lucas on 10/07/2017.
 */
angular.module('administration').controller('bookingOffsiteNewController', [
    'fixedListService', 'requestService', '$translatePartialLoader', '$translate', '$location', 'dateService',
    'errorModalService', 'validationService',
    function (fixedListService, requestService, $translatePartialLoader, $translate, $location, dateService,
              errorModalService, validationService) {
        // Make context accessible inside other scopes
        var ctrl = this;

        // Load translations
        $translatePartialLoader.addPart('boat_types');
        $translatePartialLoader.addPart('languages');
        $translatePartialLoader.addPart('currencies');
        $translate.refresh();

        // Field rules
        ctrl.fields = {
            'first_name_guest': {
                required: true,
                max: 150
            },
            'last_name_guest': {
                required: false,
                max: 150
            },
            'address_guest': {
                required: false,
                max: 150
            },
            'city_guest': {
                required: false,
                max: 150
            },
            'phone_guest': {
                required: true,
                max: 150
            },
            'email_guest': {
                required: true,
                max: 150
            },
            'vat_guest': {
                required: false,
                max: 150
            },
            'experience_date': {
                optional: false,
                min: new Date()
            },
            'arrival_date': {
                optional: false,
                min: new Date()
            },
            'experience_title': {
                required: true,
                max: 300
            },
            'experience_description': {
                required: true,
                max: 65535
            },
            'boat_type': {
                required: true
            },
            'departure_port': {
                required: true,
                max: 100
            },
            'arrival_port': {
                required: true,
                max: 100
            },
            'departure_time': {
                required: true,
                max: 150
            },
            'arrival_time': {
                required: true,
                max: 150
            },
            'adults': {
                required: true,
                max: 150
            },
            'kids': {
                required: true,
                max: 150
            },
            'babies': {
                required: true,
                max: 150
            },
            'included_services': {
                required: false,
                max: 150
            },
            'excluded_services': {
                required: false,
                max: 150
            },
            'drinks': {
                required: false,
                max: 150
            },
            'dishes': {
                required: false,
                max: 150
            },
            'price': {
                required: true,
                min: 10,
                max: (Math.pow(2, 31) - 1)
            },
            'fee': {
                required: true
            },
            'expiration_date': {
                required: true,
                min: new Date()
            },
            'language_guest': {
                required: true,
                max: 150
            },
            'currency_guest': {
                required: true,
                max: 150
            },
            'language_captain': {
                required: true,
                max: 150
            },
            'currency_captain': {
                required: true,
                max: 150
            },
            'first_name_captain': {
                required: true,
                max: 150
            },
            'last_name_captain': {
                required: true,
                max: 150
            },
            'email_captain': {
                required: true,
                max: 150
            },
            'phone_captain': {
                required: true,
                max: 150
            }
        };

        // Field values
        ctrl.booking = {
            'first_name_guest': null,
            'last_name_guest': null,
            'address_guest': null,
            'city_guest': null,
            'phone_guest': null,
            'email_guest': null,
            'vat_guest': null,
            'experience_date': new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
            'arrival_date': new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
            'experience_title': null,
            'experience_description': null,
            'boat_type': 'sailboat',
            'departure_port': null,
            'arrival_port': null,
            'departure_time': new Date(),
            'arrival_time': new Date(),
            'adults': 2,
            'kids': 0,
            'babies': 0,
            'included_services': null,
            'excluded_services': null,
            'drinks': null,
            'dishes': null,
            'price': 1,
            'fee': 1,
            'expiration_date': new Date(),
            'language_guest': 'en',
            'currency_guest': 'USD',
            'language_captain': 'en',
            'currency_captain': 'USD',
            'first_name_captain': null,
            'last_name_captain': null,
            'email_captain': null,
            'phone_captain': null
        };

        // Boat types
        fixedListService.getFixedList('boat-types').then(
            function (response) {
                ctrl.boat_types = response.data.data;
            },
            function (response) {

            }
        );

        // Languages
        fixedListService.getFixedList('languages').then(
            function (response) {
                ctrl.languages = response.data.data;
            },
            function (response) {

            }
        );

        // Currencies
        fixedListService.getFixedList('currencies').then(
            function (response) {
                ctrl.currencies = response.data.data;
            },
            function (response) {

            }
        );

        // Set departure date and arrival date to null
        ctrl.clearDates = function () {
            if (!ctrl.no_dates) return;

            ctrl.fields.experience_date.optional = true;
            ctrl.booking.experience_date = null;

            ctrl.fields.arrival_date.optional = true;
            ctrl.booking.arrival_date = null;
        };

        // Post
        ctrl.post = function () {
            // Reset previous errors
            ctrl.errorMissingDate = false;
            ctrl.errorArrivalDate = false;

            ctrl.loading = true;

            if (!ctrl.form.$valid) {
                validationService.setFrontendErrors(ctrl.form, {});
                return;
            }

            let attributes = angular.copy(ctrl.booking);

            // Transform attributes to meet Backend specifications
            attributes.departure_time = attributes.departure_time.toLocaleTimeString('en-US', {hour12: false});
            attributes.arrival_time = attributes.arrival_time.toLocaleTimeString('en-US', {hour12: false});

            // Check that departure_date and arrival_date are set when "without date" is not checked
            if (!ctrl.no_dates && (!attributes.arrival_date || !attributes.experience_date)) {
                ctrl.errorModal = true;
                ctrl.errorMissingDate = true;
                return;
            }

            // Validate arrival date and departure date
            if (attributes.arrival_date < attributes.experience_date) {
                ctrl.errorModal = true;
                ctrl.errorArrivalDate = true;
                return;
            }

            attributes.experience_date = dateService.getDateString(attributes.experience_date);
            attributes.arrival_date = dateService.getDateString(attributes.arrival_date);
            attributes.expiration_date = dateService.getDateString(attributes.expiration_date);

            requestService.postSingle('offsite-bookings', attributes).then(
                function (response) {
                    if (response.data.data) {
                        ctrl.newBookingId = response.data.data.id;
                    }
                    ctrl.loading = false;
                    ctrl.successModal = true;
                },
                function (error) {
                    // If not a backend error, create error manually
                    if (!error.data || !error.data.errors) {
                        error = {
                            status: error.status,
                            config: {url: 'SAVING NEW OFFSITE BOOKING'},
                            data: {
                                errors: [{code: null, detail: 'There was some error', title: 'New booking'}]
                            }
                        };
                    }

                    ctrl.loading = false;
                    errorModalService.open(error)
                }
            )
        };

        // Go to route
        ctrl.goTo = function (path) {
            $location.path(path);
        }
    }]);