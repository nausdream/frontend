angular.module('administration').controller('conversationPageController', [
        '$routeParams', '$location', 'requestService',
        function ($routeParams, $location, requestService) {
            var ctrl = this;

            // When user clicks on back button in a conversation, changes route
            ctrl.conversationBackButton = function () {
                ctrl.loading = true;

                $location.path('/conversations');
            };

            ctrl.conversationPromise = requestService.getSingleAsync('conversations', $routeParams.id);
        }
    ]
);