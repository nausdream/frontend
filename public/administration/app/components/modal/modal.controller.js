angular
    .module('administration')
    .controller('modalController', ['modalService', function (modalService) {
        this.$onInit = function () {
            var ctrl = this;

            modalService.toggle();
        };

        this.$onDestroy = function () {
            modalService.toggle(false);
        }
    }]);