angular.module('administration').component('modal', {
    templateUrl: '/administration/app/components/modal/modal.view.html',
    controller: 'modalController',
    controllerAs: 'ctrl',
    transclude: true,
    bindings: {
        modalTitle: '@',
        onClose: '&'
    }
});