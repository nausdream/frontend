angular.module('administration').controller('conversationController', [
    'requestService',
    function (requestService) {
        this.$onInit = function () {
            var ctrl = this;
            ctrl.loading = true;

            // Error objects for validation
            ctrl.errors = {};
            ctrl.backendErrors = {};

            // Add single message to grouped object
            ctrl.addMessage = function (message, date = null) {
                if (date == null) date = new Date(message.attributes.created_at);

                message.day = date.withoutTime();

                // If it's still empty, add first message
                if (ctrl.conversation.grouped.length == 0) {
                    ctrl.conversation.grouped.push({
                        date: message.day,
                        messages: [message]
                    });
                }

                // If not empty, check if a group for the same day already exists
                else {
                    var lastGroup = ctrl.conversation.grouped[ctrl.conversation.grouped.length - 1];
                    if (lastGroup.date.getTime() == message.day.getTime()) {
                        // Day already exists, add the message to the group
                        lastGroup.messages.push(message);
                    }
                    else {
                        // First message with this day, create new group
                        ctrl.conversation.grouped.push({
                            date: message.day,
                            messages: [message]
                        });
                    }
                }
            };

            // Return correct photo based on sender
            ctrl.getMessageSenderPhoto = function (sender) {
                if (sender == 'guest') return ctrl.guest.photo;

                return ctrl.captain.photo;
            };

            // Edit single message
            ctrl.editMessage = function (message) {
                ctrl.loading = true;

                requestService.patchMessage(message.id, message.attributes.message).then(
                    function () {
                        ctrl.loading = false;

                        // Disable editing again
                        message.enabled = false;
                    },
                    function (response) {
                        console.log(response);
                        ctrl.error = response;
                        ctrl.loading = false;
                    }
                )
            };

            ctrl.conversationPromise.then(
                function (response) {
                    ctrl.conversation = response.data.data;
                    var included = response.data.included;
                    // Init messages array grouped by day
                    ctrl.conversation.grouped = [];
                    ctrl.conversation.total_messages = 0;

                    // Set user data
                    ctrl.guest = {
                        name: ctrl.conversation.attributes.first_name_guest + ' ' + ctrl.conversation.attributes.last_name_guest,
                        photo: ctrl.conversation.attributes.profile_image_guest
                    };
                    // Set captain data
                    ctrl.captain = {
                        name: ctrl.conversation.attributes.first_name_captain + ' ' + ctrl.conversation.attributes.last_name_captain,
                        photo: ctrl.conversation.attributes.profile_image_captain
                    };

                    // Fetch and normalize included objects. Offset dates by user timezones and create message object
                    var date;
                    included.forEach(function (includedObject) {
                        switch (includedObject.type) {
                            case 'messages':
                                // Increase total count by 1
                                ctrl.conversation.total_messages++;

                                // Add each message to group object
                                ctrl.addMessage(includedObject);
                        }
                    });

                    ctrl.loading = false;

                },
                function (response) {
                    // TODO MANAGE REJECTION
                }
            );
        };
    }
]);