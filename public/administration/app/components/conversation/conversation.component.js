angular.module('administration').component('conversation', {
    templateUrl: '/administration/app/components/conversation/conversation.view.html',
    controller: 'conversationController',
    controllerAs: 'ctrl',
    bindings: {
        conversationPromise: '<',
        onBackClick: '&',
        onEdit: '&'
    }
});