angular
    .module('administration')
    .controller('boatFormController', [
        'photoService', '$routeParams', '$uibModal', 'staticDataService', 'requestService', '__env', 'validationModalService',
        'errorModalService', 'successModalService', '$q', 'mapsService', '$translatePartialLoader', '$translate',
        function (photoService, $routeParams, $uibModal, staticDataService, requestService, __env, validationModalService,
                  errorModalService, successModalService, $q, mapsService, $translatePartialLoader, $translate) {
            // Make context accessible inside other scopes
            var ctrl = this;

            // Load translations
            $translatePartialLoader.addPart('boat_types');
            $translatePartialLoader.addPart('boat_materials');
            $translatePartialLoader.addPart('accessories');
            $translatePartialLoader.addPart('motor_types');
            $translatePartialLoader.addPart('experience_types');
            $translate.refresh();

            // Set initial view
            ctrl.loading = true;

            // Initialize data
            ctrl.coordinates = {lat: 0, lng: 0};
            ctrl.experience_types = [];
            ctrl.photoTypes = ['external', 'internal', 'sea'];
            // Get ID
            ctrl.id = $routeParams.id;

            // Set disabled (read only form)
            ctrl.disabled = true;

            // Initialize data
            staticDataService.getMaterialList(function (data) {
                ctrl.materials = data;
            });
            staticDataService.getBoatTypeList(function (data) {
                ctrl.boat_types = data;
            });
            staticDataService.getMotorTypeList(function (data) {
                ctrl.motor_types = data;
            });

            // Boat
            ctrl.loadData = function () {
                requestService.getSingleEditing(function (data) {
                    ctrl.boat = data.data.attributes;
                    ctrl.boatRelationships = data.data.relationships;

                    ctrl.loading = false;

                    // Captain
                    requestService.getRelatedSingle(function (data) {
                        ctrl.captain = data.data.attributes;
                        ctrl.captain.id = data.data.id;

                    }, 'boats', ctrl.id, 'captain');

                    // Set disabled for inputs
                    ctrl.disabled = ctrl.boat.status != 'pending';

                    // Set map and marker position
                    ctrl.coordinates.lat = ctrl.boat.lat;
                    ctrl.coordinates.lng = ctrl.boat.lng;

                    // Callback for map loader
                    mapsService.mapsInitialized.then(ctrl.loadMaps);

                    // Set accessories
                    staticDataService.getAccessoryList(function (accessories) {
                        ctrl.accessories_list = accessories;
                        if (ctrl.boatRelationships) {
                            // Set 'selected' flag on accessories
                            if (ctrl.boatRelationships.accessories) {
                                ctrl.boatRelationships.accessories.data.forEach(function (accessory) {
                                    ctrl.accessories_list.filter(function (element) {
                                        return element.id == accessory.id
                                    })[0].checked = true;
                                });
                            }
                        }
                    });

                    staticDataService.getExperienceTypeList(function (data) {
                        ctrl.experience_types_list = data;

                        if (ctrl.boatRelationships) {
                            // Set experience types
                            if (ctrl.boatRelationships.experience_types) {
                                ctrl.boatRelationships.experience_types.data.forEach(function (experienceType) {
                                    ctrl.experience_types.push(ctrl.experience_types_list.filter(function (element) {
                                        return element.id == experienceType.id
                                    })[0]);
                                });
                            }
                        }
                    });


                    // Remove experience type by id
                    ctrl.removeExperienceType = function (id) {
                        ctrl.experience_types = ctrl.experience_types.filter(function (element) {
                            return element.id != id;
                        })
                    };
                    // Add experience type to experiences
                    ctrl.addExperienceType = function (experienceType) {
                        if (!experienceType) {
                            return;
                        }
                        // Check if button has been clicked without selecting an item,
                        // or if experienceType is already present in selected ones
                        var isAlreadySelected = false;
                        if (ctrl.experience_types) {
                            ctrl.experience_types.forEach(function (element) {
                                if (element.id == experienceType.id) isAlreadySelected = true;
                            });
                        }
                        if (!isAlreadySelected) {
                            ctrl.experience_types.push(experienceType);
                        }
                    };

                }, 'boats', ctrl.id);
            };
            ctrl.loadData();

            // Set modals
            ctrl.accept = function () {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: '/shared/components/modal/do-action/views/accept-modal.view.html',
                    controller: 'doActionModalController',
                    controllerAs: 'ctrl',
                    resolve: {
                        items: function () {
                            return {
                                modelType: 'BOAT'
                            };
                        }
                    }
                });
                modalInstance.result.then(function (data) {
                    if (data.actionDone) {
                        if (!ctrl.isFormValid(ctrl.form)) {
                            validationModalService.open();
                            return;
                        }
                        // Status is set to null because backend expects status to be present only when changing boat status
                        delete ctrl.boat.status;
                        // Send PUT to save boat data, then send PUT to change status. Wait for data save before the second PUT
                        ctrl.put().then(
                            function () {
                                // Change boat status to 'accepted'
                                ctrl.changeStatus('accepted');
                            },
                            function (response) {
                                ctrl.errorCallback(response);
                            }
                        );
                    }
                });
            };
            ctrl.reject = function () {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: '/shared/components/modal/do-action/views/reject-modal.view.html',
                    controller: 'doActionModalController',
                    controllerAs: 'ctrl',
                    resolve: {
                        items: function () {
                            return {
                                modelType: 'BOAT'
                            };
                        }
                    }
                });
                modalInstance.result.then(
                    function (data) {
                        if (data.actionDone) {
                            // Change boat status to 'rejected'
                            ctrl.changeStatus('rejected', data.text);
                        }
                    },
                    function (response) {
                        ctrl.errorCallback(response);
                    });
            };
            ctrl.suspend = function () {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: '/shared/components/modal/do-action/views/suspend-modal.view.html',
                    controller: 'doActionModalController',
                    controllerAs: 'ctrl',
                    resolve: {
                        items: function () {
                            return {
                                modelType: 'BOAT'
                            };
                        }
                    }
                });
                modalInstance.result.then(
                    function (data) {
                        if (data.actionDone) {
                            if (!ctrl.isFormValid(ctrl.form)) {
                                validationModalService.open();
                                return;
                            }
                            // Status is set to null because backend expects status to be present only when changing boat status
                            delete ctrl.boat.status;
                            // Send PUT to save boat data, then send PUT to change status. Wait for data save before the second PUT
                            ctrl.put().then(
                                function () {
                                    // Change boat status to 'accepted_conditionally'
                                    ctrl.changeStatus('accepted_conditionally', data.text);
                                },
                                function (response) {
                                    ctrl.errorCallback(response);
                                }
                            );
                        }
                    },
                    function (response) {
                        ctrl.errorCallback(response);
                    });
            };

            // Change photo modal
            ctrl.changePhoto = function (photoType) {
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: '/shared/components/modal/upload-photo/upload-photo-modal.view.html',
                    controller: 'uploadPhotoModalController',
                    controllerAs: 'ctrl',
                    backdrop: 'static'
                });
                modalInstance.result.then(function (photo) {
                    if (photo) {
                        photoType = photoType + '_photo';
                        ctrl.boat[photoType] = photo;
                    }
                });
            };

            // Admin actions
            ctrl.changeStatus = function (status, adminText) {
                var requestBody = {
                    'status': status,
                    'admin_text': adminText
                };

                var success = function () {
                    successModalService.open();
                    ctrl.loadData();
                };

                requestService.putBoat(ctrl.id, requestBody).then(
                    success,
                    function (response) {
                        errorModalService.open(response, 'Change status to ' + status);
                        ctrl.loadData();
                    });
            };

            ctrl.errorCallback = function (response) {
                ctrl.loadData();
                errorModalService.open(response);
            };

            ctrl.patchExperienceTypes = function () {
                return requestService.patchExperienceTypes(ctrl.id, ctrl.experience_types);
            };
            ctrl.patchAccessories = function () {
                // Get only checked accessories
                var accessories = ctrl.accessories_list.filter(function (accessory) {
                    return accessory.checked;
                });
                return requestService.patchAccessories(ctrl.id, accessories);
            };
            ctrl.postPhoto = function (photoVersion) {
                return requestService.postPhoto(ctrl.boat[photoVersion + '_photo'], 'boat', ctrl.id, photoVersion);
            };

            // Save boat fields and return a promise
            ctrl.put = function () {
                ctrl.loading = true;

                return ctrl.patchExperienceTypes().then(
                    null,
                    function (response) {
                        return $q.reject(response);
                    }
                ).then(
                    ctrl.patchAccessories,
                    function (response) {
                        return $q.reject(response);
                    }
                ).then(
                    function () {
                        return ctrl.postPhoto('external')
                    },
                    function (response) {
                        return $q.reject(response);
                    }
                ).then(
                    function () {
                        return ctrl.postPhoto('internal')
                    },
                    function (response) {
                        return $q.reject(response);
                    }
                ).then(
                    function () {
                        return ctrl.postPhoto('sea')
                    },
                    function (response) {
                        return $q.reject(response);
                    }
                ).then(
                    function () {
                        return requestService.putBoat(ctrl.id, ctrl.boat);
                    },
                    function (response) {
                        return $q.reject(response);
                    }
                )
            };

            // Inject MAP script into DOM
            ctrl.loadMaps = function () {
                // Load map
                ctrl.map = new google.maps.Map(document.getElementById('map'), {
                    center: ctrl.coordinates,
                    zoom: 8
                });
                ctrl.marker = new google.maps.Marker({
                    position: ctrl.coordinates,
                    draggable: true,
                    map: ctrl.map
                });

                ctrl.marker.setPosition(ctrl.coordinates);
                window.setTimeout(function () {
                    ctrl.map.panTo(ctrl.coordinates);
                }, 100);

                // Make map respond to click event: move the marker
                ctrl.map.addListener('click', function (event) {
                    ctrl.marker.setPosition(event.latLng);
                    window.setTimeout(function () {
                        ctrl.map.panTo(event.latLng);
                    }, 100);

                    // Set new boat coordinates
                    ctrl.coordinates.lat = event.latLng.lat();
                    ctrl.coordinates.lng = event.latLng.lng();
                    ctrl.boat.lat = event.latLng.lat();
                    ctrl.boat.lng = event.latLng.lng();
                });
                // Save new coordinates on marker drag
                ctrl.marker.addListener('dragend', function (event) {
                    // Set new boat coordinates
                    ctrl.coordinates.lat = event.latLng.lat();
                    ctrl.coordinates.lng = event.latLng.lng();
                    ctrl.boat.lat = event.latLng.lat();
                    ctrl.boat.lng = event.latLng.lng();
                });

                // Fix not loading map issue
                setTimeout(function () {
                    google.maps.event.trigger(ctrl.map, 'resize');
                    ctrl.map.setCenter(ctrl.coordinates);
                }, 100);
            };

            // Check if form can be submitted
            ctrl.isFormValid = function (form) {
                return form.$valid;
            }
        }]);