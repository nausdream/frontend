/**
 * Created by lucas on 31/08/2017.
 */
angular.module('administration').controller('voucherNewController', [
    'fixedListService', 'requestService', '$translatePartialLoader', '$translate', '$location', 'dateService',
    'errorModalService', 'validationService',
    function (fixedListService, requestService, $translatePartialLoader, $translate, $location, dateService,
              errorModalService, validationService) {
        // Make context accessible inside other scopes
        var ctrl = this;

        // Load translations
        $translatePartialLoader.addPart('languages');
        $translatePartialLoader.addPart('currencies');
        $translate.refresh();

        // Field rules
        ctrl.fields = {
            'first_name': {
                required: true,
                max: 150
            },
            'last_name': {
                required: false,
                max: 150
            },
            'departure_date': {
                optional: false,
                min: new Date()
            },
            'arrival_date': {
                optional: false,
                min: new Date()
            },
            'experience_title': {
                required: true,
                max: 300
            },
            'departure_port': {
                required: true,
                max: 100
            },
            'arrival_port': {
                required: true,
                max: 100
            },
            'departure_time': {
                required: true,
                max: 150
            },
            'arrival_time': {
                required: true,
                max: 150
            },
            'adults': {
                required: true,
                max: 150
            },
            'kids': {
                required: true,
                max: 150
            },
            'babies': {
                required: true,
                max: 150
            },
            'price': {
                required: false,
                min: 10,
                max: (Math.pow(2, 31) - 1)
            },
            'fee': {
                required: false
            },
            'language_guest': {
                required: true,
                max: 150
            },
            'currency_captain': {
                required: true,
                max: 150
            }
        };

        // Field rules
        ctrl.booking = {
            'first_name': null,
            'last_name': null,
            'departure_date': new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
            'arrival_date': new Date(new Date().getTime() + 24 * 60 * 60 * 1000),
            'experience_title': null,
            'departure_port': null,
            'arrival_port': null,
            'departure_time': new Date(),
            'arrival_time': new Date(),
            'adults': 2,
            'kids': 0,
            'babies': 0,
            'price': 1,
            'fee': 1,
            'language_guest': 'it',
            'currency_captain': 'EUR'
        };

        // Boat types
        fixedListService.getFixedList('boat-types').then(
            function (response) {
                ctrl.boat_types = response.data.data;
            },
            function (response) {

            }
        );

        // Languages
        fixedListService.getFixedList('languages').then(
            function (response) {
                ctrl.languages = response.data.data;
            },
            function (response) {

            }
        );

        // Currencies
        fixedListService.getFixedList('currencies').then(
            function (response) {
                ctrl.currencies = response.data.data;
            },
            function (response) {

            }
        );

        // Set departure date and arrival date to null
        ctrl.clearDates = function () {
            if (!ctrl.no_dates) return;

            ctrl.fields.departure_date.optional = true;
            ctrl.booking.departure_date = null;

            ctrl.fields.arrival_date.optional = true;
            ctrl.booking.arrival_date = null;
        };

        // Post
        ctrl.post = function () {
            // Reset previous errors
            ctrl.errorMissingDate = false;
            ctrl.errorArrivalDate = false;

            ctrl.loading = true;

            if (!ctrl.form.$valid) {
                validationService.setFrontendErrors(ctrl.form, {});
                return;
            }

            let attributes = angular.copy(ctrl.booking);

            // Transform attributes to meet Backend specifications
            attributes.departure_time = attributes.departure_time.toLocaleTimeString('en-US', {hour12: false});
            attributes.arrival_time = attributes.arrival_time.toLocaleTimeString('en-US', {hour12: false});

            // Check that departure_date and arrival_date are set when "without date" is not checked
            if (!ctrl.no_dates && (!attributes.arrival_date || !attributes.departure_date)) {
                ctrl.errorModal = true;
                ctrl.errorMissingDate = true;
                return;
            }

            // Validate arrival date and departure date
            if (attributes.arrival_date < attributes.departure_date) {
                ctrl.errorModal = true;
                ctrl.errorArrivalDate = true;
                return;
            }

            attributes.departure_date = dateService.getDateString(attributes.departure_date);
            attributes.arrival_date = dateService.getDateString(attributes.arrival_date);

            requestService.postSingle('vouchers', attributes).then(
                function () {
                    ctrl.loading = false;
                    ctrl.successModal = true;
                },
                function (error) {
                    // If not a backend error, create error manually
                    if (!error.data || !error.data.errors) {
                        error = {
                            status: error.status,
                            config: {url: 'Error creating new Voucher'},
                            data: {
                                errors: [{code: null, detail: 'There was some error', title: 'New voucher'}]
                            }
                        };
                    }

                    ctrl.loading = false;
                    errorModalService.open(error)
                }
            )
        };
    }]);