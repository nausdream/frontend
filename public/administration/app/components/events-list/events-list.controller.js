angular
    .module('administration')
    .controller('eventsListController',[
        'statusService', '$scope',
        function (statusService, $scope) {
        // Make context accessible inside other scopes
        var ctrl = this;
        ctrl.loading = true;

        // Fetch data from service
        requestService.getList(function (data, totalItems) {
            ctrl.loading = false;
            ctrl.totalItems = totalItems;
            ctrl.data = data;
        }, 'events');

        // Set table properties
        this.columns = [
            {name: 'name', heading: 'Nome'},
            {name: 'date', heading: 'Data'},
            {name: 'email', heading: 'E-mail'},
            {name: 'country', heading: 'Località'},
            {name: 'status', heading: 'Stato'},
            {name: 'actions', heading: 'Azioni'}
        ];

        // Set top icons
        this.icons = [
            {type: 'boat', template: 'boats'},
            {type: 'filter', template: 'filters-event'},
            {type: 'search', template: 'search-event'}
        ];

        this.controlIconsColor = 'navy-blue';

        this.getStatusName = function (status) {
            return statusService.getEventStatusName(status);
        };

        // Search event
        $scope.searchString = '';
        $scope.searchEvent = function (text) {
            console.log('Text: ' + text)
        };

        // Pagination
        this.currentPage = 1;
        this.perPage = 6;
        this.pagesToDisplay = 5;
        this.boundaryLinks = true;
        this.directionLinks = false;

        // Request new data
        this.pageChanged = function () {
            requestService.getList(function (data) {
                ctrl.data = data;
            }, 'events', this.currentPage);
        }
    }]);