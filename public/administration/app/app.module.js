'use strict';

// Reset time, keeping only the day
Date.prototype.withoutTime = function () {
    var d = new Date(this);
    d.setHours(0, 0, 0, 0);
    return d;
};

angular.module('administration', ['shared', 'pascalprecht.translate']);

// Define app constants
var env = {};
// Import variables if present (from env.js)
if (window) {
    Object.assign(env, window.__env);
}
// Set __env global constant
angular.module('administration').constant('__env', env);

// Bootstrap translateProvider
angular.module('administration').config(['$translateProvider', function ($translateProvider) {
    $translateProvider
        .useLoader('$translatePartialLoader', {
            urlTemplate: __env.apiTranslationsUrl + "translations?page={part}&language={lang}"
            //urlTemplate: '/shared/assets/json_2/{part}_{lang}.json'
        })
        .preferredLanguage('en');
}]);

angular.module('administration').constant('staticData',
    {
        materials: {
            wood: "Legno",
            fiberglass: "Vetroresina",
            steel: "Acciaio",
            aluminium: "Alluminio"
        },
        experience_types: {
            "trips-and-tours": "Escursioni e tour",
            luxury: "Di lusso",
            romantic: "Esperienze Romantiche",
            dinners: "Cene",
            aperitif: "Aperitivi",
            diving: "Immersioni",
            fishing: "Pesca",
            other: "Altro",
        },
        accessories: {
            "hot-water": "Acqua calda",
            "air-conditioning": "Aria condizionata",
            "sports-gear": "Attrezzatura sportiva",
            "outdoor-shower": "Doccia all'aperto",
            "security-equipment": "Dispositivi di sicurezza",
            "echo-sounder": "Ecoscandaglio",
            "epirb": "Epirb",
            "kitchen": "Cucina",
            "fire-extinguisher": "Estintore",
            "freezer": "Freezer",
            "fridge": "Frigorifero",
            "power-unit": "Generatore",
            "music-centre": "Music center",
            "microwave": "Microonde",
            "gangway": "Gangway",
            "wall-socket": "Presa a muro",
            "radio": "Radio",
            "heating": "Riscaldamento",
            "fire-prevention-system": "Sistema antincendio",
            "awning": "Awning",
            "tender": "Tender",
            "tv": "Tv",
            "kitchen-tools": "Utensili da cucina",
            "vhf": "Vhf",
            "wifi": "Wi-Fi"
        },
        boat_types: {
            catamaran: "Catamarano",
            motorboat: "A motore",
            sailboat: "A vela"
        },
        motor_types: {
            inboard: "Entrobordo",
            outboard: "Fuoribordo",
            sterndrive: "Entrobordo/Fuoribordo"
        },
        fixed_rules: {
            animal_small: "Animali domestici di piccola taglia",
            animal_medium: "Animali domestici di media taglia",
            animal_large: "Animali domestici di grande taglia",
            alcohol: "Alcolici",
            cooking: "Cucinare",
            kids: "Bambini di età inferiore ai 3 anni",
            babies: "Bambini di età inferiore ai 10 anni"
        },
        fixed_additional_services: {
            "cabin-for-special-events": "Cabin for special events",
            "welcome-aperitif": "Aperitivo di benvenuto",
            "aperitif-with-oysters-and-champagne": "Aperitif with oysters and champagne",
            "fishing-equipment": "Fishing equipment",
            "sport-equipment": "Attrezzatura sportiva",
            "alcoholic-drinks": "Bevande alcoliche",
            "soft-drinks": "Bevande non alcoliche",
            "bath-linen": "Bath linen",
            "snorkel": "Snorkel",
            "scuba-tank": "Scuba tank",
            "diving-boots": "Diving boots",
            "galley": "Galley",
            "diving-hood": "Diving hood",
            "diving-computer":"Diving computer",
            "fuel":"Fuel",
            "sun-cream":"Sun cream",
            "breakfast":"Breakfast",
            "sea-cook":"Sea cook",
            "possible-moorings":"Possible moorings",
            "fresh-fruit":"Fresh fruit",
            "buoyancy-compensator":"Buoyancy compensator",
            "inflatable":"Inflatable",
            "diving-gloves":"Diving gloves",
            "tour-guide-aboard":"Tour guide aboard",
            "hostess":"Hostess",
            "interpreter":"Interpreter",
            "sailor":"Sailor",
            "diving-mask":"Diving mask",
            "customisable":"Customisable",
            "music":"Music",
            "diving-suite":"Diving suite",
            "parking-lot":"Parking lot",
            "vegan-meal":"Vegan meal",
            "vegetarian-meal":"Vegetarian meal",
            "surface-marker-buoy":"Surface marker buoy",
            "overnight-stay":"Overnight stay",
            "fins":"Fins",
            "possibility-to-bring-your-own-music":"Possibility to bring your own music",
            "lunch":"Lunch",
            "bath-products":"Bath products",
            "final-cleaning-aboard":"Final cleaning aboard",
            "daily-cleaning":"Daily cleaning",
            "bed-linen":"Bed linen",
            "dock-shower":"Dock shower",
            "tender":"Tender",
            "snack":"Snack",
            "candlelight-table":"Candlelight table",
            "beach-towel":"Beach towel",
            "torch":"Torch",
            "transfer-from-to":"Transfer from to",
            "use-of-the-heating":"Use of the heating",
            "various-moorings":"Various moorings",
            "ballast":"Ballast"
        },
        experience_durations: {
            "up-to-one-day": "Fino a un giorno",
            "more-than-one-day": "Più di un giorno",
        },
        fishing_partitions: {
            skipper: "Skipper",
            guests: "Guest",
            "guests-and-skipper": "Guest and skipper"
        },
        fishing_types: {
            "fishing-line": "Fishing line",
            "pull-coast": "Pull coast",
            "pull-height": "Pull height",
            "drifting": "Drifting",
            "artificial-lure": "Artificial lure"
        },
        foods: {
            'starter_dish': 'Starter dish',
            'first_dish': 'First dish',
            'second_dish': 'Second dish',
            'side_dish': 'Side dish',
            'dessert_dish': 'Dessert dish',
            'bread': 'Bread',
            'wines': 'Wines',
            'champagne': 'Champagne'
        }
    }
);
angular.module('administration').constant('profilePic', '/shared/assets/img/ragazza.jpg');

angular.module('administration').config(['$locationProvider', function($locationProvider) {
    $locationProvider.hashPrefix('');
    // use the HTML5 History API
    $locationProvider.html5Mode(true);
}]);

angular.module('administration').config(['$cookiesProvider', function($cookiesProvider) {
    $cookiesProvider.defaults.path = '/admin';
}]);

// FILTERS
// Date filter
angular.module('administration').filter('userDate', ['dateService', '$filter', function (dateService, $filter) {
        return function (value, language, convert = true) {
            // Fetch format from service
            var format = dateService.getFormat('date', language);

            // If date not provided, defaults to today
            if (!value) return $filter('date')(new Date(), format);

            // If string instead of date is provided, convert it to date object
            if (typeof value == "string") value = dateService.getDateFromString(value);

            // Create new object before converting to user timezone
            var newDate = new Date(value.getTime());

            // If convert is set to true, offset date to user timezone
            if (convert) {
                dateService.toUserTimezone(newDate);
            }

            return $filter('date')(newDate, format);
        };
    }]
);