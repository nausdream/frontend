angular
    .module('administration')
    .config([
        '$routeProvider',
        function ($routeProvider) {
        $routeProvider
            .when("/", {
                redirectTo: '/users'
            })
            .when("/dashboard", {
                redirectTo: "/"
            })
            .when("/experiences", {
                currentMenu: "experience",
                title: "Experiences",
                templateUrl: "/administration/app/components/experiences-list/experiences-list.view.html",
                controller: "experiencesListController",
                controllerAs: "ctrl"
            })
            .when("/boats", {
                currentMenu: "boat",
                title: "Boats",
                templateUrl: "/administration/app/components/boats-list/boats-list.view.html",
                controller: "boatsListController",
                controllerAs: "ctrl"
            })
            .when("/captains", {
                currentMenu: "captain",
                title: "Captains",
                templateUrl: "/administration/app/components/captains-list/captains-list.view.html",
                controller: "captainsListController",
                controllerAs: "ctrl"
            })
            .when("/users", {
                currentMenu: "user",
                title: "Users",
                templateUrl: "/administration/app/components/users-list/users-list.view.html",
                controller: "usersListController",
                controllerAs: "ctrl"
            })
            .when("/bookings", {
                currentMenu: "booking",
                title: "Bookings",
                templateUrl: "/administration/app/components/bookings-list/bookings-list.view.html",
                controller: "bookingsListController",
                controllerAs: "ctrl"
            })
            .when("/bookings-offsite", {
                currentMenu: "booking-offsite",
                title: "Offsite Bookings",
                templateUrl: "/administration/app/components/bookings-offsite-list/bookings-offsite-list.view.html",
                controller: "bookingsOffsiteListController",
                controllerAs: "ctrl"
            })
            .when("/bookings-offsite/new", {
                currentMenu: "booking-offsite",
                title: "Insert new Offsite Bookings",
                templateUrl: "/administration/app/components/booking-offsite-new/booking-offsite-new.view.html",
                controller: "bookingOffsiteNewController",
                controllerAs: "ctrl"
            })
            .when("/bookings-offsite/:id", {
                currentMenu: "booking-offsite",
                title: "Offsite Booking",
                templateUrl: "/administration/app/components/booking-offsite/booking-offsite.view.html",
                controller: "bookingOffsiteController",
                controllerAs: "ctrl"
            })
            .when("/vouchers/new", {
                currentMenu: "booking-offsite",
                title: "Create new voucher",
                templateUrl: "/administration/app/components/voucher-new/voucher-new.view.html",
                controller: "voucherNewController",
                controllerAs: "ctrl"
            })
            .when("/eden-vouchers/new", {
                currentMenu: "eden-voucher",
                title: "Insert new Eden voucher",
                templateUrl: "/administration/app/components/eden-voucher-new/eden-voucher-new.view.html",
                controller: "edenVoucherNewController",
                controllerAs: "ctrl"
            })
            .when("/events", {
                currentMenu: "experience",
                title: "Events",
                templateUrl: "/administration/app/components/events-list/events-list.view.html",
                controller: "eventsListController",
                controllerAs: "ctrl"
            })
            .when("/conversations", {
                currentMenu: "chat",
                title: "Conversations",
                templateUrl: "/administration/app/components/conversations-list/conversations-list.view.html",
                controller: "conversationsListController",
                controllerAs: "ctrl"
            })
            .when("/conversations/:id", {
                currentMenu: "chat",
                title: "Conversations",
                templateUrl: "/administration/app/components/conversation-page/conversation-page.view.html",
                controller: "conversationPageController",
                controllerAs: "ctrl"
            })
            .when("/coupons", {
                currentMenu: "coupon",
                title: "Coupons",
                templateUrl: "/administration/app/components/coupons-list/coupons-list.view.html",
                controller: "couponsListController",
                controllerAs: "ctrl"
            })
            .when("/experiences/:id", {
                currentMenu: "experience",
                title: "Experience",
                templateUrl: "/administration/app/components/experience-form/experience-form.view.html",
                controller: "experienceFormController",
                controllerAs: "ctrl"
            })
            .when("/boats/:id", {
                currentMenu: "boat",
                title: "Boat",
                templateUrl: "/administration/app/components/boat-form/boat-form.view.html",
                controller: "boatFormController",
                controllerAs: "ctrl"
            })
            .otherwise({
                redirectTo: "/"
            });
    }]);

// Auto update title and currentMenu element on route change
angular
    .module('administration')
    .run(['$rootScope', '$location', function ($rootScope, $location) {
        $rootScope.$on('$routeChangeSuccess', function (event, current, previous) {
            $rootScope.title = current.$$route.title;
            $rootScope.currentMenu = current.$$route.currentMenu;
            $rootScope.currentUrl = $location.path();
        });
    }]);