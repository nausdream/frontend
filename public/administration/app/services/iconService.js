angular.module('administration').service('iconService', function() {

    // Inizialize notifications numbers array
    // TODO: Fetch data from API
    var data = {
        home: {
            name: null, count: 0,
            dropdownData: null
        },
        experience: {
            name: 'Annunci', count: 5,
            dropdownData: [
                {name: 'Annunci', url: 'experiences'},
                // {name: 'Eventi e Feste', url: 'events'},
                // {name: 'Inserisci nuovo annuncio', url: 'new-booking'}
            ]
        },
        captain: {
            name: 'Capitani', count: 0,
            dropdownData: [
                {name: 'Capitani', url: 'captains'},
                {name: 'Rubrica', url: 'number-pad'}
            ]
        },
        boat: {
            name: 'Barche', count: 1,
            dropdownData: [
                {name: 'Barche', url: 'boats'},
                // {name: 'Catamarano', url: 'catamarans'},
                // {name: 'A motore', url: 'motorboats'},
                // {name: 'A vela', url: 'sailingboats'},
                // {name: 'Inserisci nuova barca', url: 'new-boat'}
            ]
        },
        hosts: {
            name: 'Users', count: 5,
            dropdownData: [
                {name: 'Users', url: 'users'},
            ]
        },
        chat: {
            name: 'New conversations', count: 7,
            dropdownData: [
                {name: 'Conversations', url: 'conversations'}
            ]
        },
        mail: {
            name: 'Nuove Mail', count: 18,
            dropdownData: null
        },
        booking: {
            name: 'Prenotazioni', count: 3,
            dropdownData: [
                {name: 'Prenotazioni', url: 'bookings'},
                {name: 'Voce 2', url: 'prova'}
            ]
        },
        'booking-offsite': {
            name: 'Offsite Bookings', count: 3,
            dropdownData: [
                {name: 'Offsite Bookings', url: 'bookings-offsite'},
                {name: 'Insert new', url: 'bookings-offsite/new'},
                {name: 'Create Voucher', url: 'vouchers/new'}
            ],
            icon: 'booking'
        },
        'eden-voucher': {
            name: 'Eden Vouchers',
            dropdownData: [
                {name: 'Insert new Eden Voucher', url: 'eden-vouchers/new'},
            ],
            icon: null,
            url: 'eden-voucher',
            text: 'E'
        },
        notifications: {
            name: null, count: 0,
            dropdownData: null
        },
        coupon: {
            name: 'Coupons', count: 3,
            dropdownData: [
                {name: 'Coupons', url: 'coupons'},
                {name: 'Voce 2', url: 'prova'}
            ]
        }
    };
    data.notifications.count =
        data.home.count +
        data.captain.count +
        data.boat.count +
        data.experience.count +
        data.hosts.count +
        data.booking.count;

    this.getIconsData = function (iconName) {
        return data[iconName];
    };
});