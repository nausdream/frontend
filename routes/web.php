<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

// ADMINS
Route::get('/admin/login', function () {
    return view('admin-login');
});
// Catch all routes
Route::get('/admin/', function () {
    return view('admin-index');
});
Route::get('/admin{any?}', function ($any = null) {
    return view('admin-index');
})->where('any', '.*');

// TRANSLATORS
Route::get('/translator/login', function () {
    return view('translator-login');
});
Route::get('/translator/', function () {
    return view('translator-index');
});
// Catch all routes
Route::get('/translator{any?}', function ($any = null) {
    return view('translator-index');
})->where('any', '.*');

// USERS
// Catch all routes
Route::get('/{any?}', function ($any = null) {
    return view('user-index');
})->where('any', '.*');
