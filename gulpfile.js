const elixir = require('laravel-elixir');
const gulp = require('gulp');
const templateCache = require('gulp-angular-templatecache');
const concat = require('gulp-concat');
const uglify = require('gulp-uglify');
const util = require('gulp-util');
const sourcemaps = require('gulp-sourcemaps');
const sass = require('gulp-sass');
const uglifycss = require('gulp-uglifycss');

let production = !!util.env.production;

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

gulp.task('components_template_cache_administration', function () {
    return gulp.src('./public/administration/app/components/**/*.html')
        .pipe(templateCache('components-templates.js', {
            module: 'administration',
            root: '/administration/app/components/'
        }))
        .pipe(gulp.dest('./public/administration/assets/js/'));
});
gulp.task('components_template_cache_translation', function () {
    return gulp.src('./public/translation/app/components/**/*.html')
        .pipe(templateCache('components-templates.js', {module: 'translation', root: '/translation/app/components/'}))
        .pipe(gulp.dest('./public/translation/assets/js/'));
});
gulp.task('shared_app', function () {
    return gulp.src(
        [
            './public/shared/*.js',

        ]
    )
        .pipe(concat('app.js'))
        .pipe(gulp.dest('./public/shared/assets/js/'));
});
gulp.task('shared_services', function () {
    return gulp.src(
        [
            './public/shared/services/*.js',

        ]
    )
        .pipe(concat('services.js'))
        .pipe(gulp.dest('./public/shared/assets/js/'));
});
gulp.task('shared_components', function () {
    return gulp.src(
        [
            './public/shared/components/**/*.js',

        ]
    )
        .pipe(concat('components.js'))
        .pipe(gulp.dest('./public/shared/assets/js/'));
});
gulp.task('shared_components', function () {
    return gulp.src(
        [
            './public/shared/components/**/*.js',

        ]
    )
        .pipe(concat('components.js'))
        .pipe(gulp.dest('./public/shared/assets/js/'));
});
gulp.task('components_template_cache', function () {
    return gulp.src('./public/users/app/components/**/*.html')
        .pipe(templateCache('components-templates.js', {module: 'users', root: '/users/app/components/'}))
        .pipe(gulp.dest('./public/users/assets/js/'));
});
gulp.task('pages_template_cache', function () {
    return gulp.src('./public/users/app/pages/**/*.html')
        .pipe(templateCache('pages-templates.js', {module: 'users', root: '/users/app/pages/'}))
        .pipe(gulp.dest('./public/users/assets/js/'));
});
gulp.task('concat_libraries', function () {
    return gulp.src(
        [
            './public/shared/assets/libraries/jquery-3.2.0.min.js',
            './public/shared/assets/libraries/angular.min.js',
            './public/shared/assets/libraries/angular-route.min.js',
            './public/shared/assets/libraries/angular-middleware/dist/angular-middleware.min.js',
            './public/shared/assets/libraries/angular-animate.min.js',
            './public/shared/assets/libraries/angular-sanitize.min.js',
            './public/shared/assets/libraries/angular-cookies.min.js',
            './public/shared/assets/libraries/angular-filter.min.js',
            './public/shared/assets/libraries/ng-file-upload.min.js',
            './public/shared/assets/libraries/angular-css.min.js',
            './public/shared/assets/libraries/ng-touch/angular-touch.min.js',
            './public/shared/assets/libraries/angular-translate/dist/angular-translate.min.js',
            './public/shared/assets/libraries/angular-translate-loader-partial/angular-translate-loader-partial.min.js',
            './node_modules/messageformat/messageformat.min.js',
            './public/shared/assets/libraries/angular-translate-interpolation-messageformat/angular-translate-interpolation-messageformat.min.js',
            './public/shared/assets/libraries/countup.js/dist/countUp.min.js',
            './public/shared/assets/libraries/countup.js/dist/angular-countUp.min.js',
            './public/shared/assets/libraries/angular-ui-bootstrap/dist/ui-bootstrap-tpls.min.js',
            './public/shared/assets/libraries/ui-select/dist/select.min.js',
            './public/shared/assets/libraries/scroll-trigger/dist/scroll-trigger.min.js',
            './public/shared/assets/libraries/underscore-min.js',
            './public/shared/assets/libraries/angular-underscore-module.min.js',
            './public/shared/assets/libraries/angularjs-slider/rzslider.min.js',
            './public/shared/assets/libraries/slick-carousel/slick.min.js',
            './public/shared/assets/libraries/angular-slick-carousel/dist/angular-slick.min.js'
        ]
    )
        .pipe(sourcemaps.init({largeFile: true}))
        .pipe(concat('libraries.js'))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('./public/users/assets/js/'));
});
gulp.task('cook_stylesheets', function () {
    return gulp.src(
        [
            './public/shared/assets/css/bootstrap.min.css',
            './public/shared/assets/css/iconfont.css',
            './public/users/assets/sass/style.scss',
            './public/shared/assets/libraries/angularjs-slider/rzslider.min.css',
            './public/shared/assets/libraries/slick-carousel/slick.css',
            './public/shared/assets/libraries/ui-select/dist/select.min.css'
        ]
    )
        .pipe(concat('style.scss'))
        .pipe(sass())
        .pipe((production ? uglifycss() : util.noop()))
        .pipe(gulp.dest('./public/users/assets/css/'));
});

gulp.task('make_app_file',
    ['shared_app', 'shared_services', 'shared_components', 'components_template_cache', 'pages_template_cache', 'concat_libraries'], function () {
        return gulp.src(
            [
                './public/shared/assets/js/*.js',
                './public/users/app/app.module.js',
                './public/users/app/app.route.js',
                './public/users/app/app.constants.js',
                './public/users/app/components/**/*.js',
                './public/users/app/directives/**/*.js',
                './public/users/app/services/*.js',
                './public/users/app/pages/**/*.js',
                './public/users/app/filters/**/*.js',
                './public/users/assets/js/components-templates.js',
                './public/users/assets/js/pages-templates.js'
            ]
        )
            .pipe(sourcemaps.init({largeFile: true}))
            .pipe(concat('app.js'))
            .pipe(sourcemaps.write('.'))
            .pipe(gulp.dest('./public/users/assets/js/'));
    });

elixir(function (mix) {
    mix
        .sass('./public/administration/assets/sass/style.scss', './public/administration/assets/css/style.css')
        .sass('./public/translation/assets/sass/style.scss', './public/translation/assets/css/style.css')

        .sass('./public/shared/assets/sass/login.scss', './public/shared/assets/css/login.css')
        .sass('./public/shared/assets/sass/style.scss', './public/shared/assets/css/style.css')

        .webpack(['./public/administration/app/*.js'], './public/administration/assets/js/app.js')
        .webpack(['./public/administration/app/components/**/*.js'], './public/administration/assets/js/components.js')
        .webpack(['./public/administration/app/services/*.js'], './public/administration/assets/js/services.js')

        .webpack(['./public/translation/app/*.js'], './public/translation/assets/js/app.js')
        .webpack(['./public/translation/app/components/**/*.js'], './public/translation/assets/js/components.js')
        .webpack(['./public/translation/app/services/*.js'], './public/translation/assets/js/services.js')

        .task('components_template_cache_administration', './public/administration/app/components/**/*.html')
        .task('components_template_cache_translation', './public/translation/app/components/**/*.html')

        .task('cook_stylesheets')
        .task('make_app_file')
});
